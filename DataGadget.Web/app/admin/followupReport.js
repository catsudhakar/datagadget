﻿//followupReport




(function () {
    'use strict';
    var controllerId = 'followupReport';
    angular
    .module('app')
    .controller(controllerId, followupReport);

    followupReport.$inject = ['$scope', '$http', '$location', 'common', '$window'];

    function followupReport($scope, $http, $location, common, $window) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error');
        $scope.title = 'followupReport';
        var vm = this;
        $scope.vm.editData = {};
        $scope.vm.isList = false;
        $scope.vm.OrganizationList = [];
        $scope.vm.editDataList = [];

        $scope.isdisplay = false;
        $scope.ismessage = '';

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }

        activate();

        function activate() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }
            //vm.editData.startDate = formattedDate();//new Date();
            //vm.editData.endDate = formattedDate();//new Date();
            vm.editData.regionSiteCodes = '0';

            $http({ method: 'POST', url: 'api/Reports', data: { method: 'InitializeUserForm', parameters: [] } })
            .success(function (pass) {
                $scope.vm.OrganizationList = pass.locationList;;

            })
            .error(function (fail) {
                console.log(fail);
            });


        }

        $scope.applyFilter = function (editData) {
            var sdt = vm.editData.startDate;
            var edt = vm.editData.endDate;
            var issubmit = true;
            if (sdt !== undefined && edt !== undefined) {
                var sday = sdt.getDate();
                var smonth = sdt.getMonth();
                var syear = sdt.getFullYear();

                var eday = edt.getDate();
                var emonth = edt.getMonth();
                var eyear = edt.getFullYear();

                //var dateOne = new Date(2010, 11, 25);
                var sdate = new Date(syear, smonth, sday);
                var edate = new Date(eyear, emonth, eday);
                if (sdate > edate) {
                    // $scope.ismessage = 'Please enter end date should be greater then start date'
                    issubmit = false;
                }
            }
            var startingDate = GetFormattedDate(new Date(vm.editData.startDate));
            var endingDate = GetFormattedDate(new Date(vm.editData.endDate));

            if ($scope.editData.$valid && issubmit) {
                $scope.isdisplay = false;
                $scope.ismessage = '';

                $http({
                    method: 'POST', url: 'api/Common',
                    data: { method: "FollowUpContactsReport", parameters: [vm.editData.regionSiteCodes, startingDate, endingDate] }
                })
                .success(function (result) {

                    if (result != null) {
                        $scope.vm.isList = true;
                        if (result.item2.length == 0) {
                            $scope.emptyMessage = "No records found.";
                            $scope.vm.editDataList = [];
                        } else {
                            $scope.vm.editDataList = result.item2;
                        }
                        
                    }

                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (sdt === undefined && edt === undefined) {
                    $scope.ismessage = 'Please enter start date and end date'
                }
                else if (sdt === undefined) {
                    $scope.ismessage = 'Please enter start date '
                }
                else if (edt === undefined) {
                    $scope.ismessage = 'Please enter end date should be greater then start date'
                }
                else {
                    var sday = sdt.getDate();
                    var smonth = sdt.getMonth();
                    var syear = sdt.getFullYear();

                    var eday = edt.getDate();
                    var emonth = edt.getMonth();
                    var eyear = edt.getFullYear();

                    //var dateOne = new Date(2010, 11, 25);
                    var sdate = new Date(syear, smonth, sday);
                    var edate = new Date(eyear, emonth, eday);
                    if (sdate > edate) {
                        $scope.ismessage = 'Please enter end date should be greater then start date'
                    }
                }
                $scope.isdisplay = true;

            }
        };


        vm.editDataListOpen = function (data) {

            var p = '/editEventData/' + JSON.stringify(data);
            $location.path(p);

        }

    }
})();

