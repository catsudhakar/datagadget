﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.DataModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web.SessionState;
using System.Web;

namespace DataGadget.Web.Controllers
{
    public class EditDataController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "OrganizationList":
                        return await BindOrganizations(param.Parameters);
                    case "EditDataList":
                        return await BindDataGrid(param.Parameters);
                    case "UpdateEvent":
                        return  UpdateEvent(param.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //WebLogger.Error("Exception In EditDataController : " + ex.Message); 

                //return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<HttpResponseMessage> BindOrganizations(string[] p)
        {

            if (p[0] == "SIT2A" || p[0] == "SIT3U")
            {
                return null;
            }
            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    //var result = await dbContext.tblLocations.Where(x => x.StateAbbrev == "MS")
                    var result = await dbContext.tblLocations.Where(x => x.IsActive == true)
                           .Select(x => new OrganizationModel
                           {
                               Regionsitecodes = x.StateRegionCode + "_" + x.SiteCode,
                               FriendlyName_loc = x.FriendlyName_loc
                           }).OrderBy(x => x.FriendlyName_loc).ToListAsync();
                    result.Insert(0,new OrganizationModel { Regionsitecodes = "0_0", FriendlyName_loc = "All" });
                    return this.Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindDataGrid(string[] parameter)
        {
            try
            {
                var dataResult = await BindGrid(parameter);
                //var data = dataResult.Select(x => new OrganizationModel { OrganizationName = x.OrgName, Name = x.Name, StartDate = x.StartDate, EndDate = x.EndDate }).ToList();
                //return this.Request.CreateResponse(HttpStatusCode.OK, data);

                var data = dataResult.Select(x => new OneTimeEvent
                {
                    ID = x.ID,
                    Program = x.ProgramName,
                    Organization = x.OrgName,
                    BeginDate = x.StartDate,
                    EndDate = x.EndDate,
                    Group = x.GroupName,

                    ServiceLocationName = x.ServiceLocationName,
                    ServiceSchoolName = x.ServiceSchoolName,

                    
                    Age0 = x.Age0,
                    Age12 = x.Age12,
                    Age15 = x.Age15,
                    Age18 = x.Age18,
                    Age21 = x.Age21,
                    Age25 = x.Age25,
                    Age45 = x.Age45,
                    Age5 = x.Age5,
                    Age65 = x.Age65,
                    AvgCostPerParticipant = x.AvgCostPerParticipant,
                    closed = x.closed,
                    CoalitionMeeting = x.CoalitionMeeting,
                    CoalitionType = x.CoalitionType,
                    Curriculum = x.Curriculum,
                    D_HiRi_AbuVict = x.D_HiRi_AbuVict,
                    D_HiRi_ChildOfSA = x.D_HiRi_ChildOfSA,
                    D_HiRi_EconDis = x.D_HiRi_EconDis,
                    D_HiRi_FrLun = x.D_HiRi_FrLun,
                    D_HiRi_Homel = x.D_HiRi_Homel,
                    D_HiRi_K12DO = x.D_HiRi_K12DO,
                    D_HiRi_MentH = x.D_HiRi_MentH,
                    D_HiRi_NotAp = x.D_HiRi_NotAp,
                    D_HiRi_Other = x.D_HiRi_Other,
                    D_HiRi_OtherText = x.D_HiRi_OtherText,
                    D_HiRi_PhysDis = x.D_HiRi_PhysDis,
                    D_HiRi_PregUse = x.D_HiRi_PregUse,
                    D_HiRi_Using = x.D_HiRi_Using,
                    D_HiRi_Violent = x.D_HiRi_Violent,
                    Degree = x.Degree,
                    DirectServiceHours = x.DirectServiceHours,
                    DocOfActivities_Agenda = x.DocOfActivities_Agenda,
                    DocOfActivities_Brochure = x.DocOfActivities_Brochure,
                    DocOfActivities_ContactForm = x.DocOfActivities_ContactForm,
                    DocOfActivities_EvalForm = x.DocOfActivities_EvalForm,
                    DocOfActivities_Other = x.DocOfActivities_Other,
                    DocOfActivities_OtherText = x.DocOfActivities_OtherText,
                    DocOfActivities_PrePostTest = x.DocOfActivities_PrePostTest,
                    DocOfActivities_SignIn = x.DocOfActivities_SignIn,
                    EntryDate = x.EntryDate,
                    Females = x.Females,
                    FiscalYear = x.FiscalYear,
                    GroupName = x.GroupName,
                    Hispanic = x.Hispanic,
                    //ID = x.ID,
                    ID02_DirectoriesDistributed = x.ID02_DirectoriesDistributed,
                    ID04_BrochuresDistributed = x.ID04_BrochuresDistributed,
                    InterventionType = x.InterventionType,
                    Males = x.Males,
                    Name = x.Name,
                    NotHispanic = x.NotHispanic,
                    NumberOfEmployees = x.NumberOfEmployees,
                    NumberParticipants = x.NumberParticipants,
                    NumberProgHrsRecd = x.NumberProgHrsRecd,
                    OrgName = x.OrgName,
                    OverallComments = x.OverallComments,
                    ParentEntry = x.ParentEntry,
                    PrepHours = x.PrepHours,
                    ProgramName = x.ProgramName,
                    ProgramNameSource = x.ProgramNameSource,
                    ProgramType_IP = x.ProgramType_IP,
                    RaceAmIndian = x.RaceAmIndian,
                    RaceAsian = x.RaceAsian,
                    RaceBlack = x.RaceBlack,
                    RaceHawaii = x.RaceHawaii,
                    RaceMoreThanOne = x.RaceMoreThanOne,
                    RaceUnknownOther = x.RaceUnknownOther,
                    RaceWhite = x.RaceWhite,
                    RegionCode = x.RegionCode,
                    SessionName = x.SessionName,
                    SiteCode = x.SiteCode,
                    StaffFname = x.StaffFname,
                    StaffLName = x.StaffLName,
                    StartDate = x.StartDate,
                    Strat_Alt_21 = x.Strat_Alt_21,
                    Strat_Alt_22 = x.Strat_Alt_22,
                    Strat_Alt_23 = x.Strat_Alt_23,
                    Strat_Alt_24 = x.Strat_Alt_24,
                    Strat_Alt_25 = x.Strat_Alt_25,
                    Strat_Alt_26 = x.Strat_Alt_26,
                    Strat_Alt_27 = x.Strat_Alt_27,
                    Strat_Alt_27Text = x.Strat_Alt_27Text,
                    Strat_CBP_41 = x.Strat_CBP_41,
                    Strat_CBP_42 = x.Strat_CBP_42,
                    Strat_CBP_43 = x.Strat_CBP_43,
                    Strat_CBP_44 = x.Strat_CBP_44,
                    Strat_CBP_45 = x.Strat_CBP_45,
                    Strat_CBP_46 = x.Strat_CBP_46,
                    Strat_CBP_46Text = x.Strat_CBP_46Text,
                    Strat_Ed_11 = x.Strat_Ed_11,
                    Strat_Ed_12 = x.Strat_Ed_12,
                    Strat_Ed_13 = x.Strat_Ed_13,
                    Strat_Ed_14 = x.Strat_Ed_14,
                    Strat_Ed_15 = x.Strat_Ed_15,
                    Strat_Ed_16 = x.Strat_Ed_16,
                    Strat_Ed_17 = x.Strat_Ed_17,
                    Strat_Ed_17Text = x.Strat_Ed_17Text,
                    Strat_Env_51 = x.Strat_Env_51,
                    Strat_Env_52 = x.Strat_Env_52,
                    Strat_Env_53 = x.Strat_Env_53,
                    Strat_Env_54 = x.Strat_Env_54,
                    Strat_Env_55 = x.Strat_Env_55,
                    Strat_Env_56_MW = x.Strat_Env_56_MW,
                    Strat_Env_57 = x.Strat_Env_57,
                    Strat_Env_57Text = x.Strat_Env_57Text,
                    Strat_ID_01 = x.Strat_ID_01,
                    Strat_ID_02 = x.Strat_ID_02,
                    Strat_ID_03 = x.Strat_ID_03,
                    Strat_ID_04 = x.Strat_ID_04,
                    Strat_ID_05 = x.Strat_ID_05,
                    Strat_ID_06 = x.Strat_ID_06,
                    Strat_ID_07 = x.Strat_ID_07,
                    Strat_ID_08 = x.Strat_ID_08,
                    Strat_ID_09 = x.Strat_ID_09,
                    Strat_ID_09Text = x.Strat_ID_09Text,
                    Strat_IR_31 = x.Strat_IR_31,
                    Strat_IR_32 = x.Strat_IR_32,
                    Strat_IR_34 = x.Strat_IR_34,
                    Strat_IR_34Text = x.Strat_IR_34Text,
                    SurveysEntered = x.SurveysEntered,
                    TargetedStatewideInitiative = x.TargetedStatewideInitiative,
                    TotalCostofProgram = x.TotalCostofProgram,
                    TravelHours = x.TravelHours,
                    TyoeOfProgram_EA = x.TyoeOfProgram_EA,
                    UniversalType_DI = x.UniversalType_DI,
                    UserGUID = x.UserGUID,
                    WithinCostBand_YN = x.WithinCostBand_YN
                }).ToList();
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<List<tbl2008_NOM_Entries>> BindGrid(string[] parameter)
        {
            var viewResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(parameter[0]);
            string regionSiteCodes = viewResult.FirstOrDefault(x => x.Key.Contains("regionSiteCodes")).Value;
            string stateRegionCode = "";
            string siteCode = "";
            if (!string.IsNullOrWhiteSpace(regionSiteCodes))
            {
                stateRegionCode = regionSiteCodes.Split('_')[0];
                siteCode = regionSiteCodes.Split('_')[1];
            }

            DateTime? startDate = Convert.ToDateTime(viewResult.FirstOrDefault(x => x.Key.Contains("startDate")).Value);
            DateTime? endDate = Convert.ToDateTime(viewResult.FirstOrDefault(x => x.Key.Contains("endDate")).Value);

            //string eventType = "";
            //eventType = viewResult.FirstOrDefault(x => x.Key.Contains("eventType")).Value;

            var eventType = Convert.ToInt32(parameter[1]);


            using (datagadget_testEntities dbContext = new datagadget_testEntities())
            {
                var tempEvents = await dbContext.tbl2008_NOM_Entries
                       .Where(x => (DbFunctions.TruncateTime(x.StartDate) >= startDate)).ToListAsync();
                tempEvents = tempEvents
                   .Where(x => (x.EndDate >= startDate) && (x.EndDate <= endDate)&&x.RegionCode == (stateRegionCode == "" || stateRegionCode=="0" ? x.RegionCode : stateRegionCode) &&
                    x.SiteCode == (siteCode == "" || siteCode == "0" ? x.SiteCode : siteCode)).ToList();

                if (eventType == 0)
                    tempEvents = tempEvents.Where(x => x.Curriculum == 0).ToList();
                else if (eventType == 1)
                    tempEvents = tempEvents.Where(x => x.Curriculum == 1).ToList();

                //tempEvents = tempEvents.Where(x => x.ParentEntry == 0).ToList();
                 

                //var dataResult = await dbContext.tbl2008_NOM_Entries.Where
                //    (
                //    x => x.StartDate >= startDate &&
                //    x.EndDate <= endDate &&
                //    x.RegionCode == (stateRegionCode == "" || stateRegionCode=="0" ? x.RegionCode : stateRegionCode) &&
                //    x.SiteCode == (siteCode == "" || siteCode == "0" ? x.SiteCode : siteCode)
                //    ).ToListAsync();
                return tempEvents;
            }
        }

        private HttpResponseMessage UpdateEvent(string[] p)
        {


            try
            {

                Models.OneTimeEvent oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OneTimeEvent>(p[0]);

                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var Entries = context.tbl2008_NOM_Entries.Where(f => f.ID == oneTime.ID).FirstOrDefault();

                    Entries.StartDate = oneTime.StartDate;
                    Entries.EndDate = oneTime.EndDate;
                    Entries.ProgramType_IP = oneTime.ProgramType_IP;
                    Entries.InterventionType = oneTime.InterventionType;
                    
                    Entries.TyoeOfProgram_EA = oneTime.TyoeOfProgram_EA;
                    
                    Entries.UniversalType_DI = oneTime.UniversalType_DI;
                    
                    Entries.ProgramNameSource = oneTime.ProgramNameSource;
                    Entries.ProgramName = oneTime.ProgramName; //p[7];// null;
                    Entries.GroupName = oneTime.GroupName;// p[8];// null;

                    Entries.ServiceLocationName = oneTime.ServiceLocationName;
                    Entries.ServiceSchoolName = oneTime.ServiceSchoolName;
                                        
                    Entries.NumberParticipants = oneTime.NumberParticipants;
                    Entries.Males = oneTime.Males;
                    Entries.Females = oneTime.Females;



                    Entries.Age0 = oneTime.Age0.HasValue ? oneTime.Age0 : 0;
                    Entries.Age5 = oneTime.Age5.HasValue ? oneTime.Age5 : 0;
                    Entries.Age12 = oneTime.Age12.HasValue ? oneTime.Age12 : 0;
                    Entries.Age15 = oneTime.Age15.HasValue ? oneTime.Age15 : 0;
                    Entries.Age18 = oneTime.Age18.HasValue ? oneTime.Age18 : 0;
                    Entries.Age21 = oneTime.Age21.HasValue ? oneTime.Age21 : 0;
                    Entries.Age25 = oneTime.Age25.HasValue ? oneTime.Age25 : 0;
                    Entries.Age45 = oneTime.Age45.HasValue ? oneTime.Age45 : 0;
                    Entries.Age65 = oneTime.Age65.HasValue ? oneTime.Age65 : 0;

                    Entries.RaceWhite = oneTime.RaceWhite.HasValue ? oneTime.RaceWhite : 0;
                    Entries.RaceBlack = oneTime.RaceBlack.HasValue ? oneTime.RaceBlack : 0;
                    Entries.RaceHawaii = oneTime.RaceHawaii.HasValue ? oneTime.RaceHawaii : 0;
                    Entries.RaceAsian = oneTime.RaceAsian.HasValue ? oneTime.RaceAsian : 0;
                    Entries.RaceAmIndian = oneTime.RaceAmIndian.HasValue ? oneTime.RaceAmIndian : 0;
                    Entries.RaceUnknownOther = oneTime.RaceUnknownOther.HasValue ? oneTime.RaceUnknownOther : 0;
                    Entries.RaceMoreThanOne = oneTime.RaceMoreThanOne.HasValue ? oneTime.RaceMoreThanOne : 0;

                    Entries.NotHispanic = oneTime.NotHispanic.HasValue ? oneTime.NotHispanic : 0;
                    Entries.Hispanic = oneTime.Hispanic.HasValue ? oneTime.Hispanic : 0;

                    // modification
                    Entries.D_HiRi_ChildOfSA = oneTime.D_HiRi_ChildOfSA.HasValue ? oneTime.D_HiRi_ChildOfSA : 0; // Convert.ToInt32(p[33]);
                    Entries.D_HiRi_PregUse = oneTime.D_HiRi_PregUse.HasValue ? oneTime.D_HiRi_PregUse : 0; //null;
                    Entries.D_HiRi_K12DO = oneTime.D_HiRi_K12DO.HasValue ? oneTime.D_HiRi_K12DO : 0; //null;

                    Entries.D_HiRi_Violent = oneTime.D_HiRi_Violent.HasValue ? oneTime.D_HiRi_Violent : 0; //Convert.ToInt32(p[34]);
                    Entries.D_HiRi_MentH = oneTime.D_HiRi_MentH.HasValue ? oneTime.D_HiRi_MentH : 0; //Convert.ToInt32(p[38]);
                    Entries.D_HiRi_EconDis = oneTime.D_HiRi_EconDis.HasValue ? oneTime.D_HiRi_EconDis : 0; //Convert.ToInt32(p[41]);
                    Entries.D_HiRi_PhysDis = oneTime.D_HiRi_PhysDis.HasValue ? oneTime.D_HiRi_PhysDis : 0; //Convert.ToInt32(p[35]);
                    Entries.D_HiRi_AbuVict = oneTime.D_HiRi_AbuVict.HasValue ? oneTime.D_HiRi_AbuVict : 0; //Convert.ToInt32(p[39]);
                    Entries.D_HiRi_Using = oneTime.D_HiRi_Using.HasValue ? oneTime.D_HiRi_Using : 0; //null;
                    Entries.D_HiRi_Homel = oneTime.D_HiRi_Homel.HasValue ? oneTime.D_HiRi_Homel : 0; //Convert.ToInt32(p[36]);

                    Entries.D_HiRi_FrLun = oneTime.D_HiRi_FrLun.HasValue ? oneTime.D_HiRi_FrLun : 0; //null;
                    Entries.D_HiRi_Other = oneTime.D_HiRi_Other.HasValue ? oneTime.D_HiRi_Other : 0; //null;             
                    Entries.D_HiRi_NotAp = oneTime.D_HiRi_NotAp.HasValue ? oneTime.D_HiRi_NotAp : 0; //null;

                    Entries.Strat_ID_01 = oneTime.Strat_ID_01.HasValue ? oneTime.Strat_ID_01 : 0; //Convert.ToBoolean(p[45]) ? 1 : 0;

                    Entries.Strat_ID_02 = oneTime.Strat_ID_02.HasValue ? oneTime.Strat_ID_02 : 0; //Convert.ToBoolean(p[46]) ? 1 : 0;

                    Entries.Strat_ID_03 = oneTime.Strat_ID_03.HasValue ? oneTime.Strat_ID_03 : 0;//Convert.ToBoolean(p[47]) ? 1 : 0;
                    Entries.Strat_ID_04 = oneTime.Strat_ID_04.HasValue ? oneTime.Strat_ID_04 : 0;//Convert.ToBoolean(p[48]) ? 1 : 0;
                    Entries.Strat_ID_05 = oneTime.Strat_ID_05.HasValue ? oneTime.Strat_ID_05 : 0;//Convert.ToBoolean(p[49]) ? 1 : 0;
                    Entries.Strat_ID_06 = oneTime.Strat_ID_06.HasValue ? oneTime.Strat_ID_06 : 0; //Convert.ToBoolean(p[50]) ? 1 : 0;
                    Entries.Strat_ID_07 = oneTime.Strat_ID_07.HasValue ? oneTime.Strat_ID_07 : 0; //Convert.ToBoolean(p[51]) ? 1 : 0;
                    Entries.Strat_ID_08 = oneTime.Strat_ID_08.HasValue ? oneTime.Strat_ID_08 : 0; //Convert.ToBoolean(p[52]) ? 1 : 0;
                    Entries.Strat_ID_09 = oneTime.Strat_ID_09.HasValue ? oneTime.Strat_ID_09 : 0; //Convert.ToBoolean(p[53]) ? 1 : 0;
                    Entries.Strat_ID_09Text = oneTime.Strat_ID_09Text; //p[54];

                    Entries.Strat_Ed_11 = oneTime.Strat_Ed_11.HasValue ? oneTime.Strat_Ed_11 : 0; //Convert.ToBoolean(p[55]) ? 1 : 0;
                    Entries.Strat_Ed_12 = oneTime.Strat_Ed_12.HasValue ? oneTime.Strat_Ed_12 : 0; //Convert.ToBoolean(p[56]) ? 1 : 0;
                    Entries.Strat_Ed_13 = oneTime.Strat_Ed_13.HasValue ? oneTime.Strat_Ed_13 : 0;//Convert.ToBoolean(p[57]) ? 1 : 0;
                    Entries.Strat_Ed_14 = oneTime.Strat_Ed_14.HasValue ? oneTime.Strat_Ed_14 : 0;//Convert.ToBoolean(p[58]) ? 1 : 0;
                    Entries.Strat_Ed_15 = oneTime.Strat_Ed_15.HasValue ? oneTime.Strat_Ed_15 : 0;//Convert.ToBoolean(p[59]) ? 1 : 0;
                    Entries.Strat_Ed_16 = oneTime.Strat_Ed_16.HasValue ? oneTime.Strat_Ed_16 : 0;//Convert.ToBoolean(p[60]) ? 1 : 0;
                    Entries.Strat_Ed_17 = oneTime.Strat_Ed_17.HasValue ? oneTime.Strat_Ed_17 : 0;//Convert.ToBoolean(p[61]) ? 1 : 0;
                    Entries.Strat_Ed_17Text = oneTime.Strat_Ed_17Text; //p[62];

                    Entries.Strat_Alt_21 = oneTime.Strat_Alt_21.HasValue ? oneTime.Strat_Alt_21 : 0; //Convert.ToBoolean(p[63]) ? 1 : 0;
                    Entries.Strat_Alt_22 = oneTime.Strat_Alt_22.HasValue ? oneTime.Strat_Alt_22 : 0;//Convert.ToBoolean(p[64]) ? 1 : 0;
                    Entries.Strat_Alt_23 = oneTime.Strat_Alt_23.HasValue ? oneTime.Strat_Alt_23 : 0;//Convert.ToBoolean(p[65]) ? 1 : 0;
                    Entries.Strat_Alt_24 = oneTime.Strat_Alt_24.HasValue ? oneTime.Strat_Alt_24 : 0;//Convert.ToBoolean(p[66]) ? 1 : 0;
                    Entries.Strat_Alt_25 = oneTime.Strat_Alt_25.HasValue ? oneTime.Strat_Alt_25 : 0;//Convert.ToBoolean(p[67]) ? 1 : 0;
                    Entries.Strat_Alt_26 = oneTime.Strat_Alt_26.HasValue ? oneTime.Strat_Alt_26 : 0;//Convert.ToBoolean(p[68]) ? 1 : 0;
                    Entries.Strat_Alt_27 = oneTime.Strat_Alt_27.HasValue ? oneTime.Strat_Alt_27 : 0; //Convert.ToBoolean(p[69]) ? 1 : 0;
                    Entries.Strat_Alt_27Text = oneTime.Strat_Alt_27Text; //p[70];

                    Entries.Strat_IR_31 = oneTime.Strat_IR_31.HasValue ? oneTime.Strat_IR_31 : 0; ; //Convert.ToBoolean(p[71]) ? 1 : 0;
                    Entries.Strat_IR_32 = oneTime.Strat_IR_32.HasValue ? oneTime.Strat_IR_32 : 0; ; //Convert.ToBoolean(p[72]) ? 1 : 0;
                    Entries.Strat_IR_34 = oneTime.Strat_IR_34.HasValue ? oneTime.Strat_IR_34 : 0; ; //Convert.ToBoolean(p[73]) ? 1 : 0;
                    Entries.Strat_IR_34Text = oneTime.Strat_IR_34Text; //p[74];

                    Entries.Strat_CBP_41 = oneTime.Strat_CBP_41.HasValue ? oneTime.Strat_CBP_41 : 0;
                    Entries.Strat_CBP_42 = oneTime.Strat_CBP_42.HasValue ? oneTime.Strat_CBP_42 : 0;
                    Entries.Strat_CBP_43 = oneTime.Strat_CBP_43.HasValue ? oneTime.Strat_CBP_43 : 0;
                    Entries.Strat_CBP_44 = oneTime.Strat_CBP_44.HasValue ? oneTime.Strat_CBP_44 : 0;
                    Entries.Strat_CBP_45 = oneTime.Strat_CBP_45.HasValue ? oneTime.Strat_CBP_45 : 0;
                    Entries.Strat_CBP_46 = oneTime.Strat_CBP_46.HasValue ? oneTime.Strat_CBP_46 : 0;
                    Entries.Strat_CBP_46Text = oneTime.Strat_CBP_46Text;

                    Entries.Strat_Env_51 = oneTime.Strat_Env_51.HasValue ? oneTime.Strat_Env_51 : 0;
                    Entries.Strat_Env_52 = oneTime.Strat_Env_52.HasValue ? oneTime.Strat_Env_52 : 0;
                    Entries.Strat_Env_53 = oneTime.Strat_Env_53.HasValue ? oneTime.Strat_Env_53 : 0;
                    Entries.Strat_Env_54 = oneTime.Strat_Env_54.HasValue ? oneTime.Strat_Env_54 : 0;
                    Entries.Strat_Env_55 = oneTime.Strat_Env_55.HasValue ? oneTime.Strat_Env_55 : 0;
                    Entries.Strat_Env_56_MW = oneTime.Strat_Env_56_MW.HasValue ? oneTime.Strat_Env_56_MW : 0;
                    Entries.Strat_Env_57 = oneTime.Strat_Env_57.HasValue ? oneTime.Strat_Env_57 : 0;
                    Entries.Strat_Env_57Text = oneTime.Strat_Env_57Text;

                    Entries.NumberProgHrsRecd = oneTime.NumberProgHrsRecd.HasValue ? oneTime.NumberProgHrsRecd : 0;
                    Entries.TotalCostofProgram = oneTime.TotalCostofProgram.HasValue ? oneTime.TotalCostofProgram : 0;
                    Entries.AvgCostPerParticipant = oneTime.AvgCostPerParticipant.HasValue ? oneTime.AvgCostPerParticipant : 0;
                    Entries.WithinCostBand_YN = oneTime.WithinCostBand_YN.HasValue ? oneTime.WithinCostBand_YN : 0;
                   // Entries.EntryDate = DateTime.Now; //oneTime.EntryDate; 

                    //Entries.Curriculum = 0;
                    //Entries.ParentEntry = 0;
                    //Entries.SessionName = "";
                    //Entries.closed = 1;

                    Entries.CoalitionMeeting = oneTime.CoalitionMeeting;
                    Entries.CoalitionType = oneTime.CoalitionType;

                    Entries.TargetedStatewideInitiative = "";

                    if (oneTime.lstStatewideInitiative.Count > 0)
                    {

                        foreach (var item in oneTime.lstStatewideInitiative)
                        {
                            if (item.IsSelected == true)
                            {
                                if (Entries.TargetedStatewideInitiative != "")
                                    Entries.TargetedStatewideInitiative = Entries.TargetedStatewideInitiative + "|" + item.initiativeName;
                                else
                                    Entries.TargetedStatewideInitiative = item.initiativeName;

                            }
                        }

                    }

                    //Entries.TargetedStatewideInitiative = "Marijuana use by adolescents|Underage Drinking|";

                    Entries.NumberOfEmployees = oneTime.NumberOfEmployees;
                    Entries.TravelHours = oneTime.TravelHours; ;
                    Entries.DirectServiceHours = oneTime.DirectServiceHours;
                    Entries.PrepHours = oneTime.PrepHours;

                    Entries.DocOfActivities_Agenda = oneTime.DocOfActivities_Agenda != null ? oneTime.DocOfActivities_Agenda : 0;
                    Entries.DocOfActivities_EvalForm = oneTime.DocOfActivities_EvalForm != null ? oneTime.DocOfActivities_EvalForm : 0;
                    Entries.DocOfActivities_PrePostTest = oneTime.DocOfActivities_PrePostTest != null ? oneTime.DocOfActivities_PrePostTest : 0;
                    Entries.DocOfActivities_ContactForm = oneTime.DocOfActivities_ContactForm != null ? oneTime.DocOfActivities_ContactForm : 0;
                    Entries.DocOfActivities_SignIn = oneTime.DocOfActivities_SignIn != null ? oneTime.DocOfActivities_SignIn : 0;
                    Entries.DocOfActivities_Brochure = oneTime.DocOfActivities_Brochure != null ? oneTime.DocOfActivities_Brochure : 0;
                    Entries.DocOfActivities_Other = oneTime.DocOfActivities_Other;
                    Entries.DocOfActivities_OtherText = "";// oneTime.DocOfActivities_OtherText;
                    Entries.OverallComments = oneTime.OverallComments != null ? oneTime.OverallComments : "";

                    Entries.ID02_DirectoriesDistributed = oneTime.ID02_DirectoriesDistributed;
                    Entries.ID04_BrochuresDistributed = oneTime.ID04_BrochuresDistributed;

                    i=context.SaveChanges();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, i);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}