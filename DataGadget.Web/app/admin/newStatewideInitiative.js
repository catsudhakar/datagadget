﻿(function () {
    'use strict';
    var controllerId = 'newStatewideInitiative';
    angular
        .module('app')
        .controller(controllerId, newStatewideInitiative);

    newStatewideInitiative.$inject = ['$scope', '$http', '$modalInstance', 'statewideInfo', 'mySharedService', 'common'];



    function newStatewideInitiative($scope, $http, $modalInstance, statewideInfo, sharedService, common) {


        if (statewideInfo) {
            $scope.title = "Edit Statewide Initiative";
        }
        else {
            $scope.title = "Add Statewide Initiative";
        }

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        $scope.statewide = {};

        $scope.isSubmit = false;
        activate();

        $scope.initiativeID = 0;
        $scope.initiativeName = "";

        function activate() {
            if (statewideInfo) {

                $scope.statewide.id = statewideInfo.id;
                $scope.statewide.initiativeName = statewideInfo.initiativeName;
                $scope.statewide.isActive = statewideInfo.isActive;
            }
            else {
            }


        }

        $scope.ok = function (statewideForm) {
            $scope.isSubmit = true;
            if (statewideForm.$valid) {

                if ($scope.statewide.id != 0) {
                    $scope.initiativeID = $scope.statewide.id;
                    $scope.initiativeName = $scope.statewide.initiativeName;
                    $scope.isActive = $scope.statewide.isActive;
                }
                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateStateWideInitiatives",
                        //parameters:
                        //    [loginid, password, lastname, firstname, statewidelevel, prespec, email, degree, agName, preexp, state, stateCode,
                        //    offPhone, offFax, fulltime, locId, treatmentPrgid]

                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {
                       
                        $modalInstance.close(result);
                        if ($scope.initiativeID != 0) {
                            for (var i = 0; i < sharedService.statewides.length; i++) {
                                if (sharedService.statewides[i].id == $scope.initiativeID) {
                                    sharedService.statewides[i].initiativeName = $scope.initiativeName;
                                    sharedService.statewides[i].isActive = $scope.isActive;
                                }
                            }
                        }
                       
                        log("StateWideInitiative saved successfully");

                    })
                    .error(function (error) {
                        console.log(error);
                    })
            }
        };

        $scope.updateSite = function (site) {
            $scope.sites = $scope.sites || [];
            if (site.isSelected) {
                $scope.sites.push(site.categoryID);
                $scope.sites = _.uniq($scope.sites);
            }
            else {
                $scope.sites = _.without($scope.sites, site.categoryID);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
})();
