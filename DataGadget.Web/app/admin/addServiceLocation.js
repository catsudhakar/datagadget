﻿




(function () {
    'use strict';
    var controllerId = 'addServiceLocation';
    angular
        .module('app')
        .controller(controllerId, addServiceLocation);

    addServiceLocation.$inject = ['$scope', '$http', '$modalInstance', '$location', 'resourcesInfo', '$window', '$route','common'];

    function addServiceLocation($scope, $http, $modalInstance, $location, resourcesInfo, $window, $route,common) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        $scope.title = 'Add Service Location';

        if (resourcesInfo) {
            $scope.title = "Edit Service Location";
        }
        else {
            $scope.title = "Add Service Location";
        }
        $scope.statewide = {};
        $scope.isSubmit = false;


        activate();

        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (userInfo.level != "SIT3U" && userInfo.level != "SIT2A") {
                if (resourcesInfo) {
                    $scope.statewide.providerID = resourcesInfo.id;
                    $scope.statewide.providerName = resourcesInfo.servicelocation;
                    $scope.statewide.isActive = resourcesInfo.isActive;
                  
                }
            }
            else {
                $location.path('/dashboard');
            }


        }


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function (frmProvider) {
            $scope.isSubmit = true;

            if (frmProvider.$valid) {
                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateNewServiceLocation",
                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {
                        $modalInstance.close(result);
                        $location.path('/serviceLocations');
                        $route.reload();
                        log("ServiceLocation saved successfully");
                    })
                    .error(function (error) {
                        console.log(error);
                    })

                $location.path('/serviceLocations');
                $route.reload();
            }
        };
    }
})();

