﻿(function () {
    'use strict';
    var controllerId = 'treatmentSurveyTools';   //ongoingEvents ng cotroller in ongoingevents.html
    angular
        .module('app')
        .controller(controllerId, treatmentSurveyTools);
    treatmentSurveyTools.$inject = ['$scope', '$http', '$location', 'common', '$modal', '$window'];
    function treatmentSurveyTools($scope, $http, $location, common, $modal, $window) {
        var userid = localStorage.getItem('userId');
        var userInfo = JSON.parse(localStorage.getItem('userInfo'));
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'survey tools';
        $scope.survey = [];
        var vm = this;
        vm.AgencyData = true;
        $scope.disableTagButton = { 'visibility': 'hidden' };
        $scope.administeredData = [];
        $scope.bindSurveyTypeData = [];
        $scope.surveyData = [];

        $scope.intakeData = [];

        $scope.surveyService = [];
        $scope.surveyProgram = [];
        $scope.DynamicDataCtrl = [];
        $scope.agencyData = [];
        $scope.UserID = "";
        $scope.SessionID = "";
        $scope.LocationId = "";
        $scope.ClientSessionStart = "";
        $scope.isUserOrAdmin = true;

        $scope.isShowProgram = true;

        $scope.adminvalue = false;

        $scope.from = '';



        //bind dropdownlist's here       
        bindData();



        function bindData() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            vm.CaseNumber = sessionStorage.getItem("patientId") == undefined ? null : sessionStorage.getItem("patientId");



            vm.ClientGenderType = sessionStorage.getItem('gender');

            vm.ClientEntryDate = sessionStorage.getItem('entryDate');
            vm.ClientDischargeDate = sessionStorage.getItem('dischargeDate');

            //sessionStorage.removeItem('gender');


            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $scope.isUserOrAdmin = false;
            }

            $scope.agencyData = "";
            if (sessionStorage.getItem("locationId")) {
                $scope.vm.Agency = sessionStorage.getItem("locationId");
                //sessionStorage.removeItem('locationId');
            }
            else {

                $scope.vm.Agency = userInfo.locationId;
            }



            //sessionStorage.removeItem('locationId');


            var userInfoData = localStorage.getItem('userInfo');
            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindSurveyType", parameters: [userInfoData] } })
            .success(function (result) {
                $scope.bindSurveyTypeData = result;
                if (sessionStorage.getItem("patientId")) {
                    //vm.SurveyType = _.find($scope.bindSurveyTypeData, function (surveyType) {
                    //    return surveyType.id == "2";
                    //});

                    vm.SurveyType = "2";
                    //sessionStorage.removeItem('patientId');

                }
            })
            .error(function (error) {
                //logError(error);
                console.log(error);
            });
            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindSurveyData", parameters: [userInfoData] } })
            .success(function (result) {
                $scope.surveyData = result;
            })
            .error(function (error) {
                //logError(error);
                console.log(error);
            });

            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindIntakeData", parameters: [userInfoData] } })
            .success(function (result) {
                $scope.intakeData = result;
            })
            .error(function (error) {
                //logError(error);
                console.log(error);
            });



            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindAgencies" } })
           .success(function (result) {
               $scope.agencyData = result;


               if (sessionStorage.getItem("locationId")) {
                   vm.Agency = sessionStorage.getItem("locationId");
                   //sessionStorage.removeItem('locationId');
               }
               else {
                   $scope.vm.Agency = userInfo.locationId.toString();
                   //BindDataByAgency(userInfo.locationId.toString());

               }

               BindDataByAgency($scope.vm.Agency);
           })
           .error(function (error) {
               //logError(error);
               console.log(error);
           });

        }
        function BindDataByAgency(id) {

            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindSurveyService", parameters: [localStorage.getItem('userInfo'), id] } })
          .success(function (result) {
              $scope.surveyService = result;
              vm.ServiceProvide = parseInt(sessionStorage.getItem("serviceID"));
              if (vm.ServiceProvide > 1) {
                  $scope.isShowProgram = false;
                  $scope.isDisableProgram = true;
              }
              else {
                  $scope.isShowProgram = true;
                  $scope.isDisableProgram = false;
              }
          })
          .error(function (error) {
              //logError(error);
              console.log(error);
          });

            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "AdminStateByRole", parameters: [userid, id] } })
            .success(function (result) {
                $scope.administeredData = result;
                if (sessionStorage.getItem("surveyAdmin")) {

                    //vm.Administered = _.find($scope.administeredData, function (surveyAdmin) {
                    //    return surveyAdmin.id == sessionStorage.getItem('surveyAdmin');
                    //});
                    vm.Administered = sessionStorage.getItem('surveyAdmin');
                    BindProgramData(sessionStorage.getItem('surveyAdmin'), id);
                    //sessionStorage.removeItem('surveyAdmin');
                }
                else {
                    if (result.length != 0) {
                        //BindProgramData(result[0].id);
                        BindProgramData(userid, id);
                    }
                }

            })
            .error(function (error) {
                //logError(error);
                console.log(error);
            });
        }
        $scope.BindUsersByAgency = function (agency) {
            var id = agency;
            var userInfoData = localStorage.getItem('userInfo');
            // vm.ServiceProvide = '';

            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindSurveyService", parameters: [userInfoData, id] } })
          .success(function (result) {
              $scope.surveyService = result;
              //vm.ServiceProvide = '';

          })
          .error(function (error) {
              logError(error);

          });
            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "AdminStateByRole", parameters: [userid, id] } })
            .success(function (result) {
                $scope.administeredData = result;
                if (sessionStorage.getItem("surveyAdmin")) {
                    //vm.Administered = _.find($scope.administeredData, function (surveyAdmin) {
                    //    return surveyAdmin.id == sessionStorage.getItem('surveyAdmin');
                    //});
                    vm.Administered = sessionStorage.getItem('surveyAdmin');
                    //sessionStorage.removeItem('surveyAdmin');
                }
                if (result.length != 0) {
                    BindProgramData(result[0].id, id);
                }

            })
            .error(function (error) {
                logError(error);

            });
        };

        $scope.clearIntake = function () {
            //vm.IsIndigent = false;
            //vm.IsWithdrawal = false;
            //vm.IsPregnant = false;
            //vm.IsIVDrugUser = false;
            //vm.IsPregnantParenting = false;

            //vm.IsCooccurring = false;

            //vm.IsAlcohol = false;
            //vm.IsOpioid = false;
            //vm.IsMethamphetamine = false;
            //vm.IsBenzodiazapines = false;
            //vm.IsMarijuana = false;
            //vm.IsOtherSubstances = false;


            //vm.IsMedicaid = false;
            //vm.IsPrivateInsurance = false;
            //vm.IsSelfPay = false;
            angular.forEach($scope.intakeData, function (value, key) {
                value.isSelect = false;;
            });
        }
        $scope.clearIndigentPregnant = function () {
            if (vm.ClientGenderType == "Adolescent Male") {
                $scope.clearIntake();
                //$scope.NotIndigent = true;
                //$scope.NotPregnant = true;

                angular.forEach($scope.intakeData, function (value, key) {
                    if (value.intakeTypeName == 'Pregnant' || value.intakeTypeName == 'Pregnant and Parenting with Children at the facility' || value.intakeTypeName == 'Indigent') {
                        value.isDisable = true;
                    } else { value.isDisable = false; }
                });
            } else if (vm.ClientGenderType == "Adolescent Female") {
                $scope.clearIntake();
                //$scope.NotIndigent = true;
                //$scope.NotPregnant = false;

                angular.forEach($scope.intakeData, function (value, key) {
                    if (value.intakeTypeName == 'Indigent') {
                        value.isDisable = true;
                    }
                    else { value.isDisable = false; }
                });

            }
            else if (vm.ClientGenderType == "Adult Male") {
                $scope.clearIntake();
                //$scope.NotIndigent = false;
                //$scope.NotPregnant = true;
                angular.forEach($scope.intakeData, function (value, key) {
                    if (value.intakeTypeName == 'Pregnant' || value.intakeTypeName == 'Pregnant and Parenting with Children at the facility') {
                        value.isDisable = true;
                    } else { value.isDisable = false; }
                });

            } else if (vm.ClientGenderType == "Adult Female") {
                $scope.clearIntake();
                //$scope.NotIndigent = false;
                //$scope.NotPregnant = false;
                angular.forEach($scope.intakeData, function (value, key) {
                    value.isDisable = false;
                });



            }
        }

        $scope.BindUsersByService = function (service, location) {
            var serID = service;
            var locID = location;
            $scope.isAdult = true;
            $scope.isAdolescent = true;
            $scope.isDisableProgram = false;




            var e = document.getElementById("ServiceProvide");

            var name = e.options[e.selectedIndex].text;
            if (name == 'Adult IOP') {
                $scope.isAdult = true;
                $scope.isAdolescent = false;
                $scope.isShowProgram = false;
                $scope.isDisableProgram = true;
                $scope.NotIndigent = false;
            }
            else if (name == 'Adolescent IOP') {
                $scope.isAdult = false;
                $scope.isAdolescent = true;
                $scope.isShowProgram = false;
                $scope.isDisableProgram = true;
                $scope.NotIndigent = true;
            }
            else if (name == 'Transitional') {
                $scope.isAdult = true;
                $scope.isAdolescent = true;
                $scope.isShowProgram = false;
                $scope.isDisableProgram = true;
                $scope.NotIndigent = false;
            }
            else {
                $scope.isAdult = true;
                $scope.isAdolescent = true;
                $scope.isShowProgram = true;
                $scope.isDisableProgram = false;
                $scope.NotIndigent = false;
            }


            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindUsersBySerciveId", parameters: [serID, locID] } })
            .success(function (result) {
                $scope.administeredData = result;
                vm.Administered = 0;
                //if (sessionStorage.getItem("surveyAdmin")) {
                //    //vm.Administered = _.find($scope.administeredData, function (surveyAdmin) {
                //    //    return surveyAdmin.id == sessionStorage.getItem('surveyAdmin');
                //    //});
                //    vm.Administered = sessionStorage.getItem('surveyAdmin');
                //    //sessionStorage.removeItem('surveyAdmin');
                //}
                if (result.length != 0) {
                    BindProgramData(result[0].id, locID);
                }

            })
            .error(function (error) {
                logError(error);

            });
        };




        function BindProgramData(id, LocationId) {
            var loginUserId = localStorage.getItem('userId');
            // alert(loginUserId);  check 144 line
            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "NewBindProgramData", parameters: [id, LocationId] } })
           // $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindProgramData", parameters: [id] } })
            //$http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindProgramData", parameters: [loginUserId] } })
          .success(function (result) {
              $scope.surveyProgram = result;
              vm.ProgramName = parseInt(sessionStorage.getItem("programId"));
              //sessionStorage.removeItem("programId");
          })
          .error(function (error) {
              //logError(error);
              console.log(error);
          });
        }

        function BindProgramDataByUser(id, locId) {

            //$http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindProgramData", parameters: [id] } })
            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "NewBindProgramData", parameters: [id, locId] } })
            .success(function (result) {
                $scope.surveyProgram = result;

            })
          .error(function (error) {
              console.log(error);
          });
        }

        vm.deleteEvent = function (event) {
            var modalInstance = $modal.open({
                templateUrl: '/app/common/surveyDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {
                $location.path('/surveyTools');
                $route.reload();
            }, function () {
            });
        };
        vm.checkStatusCahnge = function (event) {
            if (event == true) {
                vm.AgencyData = false;
            }
            else {
                vm.AgencyData = true;
            }
        };

        vm.checkNoResponse = function (event) {
            if (event == true) {
                vm.question_1 = "41";
                vm.question_2 = "41";
                vm.question_3 = "41";
                vm.question_4 = "41";
                vm.question_5 = "41";
                vm.question_6 = "41";
                vm.question_8 = "41";
            }
            else {
                vm.question_1 = "0";
                vm.question_2 = "0";
                vm.question_3 = "0";
                vm.question_4 = "0";
                vm.question_5 = "0";
                vm.question_6 = "0";
                vm.question_8 = "0";
            }
        };
        vm.ChangeAgency = function (event) {
            BindDataByAgency(event);
        };
        vm.ChangeAdministeredBy = function (event, locId) {


            if (vm.Administered === undefined || vm.Administered == '0') {
                $scope.adminvalue = true;
            }
            else {
                $scope.adminvalue = false;
            }

            BindProgramDataByUser(event, locId);
        };
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $scope.opened1 = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.open1 = function ($event) {
            $scope.openBeginDateopened = false;
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened1 = true;
        };
        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $scope.opened1 = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];
        $scope.Finish = function (surveyForm) {
            //vm.DynamicDataCtrl = document.getElementById('hdf').value;

            $scope.from = sessionStorage.getItem('from') == undefined ? 'survey' : sessionStorage.getItem('from');


            vm.UserID = userid;
            vm.SessionID = document.getElementById("hdfsd").value;
            vm.ClientSessionStart = document.getElementById("hdfcst").value;
            vm.Survey = "1";

            //vm.lstIntakevalues = $scope.intakeData


            var issubmit = true;
            if (vm.ProgramName == 0 && vm.ServiceProvide == 1) {
                issubmit = false;
            }
            if (vm.ServiceProvide > 1) {
                vm.ProgramName = 0;
            }

            if (vm.Administered === undefined || vm.Administered == '0') {
                $scope.adminvalue = true;
            }

            if ($scope.surveyForm.$valid && issubmit == true) {
                if (vm.SurveyType == 1) {
                    vm.ClientDischargeDate = null;
                }
                $http({
                    method: 'POST', url: 'api/TreatmentSurveyTool',
                    data: { method: "CreateSurveryTools", parameters: [JSON.stringify(vm), $scope.from, JSON.stringify($scope.intakeData)] }
                })
                .success(function (result) {
                    log("Survey submitted succesfully.");
                    // $location.path('/dashboard');
                    sessionStorage.removeItem('from');

                    var addUserModal = $modal.open({
                        templateUrl: 'app/treatment/treatmentSurveySummary.html',
                        controller: 'treatmentSurveySummary',
                        backdrop: 'static',
                        resolve: {
                            eventInfo: function () {
                                return result;
                            }
                        }

                    });
                })
                .error(function (error) {
                    //logError(error);
                    console.log(error);
                })
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        //$scope.checkAllPopulation = function () {
        //    vm.IsPregnant = vm.selectAllPopulation;
        //    vm.IsPregnantParenting = vm.selectAllPopulation;
        //    vm.IsIVDrugUser = vm.selectAllPopulation;
        //    vm.IsCooccurring = vm.selectAllPopulation;
        //    vm.IsWithdrawal = vm.selectAllPopulation;
        //}

        //$scope.checkAllSubstance = function () {
        //    vm.IsAlcohol = vm.selectAllSubstance;
        //    vm.IsOpioid = vm.selectAllSubstance;
        //    vm.IsMethamphetamine = vm.selectAllSubstance;
        //    vm.IsBenzodiazapines = vm.selectAllSubstance;
        //    vm.IsMarijuana = vm.selectAllSubstance;
        //    vm.IsOtherSubstances = vm.selectAllSubstance;

        //}

        //$scope.checkAllPayorSource = function () {
        //    vm.IsIndigent = vm.selectAllPayorSource;
        //    vm.IsMedicaid = vm.selectAllPayorSource;
        //    vm.IsPrivateInsurance = vm.selectAllPayorSource;
        //    vm.IsSelfPay = vm.selectAllPayorSource;
        //}
    }
})();










