﻿using System.Threading.Tasks;
namespace DataGadget.ScheduledTaskRunner.Runners
{
    [ScheduledTaskTag(Tag = "nightly")]
    public class NightlyTaskRunner : ITaskRunner
    {
        public async Task Run(params string[] args)
        {
            Logger.Debug("Running out nightly tasks here");
            await Task.FromResult<object>(null);
        }
    }
}
