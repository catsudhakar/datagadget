﻿(function () {
    'use strict';
    var controllerId = 'newOngoingEvent';
    angular
        .module('app')
        .controller(controllerId, newOngoingEvent);

    newOngoingEvent.$inject = ['$scope', 'common', '$http', '$window', '$location', '$routeParams', '$modal', '$route', 'sessionService'];

    function newOngoingEvent($scope, common, $http, $window, $location, $routeParams, $modal, $route, sessionService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.onetime = {};

        $scope.isDisabled = false;
        $scope.isdisplay = false;
        $scope.isdisplayCount = false;

        $scope.isdisplaymales = false;
        $scope.isdisplayfemales = false;

        $scope.backcolor = "";

        $scope.s01 = 0;
        $scope.s02 = 0;
        $scope.s03 = 0;


        //if ($routeParams.goingEvent) {
        //    vm.onetime = JSON.parse($routeParams.goingEvent);
        //}
        //else {
        //    vm.onetime = {};
        //}


        vm.helpEvent = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/common/newOneTimeDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',

            });
        };

        vm.directServiceHoursEvent = function ()
        {
            var directHours = parseFloat(vm.onetime.DirectServiceHours);
             if(directHours<=0)
            {
                $scope.directServiceMessage = "Direct service hours must be greater than zero";
            }
            else
            {
                $scope.directServiceMessage = "";
            }
        }

        vm.onetime.oneTimeEventForm3Submit = false;

        var isSelected = false;
        $scope.oneTimeEventForm1Submit = false;


        vm.onetime.oneTimeEventForm5Submit = false;



        $scope.tyoeofprogram_ea = 1;


        $scope.title = 'newOngoingEvent';
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];



        $scope.total = function () {
            var firstNum = parseInt($scope.male)
            var secondNum = parseInt($scope.female);

            if (!firstNum)
                firstNum = 0;
            if (!secondNum)
                secondNum = 0;

            return firstNum + secondNum;
        }


        // var vm = this;
        vm.programNREPP = [];
        vm.programState = [];
        vm.statewide = [];



        vm.programNames = [];

        var programType = '';
        var showProgram = false;



        activate();
        vm.back = function () {
            $window.history.back();
        }


        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }

        function activate() {
            //vm.onetime.programNREPP = "";
            var resState = [];
            vm.onetime.programNREPP = [];
            vm.programName = "";
            vm.onGoingEvent = {};
            if ($routeParams.goingEvent) {
                $scope.backcolor = "#ccc";
                $scope.isDisabled = true;

            }
            if (!_.isEmpty(sessionService.getData())) {
                $scope.backcolor = "#ccc";
                $scope.isDisabled = true;
                vm.onGoingEvent = sessionService.getData();
                sessionService.setData({});
            }

            if (vm.onGoingEvent.id) {


               

                vm.onGoingEventClone = JSON.parse(JSON.stringify(vm.onGoingEvent));

                resState = vm.onGoingEvent.targetedStatewideInitiative.split("|");


                vm.onetime.id = vm.onGoingEvent.id;
                vm.onetime.parentEntry = vm.onGoingEvent.parentEntry;

                vm.onetime.ProgramType_IP = vm.onGoingEvent.programType_IP;
                vm.onetime.startDate = vm.onGoingEvent.beginDate;
                vm.onetime.endDate = vm.onGoingEvent.endDate;
                vm.onetime.InterventionType = vm.onGoingEvent.interventionType.trim();

                vm.onetime.UniversalType_DI = vm.onGoingEvent.universalType_DI.trim();
                vm.onetime.tyoeofprogram_ea = vm.onGoingEvent.tyoeOfProgram_EA.trim();
                vm.onetime.ProgramNameSource = vm.onGoingEvent.programNameSource.trim();
                //vm.onetime.programName = vm.onGoingEvent.programName.trim();
                vm.programName = vm.onGoingEvent.programName.trim();
                vm.onetime.groupName = vm.onGoingEvent.groupName.trim();

                vm.onetime.programId = vm.onGoingEvent.programId;

                //if (vm.onGoingEvent.serviceLocationName)
                //    vm.onetime.serviceLocationName = vm.onGoingEvent.serviceLocationName.trim();

                if (vm.onGoingEvent.serviceSchoolName)
                    vm.onetime.serviceSchoolName = vm.onGoingEvent.serviceSchoolName.trim();

                //Second section
                vm.onetime.males = vm.onGoingEvent.males;
                vm.onetime.females = vm.onGoingEvent.females;
                vm.onetime.totalParticipants = vm.onGoingEvent.numberParticipants;

                vm.onetime.Age0 = vm.onGoingEvent.age0;
                vm.onetime.Age5 = vm.onGoingEvent.age5;
                vm.onetime.Age12 = vm.onGoingEvent.age12;
                vm.onetime.Age15 = vm.onGoingEvent.age15;
                vm.onetime.Age18 = vm.onGoingEvent.age18;
                vm.onetime.Age21 = vm.onGoingEvent.age21;
                vm.onetime.Age25 = vm.onGoingEvent.age25;
                vm.onetime.Age45 = vm.onGoingEvent.age45;
                vm.onetime.Age65 = vm.onGoingEvent.age65;

                vm.onetime.RaceWhite = vm.onGoingEvent.raceWhite;
                vm.onetime.RaceBlack = vm.onGoingEvent.raceBlack;
                vm.onetime.RaceAsian = vm.onGoingEvent.raceAsian;
                vm.onetime.RaceAmIndian = vm.onGoingEvent.raceAmIndian;
                vm.onetime.RaceHawaii = vm.onGoingEvent.raceHawaii;
                vm.onetime.RaceUnknownOther = vm.onGoingEvent.raceUnknownOther;
                vm.onetime.RaceMoreThanOne = vm.onGoingEvent.raceMoreThanOne;
                vm.onetime.TotalRace = vm.onGoingEvent.totalRace;

                vm.onetime.NotHispanic = vm.onGoingEvent.notHispanic;
                vm.onetime.Hispanic = vm.onGoingEvent.hispanic;
                vm.onetime.TotalEthni = vm.onGoingEvent.totalEthni;

                vm.onetime.D_HiRi_ChildOfSA = vm.onGoingEvent.d_HiRi_ChildOfSA;
                vm.onetime.D_HiRi_Violent = vm.onGoingEvent.d_HiRi_Violent;
                vm.onetime.D_HiRi_PhysDis = vm.onGoingEvent.d_HiRi_PhysDis;
                vm.onetime.D_HiRi_Homel = vm.onGoingEvent.d_HiRi_Homel;
                vm.onetime.D_HiRi_PregUse = vm.onGoingEvent.d_HiRi_PregUse;
                vm.onetime.D_HiRi_MentH = vm.onGoingEvent.d_HiRi_MentH;
                vm.onetime.D_HiRi_AbuVict = vm.onGoingEvent.d_HiRi_AbuVict;
                vm.onetime.D_HiRi_K12DO = vm.onGoingEvent.d_HiRi_K12DO;
                vm.onetime.D_HiRi_EconDis = vm.onGoingEvent.d_HiRi_EconDis;
                vm.onetime.AlreadyUse = vm.onGoingEvent.alreadyUse;
                vm.onetime.D_HiRi_Other = vm.onGoingEvent.d_HiRi_Other;
                vm.onetime.TotalRisk = vm.onGoingEvent.totalRisk;



                //Third Section
                vm.onetime.Strat_ID_01 = vm.onGoingEvent.strat_ID_01;
                //if (vm.onetime.Strat_ID_01 == 1)
                //    vm.onetime.Strat_ID_01 = true;

                $scope.s01 = vm.onGoingEvent.strat_ID_01;
                $scope.s02 = vm.onGoingEvent.strat_ID_02;

                vm.onetime.Strat_ID_02 = vm.onGoingEvent.strat_ID_02;
                vm.onetime.Strat_ID_03 = vm.onGoingEvent.strat_ID_03;
                vm.onetime.Strat_ID_04 = vm.onGoingEvent.strat_ID_04;
                vm.onetime.Strat_ID_05 = vm.onGoingEvent.strat_ID_05;
                vm.onetime.Strat_ID_06 = vm.onGoingEvent.strat_ID_06;
                vm.onetime.Strat_ID_07 = vm.onGoingEvent.strat_ID_07;
                vm.onetime.Strat_ID_08 = vm.onGoingEvent.strat_ID_08;
                vm.onetime.Strat_ID_09 = vm.onGoingEvent.strat_ID_09;
                vm.onetime.Strat_ID_Strat_ID_09Text = vm.onGoingEvent.strat_ID_09Text;


                vm.onetime.Strat_Ed_11 = vm.onGoingEvent.strat_Ed_11;
                vm.onetime.Strat_Ed_12 = vm.onGoingEvent.strat_Ed_12;
                vm.onetime.Strat_Ed_13 = vm.onGoingEvent.strat_Ed_13;
                vm.onetime.Strat_Ed_14 = vm.onGoingEvent.strat_Ed_14;
                vm.onetime.Strat_Ed_15 = vm.onGoingEvent.strat_Ed_15;
                vm.onetime.Strat_Ed_16 = vm.onGoingEvent.strat_Ed_16;
                vm.onetime.Strat_Ed_17 = vm.onGoingEvent.strat_Ed_17;
                vm.onetime.Strat_Ed_17Text = vm.onGoingEvent.strat_Ed_17Text;

                vm.onetime.Strat_Alt_21 = vm.onGoingEvent.strat_Alt_21;
                vm.onetime.Strat_Alt_22 = vm.onGoingEvent.strat_Alt_22;
                vm.onetime.Strat_Alt_23 = vm.onGoingEvent.strat_Alt_23;
                vm.onetime.Strat_Alt_24 = vm.onGoingEvent.strat_Alt_24;
                vm.onetime.Strat_Alt_25 = vm.onGoingEvent.strat_Alt_25;
                vm.onetime.Strat_Alt_26 = vm.onGoingEvent.strat_Alt_26;
                vm.onetime.Strat_Alt_27 = vm.onGoingEvent.strat_Alt_27;
                vm.onetime.Strat_Alt_27Text = vm.onGoingEvent.strat_Alt_27Text;

                vm.onetime.Strat_IR_31 = vm.onGoingEvent.strat_IR_31;
                vm.onetime.Strat_IR_32 = vm.onGoingEvent.strat_IR_32;
                vm.onetime.Strat_IR_34 = vm.onGoingEvent.strat_IR_34;
                vm.onetime.Strat_IR_34Text = vm.onGoingEvent.strat_IR_34Text;

                vm.onetime.Strat_CBP_41 = vm.onGoingEvent.strat_CBP_41;
                vm.onetime.Strat_CBP_42 = vm.onGoingEvent.strat_CBP_42;
                vm.onetime.Strat_CBP_43 = vm.onGoingEvent.strat_CBP_43;
                vm.onetime.Strat_CBP_44 = vm.onGoingEvent.strat_CBP_44;
                vm.onetime.Strat_CBP_45 = vm.onGoingEvent.strat_CBP_45;
                vm.onetime.Strat_CBP_46 = vm.onGoingEvent.strat_CBP_46;
                vm.onetime.Strat_CBP_46Text = vm.onGoingEvent.strat_CBP_46Text;

                vm.onetime.Strat_CBP_Faith = vm.onGoingEvent.strat_CBP_Faith; //Faith

                vm.onetime.Strat_Env_51 = vm.onGoingEvent.strat_Env_51;
                vm.onetime.Strat_Env_52 = vm.onGoingEvent.strat_Env_52;
                vm.onetime.Strat_Env_53 = vm.onGoingEvent.strat_Env_53;
                vm.onetime.Strat_Env_54 = vm.onGoingEvent.strat_Env_54;
                vm.onetime.Strat_Env_55 = vm.onGoingEvent.strat_Env_55;
                vm.onetime.Strat_Env_56_MW = vm.onGoingEvent.strat_Env_56_MW;
                vm.onetime.Strat_Env_57 = vm.onGoingEvent.strat_Env_57;
                vm.onetime.Strat_Env_57Text = vm.onGoingEvent.strat_Env_57Text;

                vm.onetime.NumberProgHrsRecd = vm.onGoingEvent.numberProgHrsRecd;
                vm.onetime.TotalCostofProgram = vm.onGoingEvent.totalCostofProgram;
                vm.onetime.AvgCostPerParticipant = vm.onGoingEvent.avgCostPerParticipant;
                vm.onetime.WithinCostBand_YN = vm.onGoingEvent.withinCostBand_YN;

                vm.onetime.NumberOfEmployees = vm.onGoingEvent.numberOfEmployees;
                vm.onetime.TravelHours = 0;
                //vm.onetime.DirectServiceHours = 0;
                vm.onetime.PrepHours = 0;

                vm.onetime.DocOfActivities_Agenda = vm.onGoingEvent.docOfActivities_Agenda;
                vm.onetime.DocOfActivities_EvalForm = vm.onGoingEvent.docOfActivities_EvalForm;
                vm.onetime.DocOfActivities_PrePostTest = vm.onGoingEvent.docOfActivities_PrePostTest;
                vm.onetime.DocOfActivities_ContactForm = vm.onGoingEvent.docOfActivities_ContactForm;
                vm.onetime.DocOfActivities_SignIn = vm.onGoingEvent.docOfActivities_SignIn;
                vm.onetime.DocOfActivities_Brochure = vm.onGoingEvent.docOfActivities_Brochure;
                vm.onetime.DocOfActivities_Other = vm.onGoingEvent.docOfActivities_Other;
                vm.onetime.DocOfActivities_OtherText = vm.onGoingEvent.docOfActivities_OtherText;
                vm.onetime.OverallComments = vm.onGoingEvent.overallComments;
                vm.onetime.closed = 0;






            }
            else {
                // vm.onetime.programType_IP = 'Individual';
                vm.onetime.ProgramType_IP = 'Individual';
                vm.onetime.startDate = '';//formattedDate();//new Date();
                vm.onetime.endDate = '';//formattedDate();//new Date();
                vm.onetime.InterventionType = "Universal";

                vm.onetime.UniversalType_DI = "Direct";
                vm.onetime.tyoeofprogram_ea = "Evidence";
                vm.onetime.ProgramNameSource = "State";

                vm.onetime.closed = 0;



            }


            //$http({
            //    method: 'POST', url: 'api/oneTimeEvent',
            //    data: { method: "GetProgramsAndStrategies", parameters: ['NREPP'] }
            //})
            //.success(function (result) {
            //    vm.programNREPP = result;
            //})
            //.error(function (error) {
            //    console.log(error);
            //})

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetProgramsAndStrategies", parameters: [vm.onetime.ProgramNameSource] }
            })
            .success(function (result) {
                vm.onetime.programNREPP = result;
                vm.onetime.programName = vm.programName;
            })
            .error(function (error) {
                console.log(error);
            })


            vm.onetime.serviceLocationsList = [];

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetServiceLocations", parameters: '' }
            })
           .success(function (result) {
               vm.onetime.serviceLocationsList = result;


               if (vm.onGoingEvent.id) {
                   if (vm.onGoingEvent.serviceLocationName) {
                       vm.onetime.serviceLocationName = vm.onGoingEvent.serviceLocationName.trim();
                   }

               }
               //else {
               // vm.onetime.serviceLocationName = 'School';
               //}
           })
           .error(function (error) {
               console.log(error);
           })



            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetStatewideInitiatives", parameters: '' }
            })
                       .success(function (result) {
                           //vm.statewide = result;
                           // 
                           vm.onetime.lstStatewideInitiative = result;
                           for (var i = 0; i < vm.onetime.lstStatewideInitiative.length; i++) {
                               for (var j = 0; j < resState.length; j++) {
                                   if (vm.onetime.lstStatewideInitiative[i].initiativeName == resState[j]) {
                                       vm.onetime.lstStatewideInitiative[i].isSelected = true;
                                       vm.onetime.lstStatewideInitiative[i].isDisabled = true;
                                       break;
                                   }

                               }

                           }


                       })
                       .error(function (error) {
                           console.log(error);
                       })


        }

        vm.gotoStep1 = function () {

            document.querySelector('core-animated-pages').selected = 0;
        }


        vm.gotoStep2 = function (form, currentStep) {

            var pn = vm.onetime.programName;
            $scope.ismessage = "";
            $scope.isdisplay = false;
            var sdt = vm.onetime.startDate;
            var edt = vm.onetime.endDate;
            var issubmit = true;
            if (sdt !== undefined && edt !== undefined) {
                if (sdt > edt) {
                    $scope.ismessage = 'Please enter end date should be greater then start date'
                    issubmit = false;
                    $scope.isdisplay = true;
                }
            }


            if (currentStep == 1) {
                $scope.oneTimeEventForm1Submit = true;

                if (form.$valid && issubmit) {

                    document.querySelector('core-animated-pages').selected = 1;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 1;
            }

            //saveStep1();

        }
        vm.gotoStep3 = function (ongoingEventForm2, step) {


            var i = vm.onetime.totalParticipant;
            var a = vm.onetime.totalAge;
            var r = vm.onetime.totalRace;
            var e = vm.onetime.totalEthnicity;
            var b = false;
            if (i == a && a == r && r == e) {
                b = true;
            }
            //if (b == false)
            //{
            //    document.querySelector('core-animated-pages').selected = 2;
            //}
            if (i > 0 && b == true) {
                $scope.isdisplay = false;
                $scope.isdisplayCount = false;

                if (ongoingEventForm2) {
                    ongoingEventForm2.$submitted = true;
                    if (ongoingEventForm2.$valid && !ongoingEventForm2.$invalid) {
                        document.querySelector('core-animated-pages').selected = 2;
                    }
                }
                else {
                    document.querySelector('core-animated-pages').selected = 2;
                }
            }
            else {
                if (b) {
                    $scope.isdisplay = true;
                    $scope.isdisplayCount = false;
                }
                else if (i > 0) {
                    $scope.isdisplayCount = false;
                    $scope.isdisplay = false;
                }
                else {
                    $scope.isdisplay = true;
                    $scope.isdisplayCount = false;
                }

            }

            if (vm.onGoingEvent.id) {
                if (vm.onetime.males < vm.onGoingEventClone.males) {
                    $scope.isdisplaymales = true;
                    //ongoingEventForm2.$submitted = false;
                }
                else {
                    $scope.isdisplaymales = false;
                }

                if (vm.onetime.females < vm.onGoingEventClone.females) {
                    $scope.isdisplayfemales = true;
                }
                else {
                    $scope.isdisplayfemales = false;
                }

            }

            else {
                $scope.isdisplaymales = false;
                $scope.isdisplayfemales = false;
            }
        }
        vm.gotoStep4 = function () {
            $scope.ispretestChecked = false;
            document.querySelector('core-animated-pages').selected = 3;
            if (vm.onetime.tyoeofprogram_ea == "Evidence" && vm.onetime.ProgramNameSource == "NREPP") {
                vm.onetime.DocOfActivities_PrePostTest = 1;
                $scope.ispretestChecked = true;
            }

        }

        vm.gotoStep5 = function (form, currentStep) {
            $scope.formValid = true;

            if (form) {

                if (vm.onetime.tyoeofprogram_ea == "Evidence")

                    vm.onetime.tyoeofprogramValue = "Evidence Based"
                else

                    vm.onetime.tyoeofprogramValue = "Approved"


                if (vm.onetime.tyoeofprogram_ea == "Evidence" && vm.onetime.ProgramNameSource == "NREPP") {
                    $scope.formValid = vm.onetime.DocOfActivities_PrePostTest == 1;
                }



                form.$valid = (vm.onetime.DirectServiceHours != 0 && vm.onetime.DirectServiceHours != null && $scope.formValid == true);

                $scope.oneTimeEventForm5Submit = true;

                if (form.$valid) {

                    document.querySelector('core-animated-pages').selected = 4;
                }
            }

            else {
                document.querySelector('core-animated-pages').selected = 4;
            }
        }


        vm.showProgName = function (form, currentStep) {




        }

        vm.singleDecimal=function()
        {
            alert("sudha");
        }
        vm.stateApproveChange = function () {
            vm.onetime.programNREPP = "";
            vm.onetime.programName = "";

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetProgramsAndStrategies", parameters: [vm.onetime.ProgramNameSource] }
            })
           .success(function (result) {
               vm.onetime.programNREPP = result;

           })
           .error(function (error) {
               console.log(error);
           })

        }

        vm.getProgramId = function () {

            //alert(vm.onetime.programName);
            $scope.prgId = 0;

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetProgramIdByProgramName", parameters: [vm.onetime.programName] }
            })
           .success(function (result) {
               $scope.prgId = result;

           })
           .error(function (error) {
               console.log(error);
           })

        }


        vm.saveStep1 = function (form, currentStep) {

            if (currentStep == 3) {

                vm.onetime.oneTimeEventForm3Submit = true;

                if (form.$valid) {

                    document.querySelector('core-animated-pages').selected = 3;

                }

            }

            var userId = localStorage.getItem("userId");
            vm.onetime.userGUID = userId;

            vm.onetime.curriculum = 1;
            //vm.onetime.parentEntry = vm.onetime.id;
            vm.onetime.parentEntry = vm.onetime.parentEntry;
            vm.onetime.sessionName = "";
            vm.onetime.closed = vm.onetime.closed;


            return $http({
                method: 'POST', url: 'api/ongoingEvents',
                data: {
                    method: "SaveEventDetails",
                    parameters: [JSON.stringify(vm.onetime)]

                }
            });
            //.success(function (result) {
            //    vm.statewide = result;
            //})
            //.error(function (error) {
            //    console.log(error);
            //})

            // $location.path('/oneTimeEvents');
        }


        vm.save1 = function () {


            var userId = localStorage.getItem("userId");
            vm.onetime.userGUID = userId;

            vm.onetime.curriculum = 1;
            //vm.onetime.parentEntry = vm.onetime.id;
            vm.onetime.parentEntry = vm.onetime.parentEntry;
            vm.onetime.sessionName = "";
            vm.onetime.closed = vm.onetime.closed;

            if (vm.onetime.males === undefined)
                vm.onetime.males = 0;
            if (vm.onetime.females === undefined)
                vm.onetime.females = 0;

            vm.onetime.NumberParticipants = vm.onetime.males + vm.onetime.females;

            
            vm.onetime.startDate = GetFormattedDate(new Date(vm.onetime.startDate));
            vm.onetime.endDate = GetFormattedDate(new Date(vm.onetime.endDate));


            return $http({
                method: 'POST', url: 'api/ongoingEvents',
                data: {
                    method: "SaveEventDetails",
                    parameters: [JSON.stringify(vm.onetime)]

                }
            })
            .success(function (result) {
                vm.statewide = result;
                if ($scope.prgId == null) {
                    $scope.prgId = vm.onetime.programId;
                }
                localStorage.setItem('OnGoingEventData', JSON.stringify(result));
                localStorage.setItem("prgName", vm.onetime.programName);
                localStorage.setItem("grName", vm.onetime.groupName);
                localStorage.setItem("prgId", $scope.prgId);

                localStorage.setItem("prgType", vm.onetime.ProgramNameSource);

                var addUserModal = $modal.open({
                    templateUrl: 'app/prevention/printsummary.html',
                    controller: 'printsummary',
                    backdrop: 'static',
                    resolve: {
                        eventInfo: function () {
                            return result;
                        }
                    }

                });



                //sessionStorage.setItem("OnGoingEventData", result);
                //$location.path('/ongoingEvents');
                //var p = '/surveyTools/' + JSON.stringify(vm.onetime);
                // $location.path(p);
                // $location.path('/surveyTools')
                //$route.reload();
            })
            .error(function (error) {
                console.log(error);
                logError(error.exceptionMessage);
                $location.path('/ongoingEvents');
                $route.reload();
            })


        }

    }
})();


























//(function () {
//    'use strict';
//    var controllerId = 'newOngoingEvent';
//    angular
//        .module('app')
//        .controller(controllerId, newOngoingEvent);

//    newOngoingEvent.$inject = ['$scope', 'common', '$http', '$window', '$location'];

//    function newOngoingEvent($scope, common, $http, $window, $location) {
//        var getLogFn = common.logger.getLogFn;
//        var log = getLogFn(controllerId);
//        var logError = getLogFn(controllerId, 'error');

//        $scope.title = 'newOngoingEvent';
//        var vm = this;

//        activate();
//        vm.back = function () {
//            $window.history.back();
//        }
//        function activate() {

//        }

//        vm.gotoStep1 = function () {

//            document.querySelector('core-animated-pages').selected = 0;
//        }


//        vm.gotoStep2 = function () {

//            document.querySelector('core-animated-pages').selected = 1;
//        }
//        vm.gotoStep3 = function () {

//            document.querySelector('core-animated-pages').selected = 2;
//        }
//        vm.gotoStep4 = function () {

//            document.querySelector('core-animated-pages').selected = 3;
//        }
//        vm.save = function () {
//            $location.path('/ongoingEvents');
//        }

//    }
//})();










