﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('weeklyMailTemplate', weeklyMailTemplate);

    weeklyMailTemplate.$inject = ['$scope','$http','$location','common','$modal','$window']; 

    function weeklyMailTemplate($scope, $http, $location, common, $modal, $window) {
        $scope.title = 'weeklyMailTemplate';
        $scope.Id = 0;
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn('weeklyMailTemplate');
        var logError = getLogFn('weeklyMailTemplate', 'error');
        var userid = localStorage.getItem('userId');
        activate();


        function activate() {

           
            $http({ method: 'POST', url: 'api/MailTemplate', data: { method: 'GetTemplate' } })
           
            .success(function (res) {
                if (res != null) {
                    if (res.id > 0)
                        {
                    vm.template = res.template;
                    vm.Id = res.id
                    }
                    else
                    {
                        vm.template = "";
                        vm.Id = 0;
                    }

                }

                else {

                    vm.template = "";
                    vm.Id = 0;
                }

            }).error(function (err) {
                logError(err);
            })
        }


        $scope.submit = function () {

            $http({ method: 'POST', url: 'api/MailTemplate', data: { method: 'CreateEmailTemplate', parameters: [vm.template, vm.Id] } })
           
              .success(function (res) {
                  log("Weekly mail template has been saved succussfully.");
                              
              })
              .error(function (err) {
                 logError(err)
              })
        }
    }
})();
