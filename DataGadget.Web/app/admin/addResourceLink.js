﻿(function () {
    'use strict';
    var controllerId = 'addResourceLink';
    angular
        .module('app')
        .controller(controllerId, addResourceLink);

    addResourceLink.$inject = ['$scope', '$http', '$modalInstance', '$location', 'resourcesInfo', '$window', '$route','common'];//, 'SharedService'];

    function addResourceLink($scope, $http, $modalInstance, $location, resourcesInfo, $window, $route,common) {//, sharedService) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        $scope.title = 'addResourceLink';

        if (resourcesInfo) {
            $scope.title = "Edit Resource Link";
        }
        else {
            $scope.title = "Add Resource Link";
        }
        $scope.statewide = {};
        $scope.isSubmit = false;

        //$scope.initiativeID = 0;
        //$scope.initiativeName = "";
        activate();

        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (userInfo.level != "SIT3U" && userInfo.level != "SIT2A") {
                if (resourcesInfo) {
                    $scope.statewide.resourceId = resourcesInfo.resourceId;
                    $scope.statewide.resourceName = resourcesInfo.resourceName;
                    $scope.statewide.resourceLink1 = resourcesInfo.resourceLink1;
                }
            }
            else {
                $location.path('/dashboard');
            }


        }


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function (frmResourceLink) {
            $scope.isSubmit = true;

            if (frmResourceLink.$valid) {

                //if ($scope.statewide.resourceId != 0) {
                //    $scope.initiativeID = $scope.statewide.resourceId;
                //    $scope.initiativeName = $scope.statewide.resourceName;
                //}
                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateNewLink",

                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {

                        $modalInstance.close(result);
                        $location.path('/resourceLinksList');
                        $route.reload();
                        log("Resource saved successfully");

                        //if ($scope.initiativeID != 0) {
                        //    for (var i = 0; i < sharedService.statewides.length; i++) {
                        //        if (sharedService.statewides[i].id == $scope.initiativeID) {
                        //            sharedService.statewides[i].initiativeName = $scope.initiativeName;
                        //        }
                        //    }
                        //}

                    })
                    .error(function (error) {
                        console.log(error);
                    })

                $location.path('/resourceLinksList');
                $route.reload();
            }
        };
    }
})();
