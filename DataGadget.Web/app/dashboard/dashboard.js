﻿(function () {
    'use strict';
    var controllerId = 'dashboard';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$http', '$scope', '$window', '$modal', dashboard]);



    function dashboard(common, datacontext, $http, $scope, $window, $modal) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var log1 = getLogFn(controllerId, 'warn');

        var userid = localStorage.getItem('userId');
        var vm = this;
        vm.news = { title: 'DataGadget', description: 'DataGadgte' };
        vm.messageCount = 0;

        $scope.isUsercontactData = true;

        $scope.isdescending = false;
        $scope.isAescending = false;

        vm.typeFollowup = 1;
        vm.people = [];
        vm.links = [];
        vm.title = 'Dashboard';
        vm.adminDashBoard = [];
        vm.userDashBoard = [];
        vm.adminStyle = "display:none;";
        vm.userStyle = "display:none;";
        vm.loadAdminStyle = [];
        $scope.isUserData = true;
        $scope.isAdminData = true;
        $scope.evidenceBasedHours = 0;
        $scope.nonEvidenceBasedHours = 0;
        $scope.targetHours = 0;
        $scope.isdataempty = false;
        $scope.isOnlyAdmin = true;
        $scope.isPaging = true;
        vm.contactDetails = [];
        $scope.isContactPaging = false;
        vm.pagerContactData;
        vm.pageContactList = [];

        vm.bedDetails = [];
        $scope.isBedPaging = false;
        vm.pagerBedData;
        vm.pageBedList = [];

        vm.pagerData;
        vm.pageList = [];
        $scope.isPaging = false;
        $scope.agencyData = [];

        $scope.eventsCount = [];

        $scope.isUserHours = false;

        vm.usermarginstyle = "margin-top: 0px;";

        $scope.categoryList = [];
        $scope.isPrevention = false;
        $scope.isTreatment = false;
        vm.bedSort = 0;


        activate();
        vm.Site;

        function activate() {

            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));

            $scope.categoryList = loginUserInfo.selectedLocationCategoryList;

            //for (i = 0; i < $scope.categoryList.length; i++) {
            //    if($scope.categoryList[i].categoryID==2)
            //    {

            //    }
            //}

            switch ($scope.categoryList.length) {
                case 2:
                    $scope.isPrevention = true;
                    $scope.isTreatment = true;
                    break;
                case 1:
                    if ($scope.categoryList[0]["categoryID"] == 2) {
                        $scope.isTreatment = true;
                        $scope.isPrevention = false;
                    }
                    else {
                        $scope.isTreatment = false;
                        $scope.isPrevention = true;
                    }
                    break;
                default:
                    $scope.isPrevention = false;
                    $scope.isTreatment = false;
            }




            vm.typeFollowup = '1';
            if (document.getElementById('hdnSortType'))
                document.getElementById('hdnSortType').value = '';

            var promises = [getMessageCount(), getPeople()];
            common.activateController(promises, controllerId)
				.then(function () { });
            if ($scope.isPrevention == true)
                getDashBoardData();

            // if ($scope.isTreatment == true)
            getTreatmentDashBoardData(1,vm.bedSort);

            // getContactsDashBoard(1, 3, '');


            if ($scope.isTreatment == true)
                getContactsDashBoard(1, 1, '');
            //vm.typeFollowup = 3;

            getuserhours();
            $scope.isdescending = true;

            //GetWeekllyEmailData();


            //GetEventsCount();

          SendEmailTreatmentUser();






        }

        function GetWeekllyEmailData() {

            var userInfo = JSON.parse(localStorage.getItem('userInfo'));

            $http({ method: 'POST', url: 'api/Common', data: { method: "GetWeekllyEmailData", parameters: [userid, userInfo.locationId, userInfo.level, userInfo.stateabbrev] } })
           .success(function (result) {
               if (result == false) {
                   //log1("User Dont Have Target Hours");
               }

           })
           .error(function (error) {

               console.log(error);
           });
        }

        function SendEmailTreatmentUser() {

            var userInfo = JSON.parse(localStorage.getItem('userInfo'));

            $http({ method: 'POST', url: 'api/Common', data: { method: "SendEmailTreatmentUser", parameters: [userid, userInfo.locationId, userInfo.level, userInfo.stateabbrev] } })
           .success(function (result) {
               if (result == false) {
                   //log1("User Dont Have Target Hours");
               }

           })
           .error(function (error) {

               console.log(error);
           });
        }

        function getMessageCount() {
            return datacontext.getMessageCount().then(function (data) {
                return vm.messageCount = data;
            });
        }
        function getPeople() {
            return datacontext.getPeople().then(function (data) {
                return vm.people = data;
            });
        }

        function GetEventsCount() {

            var userInfo = JSON.parse(localStorage.getItem('userInfo'));

            $http({ method: 'POST', url: 'api/Common', data: { method: "GetEventsCount", parameters: [userid, userInfo.locationId, userInfo.level, userInfo.stateabbrev] } })
           .success(function (result) {
               $scope.eventsCount = result;

           })
           .error(function (error) {

               console.log(error);
           });
        }
        function getuserhours() {

            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));

            $http({ method: 'POST', url: 'api/Common', data: { method: "CheckUserHours", parameters: [loginUserInfo.pkey] } })
           .success(function (result) {
               if (result == false) {
                   log1("User Dont Have Target Hours");
               }

           })
           .error(function (error) {

               console.log(error);
           });
        }


        function paging(pageId) {
            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
            loginUserInfo.pageId = pageId;
            loginUserInfo.agency = vm.Site;
            $http({ method: 'POST', url: 'api/Common', data: { method: "AdminDashBoard", parameters: [loginUserInfo.level, loginUserInfo.locationId, loginUserInfo.stateabbrev, loginUserInfo.pageId, loginUserInfo.agency, userid] } })
			.success(function (result) {
			    $scope.isPaging = false;
			    vm.pagerData = result.item1[0];
			    if (parseInt(vm.pagerData.numberOfPages) > 1) {
			        $scope.isPaging = true;
			    }
			    vm.adminStyle = "display:block;";
			    vm.loadAdminStyle = new Array(1);
			    var userData = result.item2;
			    vm.adminDashBoard = userData;
			    vm.pageList = [];
			    if (pageId > 3) {
			        vm.pageList.push(pageId - 2);
			        vm.pageList.push(pageId - 1);
			        vm.pageList.push(pageId);
			        if (pageId < vm.pagerData.numberOfPages) {
			            vm.pageList.push(pageId + 1);
			            if (pageId < (vm.pagerData.numberOfPages - 1)) {
			                vm.pageList.push(pageId + 2);
			            }
			        }
			    }
			    else {
			        if (pageId == 1) {
			            vm.pageList.push(pageId);
			            if (pageId + 1 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 1);
			            }
			            if (pageId + 2 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 2);
			            }
			            if (pageId + 3 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 3);
			            }
			            if (pageId + 4 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 4);
			            }

			        }
			        if (pageId == 2) {
			            vm.pageList.push(pageId - 1);
			            vm.pageList.push(pageId);

			            if (pageId + 1 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 1);
			            }
			            if (pageId + 2 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 2);
			            }
			            if (pageId + 3 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 3);
			            }
			        }
			        if (pageId == 3) {
			            vm.pageList.push(pageId - 2);
			            vm.pageList.push(pageId - 1);
			            vm.pageList.push(pageId);
			            if (pageId + 1 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 1);
			            }
			            if (pageId + 2 <= vm.pagerData.numberOfPages) {
			                vm.pageList.push(pageId + 2);
			            }
			        }

			    }

			})
			.error(function (error) {
			    console.log(error);
			})
        }

        vm.sitePagination = function (Site) {
            vm.Site = Site;
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT2A") {
                $scope.isOnlyAdmin = false;
            }
            paging(1);
        }

        vm.pagination = function (pageId) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT2A") {
                $scope.isOnlyAdmin = false;
            }
            paging(pageId);
        }

        function getDashBoardData() {

            $http({ method: 'POST', url: 'api/TreatmentSurveyTool', data: { method: "BindAgencies" } })
          .success(function (result) {
              $scope.agencyData = result;

          })
          .error(function (error) {
              //logError(error);
              console.log(error);
          });

            var userInfo = JSON.parse($window.localStorage["userInfo"]);



            if (userInfo.level == "SIT2A") {
                $scope.isOnlyAdmin = false;
            }

            if (userInfo.level == "SIT3U") {
                $scope.isOnlyAdmin = false;
                vm.usermarginstyle = "margin-top: 200px;";
            }

            $scope.isUserData = false;
            $scope.isAdminData = true;
            paging(1);

            //if (userInfo.level == "SIT3U") {

            //    $http({ method: 'POST', url: 'api/Common', data: { method: "DashBoard", parameters: [userid] } })
            //		.success(function (result) {
            //		    vm.userStyle = "display:block;";
            //		    $scope.isAdminData = false;
            //		    $scope.isUserData = true;
            //		    if (result) {
            //		        // debugger;
            //		        $scope.evidenceBasedHours = result.evidenceBasedHours;
            //		        $scope.nonEvidenceBasedHours = result.nonEvidenceBasedHours;
            //		        $scope.targetHours = result.targetHours;
            //		        var avgResult = (parseFloat($scope.evidenceBasedHours * 10) / parseFloat(result.targetHours)) * 100;
            //		        $scope.avgResult = Math.round(avgResult).toFixed(2)
            //		        if (isNaN($scope.avgResult)) {
            //		            $scope.avgResult = 0;
            //		        }
            //		        if (parseFloat(result.nonEvidenceBasedHours) == 0 && $scope.evidenceBasedHours == 0) {
            //		            result.nonEvidenceBasedHours = 100;
            //		        }
            //		        var userDetails = {};
            //		        userDetails.userName = userInfo.userName;
            //		        userDetails.targetHours = $scope.targetHours / 10;
            //		        userDetails.totalHours = $scope.evidenceBasedHours * 10;
            //		        userDetails.totalTargetHours = $scope.targetHours;
            //		        userDetails.evidenceBasedHours = $scope.evidenceBasedHours;
            //		        userDetails.totalRemainingHours = $scope.nonEvidenceBasedHours;
            //		        userDetails.remainingHours = $scope.nonEvidenceBasedHours / 10;
            //		        userDetails.avgResult = $scope.avgResult;
            //		        vm.userDashBoard.push(userDetails);

            //		        $scope.chart = new CanvasJS.Chart("chartContainer", {
            //		            theme: 'theme1',
            //		            title: {
            //		                text: ""
            //		            },
            //		            axisY: {
            //		                title: "",
            //		                labelFontSize: 12,
            //		            },
            //		            axisX: {
            //		                labelFontSize: 12,
            //		            },
            //		            data: [
            //                        {
            //                            indexLabelFontSize: 16,
            //                            indexLabelFontFamily: "Monospace",
            //                            indexLabelFontColor: "darkgrey",
            //                            indexLabelLineColor: "darkgrey",
            //                            indexLabelPlacement: "outside",
            //                            type: "pie",
            //                            showInLegend: true,
            //                            toolTipContent: "{legendText}: <strong>{y} Hours</strong>",
            //                            indexLabel: "{label} {y}%",
            //                            dataPoints: [
            //                                //{ color: '#5F9EA0', y: parseFloat(result.sumOfPrepHours) * 10, legendText: "Prep", indexLabel: "Prep Hours" },
            //                        		//{ color: '#B8860B', y: parseFloat(result.sumOfTravelHours) * 10, legendText: "Travel", indexLabel: "Travel Hours" },
            //                                //{ color: '#A9A9A9', y: parseFloat(result.sumOfServiceHours) * 10, legendText: "Service", indexLabel: "Service Hours" },
            //                                { color: '#5F9EA0', y: parseFloat(result.sumOfPrepHours), legendText: "Prep", indexLabel: "Prep Hours" },
            //                        		{ color: '#B8860B', y: parseFloat(result.sumOfTravelHours), legendText: "Travel", indexLabel: "Travel Hours" },
            //                                { color: '#A9A9A9', y: parseFloat(result.sumOfServiceHours), legendText: "Service", indexLabel: "Service Hours" },
            //                                { color: 'red', y: parseFloat(result.totalRemainingHours), legendText: "Reaming Hours", exploded: true, indexLabel: "Remaining Hours" },
            //                                //{ color: 'green', y: parseFloat(result.evidenceBasedHours * 10), legendText: "Evidencebased", indexLabel: "Evidencebased Hours" }



            //                            ]
            //                        }
            //		            ]
            //		        });

            //		        $scope.chart.render(); //render the chart for the first time

            //		        $scope.changeChartType = function (chartType) {
            //		            $scope.chart.options.data[0].type = chartType;
            //		            $scope.chart.render(); //re-render the chart to display the new layout
            //		        }
            //		        $scope.evidenceBasedHours = result.evidenceBasedHours * 10;

            //		    }
            //		})
            //		.error(function (error) {
            //		    console.log(error);
            //		});
            //}
            //else {
            //    if (userInfo.level == "SIT2A") {
            //        $scope.isOnlyAdmin = false;
            //    }
            //    $scope.isUserData = false;
            //    $scope.isAdminData = true;
            //    paging(1);
            //}





            $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "GetAllResourcesLinksDashBoard", parameters: '' } })
				   .success(function (result) {
				       if (result) {
				           vm.links = result;
				       }
				       else {
				           vm.links = [];
				       }
				   })
					.error(function (error) {
					    console.log(error);
					});

        }

        function getTreatmentDashBoardData1() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);


            $http({ method: 'POST', url: 'api/Common', data: { method: "UserTreatmentDashBoard", parameters: [userid, userInfo.locationId, userInfo.level, userInfo.stateabbrev] } })
					.success(function (result) {


					    if (result) {

					        if (result.totalAssignedBeds == 0 && result.totalBeds == 0) {

					            $scope.isdataempty = true;
					        }
					        else {

					            var dataPoints = '[';
					            for (var i = 0; i < result.length; i++) {
					                var comma = ',';
					                if (i === result.length - 1) {
					                    comma = '';
					                }
					                dataPoints += '{"y":' + parseFloat(result[i].totalAdultMaleAssignedBeds) + ',"label":"' + result[i].friendlyName_loc + '" }' + comma + '';
					                // dataPoints += '{"y":' + parseFloat(result[i].totalAssignedBeds) + ',"label":"" }' + comma + '';

					                //{ y: 135305, label: "USA" },
					            }

					            dataPoints += ']';

					            var dps = JSON.parse(dataPoints);

					            var dataPoints1 = '[';
					            for (var i = 0; i < result.length; i++) {
					                var comma = ',';
					                if (i === result.length - 1) {
					                    comma = '';
					                }

					                dataPoints1 += '{"y":' + parseFloat(result[i].totalAdultFemaleAssignedBeds) + ',"label":"' + result[i].friendlyName_loc + '","indexLabel":"' + result[i].totalAdultFemaleAssignedBeds + '" }' + comma + '';
					                //dataPoints1 += '{"y":' + parseFloat(result[i].bedCount) + ',"label":"' + result[i].friendlyName_loc + '","indexLabel":"' + result[i].bedCount + '" }' + comma + '';
					                //dataPoints1 += '{"y":' + parseFloat(result[i].bedCount) + ',"label":"" }' + comma + '';

					                //{ y: 135305, label: "USA" },
					            }

					            dataPoints1 += ']';

					            var dps1 = JSON.parse(dataPoints1);

					            //dataponts 3
					            var dataPoints2 = '[';
					            for (var i = 0; i < result.length; i++) {
					                var comma = ',';
					                if (i === result.length - 1) {
					                    comma = '';
					                }

					                dataPoints2 += '{"y":' + parseFloat(result[i].totalAdolescentMaleAssignedBeds) + ',"label":"' + result[i].friendlyName_loc + '","indexLabel":"' + result[i].totalAdolescentMaleAssignedBeds + '" }' + comma + '';

					            }

					            dataPoints2 += ']';

					            var dps2 = JSON.parse(dataPoints2);

					            //datapoints 4

					            var dataPoints3 = '[';
					            for (var i = 0; i < result.length; i++) {
					                var comma = ',';
					                if (i === result.length - 1) {
					                    comma = '';
					                }

					                dataPoints3 += '{"y":' + parseFloat(result[i].adolescentFemale) + ',"label":"' + result[i].friendlyName_loc + '","indexLabel":"' + result[i].adolescentFemale + '" }' + comma + '';

					            }

					            dataPoints3 += ']';

					            var dps3 = JSON.parse(dataPoints3);

					            //data points 5

					            var dataPoints4 = '[';
					            for (var i = 0; i < result.length; i++) {
					                var comma = ',';
					                if (i === result.length - 1) {
					                    comma = '';
					                }

					                dataPoints4 += '{"y":' + parseFloat(result[i].totalAssignedBeds) + ',"label":"' + result[i].friendlyName_loc + '","indexLabel":"' + result[i].totalAssignedBeds + '" }' + comma + '';
					                //dataPoints1 += '{"y":' + parseFloat(result[i].bedCount) + ',"label":"" }' + comma + '';

					                //{ y: 135305, label: "USA" },
					            }

					            dataPoints4 += ']';

					            var dps4 = JSON.parse(dataPoints4);


					            $scope.userTreatmentChart = new CanvasJS.Chart("chartTreatmentContainer", {
					                theme: 'theme1',
					                title: {
					                    text: ""
					                },

					                legend: {
					                    fontSize: 12,
					                    fontFamily: "Segoe UI",
					                    fontColor: "#767676",
					                },
					                axisY: {
					                    title: "",
					                    labelFontSize: 16,
					                    labelMaxWidth: 50,
					                    labelWrap: true,

					                },
					                axisX: {
					                    labelFontSize: 12,
					                    labelFontFamily: "Segoe UI",
					                    labelFontColor: "#767676",

					                    // prefix: "Loc ",
					                    //labelMaxWidth: 50,  
					                    //labelWrap: true,
					                    //labelAngle: 90,
					                    // labelAutoFit:true,
					                },
					                //data: [
					                //	{
					                //	    type: "pie",
					                //	    dataPoints: [
					                //			{ color: "#A2AB58", label: "Assigned Beds", y: parseFloat(result.totalAssignedBeds) },
					                //			{ color: "#E44424", label: "Total Beds", exploded: true, y: parseFloat(result.totalBeds) },


					                //	    ]
					                //	}
					                //]
					                animationEnabled: true,
					                data: [
                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'></span>: {y}",
                                           name: "Adult Male",//"Total Assigned Beds",
                                           showInLegend: "true",
                                           dataPoints: dps

                                       },
                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'></span>: {y}",
                                           name: "Adult Female",//"Total Assigned Beds",
                                           showInLegend: "true",
                                           dataPoints: dps1

                                       },
                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'></span>: {y}",
                                           name: "Adolescent Male",//"Total Assigned Beds",
                                           showInLegend: "true",
                                           dataPoints: dps2

                                       },
                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'></span>: {y}",
                                           name: "Adolescent Female",//"Total Assigned Beds",
                                           showInLegend: "true",
                                           dataPoints: dps3

                                       },
									   {
									       type: "stackedColumn100",
									       //toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
									       toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'></span>: {y}",
									       name: "Available",//"Total BedCount",
									       showInLegend: "true",
									       dataPoints: dps4
									   },

					                ]
					            });



					            $scope.userTreatmentChart.render(); //render the chart for the first time

					            $scope.userTreatmentChart = function (chartType) {
					                $scope.userTreatmentChart.options.data[0].type = chartType;
					                $scope.userTreatmentChart.render(); //re-render the chart to display the new layout
					            }

					        }

					    }
					})
					.error(function (error) {
					    console.log(error);
					});


        }

        vm.bpagination1 = function (pageId) {
            //var userInfo = JSON.parse($window.localStorage["userInfo"]);
            //if (userInfo.level == "SIT2A") {
            //    $scope.isOnlyAdmin = false;
            //}
            getTreatmentDashBoardData(pageId);
        }

        function getTreatmentDashBoardData(bPageId) {

            //debugger;

            var userInfo = JSON.parse($window.localStorage["userInfo"]);


            $http({
                method: 'POST', url: 'api/Common', data: {
                    method: "UserTreatmentDashBoard", parameters:
                        [userid, userInfo.locationId, userInfo.level,
                            userInfo.stateabbrev, bPageId,vm.bedSort]
                }
            })
					.success(function (result) {


					    if (result) {
					      
					        vm.pagerBedData = result.item1[0];
					        vm.bedDetails = result.item2;
					        if (_.isEmpty(result.item2)) {
					            $scope.userTreatmentChart = new CanvasJS.Chart("chartTreatmentContainer", {});
					            $scope.userTreatmentChart.options.data = [];
					            $scope.userTreatmentChart.render();
					            return;
					        }
					        vm.pageBedList = [];

					        $scope.isBedPaging = false;

					        if (parseInt(vm.pagerBedData.numberOfPages) > 1) {
					            $scope.isBedPaging = true;
					        }

					        if (bPageId > 3) {
					            vm.pageBedList.push(bPageId - 2);
					            vm.pageBedList.push(bPageId - 1);
					            vm.pageBedList.push(bPageId);
					            if (bPageId < vm.pagerBedData.numberOfPages) {
					                vm.pageBedList.push(bPageId + 1);
					                if (bPageId < (vm.pagerBedData.numberOfPages - 1)) {
					                    vm.pageBedList.push(bPageId + 2);
					                }
					            }
					        }
					        else {
					            if (bPageId == 1) {
					                vm.pageBedList.push(bPageId);
					                if (bPageId + 1 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 1);
					                }
					                if (bPageId + 2 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 2);
					                }
					                if (bPageId + 3 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 3);
					                }
					                if (bPageId + 4 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 4);
					                }

					            }
					            if (bPageId == 2) {
					                vm.pageBedList.push(bPageId - 1);
					                vm.pageBedList.push(bPageId);

					                if (bPageId + 1 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 1);
					                }
					                if (bPageId + 2 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 2);
					                }
					                if (bPageId + 3 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 3);
					                }
					            }
					            if (bPageId == 3) {
					                vm.pageBedList.push(bPageId - 2);
					                vm.pageBedList.push(bPageId - 1);
					                vm.pageBedList.push(bPageId);
					                if (bPageId + 1 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 1);
					                }
					                if (bPageId + 2 <= vm.pagerBedData.numberOfPages) {
					                    vm.pageBedList.push(bPageId + 2);
					                }
					            }

					        }


					        if (vm.bedDetails.length == 0) {// && vm.bedDetails.totalBeds == 0) {

					            $scope.isdataempty = true;
					        }
					        else {

					            var dataPoints = '[';
					            for (var i = 0; i < vm.bedDetails.length; i++) {
					                var comma = ',';
					                if (i === vm.bedDetails.length - 1) {
					                    comma = '';
					                }
					                //http://canvasjs.com/docs/charts/chart-options/data/datapoints/tooltipcontent/
					                //dataPoints += '{"y":' + parseFloat(vm.bedDetails[i].totalAdultMaleAssignedBeds) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';
					                dataPoints += '{"z":' + parseFloat(vm.bedDetails[i].adultMale) + ',"y":' + parseFloat(vm.bedDetails[i].totalAdultMaleAssignedBeds) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';

					                //dataPointsAdolescentFemale += '{"z":' + parseFloat(vm.bedDetails[i].totalAdolescentMaleAssignedBeds) + ',"y":' + parseFloat(vm.bedDetails[i].totalAdolescentFemaleAssignedBeds) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';
					                //dataPoints += '{"y":1,"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';

					            }

					            dataPoints += ']';

					            var dps = JSON.parse(dataPoints);
					            ////data point for female
					            var dataPointsFemale = '[';
					            for (var i = 0; i < vm.bedDetails.length; i++) {
					                var comma = ',';
					                if (i === vm.bedDetails.length - 1) {
					                    comma = '';
					                }
					                dataPointsFemale += '{"z":' + parseFloat(vm.bedDetails[i].adultFemale) + ',"y":' + parseFloat(vm.bedDetails[i].totalAdultFemaleAssignedBeds) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';
					                //dataPoints += '{"y":1,"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';

					            }

					            dataPointsFemale += ']';

					            var dpsFemale = JSON.parse(dataPointsFemale);


					            //data point for TotalAdolescentMaleAssignedBeds
					            var dataPointsAdolescentMale = '[';
					            for (var i = 0; i < vm.bedDetails.length; i++) {
					                var comma = ',';
					                if (i === vm.bedDetails.length - 1) {
					                    comma = '';
					                }
					                dataPointsAdolescentMale += '{"z":' + parseFloat(vm.bedDetails[i].adolescentMale) + ',"y":' + parseFloat(vm.bedDetails[i].totalAdolescentMaleAssignedBeds) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';
					                //dataPoints += '{"y":1,"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';

					            }

					            dataPointsAdolescentMale += ']';

					            var dpsAdolescentMale = JSON.parse(dataPointsAdolescentMale);

					            //data point for TotalAdolescentFemaleAssignedBeds
					            var dataPointsAdolescentFemale = '[';
					            for (var i = 0; i < vm.bedDetails.length; i++) {
					                var comma = ',';
					                if (i === vm.bedDetails.length - 1) {
					                    comma = '';
					                }
					                dataPointsAdolescentFemale += '{"z":' + parseFloat(vm.bedDetails[i].adolescentFemale) + ',"y":' + parseFloat(vm.bedDetails[i].totalAdolescentFemaleAssignedBeds) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';
					                //dataPoints += '{"y":1,"label":"' + vm.bedDetails[i].friendlyName_loc + '" }' + comma + '';

					            }

					            dataPointsAdolescentFemale += ']';

					            var dpsAdolescentFemale = JSON.parse(dataPointsAdolescentFemale);

					            var dataPoints1 = '[';
					            for (var i = 0; i < vm.bedDetails.length; i++) {
					                var comma = ',';
					                if (i === vm.bedDetails.length - 1) {
					                    comma = '';
					                }
					                dataPoints1 += '{"z":' + parseFloat(vm.bedDetails[i].bedCount) + ',"y":' + parseFloat(vm.bedDetails[i].availableBedCount) + ',"label":"' + vm.bedDetails[i].friendlyName_loc + '","indexLabel":"' + vm.bedDetails[i].availableBedCount + '"}' + comma + '';

					            }

					            dataPoints1 += ']';

					            var dps1 = JSON.parse(dataPoints1);


					            $scope.userTreatmentChart = new CanvasJS.Chart("chartTreatmentContainer", {
					                theme: 'theme1',
					                title: {
					                    text: ""
					                },

					                legend: {
					                    fontSize: 11,
					                    fontFamily: "Segoe UI",
					                    fontColor: "#767676",
					                    verticalAlign: "top",
					                },
					                axisY: {
					                    title: "",
					                    labelFontSize: 16,
					                    labelMaxWidth: 50,
					                    labelWrap: true,

					                },
					                axisX: {
					                    labelFontSize: 14,
					                    labelFontFamily: "Segoe UI",
					                    labelFontColor: "#767676",
					                    labelAngle: -60,
					                    labelWrap: true,
					                    // labelMaxWidth: 2,
					                    //labelAngle: 50,
					                    //labelWrap: true,
					                    ////labelAutoFit:true,
					                },

					                animationEnabled: true,
					                // dataPointMaxWidth: 500,
					                data: [


                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}" + "--Adult Male--" + "<br/><span style='\"'color: {color};'\"'></span>: {y} of {z}",
                                           name: "Adult Male",//"Total Assigned Beds",
                                           showInLegend: "true",

                                           dataPoints: dps

                                       },

                                       {
                                           type: "stackedColumn100",
                                           toolTipContent: "{label}" + "--Adult Female--" + "<br/><span style='\"'color: {color};'\"'></span>: {y}  of {z}",
                                           name: "Adult Female",//"Total Assigned Beds",
                                           showInLegend: "true",
                                           dataPoints: dpsFemale

                                       },
                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}" + "--Adolescent Male--" + "<br/><span style='\"'color: {color};'\"'></span>: {y} of {z}",
                                           name: "Adolescent Male",//"Total Assigned Beds",
                                           showInLegend: "true",


                                           dataPoints: dpsAdolescentMale

                                       },
                                       {
                                           type: "stackedColumn100",
                                           // toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}" + "--Adolescent Female--" + "<br/><span style='\"'color: {color};'\"'></span>: {y} of {z}",
                                           name: "Adolescent Female",//"Total Assigned Beds",
                                           showInLegend: "true",



                                           dataPoints: dpsAdolescentFemale

                                       },

                                       {
                                           type: "stackedColumn100", color: "#F3F3F3",
                                           //toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",
                                           toolTipContent: "{label}" + "--Available--" + "<br/><span style='\"'color: {color};'\"'></span>: {y} of {z}",
                                           name: "Available",//"Total BedCount",
                                           indexLabelPlacement: "outside",
                                           indexLabelFontColor: "black",
                                           indexLabelFontWeight: 100,
                                           indexLabelFontFamily: "Verdana",
                                           showInLegend: "true",
                                           dataPoints: dps1

                                       },




					                ]
					            });

					           

					            $scope.userTreatmentChart.render(); //render the chart for the first time

					            $scope.userTreatmentChart = function (chartType) {
					                $scope.userTreatmentChart.options.data[0].type = chartType;
					                $scope.userTreatmentChart.render(); //re-render the chart to display the new layout
					            }

					        }

					    }
					})
					.error(function (error) {
					    console.log(error);
					});


        }

        vm.pagination1 = function (pageId, followupId) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            var strSort = document.getElementById('hdnSortType').value;

            if (userInfo.level == "SIT2A") {
                $scope.isOnlyAdmin = false;
            }
            getContactsDashBoard(pageId, followupId, strSort);
        }

        vm.contactsById = function (typeFollowup) {
            //vm.Site = Site;
            //var userInfo = JSON.parse($window.localStorage["userInfo"]);
            //if (userInfo.level == "SIT2A") {
            //    $scope.isOnlyAdmin = false;
            //}
            //paging(1);

            var strSort = document.getElementById('hdnSortType').value;

            getContactsDashBoard(1, typeFollowup, strSort);
        }

        vm.ContactSorting = function (sortType) {



            if (sortType == "Desc") {
                document.getElementById('hdnSortType').value = "Desc";
                $scope.isdescending = false;
                $scope.isAescending = true;
            }
            else if (sortType == "Asc") {
                document.getElementById('hdnSortType').value = "Asc";
                $scope.isdescending = true;
                $scope.isAescending = false;
            }
            else {
                document.getElementById('hdnSortType').value = "Nothing";
                $scope.isdescending = true;
                $scope.isAescending = false;
            }


            getContactsDashBoard(1, vm.typeFollowup, sortType);
        }

        function getContactsDashBoard(cpageId, FollowupID, sortType) {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (userInfo.level == "SIT3U") {
                $scope.isUsercontactData = false;
            }


            $http({ method: 'POST', url: 'api/Common', data: { method: "ContactsDashBoard", parameters: [userid, userInfo.locationId, userInfo.level, userInfo.stateabbrev, cpageId, FollowupID, sortType] } })
					.success(function (result) {

					    if (result) {
					        //vm.contactDetails = result;

					        vm.pagerContactData = result.item1[0];

					        vm.pageContactList = [];

					        $scope.isContactPaging = false;

					        if (parseInt(vm.pagerContactData.numberOfPages) > 1) {
					            $scope.isContactPaging = true;
					        }




					        if (cpageId > 3) {
					            vm.pageContactList.push(cpageId - 2);
					            vm.pageContactList.push(cpageId - 1);
					            vm.pageContactList.push(cpageId);
					            if (cpageId < vm.pagerContactData.numberOfPages) {
					                vm.pageContactList.push(cpageId + 1);
					                if (cpageId < (vm.pagerContactData.numberOfPages - 1)) {
					                    vm.pageContactList.push(cpageId + 2);
					                }
					            }
					        }
					        else {
					            if (cpageId == 1) {
					                vm.pageContactList.push(cpageId);
					                if (cpageId + 1 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 1);
					                }
					                if (cpageId + 2 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 2);
					                }
					                if (cpageId + 3 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 3);
					                }
					                if (cpageId + 4 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 4);
					                }

					            }
					            if (cpageId == 2) {
					                vm.pageContactList.push(cpageId - 1);
					                vm.pageContactList.push(cpageId);

					                if (cpageId + 1 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 1);
					                }
					                if (cpageId + 2 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 2);
					                }
					                if (cpageId + 3 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 3);
					                }
					            }
					            if (cpageId == 3) {
					                vm.pageContactList.push(cpageId - 2);
					                vm.pageContactList.push(cpageId - 1);
					                vm.pageContactList.push(cpageId);
					                if (cpageId + 1 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 1);
					                }
					                if (cpageId + 2 <= vm.pagerContactData.numberOfPages) {
					                    vm.pageContactList.push(cpageId + 2);
					                }
					            }

					        }
					        //var userData = result.item2;
					        vm.contactDetails = result.item2;
					    }
					})
					.error(function (error) {
					    console.log(error);
					});


        }

        vm.showFollowModel = function (followupItem) {

            //alert(PatId);
            var modalInstance = $modal.open({
                templateUrl: '/app/dashboard/followupModel.html',
                controller: 'followupModel',
                backdrop: 'static',
                resolve: {
                    eventInfo: function () {
                        return followupItem;
                    }
                }
            });
        };


        vm.viewReport = function () {

            var modalInstance = $modal.open({
                templateUrl: '/app/dashboard/displayPdf.html',
                controller: 'displayPdf',
                backdrop: 'static',

            });
        };

        vm.openFollowup = function () {

            var followupModal = $modal.open({
                templateUrl: 'app/treatment/followupModal.html',
                controller: 'followupModal',
                backdrop: 'static',
                resolve: {
                    userInfo: function () {
                        return undefined;
                    }
                }

            });
            followupModal.result.then(function (user) {





            }, function () {
            });
        }

        vm.getWidth = function (value) {
            return {
                width: value + "%"
            }
        }

        vm.getLeft = function (value) {
            return {
                left: value + "%"
            }
        }

    }
})();