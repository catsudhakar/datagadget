﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class SurveyItemVM
    {
        public int RecNo { get; set; }
        public string SessionID { get; set; }
        public int SurveyID { get; set; }
        public int SurveyClientID { get; set; }
        public Nullable<int> ServiceID { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<System.DateTime> DischargeDate { get; set; }
        public Nullable<int> ProgramID { get; set; }
        public int SurveyTypeID { get; set; }
        public byte[] Timestamp { get; set; }
        public System.Guid SurveyAdminID { get; set; }
        public int ? LocationID { get; set; }
        public Nullable<System.DateTime> AdminDate { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string Notes { get; set; }
        public Nullable<int> EventID { get; set; }
        public string GroupName { get; set; }
        public string ClientGenderType { get; set; }
        public Nullable<int> DischargeId { get; set; }
        public string LocationName { get; set; }
        public string SurveyAdminName { get; set; }
        public string ClientName { get; set; }
        public string ProgramName { get; set; }

        public int ClientID { get; set; }
        public int? ClientStatusID { get; set; }
    }
}