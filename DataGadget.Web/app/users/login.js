﻿(function () {
    'use strict';
    var controllerId = 'login';
    angular
        .module('app')
        .controller(controllerId, login);

    login.$inject = ['$scope', '$location', '$http', 'common', '$window'];

    function login($scope, $location, $http, common, $window) {
        if (localStorage.getItem("isUserAuthentic") == "true") {
            $location.path();
        }
        $scope.title = 'login';
        $scope.errormsg = '';
        $scope.errormsg1 = 'TREATMENT Users are not to enter data at this time';
        $scope.userName = '';
        $scope.password = '';
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        var userInfo;//@rafeeq    
        $scope.loginmessage = '';
        activate();

        $scope.forGotPassword = function () {
            $location.path('/forgotpassword');
        }

        function activate() {
            

            $http({ method: 'POST', url: 'api/User', data: { method: "GetActiveUserMessage" } })
                .success(function (result) {
                 
                    if (result != null) {
                        
                        $scope.loginmessage = result.message;
                       
                    }
                })
                .error(function (error) {
                    console.log(error);
                })

        }

        localStorage.setItem("isUserAuthentic", false);
        $scope.validateUser = function () {
            $scope.errormsg = '';
            if ($scope.loginForm.$valid) {
                var data = { method: 'ValidateUser', parameters: [$scope.userName, $scope.password] };
                $http.post('api/user', data).success(function (result) {
                    if (result != null) {
                        var isAccess = true;
                        localStorage.setItem('userInfo', JSON.stringify(result));
                        localStorage.setItem("userName", $scope.userName);
                        localStorage.setItem("userId", result.userId);

                        localStorage.setItem("locationId", result.locationId);

                        //localStorage.setItem("password", $scope.password);
                        localStorage.setItem("isUserAuthentic", true);
                        //authorizeUser();
                        //if (localStorage.getItem("currentPath") == '/')
                        try {

                            userInfo = result;//@rafeeq
                            $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);//@rafeeq
                            if (userInfo.selectedLocationCategoryList) {
                                if (userInfo.selectedLocationCategoryList.length == 2) {
                                    isAccess = true;
                                }
                                else {
                                    //angular.forEach(userInfo.selectedLocationCategoryList, function (todo) {
                                    //    if (todo.categoryID == 2) {
                                    //        isAccess = false;
                                    //    }
                                    //});

                                    for (var j = 0; j < userInfo.selectedLocationCategoryList.length; j++) {
                                        if(userInfo.selectedLocationCategoryList[j].categoryID==2)
                                        {
                                            isAccess = true;
                                        }
                                    }
                                }

                                
                            }

                           


                        } catch (e) {
                            console.log(e.message);
                            logError(e.message);
                        }//@rafeeq
                        if (isAccess == false) {
                            localStorage.clear();
                            sessionStorage.clear();
                           
                            $location.path('/');
                            $scope.errormsg1 = 'TREATMENT Users are not to enter data at this time'
                        }
                        else {
                            $location.path('/dashboard');
                        }
                        //else
                        //$location.path(localStorage.getItem("currentPath"));
                        //  $location.path('/');
                    } else {
                        $scope.errormsg = 'Username or Password incorrect'
                        $scope.userName = '';
                        $scope.password = '';
                    }
                }).error(function (error) {
                    console.log(error);
                    if (error) {
                        logError(error.message + error.exceptionMessage);
                    }
                    else {
                        logError("Failed to login. Invalid username or password");
                    }
                });
            }
            else {
                $scope.errormsg = 'Please enter username and password'
            }
        }

        try {

            if ($window.sessionStorage["userInfo"] != "null") {
                var absUrl = $location.absUrl();
                if ($window.sessionStorage["userInfo"]) {
                    userInfo = JSON.parse($window.sessionStorage["userInfo"]);

                    localStorage.setItem('userInfo', JSON.stringify(userInfo));
                    localStorage.setItem("userName", userInfo.userName);
                    localStorage.setItem("userId", userInfo.userId);
                    localStorage.setItem("isUserAuthentic", true);
                    if (absUrl.split('#')[1] == "/") {
                        $location.path('/dashboard');
                    }
                    else {
                        $location.path(absUrl.split('#')[1]);
                    }
                }

            }
        } catch (e) {
            console.log(e.message);
            logError(e.message);
        }//rafeeq
    }
})();
