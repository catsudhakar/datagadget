﻿(function () {
    'use strict';

    var app = angular.module('app', [
        // Angular modules 
        'ngAnimate',        // animations
        'ngRoute',          // routing
        'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
         'angular-loading-bar',
        // Custom modules 
        'common',           // common functions, logger, spinner
        'common.bootstrap', // bootstrap dialog wrapper functions

        // 3rd Party Modules
        'ui.bootstrap',
         'textAngular',
       
        //'ngMaterial',
        //'ngAria',// ui-bootstrap (ex: carousel, pagination, dialog)
    ]);

    app.controller('confirmationDialogCtrl', function ($scope, $modalInstance) {


        $scope.ok = function () {


            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });

    app.controller('confirmationSurveyDialogCtrl', function ($scope, $modalInstance) {


        $scope.ok = function () {
            //Needs update feild like issurvey required 
            //alert("1");
            $modalInstance.close();
        };

        $scope.cancel = function () {
            //alert("0");
            $modalInstance.dismiss('cancel');
        };
    });

    // Handle routing errors and success events
    app.run(['$route', function ($route) {
        // Include $route to kick start the router.
    }]);


    app.factory('mySharedService', function ($rootScope) {
        

        var statewides = [];

        var sharedService = {};

        sharedService.id = 0;
        sharedService.initiativeName = "";

        sharedService.prepForBroadcast = function (id, name) {
            this.id = id;
            this.initiativeName = name;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function () {
            $rootScope.$broadcast('handleBroadcast');
        };

        return sharedService;
    });
    app.factory('psService', function ($rootScope) {
        

        var pssharedService = {};
        pssharedService.programs = [];


        pssharedService.prepForBroadcast = function () {

            this.broadcastItem();
        };

        pssharedService.broadcastItem = function () {
            $rootScope.$broadcast('handleBroadcast');
        };

        return pssharedService;
    });

    app.factory('mySharedLinks', function ($rootScope) {
        



        var sharedresourceLinks = {};

        sharedresourceLinks.resourceId = 0;
        sharedresourceLinks.resourceName = "";
        sharedresourceLinks.resourceLink1 = "";
        sharedresourceLinks.createdBy = "";
        sharedresourceLinks.createdDate = "";


        sharedresourceLinks.prepForBroadcast = function (id, name, link, by, createddate) {

            this.resourceId = id;
            this.resourceName = name;
            this.resourceLink1 = link;
            this.createdBy = by;
            this.createdDate = createddate;
            this.broadcastItem();
        };

        sharedresourceLinks.broadcastItem = function () {
            $rootScope.$broadcast('handleBroadcast');
        };

        return sharedresourceLinks;
    });

})();