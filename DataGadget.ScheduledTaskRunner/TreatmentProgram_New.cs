//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    using System.Collections.Generic;
    
    public partial class TreatmentProgram_New
    {
        public int RecNo { get; set; }
        public int LocationID { get; set; }
        public int ProgramTypeID { get; set; }
        public string ProgramName { get; set; }
        public int MaleBeds { get; set; }
        public int FemaleBeds { get; set; }
        public Nullable<int> TotalBeds { get; set; }
        public bool IsActive { get; set; }
    }
}
