﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGadget.Web.Models
{
    public class TreatmentSurveyIntakeValueVM
    {
        public int SurveyItemId { get; set; }
        public int IntakeValueId { get; set; }
    }
}
