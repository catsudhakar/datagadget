﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Data;
using System.Configuration;


using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_Base
    {

        protected Color cellHeaderColor = Color.LIGHT_GRAY;
        protected Color cellItemColor = Color.WHITE;
        protected Color cellAlternatingItemColor = new Color(0xe0, 0xd7, 0xc7);
        protected readonly int stdFontSize = 12;

        protected Document _pdfDoc = new Document();



        #region "header/footer"

        protected Paragraph HeaderImageElement()
        {
            Paragraph pdfParagraph = new Paragraph();
            //Chunk chunk;

            //iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(Path.Combine(ImagesPath, "ancomm_logo_green.jpg"));
            //pdfImage.ScalePercent(75);
            //chunk = new Chunk(pdfImage, 0, 0);
            //pdfParagraph.Add(chunk);


            return pdfParagraph;
        }

        protected HeaderFooter GetDocumentHeader(string reportTitle)
        {
            HeaderFooter header = new HeaderFooter(new Phrase(reportTitle), false);
            header.Border = Rectangle.NO_BORDER;
            return header;
        }

        protected HeaderFooter GetDocumentFooter()
        {
            HeaderFooter footer = new HeaderFooter(new Phrase("Page "), true);
            footer.Border = Rectangle.NO_BORDER;
            return footer;
        }

        #endregion

        #region "paragraph"

        protected Paragraph NewParagraph(string text)
        {
            return NewParagraph(text, Font.HELVETICA, stdFontSize);
        }

        protected Paragraph NewParagraph(string text, int fontFamily)
        {
            return NewParagraph(text, fontFamily, stdFontSize);
        }

        protected Paragraph NewParagraph(string text, int fontFamily, int fontSize)
        {
            return NewParagraph(text, fontFamily, fontSize, Font.NORMAL);
        }

        protected Paragraph NewParagraph(string text, int fontFamily, float fontSize, int fontStyle)
        {
            Paragraph pdfParagraph = new Paragraph(text, new Font(fontFamily, fontSize, fontStyle));
            return pdfParagraph;
        }

        #endregion

        #region "phrase"

        protected Phrase NewPhrase(string text)
        {
            return NewPhrase(text, Font.HELVETICA, stdFontSize);
        }

        protected Phrase NewPhrase(string text, int fontFamily)
        {
            return NewPhrase(text, fontFamily, stdFontSize);
        }

        protected Phrase NewPhrase(string text, int fontFamily, float fontSize)
        {
            return NewPhrase(text, fontFamily, fontSize, Font.NORMAL);
        }

        protected Phrase NewPhrase(string text, int fontFamily, float fontSize, int fontStyle)
        {
            Phrase pdfPhrase = new Phrase(text, new Font(fontFamily, fontSize, fontStyle));
            return pdfPhrase;
        }

        #endregion

        #region "cell"

        protected Color CellBackgroundColor(int cCnt)
        {
            if ((cCnt % 2) == 0)
                return cellItemColor;
            else
                return cellAlternatingItemColor;
        }

        #endregion

        protected int GetPerc(int a, int b)
        {
            try
            {
                return (int)(((decimal)a / (decimal)b) * (decimal)100);
            }
            catch 
            {
                return 0;
            }
        }

        protected int GetDataRowInt(DataRow dataRow, string fieldName)
        {
            try
            {
                return int.Parse(dataRow[fieldName].ToString());
            }
            catch
            {
                return 0;
            }
        }

        protected float GetDataRowFloat(DataRow dataRow, string fieldName)
        {
            try
            {
                return float.Parse(dataRow[fieldName].ToString());
            }
            catch
            {
                return 0.0F;
            }
        }

    }
}
