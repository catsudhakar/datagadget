//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    
    public partial class StoredProcedureDADA1_Result
    {
        public int PKey { get; set; }
        public string StateAbbrev { get; set; }
        public string StateRegionCode { get; set; }
        public string SiteCode { get; set; }
        public string ProgramCode { get; set; }
        public string FiscalYear { get; set; }
        public string FundSrcCode { get; set; }
        public string OrgName { get; set; }
        public string StaffFName { get; set; }
        public string StaffLName { get; set; }
        public string Degree { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime MyDateTimeRef { get; set; }
        public int Doc_Agenda { get; set; }
        public int FKey { get; set; }
        public int Expr1 { get; set; }
    }
}
