﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;

namespace DataGadget.Web.Controllers
{
    public class MailTemplateController : ApiController
    {
       

        public HttpResponseMessage Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                    
                    case "CreateEmailTemplate":
                        return CreateEmailTemplate(value.Parameters);
                    case "GetTemplate":
                        return GetTemplate();

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //WebLogger.Error("Exception In MailTemplateController : " + ex.Message);  
                //return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }

        private HttpResponseMessage GetTemplate()
        {
            try
            {
               // Models.MailTemplateViewModel email = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.MailTemplateViewModel>(p[0]);

                Models.MailTemplateViewModel email=new MailTemplateViewModel();

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                     var record = context.tblEmailTemplates.SingleOrDefault();
                    if(record !=null)
                    {
                        email.ID = record.ID;
                        email.Template = record.Template;
                    }


                    //return this.Request.CreateResponse(HttpStatusCode.OK, email);
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, email);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private HttpResponseMessage CreateEmailTemplate(string[] p)
        {
            try
            {
              
                var id = Convert.ToInt32(p[1]);
                var tempNmae = p[0];
                var record = new DataModel.tblEmailTemplate();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (id == 0)
                    {
                        record.Template = tempNmae;
                        context.tblEmailTemplates.Add(record);
                        context.SaveChanges();
                    }
                    else
                    {
                        record = context.tblEmailTemplates.Where(x => x.ID == id).SingleOrDefault();
                        record.Template = tempNmae;
                        context.SaveChanges();
                    }
                    return this.Request.CreateResponse(HttpStatusCode.OK, id);
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}