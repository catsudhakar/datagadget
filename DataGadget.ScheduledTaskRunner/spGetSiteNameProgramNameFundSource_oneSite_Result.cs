//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    
    public partial class spGetSiteNameProgramNameFundSource_oneSite_Result
    {
        public string StateAbbrev { get; set; }
        public string StateRegionCode { get; set; }
        public string SiteCode { get; set; }
        public string ProgramCode { get; set; }
        public string FundSrcCode { get; set; }
        public string FundSrcName { get; set; }
        public string FriendlyName_loc { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string FriendlyName_pgm { get; set; }
        public string FriendlyName_grant { get; set; }
    }
}
