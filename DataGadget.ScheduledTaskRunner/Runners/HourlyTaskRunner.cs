﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
namespace DataGadget.ScheduledTaskRunner.Runners
{
    [ScheduledTaskTag(Tag= "hourly")]
    public class HourlyTaskRunner : ITaskRunner
    {
        public async Task Run(params string[] args)
        {
            Logger.Info("Running out hourly tasks here");


            await Task.FromResult<object>(null);
        }
    }
}
