﻿(function () {
    'use strict';
    var controllerId = 'forgotpassword';
    angular
        .module('app')
        .controller(controllerId, forgotpassword);

    forgotpassword.$inject = ['$scope', '$location', '$http', 'common', '$window'];

    function forgotpassword($scope, $location, $http, common, $window) {
        $scope.title = 'Forgot Password';
        $scope.errormsg = '';
        $scope.email = '';
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        var userInfo;//@rafeeq        
        activate();

        function activate() { }

        localStorage.setItem("isUserAuthentic", false);
        $scope.validateUser = function () {
            $scope.errormsg = '';
            $scope.isSubmit = true;
            if ($scope.loginForm.$valid) {
                var data = { method: 'ForgotPassword', parameters: [$scope.email] };
                $http.post('api/user', data).success(function (result) {
                    if (result != null) {
                        if(result=="0")
                            $scope.errormsg = 'Email is sent successfully.';
                        else
                            $scope.errormsg = 'Email is sent unsuccessfully.';
                       
                    } else {
                        $scope.errormsg = "Please enter a valid Email Address";                        
                    }
                }).error(function (error) {
                    console.log(error);
                });
            }
            else {
                //$scope.errormsg = "Please enter a valid Email Address";
            }
        }
                
    }
})();
