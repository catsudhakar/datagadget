﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('treatmentConfig', treatmentConfig);

    treatmentConfig.$inject = ['$scope'];

    treatmentConfig.$inject = ['$scope', '$http', 'common', '$location','$route'];

    function treatmentConfig($scope, $http, common, $location, $route) {
        $scope.title = 'treatmentConfig';

        var vm = this;
        vm.links = [];
        $scope.statewide = {};

        activate();

        function activate() {
            $http({ method: 'POST', url: 'api/User', data: { method: "GetMessage" } })
                .success(function (result) {
                    vm.links = result;
                    if (result != null) {
                        $scope.statewide.iD = result.id;
                        $scope.statewide.message = result.message;
                        $scope.statewide.isActive = result.isActive;
                    }
                })
                .error(function (error) {
                    console.log(error);
                })
        }

        $scope.ok = function (userForm) {
            $scope.isSubmit = true;

            if (userForm.$valid) {
                $http({
                    method: 'POST', url: 'api/user',
                    data: {
                        method: "CreateLoginMessages",
                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {

                        $location.path('/Dashboard');
                        $route.reload();

                    })
                    .error(function (error) {
                        console.log(error);
                    })

                $location.path('/Dashboard');
                $route.reload();
            }

        };


    }
})();
