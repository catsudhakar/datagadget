﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('agency', agency);

    agency.$inject = ['$scope']; 

    function agency($scope) {
        $scope.title = 'agency';

        activate();

        function activate() { }
    }
})();
