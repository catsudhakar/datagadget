//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    using System.Collections.Generic;
    
    public partial class Client
    {
        public Client()
        {
            this.SurveyClients = new HashSet<SurveyClient>();
        }
    
        public int RecNo { get; set; }
        public string PAT_ID { get; set; }
    
        public virtual ICollection<SurveyClient> SurveyClients { get; set; }
    }
}
