﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataGadget.Web.Models;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Mvc;
using System.Data.Entity;
using DataGadget.Web.Reports;
using System.Threading.Tasks;


namespace DataGadget.Web.Controllers
{
    public class OngoingEventsController : ApiController
    {

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public HttpResponseMessage Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "AllEvents":
                        return GetAllEvents(param.Parameters);
                    case "GetEventByEventId":
                        return GetEventByEventId(param.Parameters);

                    case "GetLatestEvent":
                        return GetLatestEvent(param.Parameters);
                    case "SaveEventDetails":
                        return SaveEventDetails(param.Parameters);
                    case "GetBaseStringDataForReport":
                        return GetBaseStringDataForReport(param.Parameters);
                    case "GetSessions":
                        return GetSessions(param.Parameters);
                    case "Search":
                        return Search(param.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //WebLogger.Error("Exception In OngoingEventsController : " + ex.Message); 
                // return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }


        private HttpResponseMessage Search(string[] p)
        {
            try
            {
                User userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                int eventType = Convert.ToInt32(p[1]);
                string searchKey = Convert.ToString(p[2]);

                if (!userInfo.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }

                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    events = context.suretool_GetCurriculums(eventType, userInfo.UserId).Where(x => x.ProgramName.Contains(searchKey) || x.GroupName.Contains(searchKey) || x.OrgName.Contains(searchKey)).Select(oneevent => new OneTimeEvent
                    {
                        Program = oneevent.ProgramName,
                        Organization = oneevent.OrgName,
                        BeginDate = oneevent.StartDate,
                        EndDate = oneevent.EndDate,
                        Group = oneevent.GroupName,

                        lstEvents = GetChildEvents(oneevent.ID),

                        Age0 = oneevent.Age0,
                        Age12 = oneevent.Age12,
                        Age15 = oneevent.Age15,
                        Age18 = oneevent.Age18,
                        Age21 = oneevent.Age21,
                        Age25 = oneevent.Age25,
                        Age45 = oneevent.Age45,
                        Age5 = oneevent.Age5,
                        Age65 = oneevent.Age65,
                        AvgCostPerParticipant = oneevent.AvgCostPerParticipant,
                        closed = oneevent.closed,
                        CoalitionMeeting = oneevent.CoalitionMeeting,
                        CoalitionType = oneevent.CoalitionType,
                        Curriculum = oneevent.Curriculum,
                        D_HiRi_AbuVict = oneevent.D_HiRi_AbuVict,
                        D_HiRi_ChildOfSA = oneevent.D_HiRi_ChildOfSA,
                        D_HiRi_EconDis = oneevent.D_HiRi_EconDis,
                        D_HiRi_FrLun = oneevent.D_HiRi_FrLun,
                        D_HiRi_Homel = oneevent.D_HiRi_Homel,
                        D_HiRi_K12DO = oneevent.D_HiRi_K12DO,
                        D_HiRi_MentH = oneevent.D_HiRi_MentH,
                        D_HiRi_NotAp = oneevent.D_HiRi_NotAp,
                        D_HiRi_Other = oneevent.D_HiRi_Other,
                        D_HiRi_OtherText = oneevent.D_HiRi_OtherText,
                        D_HiRi_PhysDis = oneevent.D_HiRi_PhysDis,
                        D_HiRi_PregUse = oneevent.D_HiRi_PregUse,
                        D_HiRi_Using = oneevent.D_HiRi_Using,
                        D_HiRi_Violent = oneevent.D_HiRi_Violent,
                        Degree = oneevent.Degree,
                        DirectServiceHours = oneevent.DirectServiceHours,
                        DocOfActivities_Agenda = oneevent.DocOfActivities_Agenda,
                        DocOfActivities_Brochure = oneevent.DocOfActivities_Brochure,
                        DocOfActivities_ContactForm = oneevent.DocOfActivities_ContactForm,
                        DocOfActivities_EvalForm = oneevent.DocOfActivities_EvalForm,
                        DocOfActivities_Other = oneevent.DocOfActivities_Other,
                        DocOfActivities_OtherText = oneevent.DocOfActivities_OtherText,
                        DocOfActivities_PrePostTest = oneevent.DocOfActivities_PrePostTest,
                        DocOfActivities_SignIn = oneevent.DocOfActivities_SignIn,
                        EntryDate = oneevent.EntryDate,
                        Females = oneevent.Females,
                        FiscalYear = oneevent.FiscalYear,
                        GroupName = oneevent.GroupName,
                        Hispanic = oneevent.Hispanic,
                        ID = oneevent.ID,
                        ID02_DirectoriesDistributed = oneevent.ID02_DirectoriesDistributed,
                        ID04_BrochuresDistributed = oneevent.ID04_BrochuresDistributed,
                        InterventionType = oneevent.InterventionType,
                        Males = oneevent.Males,
                        Name = oneevent.Name,
                        NotHispanic = oneevent.NotHispanic,
                        NumberOfEmployees = oneevent.NumberOfEmployees,
                        NumberParticipants = oneevent.NumberParticipants,
                        NumberProgHrsRecd = oneevent.NumberProgHrsRecd,
                        OrgName = oneevent.OrgName,
                        OverallComments = oneevent.OverallComments,
                        ParentEntry = oneevent.ParentEntry,
                        PrepHours = oneevent.PrepHours,
                        ProgramName = oneevent.ProgramName,
                        ProgramNameSource = oneevent.ProgramNameSource,
                        ProgramType_IP = oneevent.ProgramType_IP,
                        RaceAmIndian = oneevent.RaceAmIndian,
                        RaceAsian = oneevent.RaceAsian,
                        RaceBlack = oneevent.RaceBlack,
                        RaceHawaii = oneevent.RaceHawaii,
                        RaceMoreThanOne = oneevent.RaceMoreThanOne,
                        RaceUnknownOther = oneevent.RaceUnknownOther,
                        RaceWhite = oneevent.RaceWhite,
                        RegionCode = oneevent.RegionCode,
                        SessionName = oneevent.SessionName,
                        SiteCode = oneevent.SiteCode,
                        StaffFname = oneevent.StaffFname,
                        StaffLName = oneevent.StaffLName,
                        StartDate = oneevent.StartDate,
                        Strat_Alt_21 = oneevent.Strat_Alt_21,
                        Strat_Alt_22 = oneevent.Strat_Alt_22,
                        Strat_Alt_23 = oneevent.Strat_Alt_23,
                        Strat_Alt_24 = oneevent.Strat_Alt_24,
                        Strat_Alt_25 = oneevent.Strat_Alt_25,
                        Strat_Alt_26 = oneevent.Strat_Alt_26,
                        Strat_Alt_27 = oneevent.Strat_Alt_27,
                        Strat_Alt_27Text = oneevent.Strat_Alt_27Text,
                        Strat_CBP_41 = oneevent.Strat_CBP_41,
                        Strat_CBP_42 = oneevent.Strat_CBP_42,
                        Strat_CBP_43 = oneevent.Strat_CBP_43,
                        Strat_CBP_44 = oneevent.Strat_CBP_44,
                        Strat_CBP_45 = oneevent.Strat_CBP_45,

                        Strat_CBP_Faith = oneevent.Strat_CBP_Faith,

                        Strat_CBP_46 = oneevent.Strat_CBP_46,
                        Strat_CBP_46Text = oneevent.Strat_CBP_46Text,
                        Strat_Ed_11 = oneevent.Strat_Ed_11,
                        Strat_Ed_12 = oneevent.Strat_Ed_12,
                        Strat_Ed_13 = oneevent.Strat_Ed_13,
                        Strat_Ed_14 = oneevent.Strat_Ed_14,
                        Strat_Ed_15 = oneevent.Strat_Ed_15,
                        Strat_Ed_16 = oneevent.Strat_Ed_16,
                        Strat_Ed_17 = oneevent.Strat_Ed_17,
                        Strat_Ed_17Text = oneevent.Strat_Ed_17Text,
                        Strat_Env_51 = oneevent.Strat_Env_51,
                        Strat_Env_52 = oneevent.Strat_Env_52,
                        Strat_Env_53 = oneevent.Strat_Env_53,
                        Strat_Env_54 = oneevent.Strat_Env_54,
                        Strat_Env_55 = oneevent.Strat_Env_55,
                        Strat_Env_56_MW = oneevent.Strat_Env_56_MW,
                        Strat_Env_57 = oneevent.Strat_Env_57,
                        Strat_Env_57Text = oneevent.Strat_Env_57Text,
                        Strat_ID_01 = oneevent.Strat_ID_01,
                        Strat_ID_02 = oneevent.Strat_ID_02,
                        Strat_ID_03 = oneevent.Strat_ID_03,
                        Strat_ID_04 = oneevent.Strat_ID_04,
                        Strat_ID_05 = oneevent.Strat_ID_05,
                        Strat_ID_06 = oneevent.Strat_ID_06,
                        Strat_ID_07 = oneevent.Strat_ID_07,
                        Strat_ID_08 = oneevent.Strat_ID_08,
                        Strat_ID_09 = oneevent.Strat_ID_09,
                        Strat_ID_09Text = oneevent.Strat_ID_09Text,
                        Strat_IR_31 = oneevent.Strat_IR_31,
                        Strat_IR_32 = oneevent.Strat_IR_32,
                        Strat_IR_34 = oneevent.Strat_IR_34,
                        Strat_IR_34Text = oneevent.Strat_IR_34Text,
                        SurveysEntered = oneevent.SurveysEntered,
                        TargetedStatewideInitiative = oneevent.TargetedStatewideInitiative,
                        TotalCostofProgram = oneevent.TotalCostofProgram,
                        TravelHours = oneevent.TravelHours,
                        TyoeOfProgram_EA = oneevent.TyoeOfProgram_EA,
                        UniversalType_DI = oneevent.UniversalType_DI,
                        UserGUID = oneevent.UserGUID,
                        WithinCostBand_YN = oneevent.WithinCostBand_YN


                    }).ToList();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, events);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetBaseStringDataForReport(string[] p)
        {
            try
            {
                Models.OneTimeEvent oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OneTimeEvent>(p[0]);

                if (oneTime.lstEvents != null)
                {
                    oneTime.lstEvents = GetChildEventsForView(oneTime.ID);
                }

                if (oneTime.BeginDate == null)
                    oneTime.BeginDate = oneTime.StartDate;

                string base64String = string.Empty; ;
                //int eventId = Convert.ToInt32(p[0]);
                System.IO.MemoryStream memStream = new System.IO.MemoryStream();
                Report_CurriculumSummary report = new Report_CurriculumSummary();

                report.GenerateToMemoryStream(memStream, oneTime);
                Byte[] byteArray = memStream.ToArray();
                // base64String = "data:application/pdf;base64;";
                base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);

                memStream.Close();

                //Response.Clear();
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.AddHeader("Content-Disposition", "inline; filename=SystemActivityReport.pdf");
                //Response.AddHeader("Content-Length", byteArray.Length.ToString());
                //Response.ContentType = "application/pdf";
                //Response.BinaryWrite(byteArray);
                //Response.End();




                return this.Request.CreateResponse(HttpStatusCode.OK, base64String);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private HttpResponseMessage GetAllEvents(string[] p)
        {
            try
            {


                User userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                int eventType = Convert.ToInt32(p[1]);

                if (!userInfo.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }


                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[2]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var events = new List<OneTimeEvent>();
                List<OneTimeEvent> ChildEvents;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = context.suretool_GetCurriculums(eventType, userInfo.UserId).Count();


                    events = context.suretool_GetCurriculums(eventType, userInfo.UserId).Select(oneevent => new OneTimeEvent
                    {
                        Program = oneevent.ProgramName,
                        Organization = oneevent.OrgName,
                        BeginDate = oneevent.StartDate,
                        EndDate = oneevent.EndDate,
                        Group = oneevent.GroupName,
                        ServiceLocationName = context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault() == null ? "" : context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault().ServiceLocationName,
                        ServiceSchoolName = context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault() == null ? "" : context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault().ServiceSchoolName,

                        lstEvents = GetChildEvents(oneevent.ID),


                        Age0 = oneevent.Age0,
                        Age12 = oneevent.Age12,
                        Age15 = oneevent.Age15,
                        Age18 = oneevent.Age18,
                        Age21 = oneevent.Age21,
                        Age25 = oneevent.Age25,
                        Age45 = oneevent.Age45,
                        Age5 = oneevent.Age5,
                        Age65 = oneevent.Age65,
                        AvgCostPerParticipant = oneevent.AvgCostPerParticipant,
                        closed = oneevent.closed,
                        CoalitionMeeting = oneevent.CoalitionMeeting,
                        CoalitionType = oneevent.CoalitionType,
                        Curriculum = oneevent.Curriculum,
                        D_HiRi_AbuVict = oneevent.D_HiRi_AbuVict,
                        D_HiRi_ChildOfSA = oneevent.D_HiRi_ChildOfSA,
                        D_HiRi_EconDis = oneevent.D_HiRi_EconDis,
                        D_HiRi_FrLun = oneevent.D_HiRi_FrLun,
                        D_HiRi_Homel = oneevent.D_HiRi_Homel,
                        D_HiRi_K12DO = oneevent.D_HiRi_K12DO,
                        D_HiRi_MentH = oneevent.D_HiRi_MentH,
                        D_HiRi_NotAp = oneevent.D_HiRi_NotAp,
                        D_HiRi_Other = oneevent.D_HiRi_Other,
                        D_HiRi_OtherText = oneevent.D_HiRi_OtherText,
                        D_HiRi_PhysDis = oneevent.D_HiRi_PhysDis,
                        D_HiRi_PregUse = oneevent.D_HiRi_PregUse,
                        D_HiRi_Using = oneevent.D_HiRi_Using,
                        D_HiRi_Violent = oneevent.D_HiRi_Violent,
                        Degree = oneevent.Degree,
                        DirectServiceHours = oneevent.DirectServiceHours,
                        DocOfActivities_Agenda = oneevent.DocOfActivities_Agenda,
                        DocOfActivities_Brochure = oneevent.DocOfActivities_Brochure,
                        DocOfActivities_ContactForm = oneevent.DocOfActivities_ContactForm,
                        DocOfActivities_EvalForm = oneevent.DocOfActivities_EvalForm,
                        DocOfActivities_Other = oneevent.DocOfActivities_Other,
                        DocOfActivities_OtherText = oneevent.DocOfActivities_OtherText,
                        DocOfActivities_PrePostTest = oneevent.DocOfActivities_PrePostTest,
                        DocOfActivities_SignIn = oneevent.DocOfActivities_SignIn,
                        EntryDate = oneevent.EntryDate,
                        Females = oneevent.Females,
                        FiscalYear = oneevent.FiscalYear,
                        GroupName = oneevent.GroupName,
                        Hispanic = oneevent.Hispanic,
                        ID = oneevent.ID,
                        ID02_DirectoriesDistributed = oneevent.ID02_DirectoriesDistributed,
                        ID04_BrochuresDistributed = oneevent.ID04_BrochuresDistributed,
                        InterventionType = oneevent.InterventionType,
                        Males = oneevent.Males,
                        Name = oneevent.Name,
                        NotHispanic = oneevent.NotHispanic,
                        NumberOfEmployees = oneevent.NumberOfEmployees,
                        NumberParticipants = oneevent.NumberParticipants,
                        NumberProgHrsRecd = oneevent.NumberProgHrsRecd,
                        OrgName = oneevent.OrgName,
                        OverallComments = oneevent.OverallComments,
                        ParentEntry = oneevent.ParentEntry,
                        PrepHours = oneevent.PrepHours,
                        ProgramName = oneevent.ProgramName,
                        ProgramNameSource = oneevent.ProgramNameSource,

                        ProgramId = context.Programs.Where(x => x.Type == oneevent.ProgramNameSource.Trim() && x.Name == oneevent.ProgramName.Trim()).Select(x => x.ID).SingleOrDefault(),
                        ProgramType_IP = oneevent.ProgramType_IP,
                        RaceAmIndian = oneevent.RaceAmIndian,
                        RaceAsian = oneevent.RaceAsian,
                        RaceBlack = oneevent.RaceBlack,
                        RaceHawaii = oneevent.RaceHawaii,
                        RaceMoreThanOne = oneevent.RaceMoreThanOne,
                        RaceUnknownOther = oneevent.RaceUnknownOther,
                        RaceWhite = oneevent.RaceWhite,
                        RegionCode = oneevent.RegionCode,
                        SessionName = oneevent.SessionName,
                        SiteCode = oneevent.SiteCode,
                        StaffFname = oneevent.StaffFname,
                        StaffLName = oneevent.StaffLName,
                        StartDate = oneevent.StartDate,
                        Strat_Alt_21 = oneevent.Strat_Alt_21,
                        Strat_Alt_22 = oneevent.Strat_Alt_22,
                        Strat_Alt_23 = oneevent.Strat_Alt_23,
                        Strat_Alt_24 = oneevent.Strat_Alt_24,
                        Strat_Alt_25 = oneevent.Strat_Alt_25,
                        Strat_Alt_26 = oneevent.Strat_Alt_26,
                        Strat_Alt_27 = oneevent.Strat_Alt_27,
                        Strat_Alt_27Text = oneevent.Strat_Alt_27Text,
                        Strat_CBP_41 = oneevent.Strat_CBP_41,
                        Strat_CBP_42 = oneevent.Strat_CBP_42,
                        Strat_CBP_43 = oneevent.Strat_CBP_43,
                        Strat_CBP_44 = oneevent.Strat_CBP_44,
                        Strat_CBP_45 = oneevent.Strat_CBP_45,

                        Strat_CBP_Faith = oneevent.Strat_CBP_Faith,

                        Strat_CBP_46 = oneevent.Strat_CBP_46,
                        Strat_CBP_46Text = oneevent.Strat_CBP_46Text,
                        Strat_Ed_11 = oneevent.Strat_Ed_11,
                        Strat_Ed_12 = oneevent.Strat_Ed_12,
                        Strat_Ed_13 = oneevent.Strat_Ed_13,
                        Strat_Ed_14 = oneevent.Strat_Ed_14,
                        Strat_Ed_15 = oneevent.Strat_Ed_15,
                        Strat_Ed_16 = oneevent.Strat_Ed_16,
                        Strat_Ed_17 = oneevent.Strat_Ed_17,
                        Strat_Ed_17Text = oneevent.Strat_Ed_17Text,
                        Strat_Env_51 = oneevent.Strat_Env_51,
                        Strat_Env_52 = oneevent.Strat_Env_52,
                        Strat_Env_53 = oneevent.Strat_Env_53,
                        Strat_Env_54 = oneevent.Strat_Env_54,
                        Strat_Env_55 = oneevent.Strat_Env_55,
                        Strat_Env_56_MW = oneevent.Strat_Env_56_MW,
                        Strat_Env_57 = oneevent.Strat_Env_57,
                        Strat_Env_57Text = oneevent.Strat_Env_57Text,
                        Strat_ID_01 = oneevent.Strat_ID_01,
                        Strat_ID_02 = oneevent.Strat_ID_02,
                        Strat_ID_03 = oneevent.Strat_ID_03,
                        Strat_ID_04 = oneevent.Strat_ID_04,
                        Strat_ID_05 = oneevent.Strat_ID_05,
                        Strat_ID_06 = oneevent.Strat_ID_06,
                        Strat_ID_07 = oneevent.Strat_ID_07,
                        Strat_ID_08 = oneevent.Strat_ID_08,
                        Strat_ID_09 = oneevent.Strat_ID_09,
                        Strat_ID_09Text = oneevent.Strat_ID_09Text,
                        Strat_IR_31 = oneevent.Strat_IR_31,
                        Strat_IR_32 = oneevent.Strat_IR_32,
                        Strat_IR_34 = oneevent.Strat_IR_34,
                        Strat_IR_34Text = oneevent.Strat_IR_34Text,
                        SurveysEntered = oneevent.SurveysEntered,
                        TargetedStatewideInitiative = oneevent.TargetedStatewideInitiative,
                        TotalCostofProgram = oneevent.TotalCostofProgram,
                        TravelHours = oneevent.TravelHours,
                        TyoeOfProgram_EA = oneevent.TyoeOfProgram_EA,
                        UniversalType_DI = oneevent.UniversalType_DI,
                        UserGUID = oneevent.UserGUID,
                        WithinCostBand_YN = oneevent.WithinCostBand_YN,




                    }).Skip(skip).Take(take).ToList();

                    foreach (OneTimeEvent o in events)
                    {
                        o.PreTestCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 1).Count();
                        o.PostTetCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 2).Count();
                        o.SurveyCount = context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount.HasValue ? context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount : 0;
                        o.TotalPostTest = context.tbl2008_NOM_Entries.Where(x => x.ParentEntry == o.ID && x.SurveyCount != null).Select(x => x.SurveyCount).Count();
                        o.TotalPostTest = o.TotalPostTest + o.PostTetCount;
                    }
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                }

                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<OneTimeEvent>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<OneTimeEvent>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, events);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetLatestEvent(string[] p)
        {
            try
            {
                int EventId = Convert.ToInt32(p[0]);
                var evt = new OneTimeEvent();

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //id = context.tbl2008_NOM_Entries.Where(x => x.ID == EventId).Select(x => x.ParentEntry).SingleOrDefault();
                    var oneevent = context.tbl2008_NOM_Entries.Where(x => x.ParentEntry == EventId || x.ID == EventId).OrderByDescending(x => x.ID).First();
                    if (oneevent != null)
                    {
                        oneevent.ParentEntry = EventId;
                        return this.Request.CreateResponse(HttpStatusCode.OK, FillEvent(context, oneevent));

                    }
                    else
                    {
                        return this.Request.CreateResponse(HttpStatusCode.OK, "");
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        private static OneTimeEvent FillEvent(DataGadget.Web.DataModel.datagadget_testEntities context, DataModel.tbl2008_NOM_Entries oneevent)
        {
            OneTimeEvent childevent = new OneTimeEvent();
            childevent.Program = oneevent.ProgramName;
            childevent.Organization = oneevent.OrgName;
            childevent.BeginDate = oneevent.StartDate;
            childevent.EndDate = oneevent.EndDate;
            childevent.Group = oneevent.GroupName;
            childevent.ServiceLocationName = oneevent.ServiceLocationName;
            childevent.ServiceSchoolName = oneevent.ServiceSchoolName;
            childevent.Age0 = oneevent.Age0;
            childevent.Age12 = oneevent.Age12;
            childevent.Age15 = oneevent.Age15;
            childevent.Age18 = oneevent.Age18;
            childevent.Age21 = oneevent.Age21;
            childevent.Age25 = oneevent.Age25;
            childevent.Age45 = oneevent.Age45;
            childevent.Age5 = oneevent.Age5;
            childevent.Age65 = oneevent.Age65;
            childevent.AvgCostPerParticipant = oneevent.AvgCostPerParticipant;
            childevent.closed = oneevent.closed;
            childevent.CoalitionMeeting = oneevent.CoalitionMeeting;
            childevent.CoalitionType = oneevent.CoalitionType;
            childevent.Curriculum = oneevent.Curriculum;
            childevent.D_HiRi_AbuVict = oneevent.D_HiRi_AbuVict;
            childevent.D_HiRi_ChildOfSA = oneevent.D_HiRi_ChildOfSA;
            childevent.D_HiRi_EconDis = oneevent.D_HiRi_EconDis;
            childevent.D_HiRi_FrLun = oneevent.D_HiRi_FrLun;
            childevent.D_HiRi_Homel = oneevent.D_HiRi_Homel;
            childevent.D_HiRi_K12DO = oneevent.D_HiRi_K12DO;
            childevent.D_HiRi_MentH = oneevent.D_HiRi_MentH;
            childevent.D_HiRi_NotAp = oneevent.D_HiRi_NotAp;
            childevent.D_HiRi_Other = oneevent.D_HiRi_Other;
            childevent.D_HiRi_OtherText = oneevent.D_HiRi_OtherText;
            childevent.D_HiRi_PhysDis = oneevent.D_HiRi_PhysDis;
            childevent.D_HiRi_PregUse = oneevent.D_HiRi_PregUse;
            childevent.D_HiRi_Using = oneevent.D_HiRi_Using;
            childevent.D_HiRi_Violent = oneevent.D_HiRi_Violent;
            childevent.Degree = oneevent.Degree;
            childevent.DirectServiceHours = oneevent.DirectServiceHours;
            childevent.DocOfActivities_Agenda = oneevent.DocOfActivities_Agenda;
            childevent.DocOfActivities_Brochure = oneevent.DocOfActivities_Brochure;
            childevent.DocOfActivities_ContactForm = oneevent.DocOfActivities_ContactForm;
            childevent.DocOfActivities_EvalForm = oneevent.DocOfActivities_EvalForm;
            childevent.DocOfActivities_Other = oneevent.DocOfActivities_Other;
            childevent.DocOfActivities_OtherText = oneevent.DocOfActivities_OtherText;
            childevent.DocOfActivities_PrePostTest = oneevent.DocOfActivities_PrePostTest;
            childevent.DocOfActivities_SignIn = oneevent.DocOfActivities_SignIn;
            childevent.EntryDate = oneevent.EntryDate;
            childevent.Females = oneevent.Females;
            childevent.FiscalYear = oneevent.FiscalYear;
            childevent.GroupName = oneevent.GroupName;
            childevent.Hispanic = oneevent.Hispanic;
            childevent.ID = oneevent.ID;
            childevent.ID02_DirectoriesDistributed = oneevent.ID02_DirectoriesDistributed;
            childevent.ID04_BrochuresDistributed = oneevent.ID04_BrochuresDistributed;
            childevent.InterventionType = oneevent.InterventionType;
            childevent.Males = oneevent.Males;
            childevent.Name = oneevent.Name;
            childevent.NotHispanic = oneevent.NotHispanic;
            childevent.NumberOfEmployees = oneevent.NumberOfEmployees;
            childevent.NumberParticipants = oneevent.NumberParticipants;
            childevent.NumberProgHrsRecd = oneevent.NumberProgHrsRecd;
            childevent.OrgName = oneevent.OrgName;
            childevent.OverallComments = oneevent.OverallComments;
            childevent.ParentEntry = oneevent.ParentEntry;
            childevent.PrepHours = oneevent.PrepHours;
            childevent.ProgramName = oneevent.ProgramName;
            childevent.ProgramNameSource = oneevent.ProgramNameSource;

            childevent.ProgramId = context.Programs.Where(x => x.Type == oneevent.ProgramNameSource.Trim() && x.Name == oneevent.ProgramName.Trim()).Select(x => x.ID).SingleOrDefault();
            childevent.ProgramType_IP = oneevent.ProgramType_IP;
            childevent.RaceAmIndian = oneevent.RaceAmIndian;
            childevent.RaceAsian = oneevent.RaceAsian;
            childevent.RaceBlack = oneevent.RaceBlack;
            childevent.RaceHawaii = oneevent.RaceHawaii;
            childevent.RaceMoreThanOne = oneevent.RaceMoreThanOne;
            childevent.RaceUnknownOther = oneevent.RaceUnknownOther;
            childevent.RaceWhite = oneevent.RaceWhite;
            childevent.RegionCode = oneevent.RegionCode;
            childevent.SessionName = oneevent.SessionName;
            childevent.SiteCode = oneevent.SiteCode;
            childevent.StaffFname = oneevent.StaffFname;
            childevent.StaffLName = oneevent.StaffLName;
            childevent.StartDate = oneevent.StartDate;
            childevent.Strat_Alt_21 = oneevent.Strat_Alt_21;
            childevent.Strat_Alt_22 = oneevent.Strat_Alt_22;
            childevent.Strat_Alt_23 = oneevent.Strat_Alt_23;
            childevent.Strat_Alt_24 = oneevent.Strat_Alt_24;
            childevent.Strat_Alt_25 = oneevent.Strat_Alt_25;
            childevent.Strat_Alt_26 = oneevent.Strat_Alt_26;
            childevent.Strat_Alt_27 = oneevent.Strat_Alt_27;
            childevent.Strat_Alt_27Text = oneevent.Strat_Alt_27Text;
            childevent.Strat_CBP_41 = oneevent.Strat_CBP_41;
            childevent.Strat_CBP_42 = oneevent.Strat_CBP_42;
            childevent.Strat_CBP_43 = oneevent.Strat_CBP_43;
            childevent.Strat_CBP_44 = oneevent.Strat_CBP_44;
            childevent.Strat_CBP_45 = oneevent.Strat_CBP_45;

            childevent.Strat_CBP_Faith = oneevent.Strat_CBP_Faith;

            childevent.Strat_CBP_46 = oneevent.Strat_CBP_46;
            childevent.Strat_CBP_46Text = oneevent.Strat_CBP_46Text;
            childevent.Strat_Ed_11 = oneevent.Strat_Ed_11;
            childevent.Strat_Ed_12 = oneevent.Strat_Ed_12;
            childevent.Strat_Ed_13 = oneevent.Strat_Ed_13;
            childevent.Strat_Ed_14 = oneevent.Strat_Ed_14;
            childevent.Strat_Ed_15 = oneevent.Strat_Ed_15;
            childevent.Strat_Ed_16 = oneevent.Strat_Ed_16;
            childevent.Strat_Ed_17 = oneevent.Strat_Ed_17;
            childevent.Strat_Ed_17Text = oneevent.Strat_Ed_17Text;
            childevent.Strat_Env_51 = oneevent.Strat_Env_51;
            childevent.Strat_Env_52 = oneevent.Strat_Env_52;
            childevent.Strat_Env_53 = oneevent.Strat_Env_53;
            childevent.Strat_Env_54 = oneevent.Strat_Env_54;
            childevent.Strat_Env_55 = oneevent.Strat_Env_55;
            childevent.Strat_Env_56_MW = oneevent.Strat_Env_56_MW;
            childevent.Strat_Env_57 = oneevent.Strat_Env_57;
            childevent.Strat_Env_57Text = oneevent.Strat_Env_57Text;
            childevent.Strat_ID_01 = oneevent.Strat_ID_01;
            childevent.Strat_ID_02 = oneevent.Strat_ID_02;
            childevent.Strat_ID_03 = oneevent.Strat_ID_03;
            childevent.Strat_ID_04 = oneevent.Strat_ID_04;
            childevent.Strat_ID_05 = oneevent.Strat_ID_05;
            childevent.Strat_ID_06 = oneevent.Strat_ID_06;
            childevent.Strat_ID_07 = oneevent.Strat_ID_07;
            childevent.Strat_ID_08 = oneevent.Strat_ID_08;
            childevent.Strat_ID_09 = oneevent.Strat_ID_09;
            childevent.Strat_ID_09Text = oneevent.Strat_ID_09Text;
            childevent.Strat_IR_31 = oneevent.Strat_IR_31;
            childevent.Strat_IR_32 = oneevent.Strat_IR_32;
            childevent.Strat_IR_34 = oneevent.Strat_IR_34;
            childevent.Strat_IR_34Text = oneevent.Strat_IR_34Text;
            childevent.SurveysEntered = oneevent.SurveysEntered;
            childevent.TargetedStatewideInitiative = oneevent.TargetedStatewideInitiative;
            childevent.TotalCostofProgram = oneevent.TotalCostofProgram;
            childevent.TravelHours = oneevent.TravelHours;
            childevent.TyoeOfProgram_EA = oneevent.TyoeOfProgram_EA;
            childevent.UniversalType_DI = oneevent.UniversalType_DI;
            childevent.UserGUID = oneevent.UserGUID;
            childevent.WithinCostBand_YN = oneevent.WithinCostBand_YN;
            return childevent;

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        private List<OneTimeEvent> GetChildEvents(int ParentEventID)
        {
            try
            {
                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbEvents = await context.tbl2008_NOM_Entries.Where(x => x.UserGUID == newUserId && x.Curriculum=0).ToListAsync();
                    events = context.suretool_GetChildCurriculums(ParentEventID).
                        Select(oneevent =>
                            new OneTimeEvent
                            {
                                Program = oneevent.ProgramName,
                                Organization = oneevent.OrgName,
                                BeginDate = oneevent.StartDate,
                                EndDate = oneevent.EndDate,
                                Group = oneevent.GroupName,
                                ID = oneevent.ID,
                                NumberOfEmployees = oneevent.NumberOfEmployees,
                                PrepHours = oneevent.PrepHours,
                                DirectServiceHours = oneevent.DirectServiceHours,
                                closed = oneevent.closed,
                                TravelHours = oneevent.TravelHours
                            }).ToList();

                    foreach (OneTimeEvent o in events)
                    {
                        o.PreTestCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 1).Count();
                        o.PostTetCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 2).Count();
                        o.SurveyCount = context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount.HasValue ? context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount : 0;
                    }

                }

                return events;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private List<OneTimeEvent> GetChildEventsForView(int ParentEventID)
        {
            try
            {
                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbEvents = await context.tbl2008_NOM_Entries.Where(x => x.UserGUID == newUserId && x.Curriculum=0).ToListAsync();
                    events = context.suretool_GetChildCurriculums(ParentEventID).
                        Select(oneevent =>
                            new OneTimeEvent
                            {
                                Program = oneevent.ProgramName,
                                Organization = oneevent.OrgName,
                                BeginDate = oneevent.StartDate,
                                EndDate = oneevent.EndDate,
                                Group = oneevent.GroupName,
                                ServiceLocationName = context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault() == null ? "" : context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault().ServiceLocationName,
                                ServiceSchoolName = context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault() == null ? "" : context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault().ServiceSchoolName,

                                //lstEvents = GetChildEvents(oneevent.ID),

                                Age0 = oneevent.Age0,
                                Age12 = oneevent.Age12,
                                Age15 = oneevent.Age15,
                                Age18 = oneevent.Age18,
                                Age21 = oneevent.Age21,
                                Age25 = oneevent.Age25,
                                Age45 = oneevent.Age45,
                                Age5 = oneevent.Age5,
                                Age65 = oneevent.Age65,
                                AvgCostPerParticipant = oneevent.AvgCostPerParticipant,
                                closed = oneevent.closed,
                                CoalitionMeeting = oneevent.CoalitionMeeting,
                                CoalitionType = oneevent.CoalitionType,
                                Curriculum = oneevent.Curriculum,
                                D_HiRi_AbuVict = oneevent.D_HiRi_AbuVict,
                                D_HiRi_ChildOfSA = oneevent.D_HiRi_ChildOfSA,
                                D_HiRi_EconDis = oneevent.D_HiRi_EconDis,
                                D_HiRi_FrLun = oneevent.D_HiRi_FrLun,
                                D_HiRi_Homel = oneevent.D_HiRi_Homel,
                                D_HiRi_K12DO = oneevent.D_HiRi_K12DO,
                                D_HiRi_MentH = oneevent.D_HiRi_MentH,
                                D_HiRi_NotAp = oneevent.D_HiRi_NotAp,
                                D_HiRi_Other = oneevent.D_HiRi_Other,
                                D_HiRi_OtherText = oneevent.D_HiRi_OtherText,
                                D_HiRi_PhysDis = oneevent.D_HiRi_PhysDis,
                                D_HiRi_PregUse = oneevent.D_HiRi_PregUse,
                                D_HiRi_Using = oneevent.D_HiRi_Using,
                                D_HiRi_Violent = oneevent.D_HiRi_Violent,
                                Degree = oneevent.Degree,
                                DirectServiceHours = oneevent.DirectServiceHours,
                                DocOfActivities_Agenda = oneevent.DocOfActivities_Agenda,
                                DocOfActivities_Brochure = oneevent.DocOfActivities_Brochure,
                                DocOfActivities_ContactForm = oneevent.DocOfActivities_ContactForm,
                                DocOfActivities_EvalForm = oneevent.DocOfActivities_EvalForm,
                                DocOfActivities_Other = oneevent.DocOfActivities_Other,
                                DocOfActivities_OtherText = oneevent.DocOfActivities_OtherText,
                                DocOfActivities_PrePostTest = oneevent.DocOfActivities_PrePostTest,
                                DocOfActivities_SignIn = oneevent.DocOfActivities_SignIn,
                                EntryDate = oneevent.EntryDate,
                                Females = oneevent.Females,
                                FiscalYear = oneevent.FiscalYear,
                                GroupName = oneevent.GroupName,
                                Hispanic = oneevent.Hispanic,
                                ID = oneevent.ID,
                                ID02_DirectoriesDistributed = oneevent.ID02_DirectoriesDistributed,
                                ID04_BrochuresDistributed = oneevent.ID04_BrochuresDistributed,
                                InterventionType = oneevent.InterventionType,
                                Males = oneevent.Males,
                                Name = oneevent.Name,
                                NotHispanic = oneevent.NotHispanic,
                                NumberOfEmployees = oneevent.NumberOfEmployees,
                                NumberParticipants = oneevent.NumberParticipants,
                                NumberProgHrsRecd = oneevent.NumberProgHrsRecd,
                                OrgName = oneevent.OrgName,
                                OverallComments = oneevent.OverallComments,
                                ParentEntry = oneevent.ParentEntry,
                                PrepHours = oneevent.PrepHours,
                                ProgramName = oneevent.ProgramName,
                                ProgramNameSource = oneevent.ProgramNameSource,

                                ProgramId = context.Programs.Where(x => x.Type == oneevent.ProgramNameSource.Trim() && x.Name == oneevent.ProgramName.Trim()).Select(x => x.ID).SingleOrDefault(),
                                ProgramType_IP = oneevent.ProgramType_IP,
                                RaceAmIndian = oneevent.RaceAmIndian,
                                RaceAsian = oneevent.RaceAsian,
                                RaceBlack = oneevent.RaceBlack,
                                RaceHawaii = oneevent.RaceHawaii,
                                RaceMoreThanOne = oneevent.RaceMoreThanOne,
                                RaceUnknownOther = oneevent.RaceUnknownOther,
                                RaceWhite = oneevent.RaceWhite,
                                RegionCode = oneevent.RegionCode,
                                SessionName = oneevent.SessionName,
                                SiteCode = oneevent.SiteCode,
                                StaffFname = oneevent.StaffFname,
                                StaffLName = oneevent.StaffLName,
                                StartDate = oneevent.StartDate,
                                Strat_Alt_21 = oneevent.Strat_Alt_21,
                                Strat_Alt_22 = oneevent.Strat_Alt_22,
                                Strat_Alt_23 = oneevent.Strat_Alt_23,
                                Strat_Alt_24 = oneevent.Strat_Alt_24,
                                Strat_Alt_25 = oneevent.Strat_Alt_25,
                                Strat_Alt_26 = oneevent.Strat_Alt_26,
                                Strat_Alt_27 = oneevent.Strat_Alt_27,
                                Strat_Alt_27Text = oneevent.Strat_Alt_27Text,
                                Strat_CBP_41 = oneevent.Strat_CBP_41,
                                Strat_CBP_42 = oneevent.Strat_CBP_42,
                                Strat_CBP_43 = oneevent.Strat_CBP_43,
                                Strat_CBP_44 = oneevent.Strat_CBP_44,
                                Strat_CBP_45 = oneevent.Strat_CBP_45,

                                Strat_CBP_Faith = oneevent.Strat_CBP_Faith,

                                Strat_CBP_46 = oneevent.Strat_CBP_46,
                                Strat_CBP_46Text = oneevent.Strat_CBP_46Text,
                                Strat_Ed_11 = oneevent.Strat_Ed_11,
                                Strat_Ed_12 = oneevent.Strat_Ed_12,
                                Strat_Ed_13 = oneevent.Strat_Ed_13,
                                Strat_Ed_14 = oneevent.Strat_Ed_14,
                                Strat_Ed_15 = oneevent.Strat_Ed_15,
                                Strat_Ed_16 = oneevent.Strat_Ed_16,
                                Strat_Ed_17 = oneevent.Strat_Ed_17,
                                Strat_Ed_17Text = oneevent.Strat_Ed_17Text,
                                Strat_Env_51 = oneevent.Strat_Env_51,
                                Strat_Env_52 = oneevent.Strat_Env_52,
                                Strat_Env_53 = oneevent.Strat_Env_53,
                                Strat_Env_54 = oneevent.Strat_Env_54,
                                Strat_Env_55 = oneevent.Strat_Env_55,
                                Strat_Env_56_MW = oneevent.Strat_Env_56_MW,
                                Strat_Env_57 = oneevent.Strat_Env_57,
                                Strat_Env_57Text = oneevent.Strat_Env_57Text,
                                Strat_ID_01 = oneevent.Strat_ID_01,
                                Strat_ID_02 = oneevent.Strat_ID_02,
                                Strat_ID_03 = oneevent.Strat_ID_03,
                                Strat_ID_04 = oneevent.Strat_ID_04,
                                Strat_ID_05 = oneevent.Strat_ID_05,
                                Strat_ID_06 = oneevent.Strat_ID_06,
                                Strat_ID_07 = oneevent.Strat_ID_07,
                                Strat_ID_08 = oneevent.Strat_ID_08,
                                Strat_ID_09 = oneevent.Strat_ID_09,
                                Strat_ID_09Text = oneevent.Strat_ID_09Text,
                                Strat_IR_31 = oneevent.Strat_IR_31,
                                Strat_IR_32 = oneevent.Strat_IR_32,
                                Strat_IR_34 = oneevent.Strat_IR_34,
                                Strat_IR_34Text = oneevent.Strat_IR_34Text,
                                SurveysEntered = oneevent.SurveysEntered,
                                TargetedStatewideInitiative = oneevent.TargetedStatewideInitiative,
                                TotalCostofProgram = oneevent.TotalCostofProgram,
                                TravelHours = oneevent.TravelHours,
                                TyoeOfProgram_EA = oneevent.TyoeOfProgram_EA,
                                UniversalType_DI = oneevent.UniversalType_DI,
                                UserGUID = oneevent.UserGUID,
                                WithinCostBand_YN = oneevent.WithinCostBand_YN,
                            }).ToList();

                    //foreach (OneTimeEvent o in events)
                    //{
                    //    o.PreTestCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 1).Count();
                    //    o.PostTetCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 2).Count();
                    //    o.SurveyCount = context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount.HasValue ? context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount : 0;
                    //}

                }

                return events;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private DataModel.tbl2008_NOM_Entries setvalue(Models.OneTimeEvent oneTime)
        {
            var Entries = new DataModel.tbl2008_NOM_Entries();
            #region "parameters"

            Entries.SurveysEntered = false;
            Entries.StartDate = oneTime.StartDate;
            Entries.EndDate = oneTime.EndDate;
            Entries.ProgramType_IP = oneTime.ProgramType_IP;
            Entries.InterventionType = oneTime.InterventionType;
            Entries.TyoeOfProgram_EA = oneTime.TyoeOfProgram_EA;
            Entries.UniversalType_DI = oneTime.UniversalType_DI;
            Entries.ProgramNameSource = oneTime.ProgramNameSource;
            Entries.ProgramName = oneTime.ProgramName; //p[7];// null;
            Entries.GroupName = oneTime.GroupName;// p[8];// null;

            Entries.ServiceLocationName = oneTime.ServiceLocationName;
            Entries.ServiceSchoolName = oneTime.ServiceSchoolName;


            Entries.NumberParticipants = oneTime.NumberParticipants;
            Entries.Males = oneTime.Males;
            Entries.Females = oneTime.Females;

            Entries.Age0 = oneTime.Age0.HasValue ? oneTime.Age0 : 0;
            Entries.Age5 = oneTime.Age5.HasValue ? oneTime.Age5 : 0;
            Entries.Age12 = oneTime.Age12.HasValue ? oneTime.Age12 : 0;
            Entries.Age15 = oneTime.Age15.HasValue ? oneTime.Age15 : 0;
            Entries.Age18 = oneTime.Age18.HasValue ? oneTime.Age18 : 0;
            Entries.Age21 = oneTime.Age21.HasValue ? oneTime.Age21 : 0;
            Entries.Age25 = oneTime.Age25.HasValue ? oneTime.Age25 : 0;
            Entries.Age45 = oneTime.Age45.HasValue ? oneTime.Age45 : 0;
            Entries.Age65 = oneTime.Age65.HasValue ? oneTime.Age65 : 0;

            Entries.RaceWhite = oneTime.RaceWhite.HasValue ? oneTime.RaceWhite : 0;
            Entries.RaceBlack = oneTime.RaceBlack.HasValue ? oneTime.RaceBlack : 0;
            Entries.RaceHawaii = oneTime.RaceHawaii.HasValue ? oneTime.RaceHawaii : 0;
            Entries.RaceAsian = oneTime.RaceAsian.HasValue ? oneTime.RaceAsian : 0;
            Entries.RaceAmIndian = oneTime.RaceAmIndian.HasValue ? oneTime.RaceAmIndian : 0;
            Entries.RaceUnknownOther = oneTime.RaceUnknownOther.HasValue ? oneTime.RaceUnknownOther : 0;
            Entries.RaceMoreThanOne = oneTime.RaceMoreThanOne.HasValue ? oneTime.RaceMoreThanOne : 0;

            Entries.NotHispanic = oneTime.NotHispanic.HasValue ? oneTime.NotHispanic : 0;
            Entries.Hispanic = oneTime.Hispanic.HasValue ? oneTime.Hispanic : 0;

            // modification
            Entries.D_HiRi_ChildOfSA = oneTime.D_HiRi_ChildOfSA.HasValue ? oneTime.D_HiRi_ChildOfSA : 0; // Convert.ToInt32(p[33]);
            Entries.D_HiRi_PregUse = oneTime.D_HiRi_PregUse.HasValue ? oneTime.D_HiRi_PregUse : 0; //null;
            Entries.D_HiRi_K12DO = oneTime.D_HiRi_K12DO.HasValue ? oneTime.D_HiRi_K12DO : 0; //null;

            Entries.D_HiRi_Violent = oneTime.D_HiRi_Violent.HasValue ? oneTime.D_HiRi_Violent : 0; //Convert.ToInt32(p[34]);
            Entries.D_HiRi_MentH = oneTime.D_HiRi_MentH.HasValue ? oneTime.D_HiRi_MentH : 0; //Convert.ToInt32(p[38]);
            Entries.D_HiRi_EconDis = oneTime.D_HiRi_EconDis.HasValue ? oneTime.D_HiRi_EconDis : 0; //Convert.ToInt32(p[41]);
            Entries.D_HiRi_PhysDis = oneTime.D_HiRi_PhysDis.HasValue ? oneTime.D_HiRi_PhysDis : 0; //Convert.ToInt32(p[35]);
            Entries.D_HiRi_AbuVict = oneTime.D_HiRi_AbuVict.HasValue ? oneTime.D_HiRi_AbuVict : 0; //Convert.ToInt32(p[39]);
            Entries.D_HiRi_Using = oneTime.D_HiRi_Using.HasValue ? oneTime.D_HiRi_Using : 0; //null;
            Entries.D_HiRi_Homel = oneTime.D_HiRi_Homel.HasValue ? oneTime.D_HiRi_Homel : 0; //Convert.ToInt32(p[36]);

            Entries.D_HiRi_FrLun = oneTime.D_HiRi_FrLun.HasValue ? oneTime.D_HiRi_FrLun : 0; //null;
            Entries.D_HiRi_Other = oneTime.D_HiRi_Other.HasValue ? oneTime.D_HiRi_Other : 0; //null;             
            Entries.D_HiRi_NotAp = oneTime.D_HiRi_NotAp.HasValue ? oneTime.D_HiRi_NotAp : 0; //null;

            Entries.Strat_ID_01 = oneTime.Strat_ID_01.HasValue ? oneTime.Strat_ID_01 : 0; //Convert.ToBoolean(p[45]) ? 1 : 0;

            Entries.Strat_ID_02 = oneTime.Strat_ID_02.HasValue ? oneTime.Strat_ID_02 : 0; //Convert.ToBoolean(p[46]) ? 1 : 0;

            Entries.Strat_ID_03 = oneTime.Strat_ID_03.HasValue ? oneTime.Strat_ID_03 : 0;//Convert.ToBoolean(p[47]) ? 1 : 0;
            Entries.Strat_ID_04 = oneTime.Strat_ID_04.HasValue ? oneTime.Strat_ID_04 : 0;//Convert.ToBoolean(p[48]) ? 1 : 0;
            Entries.Strat_ID_05 = oneTime.Strat_ID_05.HasValue ? oneTime.Strat_ID_05 : 0;//Convert.ToBoolean(p[49]) ? 1 : 0;
            Entries.Strat_ID_06 = oneTime.Strat_ID_06.HasValue ? oneTime.Strat_ID_06 : 0; //Convert.ToBoolean(p[50]) ? 1 : 0;
            Entries.Strat_ID_07 = oneTime.Strat_ID_07.HasValue ? oneTime.Strat_ID_07 : 0; //Convert.ToBoolean(p[51]) ? 1 : 0;
            Entries.Strat_ID_08 = oneTime.Strat_ID_08.HasValue ? oneTime.Strat_ID_08 : 0; //Convert.ToBoolean(p[52]) ? 1 : 0;
            Entries.Strat_ID_09 = oneTime.Strat_ID_09.HasValue ? oneTime.Strat_ID_09 : 0; //Convert.ToBoolean(p[53]) ? 1 : 0;
            Entries.Strat_ID_09Text = oneTime.Strat_ID_09Text; //p[54];

            Entries.Strat_Ed_11 = oneTime.Strat_Ed_11.HasValue ? oneTime.Strat_Ed_11 : 0; //Convert.ToBoolean(p[55]) ? 1 : 0;
            Entries.Strat_Ed_12 = oneTime.Strat_Ed_12.HasValue ? oneTime.Strat_Ed_12 : 0; //Convert.ToBoolean(p[56]) ? 1 : 0;
            Entries.Strat_Ed_13 = oneTime.Strat_Ed_13.HasValue ? oneTime.Strat_Ed_13 : 0;//Convert.ToBoolean(p[57]) ? 1 : 0;
            Entries.Strat_Ed_14 = oneTime.Strat_Ed_14.HasValue ? oneTime.Strat_Ed_14 : 0;//Convert.ToBoolean(p[58]) ? 1 : 0;
            Entries.Strat_Ed_15 = oneTime.Strat_Ed_15.HasValue ? oneTime.Strat_Ed_15 : 0;//Convert.ToBoolean(p[59]) ? 1 : 0;
            Entries.Strat_Ed_16 = oneTime.Strat_Ed_16.HasValue ? oneTime.Strat_Ed_16 : 0;//Convert.ToBoolean(p[60]) ? 1 : 0;
            Entries.Strat_Ed_17 = oneTime.Strat_Ed_17.HasValue ? oneTime.Strat_Ed_17 : 0;//Convert.ToBoolean(p[61]) ? 1 : 0;
            Entries.Strat_Ed_17Text = oneTime.Strat_Ed_17Text; //p[62];

            Entries.Strat_Alt_21 = oneTime.Strat_Alt_21.HasValue ? oneTime.Strat_Alt_21 : 0; //Convert.ToBoolean(p[63]) ? 1 : 0;
            Entries.Strat_Alt_22 = oneTime.Strat_Alt_22.HasValue ? oneTime.Strat_Alt_22 : 0;//Convert.ToBoolean(p[64]) ? 1 : 0;
            Entries.Strat_Alt_23 = oneTime.Strat_Alt_23.HasValue ? oneTime.Strat_Alt_23 : 0;//Convert.ToBoolean(p[65]) ? 1 : 0;
            Entries.Strat_Alt_24 = oneTime.Strat_Alt_24.HasValue ? oneTime.Strat_Alt_24 : 0;//Convert.ToBoolean(p[66]) ? 1 : 0;
            Entries.Strat_Alt_25 = oneTime.Strat_Alt_25.HasValue ? oneTime.Strat_Alt_25 : 0;//Convert.ToBoolean(p[67]) ? 1 : 0;
            Entries.Strat_Alt_26 = oneTime.Strat_Alt_26.HasValue ? oneTime.Strat_Alt_26 : 0;//Convert.ToBoolean(p[68]) ? 1 : 0;
            Entries.Strat_Alt_27 = oneTime.Strat_Alt_27.HasValue ? oneTime.Strat_Alt_27 : 0; //Convert.ToBoolean(p[69]) ? 1 : 0;
            Entries.Strat_Alt_27Text = oneTime.Strat_Alt_27Text; //p[70];

            Entries.Strat_IR_31 = oneTime.Strat_IR_31.HasValue ? oneTime.Strat_IR_31 : 0; ; //Convert.ToBoolean(p[71]) ? 1 : 0;
            Entries.Strat_IR_32 = oneTime.Strat_IR_32.HasValue ? oneTime.Strat_IR_32 : 0; ; //Convert.ToBoolean(p[72]) ? 1 : 0;
            Entries.Strat_IR_34 = oneTime.Strat_IR_34.HasValue ? oneTime.Strat_IR_34 : 0; ; //Convert.ToBoolean(p[73]) ? 1 : 0;
            Entries.Strat_IR_34Text = oneTime.Strat_IR_34Text; //p[74];

            Entries.Strat_CBP_41 = oneTime.Strat_CBP_41.HasValue ? oneTime.Strat_CBP_41 : 0;
            Entries.Strat_CBP_42 = oneTime.Strat_CBP_42.HasValue ? oneTime.Strat_CBP_42 : 0;
            Entries.Strat_CBP_43 = oneTime.Strat_CBP_43.HasValue ? oneTime.Strat_CBP_43 : 0;
            Entries.Strat_CBP_44 = oneTime.Strat_CBP_44.HasValue ? oneTime.Strat_CBP_44 : 0;
            Entries.Strat_CBP_45 = oneTime.Strat_CBP_45.HasValue ? oneTime.Strat_CBP_45 : 0;
            Entries.Strat_CBP_46 = oneTime.Strat_CBP_46.HasValue ? oneTime.Strat_CBP_46 : 0;
            Entries.Strat_CBP_46Text = oneTime.Strat_CBP_46Text;

            Entries.Strat_CBP_Faith = oneTime.Strat_CBP_Faith.HasValue ? oneTime.Strat_CBP_Faith : 0; //Faith


            Entries.Strat_Env_51 = oneTime.Strat_Env_51.HasValue ? oneTime.Strat_Env_51 : 0;
            Entries.Strat_Env_52 = oneTime.Strat_Env_52.HasValue ? oneTime.Strat_Env_52 : 0;
            Entries.Strat_Env_53 = oneTime.Strat_Env_53.HasValue ? oneTime.Strat_Env_53 : 0;
            Entries.Strat_Env_54 = oneTime.Strat_Env_54.HasValue ? oneTime.Strat_Env_54 : 0;
            Entries.Strat_Env_55 = oneTime.Strat_Env_55.HasValue ? oneTime.Strat_Env_55 : 0;
            Entries.Strat_Env_56_MW = oneTime.Strat_Env_56_MW.HasValue ? oneTime.Strat_Env_56_MW : 0;
            Entries.Strat_Env_57 = oneTime.Strat_Env_57.HasValue ? oneTime.Strat_Env_57 : 0;
            Entries.Strat_Env_57Text = oneTime.Strat_Env_57Text;

            Entries.NumberProgHrsRecd = oneTime.NumberProgHrsRecd.HasValue ? oneTime.NumberProgHrsRecd : 0;
            Entries.TotalCostofProgram = oneTime.TotalCostofProgram.HasValue ? oneTime.TotalCostofProgram : 0;
            Entries.AvgCostPerParticipant = oneTime.AvgCostPerParticipant.HasValue ? oneTime.AvgCostPerParticipant : 0;
            Entries.WithinCostBand_YN = oneTime.WithinCostBand_YN.HasValue ? oneTime.WithinCostBand_YN : 0;
            Entries.EntryDate = DateTime.Now; //oneTime.EntryDate; 

            //Entries.Curriculum = 1;
            //Entries.ParentEntry = 0;
            //Entries.SessionName = "";
            //Entries.closed = 0;

            Entries.Curriculum = oneTime.Curriculum;
            Entries.ParentEntry = oneTime.ParentEntry;
            Entries.SessionName = "";
            Entries.closed = oneTime.closed;

            Entries.CoalitionMeeting = 0;// oneTime.CoalitionMeeting;
            Entries.CoalitionType = "";// oneTime.CoalitionType;

            Entries.TargetedStatewideInitiative = "";

            if (oneTime.lstStatewideInitiative.Count > 0)
            {

                foreach (var item in oneTime.lstStatewideInitiative)
                {
                    if (item.IsSelected == true)
                    {
                        if (Entries.TargetedStatewideInitiative != "")
                            Entries.TargetedStatewideInitiative = Entries.TargetedStatewideInitiative + "|" + item.initiativeName;
                        else
                            Entries.TargetedStatewideInitiative = item.initiativeName;

                    }
                }

            }

            //Entries.TargetedStatewideInitiative = "Marijuana use by adolescents|Underage Drinking|";
            Entries.NumberOfEmployees = oneTime.NumberOfEmployees;
            Entries.TravelHours = oneTime.TravelHours; ;
            Entries.DirectServiceHours = oneTime.DirectServiceHours;
            Entries.PrepHours = oneTime.PrepHours;

            Entries.DocOfActivities_Agenda = oneTime.DocOfActivities_Agenda != null ? oneTime.DocOfActivities_Agenda : 0;
            Entries.DocOfActivities_EvalForm = oneTime.DocOfActivities_EvalForm != null ? oneTime.DocOfActivities_EvalForm : 0;
            Entries.DocOfActivities_PrePostTest = oneTime.DocOfActivities_PrePostTest != null ? oneTime.DocOfActivities_PrePostTest : 0;
            Entries.DocOfActivities_ContactForm = oneTime.DocOfActivities_ContactForm != null ? oneTime.DocOfActivities_ContactForm : 0;
            Entries.DocOfActivities_SignIn = oneTime.DocOfActivities_SignIn != null ? oneTime.DocOfActivities_SignIn : 0;
            Entries.DocOfActivities_Brochure = oneTime.DocOfActivities_Brochure != null ? oneTime.DocOfActivities_Brochure : 0;
            Entries.DocOfActivities_Other = oneTime.DocOfActivities_Other;
            Entries.DocOfActivities_OtherText = "";// oneTime.DocOfActivities_OtherText;
            Entries.OverallComments = oneTime.OverallComments != null ? oneTime.OverallComments : "";

            Entries.UserGUID = oneTime.UserGUID;
            Entries.ID02_DirectoriesDistributed = oneTime.ID02_DirectoriesDistributed;
            Entries.ID04_BrochuresDistributed = oneTime.ID04_BrochuresDistributed;

            #endregion
            return Entries;
        }

        private HttpResponseMessage SaveEventDetails(string[] p)
        {


            try
            {

                Models.OneTimeEvent oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OneTimeEvent>(p[0]);

                var Entries = setvalue(oneTime);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var record = context.TblUsers.Where(f => f.UserGUID == oneTime.UserGUID).FirstOrDefault();
                    Entries.RegionCode = record.StateRegionCode;//"000008";
                    Entries.SiteCode = record.SiteCode;//"000001";
                    Entries.FiscalYear = DateTime.Now.Year.ToString(); //"2014";
                    Entries.OrgName = record.tblLocation.FriendlyName_loc;
                    Entries.StaffFname = record.FirstName;// "Mat";
                    Entries.StaffLName = record.LastName;//"Douglas";
                    Entries.Name = record.FirstName + record.LastName;//"Mat Douglas";
                    Entries.Degree = record.Degree;//"MAST";

                    context.tbl2008_NOM_Entries.Add(Entries);
                    context.SaveChanges();

                    #region "Updating Parent details"
                    if (Entries.ParentEntry > 0 && Entries.closed == 1)
                    {
                        var parentry = context.tbl2008_NOM_Entries.Where(f => f.ID == oneTime.ParentEntry).FirstOrDefault();
                        parentry.closed = 1;
                        context.SaveChanges();

                    }
                    //if (Entries.ParentEntry > 0)
                    //{
                    //    //Update paarent entry details
                    //    var parentry = context.tbl2008_NOM_Entries.Where(f => f.ID == oneTime.ParentEntry).FirstOrDefault();
                    //    parentry.SurveysEntered = false;
                    //    //parentry.StartDate = oneTime.StartDate;
                    //    // parentry.EndDate = oneTime.EndDate;
                    //    parentry.ProgramType_IP = oneTime.ProgramType_IP;
                    //    parentry.InterventionType = oneTime.InterventionType;
                    //    parentry.TyoeOfProgram_EA = oneTime.TyoeOfProgram_EA;
                    //    parentry.UniversalType_DI = oneTime.UniversalType_DI;
                    //    parentry.ProgramNameSource = oneTime.ProgramNameSource;
                    //    parentry.ProgramName = oneTime.ProgramName;
                    //    parentry.GroupName = oneTime.GroupName;

                    //    parentry.ServiceLocationName = oneTime.ServiceLocationName;
                    //    parentry.ServiceSchoolName = oneTime.ServiceSchoolName;


                    //    parentry.NumberParticipants = oneTime.NumberParticipants;
                    //    parentry.Males = oneTime.Males;
                    //    parentry.Females = oneTime.Females;

                    //    parentry.Age0 = oneTime.Age0.HasValue ? oneTime.Age0 : 0;
                    //    parentry.Age5 = oneTime.Age5.HasValue ? oneTime.Age5 : 0;
                    //    parentry.Age12 = oneTime.Age12.HasValue ? oneTime.Age12 : 0;
                    //    parentry.Age15 = oneTime.Age15.HasValue ? oneTime.Age15 : 0;
                    //    parentry.Age18 = oneTime.Age18.HasValue ? oneTime.Age18 : 0;
                    //    parentry.Age21 = oneTime.Age21.HasValue ? oneTime.Age21 : 0;
                    //    parentry.Age25 = oneTime.Age25.HasValue ? oneTime.Age25 : 0;
                    //    parentry.Age45 = oneTime.Age45.HasValue ? oneTime.Age45 : 0;
                    //    parentry.Age65 = oneTime.Age65.HasValue ? oneTime.Age65 : 0;

                    //    parentry.RaceWhite = oneTime.RaceWhite.HasValue ? oneTime.RaceWhite : 0;
                    //    parentry.RaceBlack = oneTime.RaceBlack.HasValue ? oneTime.RaceBlack : 0;
                    //    parentry.RaceHawaii = oneTime.RaceHawaii.HasValue ? oneTime.RaceHawaii : 0;
                    //    parentry.RaceAsian = oneTime.RaceAsian.HasValue ? oneTime.RaceAsian : 0;
                    //    parentry.RaceAmIndian = oneTime.RaceAmIndian.HasValue ? oneTime.RaceAmIndian : 0;
                    //    parentry.RaceUnknownOther = oneTime.RaceUnknownOther.HasValue ? oneTime.RaceUnknownOther : 0;
                    //    parentry.RaceMoreThanOne = oneTime.RaceMoreThanOne.HasValue ? oneTime.RaceMoreThanOne : 0;

                    //    parentry.NotHispanic = oneTime.NotHispanic.HasValue ? oneTime.NotHispanic : 0;
                    //    parentry.Hispanic = oneTime.Hispanic.HasValue ? oneTime.Hispanic : 0;

                    //    // modification
                    //    parentry.D_HiRi_ChildOfSA = oneTime.D_HiRi_ChildOfSA.HasValue ? oneTime.D_HiRi_ChildOfSA : 0; // Convert.ToInt32(p[33]);
                    //    parentry.D_HiRi_PregUse = oneTime.D_HiRi_PregUse.HasValue ? oneTime.D_HiRi_PregUse : 0; //null;
                    //    parentry.D_HiRi_K12DO = oneTime.D_HiRi_K12DO.HasValue ? oneTime.D_HiRi_K12DO : 0; //null;

                    //    parentry.D_HiRi_Violent = oneTime.D_HiRi_Violent.HasValue ? oneTime.D_HiRi_Violent : 0; //Convert.ToInt32(p[34]);
                    //    parentry.D_HiRi_MentH = oneTime.D_HiRi_MentH.HasValue ? oneTime.D_HiRi_MentH : 0; //Convert.ToInt32(p[38]);
                    //    parentry.D_HiRi_EconDis = oneTime.D_HiRi_EconDis.HasValue ? oneTime.D_HiRi_EconDis : 0; //Convert.ToInt32(p[41]);
                    //    parentry.D_HiRi_PhysDis = oneTime.D_HiRi_PhysDis.HasValue ? oneTime.D_HiRi_PhysDis : 0; //Convert.ToInt32(p[35]);
                    //    parentry.D_HiRi_AbuVict = oneTime.D_HiRi_AbuVict.HasValue ? oneTime.D_HiRi_AbuVict : 0; //Convert.ToInt32(p[39]);
                    //    parentry.D_HiRi_Using = oneTime.D_HiRi_Using.HasValue ? oneTime.D_HiRi_Using : 0; //null;
                    //    parentry.D_HiRi_Homel = oneTime.D_HiRi_Homel.HasValue ? oneTime.D_HiRi_Homel : 0; //Convert.ToInt32(p[36]);

                    //    parentry.D_HiRi_FrLun = oneTime.D_HiRi_FrLun.HasValue ? oneTime.D_HiRi_FrLun : 0; //null;
                    //    parentry.D_HiRi_Other = oneTime.D_HiRi_Other.HasValue ? oneTime.D_HiRi_Other : 0; //null;             
                    //    parentry.D_HiRi_NotAp = oneTime.D_HiRi_NotAp.HasValue ? oneTime.D_HiRi_NotAp : 0; //null;

                    //    parentry.Strat_ID_01 = oneTime.Strat_ID_01.HasValue ? oneTime.Strat_ID_01 : 0; //Convert.ToBoolean(p[45]) ? 1 : 0;

                    //    parentry.Strat_ID_02 = oneTime.Strat_ID_02.HasValue ? oneTime.Strat_ID_02 : 0; //Convert.ToBoolean(p[46]) ? 1 : 0;

                    //    parentry.Strat_ID_03 = oneTime.Strat_ID_03.HasValue ? oneTime.Strat_ID_03 : 0;//Convert.ToBoolean(p[47]) ? 1 : 0;
                    //    parentry.Strat_ID_04 = oneTime.Strat_ID_04.HasValue ? oneTime.Strat_ID_04 : 0;//Convert.ToBoolean(p[48]) ? 1 : 0;
                    //    parentry.Strat_ID_05 = oneTime.Strat_ID_05.HasValue ? oneTime.Strat_ID_05 : 0;//Convert.ToBoolean(p[49]) ? 1 : 0;
                    //    parentry.Strat_ID_06 = oneTime.Strat_ID_06.HasValue ? oneTime.Strat_ID_06 : 0; //Convert.ToBoolean(p[50]) ? 1 : 0;
                    //    parentry.Strat_ID_07 = oneTime.Strat_ID_07.HasValue ? oneTime.Strat_ID_07 : 0; //Convert.ToBoolean(p[51]) ? 1 : 0;
                    //    parentry.Strat_ID_08 = oneTime.Strat_ID_08.HasValue ? oneTime.Strat_ID_08 : 0; //Convert.ToBoolean(p[52]) ? 1 : 0;
                    //    parentry.Strat_ID_09 = oneTime.Strat_ID_09.HasValue ? oneTime.Strat_ID_09 : 0; //Convert.ToBoolean(p[53]) ? 1 : 0;
                    //    parentry.Strat_ID_09Text = oneTime.Strat_ID_09Text; //p[54];

                    //    parentry.Strat_Ed_11 = oneTime.Strat_Ed_11.HasValue ? oneTime.Strat_Ed_11 : 0; //Convert.ToBoolean(p[55]) ? 1 : 0;
                    //    parentry.Strat_Ed_12 = oneTime.Strat_Ed_12.HasValue ? oneTime.Strat_Ed_12 : 0; //Convert.ToBoolean(p[56]) ? 1 : 0;
                    //    parentry.Strat_Ed_13 = oneTime.Strat_Ed_13.HasValue ? oneTime.Strat_Ed_13 : 0;//Convert.ToBoolean(p[57]) ? 1 : 0;
                    //    parentry.Strat_Ed_14 = oneTime.Strat_Ed_14.HasValue ? oneTime.Strat_Ed_14 : 0;//Convert.ToBoolean(p[58]) ? 1 : 0;
                    //    parentry.Strat_Ed_15 = oneTime.Strat_Ed_15.HasValue ? oneTime.Strat_Ed_15 : 0;//Convert.ToBoolean(p[59]) ? 1 : 0;
                    //    parentry.Strat_Ed_16 = oneTime.Strat_Ed_16.HasValue ? oneTime.Strat_Ed_16 : 0;//Convert.ToBoolean(p[60]) ? 1 : 0;
                    //    parentry.Strat_Ed_17 = oneTime.Strat_Ed_17.HasValue ? oneTime.Strat_Ed_17 : 0;//Convert.ToBoolean(p[61]) ? 1 : 0;
                    //    parentry.Strat_Ed_17Text = oneTime.Strat_Ed_17Text; //p[62];

                    //    parentry.Strat_Alt_21 = oneTime.Strat_Alt_21.HasValue ? oneTime.Strat_Alt_21 : 0; //Convert.ToBoolean(p[63]) ? 1 : 0;
                    //    parentry.Strat_Alt_22 = oneTime.Strat_Alt_22.HasValue ? oneTime.Strat_Alt_22 : 0;//Convert.ToBoolean(p[64]) ? 1 : 0;
                    //    parentry.Strat_Alt_23 = oneTime.Strat_Alt_23.HasValue ? oneTime.Strat_Alt_23 : 0;//Convert.ToBoolean(p[65]) ? 1 : 0;
                    //    parentry.Strat_Alt_24 = oneTime.Strat_Alt_24.HasValue ? oneTime.Strat_Alt_24 : 0;//Convert.ToBoolean(p[66]) ? 1 : 0;
                    //    parentry.Strat_Alt_25 = oneTime.Strat_Alt_25.HasValue ? oneTime.Strat_Alt_25 : 0;//Convert.ToBoolean(p[67]) ? 1 : 0;
                    //    parentry.Strat_Alt_26 = oneTime.Strat_Alt_26.HasValue ? oneTime.Strat_Alt_26 : 0;//Convert.ToBoolean(p[68]) ? 1 : 0;
                    //    parentry.Strat_Alt_27 = oneTime.Strat_Alt_27.HasValue ? oneTime.Strat_Alt_27 : 0; //Convert.ToBoolean(p[69]) ? 1 : 0;
                    //    parentry.Strat_Alt_27Text = oneTime.Strat_Alt_27Text; //p[70];

                    //    parentry.Strat_IR_31 = oneTime.Strat_IR_31.HasValue ? oneTime.Strat_IR_31 : 0; ; //Convert.ToBoolean(p[71]) ? 1 : 0;
                    //    parentry.Strat_IR_32 = oneTime.Strat_IR_32.HasValue ? oneTime.Strat_IR_32 : 0; ; //Convert.ToBoolean(p[72]) ? 1 : 0;
                    //    parentry.Strat_IR_34 = oneTime.Strat_IR_34.HasValue ? oneTime.Strat_IR_34 : 0; ; //Convert.ToBoolean(p[73]) ? 1 : 0;
                    //    parentry.Strat_IR_34Text = oneTime.Strat_IR_34Text; //p[74];

                    //    parentry.Strat_CBP_41 = oneTime.Strat_CBP_41.HasValue ? oneTime.Strat_CBP_41 : 0;
                    //    parentry.Strat_CBP_42 = oneTime.Strat_CBP_42.HasValue ? oneTime.Strat_CBP_42 : 0;
                    //    parentry.Strat_CBP_43 = oneTime.Strat_CBP_43.HasValue ? oneTime.Strat_CBP_43 : 0;
                    //    parentry.Strat_CBP_44 = oneTime.Strat_CBP_44.HasValue ? oneTime.Strat_CBP_44 : 0;
                    //    parentry.Strat_CBP_45 = oneTime.Strat_CBP_45.HasValue ? oneTime.Strat_CBP_45 : 0;
                    //    parentry.Strat_CBP_46 = oneTime.Strat_CBP_46.HasValue ? oneTime.Strat_CBP_46 : 0;
                    //    parentry.Strat_CBP_46Text = oneTime.Strat_CBP_46Text;

                    //    parentry.Strat_Env_51 = oneTime.Strat_Env_51.HasValue ? oneTime.Strat_Env_51 : 0;
                    //    parentry.Strat_Env_52 = oneTime.Strat_Env_52.HasValue ? oneTime.Strat_Env_52 : 0;
                    //    parentry.Strat_Env_53 = oneTime.Strat_Env_53.HasValue ? oneTime.Strat_Env_53 : 0;
                    //    parentry.Strat_Env_54 = oneTime.Strat_Env_54.HasValue ? oneTime.Strat_Env_54 : 0;
                    //    parentry.Strat_Env_55 = oneTime.Strat_Env_55.HasValue ? oneTime.Strat_Env_55 : 0;
                    //    parentry.Strat_Env_56_MW = oneTime.Strat_Env_56_MW.HasValue ? oneTime.Strat_Env_56_MW : 0;
                    //    parentry.Strat_Env_57 = oneTime.Strat_Env_57.HasValue ? oneTime.Strat_Env_57 : 0;
                    //    parentry.Strat_Env_57Text = oneTime.Strat_Env_57Text;

                    //    parentry.NumberProgHrsRecd = oneTime.NumberProgHrsRecd.HasValue ? oneTime.NumberProgHrsRecd : 0;
                    //    parentry.TotalCostofProgram = oneTime.TotalCostofProgram.HasValue ? oneTime.TotalCostofProgram : 0;
                    //    parentry.AvgCostPerParticipant = oneTime.AvgCostPerParticipant.HasValue ? oneTime.AvgCostPerParticipant : 0;
                    //    parentry.WithinCostBand_YN = oneTime.WithinCostBand_YN.HasValue ? oneTime.WithinCostBand_YN : 0;
                    //    parentry.EntryDate = DateTime.Now;

                    //    parentry.Curriculum = oneTime.Curriculum;
                    //    parentry.ParentEntry = 0;
                    //    //Entries.SessionName = "";
                    //    parentry.closed = oneTime.closed;

                    //    parentry.CoalitionMeeting = 0;// oneTime.CoalitionMeeting;
                    //    parentry.CoalitionType = "";// oneTime.CoalitionType;
                    //    //parentry.TargetedStatewideInitiative = "Marijuana use by adolescents|Underage Drinking|";

                    //    parentry.TargetedStatewideInitiative = "";

                    //    if (oneTime.lstStatewideInitiative.Count > 0)
                    //    {

                    //        foreach (var item in oneTime.lstStatewideInitiative)
                    //        {
                    //            if (item.IsSelected == true)
                    //            {
                    //                if (parentry.TargetedStatewideInitiative != "")
                    //                    parentry.TargetedStatewideInitiative = parentry.TargetedStatewideInitiative + "|" + item.initiativeName;
                    //                else
                    //                    parentry.TargetedStatewideInitiative = item.initiativeName;

                    //            }
                    //        }

                    //    }

                    //    parentry.NumberOfEmployees = oneTime.NumberOfEmployees;
                    //    parentry.TravelHours = oneTime.TravelHours; ;
                    //    parentry.DirectServiceHours = oneTime.DirectServiceHours;
                    //    parentry.PrepHours = oneTime.PrepHours;

                    //    parentry.DocOfActivities_Agenda = oneTime.DocOfActivities_Agenda != null ? oneTime.DocOfActivities_Agenda : 0;
                    //    parentry.DocOfActivities_EvalForm = oneTime.DocOfActivities_EvalForm != null ? oneTime.DocOfActivities_EvalForm : 0;
                    //    parentry.DocOfActivities_PrePostTest = oneTime.DocOfActivities_PrePostTest != null ? oneTime.DocOfActivities_PrePostTest : 0;
                    //    parentry.DocOfActivities_ContactForm = oneTime.DocOfActivities_ContactForm != null ? oneTime.DocOfActivities_ContactForm : 0;
                    //    parentry.DocOfActivities_SignIn = oneTime.DocOfActivities_SignIn != null ? oneTime.DocOfActivities_SignIn : 0;
                    //    parentry.DocOfActivities_Brochure = oneTime.DocOfActivities_Brochure != null ? oneTime.DocOfActivities_Brochure : 0;
                    //    parentry.DocOfActivities_Other = oneTime.DocOfActivities_Other;
                    //    parentry.DocOfActivities_OtherText = "";// oneTime.DocOfActivities_OtherText;
                    //    parentry.OverallComments = oneTime.OverallComments != null ? oneTime.OverallComments : "";
                    //    parentry.ID02_DirectoriesDistributed = oneTime.ID02_DirectoriesDistributed;
                    //    parentry.ID04_BrochuresDistributed = oneTime.ID04_BrochuresDistributed;

                    //    context.SaveChanges();

                    //}
                    #endregion


                }

                return this.Request.CreateResponse(HttpStatusCode.OK, Entries);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetSessions(string[] p)
        {
            try
            {

                int ParentEventID = Convert.ToInt32(p[0]);

                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbEvents = await context.tbl2008_NOM_Entries.Where(x => x.UserGUID == newUserId && x.Curriculum=0).ToListAsync();
                    var dbEvents = context.suretool_GetChildCurriculums(ParentEventID);
                    foreach (var oneevent in dbEvents)
                    {
                        events.Add(
                            new OneTimeEvent { Program = oneevent.ProgramName, Organization = oneevent.OrgName, BeginDate = oneevent.StartDate, EndDate = oneevent.EndDate, Group = oneevent.GroupName, ID = oneevent.ID }
                            );
                    }



                    foreach (OneTimeEvent o in events)
                    {
                        o.PreTestCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 1).Count();
                        o.PostTetCount = context.SurveyItems.Where(x => x.EventID == o.ID && x.SurveyTypeID == 2).Count();
                        o.SurveyCount = context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount.HasValue ? context.tbl2008_NOM_Entries.Where(x => x.ID == o.ID).FirstOrDefault().SurveyCount : 0;
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, events);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        private HttpResponseMessage GetEventByEventId(string[] p)
        {
            try
            {
                var events = new OneTimeEvent();
                int EventId = Convert.ToInt32(p[0]);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    events = context.suretool_GetCurriculum(EventId).Select(oneevent => new OneTimeEvent
                    {
                        Program = oneevent.ProgramName,
                        Organization = oneevent.OrgName,
                        BeginDate = oneevent.StartDate,
                        EndDate = oneevent.EndDate,
                        Group = oneevent.GroupName,
                        ServiceLocationName = context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault() == null ? "" : context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault().ServiceLocationName,
                        ServiceSchoolName = context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault() == null ? "" : context.tbl2008_NOM_Entries.Where(x => x.ID == oneevent.ID).FirstOrDefault().ServiceSchoolName,

                        //lstEvents = GetChildEvents(oneevent.ID),

                        Age0 = oneevent.Age0,
                        Age12 = oneevent.Age12,
                        Age15 = oneevent.Age15,
                        Age18 = oneevent.Age18,
                        Age21 = oneevent.Age21,
                        Age25 = oneevent.Age25,
                        Age45 = oneevent.Age45,
                        Age5 = oneevent.Age5,
                        Age65 = oneevent.Age65,
                        AvgCostPerParticipant = oneevent.AvgCostPerParticipant,
                        closed = oneevent.closed,
                        CoalitionMeeting = oneevent.CoalitionMeeting,
                        CoalitionType = oneevent.CoalitionType,
                        Curriculum = oneevent.Curriculum,
                        D_HiRi_AbuVict = oneevent.D_HiRi_AbuVict,
                        D_HiRi_ChildOfSA = oneevent.D_HiRi_ChildOfSA,
                        D_HiRi_EconDis = oneevent.D_HiRi_EconDis,
                        D_HiRi_FrLun = oneevent.D_HiRi_FrLun,
                        D_HiRi_Homel = oneevent.D_HiRi_Homel,
                        D_HiRi_K12DO = oneevent.D_HiRi_K12DO,
                        D_HiRi_MentH = oneevent.D_HiRi_MentH,
                        D_HiRi_NotAp = oneevent.D_HiRi_NotAp,
                        D_HiRi_Other = oneevent.D_HiRi_Other,
                        D_HiRi_OtherText = oneevent.D_HiRi_OtherText,
                        D_HiRi_PhysDis = oneevent.D_HiRi_PhysDis,
                        D_HiRi_PregUse = oneevent.D_HiRi_PregUse,
                        D_HiRi_Using = oneevent.D_HiRi_Using,
                        D_HiRi_Violent = oneevent.D_HiRi_Violent,
                        Degree = oneevent.Degree,
                        DirectServiceHours = oneevent.DirectServiceHours,
                        DocOfActivities_Agenda = oneevent.DocOfActivities_Agenda,
                        DocOfActivities_Brochure = oneevent.DocOfActivities_Brochure,
                        DocOfActivities_ContactForm = oneevent.DocOfActivities_ContactForm,
                        DocOfActivities_EvalForm = oneevent.DocOfActivities_EvalForm,
                        DocOfActivities_Other = oneevent.DocOfActivities_Other,
                        DocOfActivities_OtherText = oneevent.DocOfActivities_OtherText,
                        DocOfActivities_PrePostTest = oneevent.DocOfActivities_PrePostTest,
                        DocOfActivities_SignIn = oneevent.DocOfActivities_SignIn,
                        EntryDate = oneevent.EntryDate,
                        Females = oneevent.Females,
                        FiscalYear = oneevent.FiscalYear,
                        GroupName = oneevent.GroupName,
                        Hispanic = oneevent.Hispanic,
                        ID = oneevent.ID,
                        ID02_DirectoriesDistributed = oneevent.ID02_DirectoriesDistributed,
                        ID04_BrochuresDistributed = oneevent.ID04_BrochuresDistributed,
                        InterventionType = oneevent.InterventionType,
                        Males = oneevent.Males,
                        Name = oneevent.Name,
                        NotHispanic = oneevent.NotHispanic,
                        NumberOfEmployees = oneevent.NumberOfEmployees,
                        NumberParticipants = oneevent.NumberParticipants,
                        NumberProgHrsRecd = oneevent.NumberProgHrsRecd,
                        OrgName = oneevent.OrgName,
                        OverallComments = oneevent.OverallComments,
                        ParentEntry = oneevent.ParentEntry,
                        PrepHours = oneevent.PrepHours,
                        ProgramName = oneevent.ProgramName,
                        ProgramNameSource = oneevent.ProgramNameSource,

                        ProgramId = context.Programs.Where(x => x.Type == oneevent.ProgramNameSource.Trim() && x.Name == oneevent.ProgramName.Trim()).Select(x => x.ID).SingleOrDefault(),
                        ProgramType_IP = oneevent.ProgramType_IP,
                        RaceAmIndian = oneevent.RaceAmIndian,
                        RaceAsian = oneevent.RaceAsian,
                        RaceBlack = oneevent.RaceBlack,
                        RaceHawaii = oneevent.RaceHawaii,
                        RaceMoreThanOne = oneevent.RaceMoreThanOne,
                        RaceUnknownOther = oneevent.RaceUnknownOther,
                        RaceWhite = oneevent.RaceWhite,
                        RegionCode = oneevent.RegionCode,
                        SessionName = oneevent.SessionName,
                        SiteCode = oneevent.SiteCode,
                        StaffFname = oneevent.StaffFname,
                        StaffLName = oneevent.StaffLName,
                        StartDate = oneevent.StartDate,
                        Strat_Alt_21 = oneevent.Strat_Alt_21,
                        Strat_Alt_22 = oneevent.Strat_Alt_22,
                        Strat_Alt_23 = oneevent.Strat_Alt_23,
                        Strat_Alt_24 = oneevent.Strat_Alt_24,
                        Strat_Alt_25 = oneevent.Strat_Alt_25,
                        Strat_Alt_26 = oneevent.Strat_Alt_26,
                        Strat_Alt_27 = oneevent.Strat_Alt_27,
                        Strat_Alt_27Text = oneevent.Strat_Alt_27Text,
                        Strat_CBP_41 = oneevent.Strat_CBP_41,
                        Strat_CBP_42 = oneevent.Strat_CBP_42,
                        Strat_CBP_43 = oneevent.Strat_CBP_43,
                        Strat_CBP_44 = oneevent.Strat_CBP_44,
                        Strat_CBP_45 = oneevent.Strat_CBP_45,

                        Strat_CBP_Faith = oneevent.Strat_CBP_Faith,

                        Strat_CBP_46 = oneevent.Strat_CBP_46,
                        Strat_CBP_46Text = oneevent.Strat_CBP_46Text,
                        Strat_Ed_11 = oneevent.Strat_Ed_11,
                        Strat_Ed_12 = oneevent.Strat_Ed_12,
                        Strat_Ed_13 = oneevent.Strat_Ed_13,
                        Strat_Ed_14 = oneevent.Strat_Ed_14,
                        Strat_Ed_15 = oneevent.Strat_Ed_15,
                        Strat_Ed_16 = oneevent.Strat_Ed_16,
                        Strat_Ed_17 = oneevent.Strat_Ed_17,
                        Strat_Ed_17Text = oneevent.Strat_Ed_17Text,
                        Strat_Env_51 = oneevent.Strat_Env_51,
                        Strat_Env_52 = oneevent.Strat_Env_52,
                        Strat_Env_53 = oneevent.Strat_Env_53,
                        Strat_Env_54 = oneevent.Strat_Env_54,
                        Strat_Env_55 = oneevent.Strat_Env_55,
                        Strat_Env_56_MW = oneevent.Strat_Env_56_MW,
                        Strat_Env_57 = oneevent.Strat_Env_57,
                        Strat_Env_57Text = oneevent.Strat_Env_57Text,
                        Strat_ID_01 = oneevent.Strat_ID_01,
                        Strat_ID_02 = oneevent.Strat_ID_02,
                        Strat_ID_03 = oneevent.Strat_ID_03,
                        Strat_ID_04 = oneevent.Strat_ID_04,
                        Strat_ID_05 = oneevent.Strat_ID_05,
                        Strat_ID_06 = oneevent.Strat_ID_06,
                        Strat_ID_07 = oneevent.Strat_ID_07,
                        Strat_ID_08 = oneevent.Strat_ID_08,
                        Strat_ID_09 = oneevent.Strat_ID_09,
                        Strat_ID_09Text = oneevent.Strat_ID_09Text,
                        Strat_IR_31 = oneevent.Strat_IR_31,
                        Strat_IR_32 = oneevent.Strat_IR_32,
                        Strat_IR_34 = oneevent.Strat_IR_34,
                        Strat_IR_34Text = oneevent.Strat_IR_34Text,
                        SurveysEntered = oneevent.SurveysEntered,
                        TargetedStatewideInitiative = oneevent.TargetedStatewideInitiative,
                        TotalCostofProgram = oneevent.TotalCostofProgram,
                        TravelHours = oneevent.TravelHours,
                        TyoeOfProgram_EA = oneevent.TyoeOfProgram_EA,
                        UniversalType_DI = oneevent.UniversalType_DI,
                        UserGUID = oneevent.UserGUID,
                        WithinCostBand_YN = oneevent.WithinCostBand_YN
                    }).SingleOrDefault();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, events);

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
