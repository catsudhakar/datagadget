﻿(function () {
    'use strict';
    var controllerId = 'comments';   //ongoingEvents ng cotroller in ongoingevents.html
    angular
        .module('app')
        .controller(controllerId, comments);
    comments.$inject = ['$scope', '$http', '$location', 'common', '$modal', '$window'];


    function comments($scope, $http, $location, common, $modal, $window) {

        $scope.ManagecommentsSubmit = false;
        var userid = localStorage.getItem('userId');
        var userInfo = JSON.parse(localStorage.getItem('userInfo'));
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'Comments';
        var vm = this;
        var userInfo = JSON.parse($window.localStorage["userInfo"]);

        $scope.typeFollowup = '';


        $scope.isNoRecords = false;
        vm.commentList = '';
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        activate();

        function activate() {

            $scope.typeFollowup = "Inbox";
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.pkey == 0) {
                $location.path('/dashboard');
            }
            paging(1);

        }

        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $http({ method: 'POST', url: 'api/Comments', data: { method: "GetAllComments", parameters: [userInfo.pkey,pageId] } })
              .success(function (result) {
                $scope.ispaging = false;
                vm.pagerData = result.item1[0];
                if (parseInt(vm.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                vm.pageList = [];
                if (pageId > 3) {
                    vm.pageList.push(pageId - 2);
                    vm.pageList.push(pageId - 1);
                    vm.pageList.push(pageId);
                    if (pageId < vm.pagerData.numberOfPages) {
                        vm.pageList.push(pageId + 1);
                        if (pageId < (vm.pagerData.numberOfPages - 1)) {
                            vm.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);

                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        vm.pageList.push(pageId - 2);
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                vm.commentList = userData;
                if (userData.length == 0) {
                    vm.isNoRecords = true;
                }
                else {
                    vm.isNoRecords = false;
                }

            })
         .error(function (error) {
             console.log(error);
         })
        }

      $scope.Finish = function (commentsForm) {
            $scope.ManagecommentsSubmit = true;
            vm.Email = userInfo.emailId;
            if (vm.Comments != undefined) {
                $http({
                    method: 'POST', url: 'api/Comments',
                    data: { method: "SendComments", parameters: [vm.Email, vm.Comments, userid, vm.Subject] }
                })
                .success(function (result) {
                    log("Your comment has been sent.");
                    $location.path('/dashboard');
                    //vm.Comments = "";                    
                })
                .error(function (error) {
                    console.log(error);
                })
            }
        };

        $scope.deleteComment = function (singlecomment) {
            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/Comments', data: { method: "DeleteComment", parameters: [singlecomment.id] } })
            .success(function (result) {

                var i = vm.commentList.indexOf(singlecomment);
                if (i != -1) {
                    vm.commentList.splice(i, 1);
                }
                log("Comment deleted successfully");
            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {
            });
        };

        $scope.respondComment = function (singlecomment) {
            var editModal = $modal.open({
                templateUrl: 'app/admin/newRespondComment.html',
                controller: 'newRespondComment',
                backdrop: 'static',
                resolve: {
                    respondInfo: function () {
                        return singlecomment;
                    }
                }
            });
            editModal.result.then(function (singlecomment) {
                activate();
            }, function () {

            });
        }
        $scope.loadConversations = function (e, singlecomment) {


            $(e.target).parent().parent().next().toggleClass("expand")
            $(e.target).toggleClass('nestedBtn nestedBtnExpand');
            getConversations(singlecomment);


        }

        function getConversations(singlecomment) {
            $http({ method: 'POST', url: 'api/Comments', data: { method: "GetCommentsByParentId", parameters: [singlecomment.parentId] } })
              .success(function (result) {

                  singlecomment.conversationList = result;
              })
            .error(function (error) {
                logError(error);
            })
        }

    }
})();










