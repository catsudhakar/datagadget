﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class OngoingEvent
    {
        public string Program { get; set; }

        public string Group { get; set; }

        public string Organization { get; set; }

        public DateTime ? BeginDate { get; set; }

        public DateTime ? EndDate { get; set; }
    }
}