﻿

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace DataGadget.ScheduledTaskRunner
{
    class SchedulerProgram
    {
        static void Main(string[] args)
        {
           
            if (args.Length == 0)
            {
                Logger.Warn("ScheduledTaskRunner ran without any parameters.");
                return;
            }

            try
            {
                // the first parameter must be the tag (e.g. hourly, nightly, monthly)
                var tag = args[0];

                var parameters = new List<string>(args);
                parameters.RemoveAt(0); // remove this and pass the rest to the actual runner as parameters

                // get and run the specified runner passing in any command line params that were received
                TaskRunnerFactory.Run(tag, parameters.ToArray());

                
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}