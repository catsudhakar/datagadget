﻿(function () {
    'use strict';
    var controllerId = 'emailTemplate';
    angular
        .module('app')
        .controller(controllerId, emailTemplate);
    emailTemplate.$inject = ['$scope', '$http', '$location', 'common', '$modal', '$route','$window'];
    function emailTemplate($scope, $http, $location, common, $modal, $route, $window) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'Manage Email Templates';
        var userid = localStorage.getItem('userId');
        var vm = this;
        vm.records = [];
        activate();
        function activate() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level != "Admin") {
                $location.path('/dashboard');
            }
            $http({ method: 'POST', url: 'api/EmailTemplate', data: { method: "AllRecords", parameters: [userInfo.level] } })
               .success(function (result) {
                   vm.records = result;
               })
               .error(function (error) {
                   console.log(error);
               })
        }
        $scope.vm.editEvent = function (event) {

            var addEmailTemplateModal = $modal.open({
                templateUrl: 'app/admin/newEmailTemplate.html',
                controller: 'newEmailTemplate',
                resolve: {
                    emailInfo: function () {
                        return event;
                    }
                }
            });
        }
    }
        
    })();


