﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Configuration;

using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;


namespace  DataGadget.Web.Reports
{
    public class Report_12B : Report_Base
    {


        public Report_12B()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;


        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }

        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;


            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Table 12b - Population-Based Programs and Strategies - Number of Persons Served By Age, Gender, Race, and Ethnicity"));

            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);
            //PdfPCell cell;

            //table.WidthPercentage = 100;

            //float[] headerwidths = { 100 }; // percentage
            //table.SetWidths(headerwidths);

            //cell = new PdfPCell(ResultsTable(this.ReportSurveyID));
            //cell.Border = Rectangle.NO_BORDER;
            //table.AddCell(cell);


            this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataTable dtData = new DataTable();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_12B ");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");
           

            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dtData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dtData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;


            cell = new PdfPCell(NewPhrase("Category", Font.HELVETICA, 8, Font.BOLD));
            //cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("", Font.HELVETICA, 8, Font.BOLD));
            //cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            table.HeaderRows = 1;

            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("A. Age", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("0-4"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age0"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("5-11"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age5"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("12-14"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age12"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("15-17"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age15"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("18-20"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age18"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("21-24"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age21"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("25-44"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age25"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("45-64"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age45"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("65 And Over"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["age65"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Age Not Known"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("B. Gender", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Male"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["males"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Female"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["females"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Gender Not Known"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("C. Race", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;


            cell = new PdfPCell(NewPhrase("White"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["racewhite"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Black or African-American"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["raceblack"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Native Hawaiian/Other Pacific Islander"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["racehawaii"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Asian"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["raceasian"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("American Indian/Alaska Native"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["raceamindian"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("More Than One Race"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["racemorethanone"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Race Not Known or Other"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["raceunknownother"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("D. Ethnicity", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Hispanic or Latino"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["hispanic"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Not Hispanic or Latino"));
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["nothispanic"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            //---------------------------------------------------------------------------------------------------

            return table;
        }


    }
}
