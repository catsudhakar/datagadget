﻿using DataGadget.Web.DataModel;
using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DataGadget.Web.Pager;
using System.Globalization;
using System.Text;
using DREAMTool;

using Microsoft.VisualBasic;

namespace DataGadget.Web.Controllers
{
    public class CommonController : ApiController
    {
        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "DashBoard":
                        return await DashBoard(param.Parameters);
                    case "AdminDashBoard":
                        return await AdminDashBoard(param.Parameters);
                    //case "TreatmentDashBoard":
                    //    return await TreatmentDashBoard(param.Parameters);SetFollowUpdates
                    case "UserTreatmentDashBoard":
                        return await UserTreatmentDashBoard(param.Parameters);

                    case "ContactsDashBoard":
                        return await ContactsDashBoard(param.Parameters);
                    case "SetFollowUpdates":
                        return SetFollowUpdates(param.Parameters);

                    case "GetFollowUpdatesByPatId":
                        return await GetFollowUpdatesByPatId(param.Parameters);

                    case "CheckUserHours":
                        return CheckUserHours(param.Parameters);

                    case "GetWeekllyEmailData":
                        return await GetWeekllyEmailData(param.Parameters);


                    case "SendEmailTreatmentUser":
                        return await SendEmailTreatmentUser(param.Parameters);

                    //case "GetEventsCount":
                    //    return GetEventsCount(param.Parameters);

                    case "FollowUpContactsReport":
                        return await FollowUpContactsReport(param.Parameters);

                    case "IndigentFundReport":
                        return await IndigentFundReport(param.Parameters);
                    case "BindAgencies":
                        return await BindAgencies(param.Parameters);

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
                //WebLogger.Error(ex.Message);
                // WebLogger.Error("Exception In CommonController : " + ex.Message);   
                //return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<HttpResponseMessage> BindAgencies(string[] p)
        {
            try
            {

                IEnumerable<AgencyType> dataSource = (IEnumerable<AgencyType>)new List<AgencyType>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    dataSource = await context.tblLocations.Where(x => x.IsActive == true).OrderBy(x => x.FriendlyName_loc).
                        Select(x =>
                            new AgencyType
                            {
                                ID = x.PKey.ToString(),
                                Name = x.FriendlyName_loc
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage CheckUserHours(string[] p)
        {
            try
            {

                int userId = Convert.ToInt32(p[0]);
                bool isExist = false;
                FollowUpContact tblFollowUpContact = new FollowUpContact();
                using (datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var grantYear = context.GrantYears.FirstOrDefault();
                    var startDate = grantYear.StartDate;//.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate;//.ToString("MM/dd/yyyy", new CultureInfo("en-US"));

                    //                    var query = @"SELECT Sum(CAST(Hours AS decimal)) FROM tblStaffHours 
                    //                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserID = '" + userId + "'" ;

                    //var targetHours = context.tblStaffHours.Where(x => x.UserID == userId && x.StartDate >= startDate && x.EndDate <= endDate).FirstOrDefault();

                    var targetHours = context.TblUsers.Where(x => x.PKey == userId).Select(x => x.Hours).Single();



                    if (targetHours != null)
                    {
                        isExist = true;
                    }


                }

                return this.Request.CreateResponse(HttpStatusCode.OK, isExist);




            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage SetFollowUpdates(string[] p)
        {
            try
            {

                Models.FollowUpContactModel followUp = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.FollowUpContactModel>(p[0]);
                FollowUpContact tblFollowUpContact = new FollowUpContact();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    if (followUp.Id == 0)
                    {
                        tblFollowUpContact.PatientId = followUp.PatientId;
                        tblFollowUpContact.FollowupDate = followUp.FollowupDate;
                        tblFollowUpContact.FollowupComment = followUp.FollowupComment;
                        tblFollowUpContact.FollowupId = followUp.FollowupId;
                        tblFollowUpContact.SurveyItemId = followUp.SurveyItemId;

                        context.FollowUpContacts.Add(tblFollowUpContact);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {

                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, followUp);



            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetFollowUpdatesByPatId(string[] parameter)
        {


            List<FollowUpContactModel> lstBedCount = new List<FollowUpContactModel>();

            FollowUpContactModel model;

            var PatId = parameter[0];
            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {


                    var dataResult = await dbContext.FollowUpContacts.Where(x => x.PatientId == PatId).ToListAsync();

                    if (dataResult != null)
                    {
                        foreach (var item in dataResult)
                        {
                            model = new FollowUpContactModel();
                            model.PatientId = item.PatientId;
                            model.FollowupDate = item.FollowupDate;
                            model.FollowupComment = item.FollowupComment;
                            lstBedCount.Add(model);
                        }
                    }

                    return this.Request.CreateResponse(HttpStatusCode.OK, lstBedCount);
                }
            }
            catch (Exception)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, lstBedCount);
            }
        }

        async private Task<HttpResponseMessage> DashBoard(string[] parameter)
        {
            //NOMEntriesModel model = new NOMEntriesModel();

            ChartUserModel model = new ChartUserModel();

            var userID = new Guid(parameter[0]);
            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    var grantYear = dbContext.GrantYears.FirstOrDefault();

                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));

                    //                    var query = @"SELECT sum(TravelHours) as SumOfTravelHours ,sum(DirectServiceHours) as SumOfServiceHours
                    //                                        ,sum(PrepHours) as SumOfPrepHours,StartDate,EndDate
                    //                                        FROM tbl2008_NOM_Entries 
                    //                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserGUID = '" + userID + "'";

                    var query = @"SELECT COALESCE(sum(TravelHours),0) as SumOfTravelHours ,COALESCE(sum(DirectServiceHours),0) as SumOfServiceHours
                                        ,COALESCE(sum(PrepHours),0) as SumOfPrepHours,TyoeOfProgram_EA
                                        FROM tbl2008_NOM_Entries 
                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserGUID = '" + userID + "'" + "and TyoeOfProgram_EA = 'Evidence' Group By TyoeOfProgram_EA"; ;



                    var dataResult = await dbContext.Database.SqlQuery<NOMEntriesModel>(query).ToListAsync();

                    // var res = dbContext.Database.SqlQuery<NOMEntriesModel>(query);
                    if (dataResult != null)
                    {
                        //var dataResult = res.Where(x => (DbFunctions.TruncateTime(x.StartDate) >= startDate)).ToList();
                        //dataResult = dataResult
                        //   .Where(x => (x.EndDate >= startDate) && (x.EndDate <= endDate)).ToList();

                        //var dataModel = dataResult.FirstOrDefault();
                        //model.SumOfPrepHours = (dataModel.SumOfPrepHours) / 10;
                        //model.SumOfTravelHours = (dataModel.SumOfTravelHours) / 10;
                        //model.SumOfServiceHours = (dataModel.SumOfServiceHours) / 10;
                        //model.EvidenceBasedHours = (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / 10;
                        //model.NonEvidenceBasedHours = 0;
                        //var UID = dbContext.TblUsers.Where(x => x.UserGUID == userID).FirstOrDefault();
                        //var targetHours = dbContext.tblStaffHours.Where(x => x.UserID == UID.PKey).FirstOrDefault();
                        //model.TargetHours = 0;
                        //if (targetHours != null)
                        //{
                        //    model.TargetHours = Convert.ToDecimal(targetHours.Hours);
                        //    model.NonEvidenceBasedHours = model.TargetHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                        //}

                        //var dataModel = dataResult.FirstOrDefault();
                        //if (dataModel != null)
                        //{
                        //    model.SumOfPrepHours = dataModel.SumOfPrepHours;
                        //    model.SumOfTravelHours = dataModel.SumOfTravelHours;
                        //    model.SumOfServiceHours = dataModel.SumOfServiceHours;


                        //    model.PrepHoursPercent = (dataModel.SumOfPrepHours) / 10;
                        //    model.TravelHoursPercent = (dataModel.SumOfTravelHours) / 10;
                        //    model.ServiceHoursPercent = (dataModel.SumOfServiceHours) / 10;
                        //    model.EvidenceBasedHours = (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / 10;


                        //    model.TotalHours = Convert.ToString((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours));
                        //    model.TotalTargetHours = "0";
                        //    model.NonEvidenceBasedHours = 0;
                        //    model.TargetHours = 0;
                        //    model.RemainingHours = 0;
                        //    model.TotalRemainingHours = "0";
                        //    int userId = dbContext.TblUsers.Where(x => x.UserGUID == userID).Select(u => u.PKey).Single();
                        //    var targetHours = await dbContext.tblStaffHours.Where(x => x.UserID == userId && x.EvidenceBased == "1").FirstOrDefaultAsync();
                        //    if (targetHours != null)
                        //    {
                        //        model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                        //        model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                        //        model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                        //        // model.NonEvidenceBasedHours = model.RemainingHours;
                        //        var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                        //        model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                        //    }
                        //}

                        //model.SumOfPrepHours = dataModel.SumOfPrepHours;
                        //model.SumOfTravelHours = dataModel.SumOfTravelHours;
                        //model.SumOfServiceHours = dataModel.SumOfServiceHours;
                        //model.PrepHoursPercent = (dataModel.SumOfPrepHours) / 10;
                        //model.TravelHoursPercent = (dataModel.SumOfTravelHours) / 10;
                        //model.ServiceHoursPercent = (dataModel.SumOfServiceHours) / 10;
                        //model.EvidenceBasedHours = (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / 10;
                        //model.TotalHours = Convert.ToString((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours));
                        //model.TotalTargetHours = "0";
                        //model.NonEvidenceBasedHours = 0;
                        //model.TargetHours = 0;
                        //model.RemainingHours = 0;
                        //model.TotalRemainingHours = "0";

                        //int i = dbContext.TblUsers.Where(x => x.UserGUID == userID).Select(u => u.PKey).Single(); ;
                        //var targetHours = await dbContext.tblStaffHours.Where(x => x.UserID == i).FirstOrDefaultAsync();

                        //if (targetHours != null)
                        //{
                        //    model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                        //    model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                        //    model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                        //    model.NonEvidenceBasedHours = model.RemainingHours;
                        //    var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                        //    model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                        //    //model.SumOfPrepHours = totalRemainingHours - (dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                        //    //model.SumOfTravelHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfServiceHours);
                        //    //model.SumOfServiceHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours); 


                        //    model.AvgResult = Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours.Hours)) * 100, 2)) + "%";
                        //}


                        var dataModel = dataResult.FirstOrDefault();
                        if (dataModel != null)
                        {
                            model.SumOfPrepHours = dataModel.SumOfPrepHours;
                            model.SumOfTravelHours = dataModel.SumOfTravelHours;
                            model.SumOfServiceHours = dataModel.SumOfServiceHours;


                            model.PrepHoursPercent = (dataModel.SumOfPrepHours) / 10;
                            model.TravelHoursPercent = (dataModel.SumOfTravelHours) / 10;
                            model.ServiceHoursPercent = (dataModel.SumOfServiceHours) / 10;
                            model.EvidenceBasedHours = (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / 10;


                            model.TotalHours = Convert.ToString((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours));
                            model.TotalTargetHours = "0";
                            model.NonEvidenceBasedHours = 0;
                            model.TargetHours = 0;
                            model.RemainingHours = 0;
                            model.TotalRemainingHours = "0";
                            int i = dbContext.TblUsers.Where(x => x.UserGUID == userID).Select(u => u.PKey).Single();
                            // var targetHours = await dbContext.tblStaffHours.Where(x => x.UserID == i && x.EvidenceBased == "1").FirstOrDefaultAsync();

                            int? targetHours = dbContext.TblUsers.Where(x => x.UserGUID == userID).Select(u => u.Hours).Single();

                            if (targetHours != null)
                            {
                                //model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                                //model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                                //model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                //// model.NonEvidenceBasedHours = model.RemainingHours;
                                //var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                //model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                                ////model.SumOfPrepHours = totalRemainingHours - (dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                ////model.SumOfTravelHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfServiceHours);
                                ////model.SumOfServiceHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours); 


                                //model.AvgResult = Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours.Hours)) * 100, 2)) + "%";


                                model.TotalTargetHours = Convert.ToString(targetHours);
                                model.TargetHours = (Convert.ToDecimal(targetHours) / 10) - 1;
                                model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                var totalRemainingHours = Convert.ToDecimal(targetHours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                model.TotalRemainingHours = Convert.ToString(totalRemainingHours);

                                model.AvgResult = Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours)) * 100, 2)) + "%";
                            }
                        }

                    }

                    return this.Request.CreateResponse(HttpStatusCode.OK, model);
                }
            }
            catch (Exception)
            {

                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    model.SumOfPrepHours = 0;
                    model.SumOfTravelHours = 0;
                    model.SumOfServiceHours = 0;
                    model.EvidenceBasedHours = 0;
                    model.NonEvidenceBasedHours = 0;
                    model.TargetHours = 0;
                    var UID = dbContext.TblUsers.Where(x => x.UserGUID == userID).FirstOrDefault();
                    var targetHours = dbContext.tblStaffHours.Where(x => x.UserID == UID.PKey).FirstOrDefault();
                    if (targetHours != null)
                    {
                        model.TargetHours = Convert.ToDecimal(targetHours.Hours);
                        model.NonEvidenceBasedHours = model.TargetHours - (model.SumOfPrepHours + model.SumOfTravelHours + model.SumOfServiceHours);
                    }

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, model);
            }
        }

        async private Task<HttpResponseMessage> AdminDashBoard(string[] parameter)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, await AdminDashBoardUsers(parameter));
        }

        private async Task<Tuple<List<PagerValue>, List<ChartUserModel>>> AdminDashBoardUsers(string[] p)
        {
            List<ChartUserModel> users = new List<ChartUserModel>();
            var pagerResult = new DataGadget.Web.Pager.Pager();

            try
            {
                string strRole = string.Empty;
                strRole = p[0];
                string strStateAbbrev = string.Empty;
                strStateAbbrev = p[2];
                int locationId = Convert.ToInt32(p[1]);
                int? pageId = Convert.ToInt32(p[3]);
                int agencyId = 0;
                if (!string.IsNullOrEmpty(p[4]))
                {
                    agencyId = Convert.ToInt32(p[4]);
                }
                int take = 11;
                int segment = 3;

                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var userID = new Guid(p[5]);



                using (datagadget_testEntities context = new datagadget_testEntities())
                {



                    var dbUsers = new List<TblUser>();
                    switch (strRole)
                    {
                        case "SIT3U": //User
                            // dbUsers = await context.TblUsers.Where(x => x.UserGUID == userID).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                            dbUsers = await context.TblUsers.Where(x => x.UserGUID == userID).OrderBy(x => x.FirstName).Skip(skip).Take(take).ToListAsync();
                            {
                                int userCount = await context.TblUsers.Where(x => x.UserGUID == userID).CountAsync();
                                pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                            }
                            break;

                        case "SIT2A": //Admin
                            dbUsers = await context.TblUsers.Where(x => x.LocationID == locationId && x.IsActive == true).OrderBy(x => x.FirstName).Skip(skip).Take(take).ToListAsync();
                            {
                                int userCount = await context.TblUsers.Where(x => x.LocationID == locationId && x.IsActive == true).CountAsync();
                                pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                            }
                            break;
                        case "STA1A": //State
                            if (agencyId == 0)
                            {
                                dbUsers = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.IsActive == true).OrderBy(x => x.FirstName).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.IsActive == true).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }
                            else
                            {
                                dbUsers = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.LocationID == agencyId && x.IsActive == true).OrderBy(x => x.FirstName).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.LocationID == agencyId && x.IsActive == true).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }
                            break;
                        case "Admin":
                            //Administrators can see everyone in the system
                            if (agencyId == 0)
                            {
                                dbUsers = await context.TblUsers.Where(x => x.IsActive == true).OrderBy(x => x.FirstName).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.IsActive == true).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }
                            else
                            {
                                dbUsers = await context.TblUsers.Where(x => x.LocationID == agencyId && x.IsActive == true).OrderBy(x => x.FirstName).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.LocationID == agencyId && x.IsActive == true).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }

                            break;
                        default:
                            break;

                    }

                    foreach (var item in dbUsers)
                    {

                        ChartUserModel model = new ChartUserModel();
                        model.UserName = item.FirstName + " " + item.LastName;
                        var grantYear = context.GrantYears.FirstOrDefault();
                        var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                        var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));



                        var query = @"SELECT COALESCE(sum(TravelHours),0) as SumOfTravelHours ,COALESCE(sum(DirectServiceHours),0) as SumOfServiceHours
                                        ,COALESCE(sum(PrepHours),0) as SumOfPrepHours,TyoeOfProgram_EA
                                        FROM tbl2008_NOM_Entries 
                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserGUID = '" + item.UserGUID + "'" + "and TyoeOfProgram_EA = 'Evidence' Group By TyoeOfProgram_EA";


                        var nonEvidenceQuery = @"SELECT COALESCE(sum(TravelHours),0) as SumOfTravelHours ,COALESCE(sum(DirectServiceHours),0) as SumOfServiceHours
                                        ,COALESCE(sum(PrepHours),0) as SumOfPrepHours,TyoeOfProgram_EA
                                        FROM tbl2008_NOM_Entries 
                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserGUID = '" + item.UserGUID + "'" + "and TyoeOfProgram_EA != 'Evidence' Group By TyoeOfProgram_EA";

                        var dataResult = await context.Database.SqlQuery<NOMEntriesModel>(query).ToListAsync();

                        if (dataResult != null)
                        {
                            var dataModel = dataResult.FirstOrDefault();
                            if (dataModel != null)
                            {
                                model.SumOfPrepHours = dataModel.SumOfPrepHours;
                                model.SumOfTravelHours = dataModel.SumOfTravelHours;
                                model.SumOfServiceHours = dataModel.SumOfServiceHours;


                                model.PrepHoursPercent = (dataModel.SumOfPrepHours) / 10;
                                model.TravelHoursPercent = (dataModel.SumOfTravelHours) / 10;
                                model.ServiceHoursPercent = (dataModel.SumOfServiceHours) / 10;

                                //if (model.ServiceHoursPercent > 100)
                                //    model.ServiceHoursPercent = 100;
                                model.EvidenceBasedHours = (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / 10;


                                model.TotalHours = Convert.ToString((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours));
                                model.TotalTargetHours = "0";
                                model.NonEvidenceBasedHours = 0;
                                model.TargetHours = 0;
                                model.RemainingHours = 0;
                                model.TotalRemainingHours = "0";


                                var targetHours = item.Hours;
                                if (targetHours != null)
                                {

                                    model.TotalTargetHours = Convert.ToString(targetHours);
                                    model.TargetHours = (Convert.ToDecimal(targetHours) / 10) - 1;
                                    model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                    var totalRemainingHours = Convert.ToDecimal(targetHours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                    model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                                    model.AvgResult = Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours)) * 100, 2)) + "%";
                                }

                                //var targetHours = await context.tblStaffHours.Where(x => x.UserID == item.PKey && x.EvidenceBased == "1").FirstOrDefaultAsync();
                                //if (targetHours != null)
                                //{
                                //    model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                                //    model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                                //    model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                //    // model.NonEvidenceBasedHours = model.RemainingHours;
                                //    var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                //    model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                                //    //model.SumOfPrepHours = totalRemainingHours - (dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                //    //model.SumOfTravelHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfServiceHours);
                                //    //model.SumOfServiceHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours); 


                                //    model.AvgResult = Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours.Hours)) * 100, 2)) + "%";
                                //}
                            }
                            else
                            {
                                var targetHours = item.Hours;
                                if (targetHours != null)
                                {
                                    model.TotalTargetHours = Convert.ToString(targetHours);
                                    model.TargetHours = (Convert.ToDecimal(targetHours) / 10) - 1;
                                    model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                    var totalRemainingHours = Convert.ToDecimal(targetHours);// -(dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                    model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                                    model.AvgResult = "0";// Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours)) * 100, 2)) + "%";
                                }
                            }
                        }

                        //var nonEvidenceResult = await context.Database.SqlQuery<NOMEntriesModel>(nonEvidenceQuery).ToListAsync();

                        //if (nonEvidenceResult != null)
                        //{

                        //    var nonEvidenceModel = nonEvidenceResult.FirstOrDefault();
                        //    if (nonEvidenceModel != null)
                        //    {
                        //        model.NonEvidenceSumofPrepHours = nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfPrepHours;
                        //        model.NonEvidenceSumofTravelHours = nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfTravelHours;
                        //        model.NonEvidenceSumofServiceHours = nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfServiceHours;


                        //        model.NonEvidencePrepHoursPercent = (nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfPrepHours) / 10;
                        //        model.NonEvidenceTravelHoursPercent = (nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfTravelHours) / 10;
                        //        model.NonEvidenceServiceHoursPercent = (nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfServiceHours) / 10;
                        //        model.NonEvidenceBasedHours = nonEvidenceModel == null ? 0 : (nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours) / 10;


                        //        model.TotalHours = Convert.ToString((nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours));
                        //        model.TotalTargetHours = "0";
                        //        model.NonEvidenceBasedHours = 0;
                        //        model.TargetHours = 0;
                        //        model.RemainingHours = 0;
                        //        model.TotalRemainingHours = "0";
                        //        var targetHours = await context.tblStaffHours.Where(x => x.UserID == item.PKey && x.EvidenceBased == "2").FirstOrDefaultAsync();
                        //        if (targetHours != null)
                        //        {
                        //            model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                        //            model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                        //            model.RemainingHours = ((model.TargetHours + 1) - (model.NonEvidenceBasedHours));
                        //            // model.NonEvidenceBasedHours = model.RemainingHours;
                        //            var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours);
                        //            model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                        //            //model.SumOfPrepHours = totalRemainingHours - (dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                        //            //model.SumOfTravelHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfServiceHours);
                        //            //model.SumOfServiceHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours); 


                        //            model.AvgResult = Convert.ToString(Math.Round(((nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours) / Convert.ToDecimal(targetHours.Hours)) * 100, 2)) + "%";
                        //        }
                        //    }
                        //}

                        users.Add(model);
                    }

                    PagerValue pagerValue = new PagerValue();
                    pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                    pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                    pagerValue.HasNextPage = pagerResult.HasNextPage;
                    pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                    pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                    pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                    pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                    pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                    Tuple<List<PagerValue>, List<ChartUserModel>> data = new Tuple<List<PagerValue>, List<ChartUserModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, users);
                    return data;




                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private async Task<Tuple<List<PagerValue>, List<ChartUserModel>>> AdminDashBoardUsers1(string[] p)
        {
            List<ChartUserModel> users = new List<ChartUserModel>();
            var pagerResult = new DataGadget.Web.Pager.Pager();

            try
            {
                string strRole = string.Empty;
                strRole = p[0];
                string strStateAbbrev = string.Empty;
                strStateAbbrev = p[2];
                int locationId = Convert.ToInt32(p[1]);
                int? pageId = Convert.ToInt32(p[3]);
                int agencyId = 0;
                if (!string.IsNullOrEmpty(p[4]))
                {
                    agencyId = Convert.ToInt32(p[4]);
                }
                int take = 8;
                int segment = 3;

                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (datagadget_testEntities context = new datagadget_testEntities())
                {

                    var dbUsers = new List<TblUser>();
                    switch (strRole)
                    {
                        case "SIT2A": //Admin
                            dbUsers = await context.TblUsers.Where(x => x.LocationID == locationId).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                            {
                                int userCount = await context.TblUsers.Where(x => x.LocationID == locationId).CountAsync();
                                pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                            }
                            break;
                        case "STA1A": //State
                            if (agencyId == 0)
                            {
                                dbUsers = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }
                            else
                            {
                                dbUsers = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.LocationID == agencyId).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.LocationID == agencyId).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }
                            break;
                        case "Admin":
                            //Administrators can see everyone in the system
                            if (agencyId == 0)
                            {
                                dbUsers = await context.TblUsers.OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }
                            else
                            {
                                dbUsers = await context.TblUsers.Where(x => x.LocationID == agencyId).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                                {
                                    int userCount = await context.TblUsers.Where(x => x.LocationID == agencyId).CountAsync();
                                    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                                }
                            }

                            break;
                        default:
                            break;

                    }

                    foreach (var item in dbUsers)
                    {
                        ChartUserModel model = new ChartUserModel();
                        model.UserName = item.FirstName + " " + item.LastName;
                        var grantYear = context.GrantYears.Where(x => x.IsActive == true).FirstOrDefault();
                        var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                        var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));

                        var query = @"SELECT COALESCE(sum(TravelHours),0) as SumOfTravelHours ,COALESCE(sum(DirectServiceHours),0) as SumOfServiceHours
                                        ,COALESCE(sum(PrepHours),0) as SumOfPrepHours
                                        FROM tbl2008_NOM_Entries 
                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserGUID = '" + item.UserGUID + "'" + "and TyoeOfProgram_EA = 'Evidence'";


                        var nonEvidenceQuery = @"SELECT COALESCE(sum(TravelHours),0) as SumOfTravelHours ,COALESCE(sum(DirectServiceHours),0) as SumOfServiceHours
                                        ,COALESCE(sum(PrepHours),0) as SumOfPrepHours
                                        FROM tbl2008_NOM_Entries 
                                        where StartDate >= '" + startDate + "' and EndDate <='" + endDate + "' and UserGUID = '" + item.UserGUID + "'" + "and TyoeOfProgram_EA != 'Evidence'";

                        var dataResult = await context.Database.SqlQuery<NOMEntriesModel>(query).ToListAsync();

                        var nonEvidenceResult = await context.Database.SqlQuery<NOMEntriesModel>(nonEvidenceQuery).ToListAsync();

                        if (dataResult != null)
                        {
                            try
                            {
                                var dataModel = dataResult.FirstOrDefault();
                                var nonEvidenceModel = nonEvidenceResult.FirstOrDefault();

                                if (dataModel.TyoeOfProgram_EA == "Evidence")
                                {
                                    model.SumOfPrepHours = dataModel.SumOfPrepHours;
                                    model.SumOfTravelHours = dataModel.SumOfTravelHours;
                                    model.SumOfServiceHours = dataModel.SumOfServiceHours;


                                    model.PrepHoursPercent = (dataModel.SumOfPrepHours) / 10;
                                    model.TravelHoursPercent = (dataModel.SumOfTravelHours) / 10;
                                    model.ServiceHoursPercent = (dataModel.SumOfServiceHours) / 10;
                                    model.EvidenceBasedHours = (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / 10;


                                    model.TotalHours = Convert.ToString((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours));
                                    model.TotalTargetHours = "0";
                                    model.NonEvidenceBasedHours = 0;
                                    model.TargetHours = 0;
                                    model.RemainingHours = 0;
                                    model.TotalRemainingHours = "0";
                                    var targetHours = await context.tblStaffHours.Where(x => x.UserID == item.PKey && x.EvidenceBased == "1").FirstOrDefaultAsync();
                                    if (targetHours != null)
                                    {
                                        model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                                        model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                                        model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                        // model.NonEvidenceBasedHours = model.RemainingHours;
                                        var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                        model.TotalRemainingHours = Convert.ToString(totalRemainingHours);
                                        //model.SumOfPrepHours = totalRemainingHours - (dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                        //model.SumOfTravelHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfServiceHours);
                                        //model.SumOfServiceHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours); 


                                        model.AvgResult = Convert.ToString(Math.Round(((dataModel.SumOfPrepHours + dataModel.SumOfTravelHours + dataModel.SumOfServiceHours) / Convert.ToDecimal(targetHours.Hours)) * 100, 2)) + "%";
                                    }
                                }
                                else
                                {
                                    model.NonEvidenceSumofPrepHours = nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfPrepHours;
                                    model.NonEvidenceSumofTravelHours = nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfTravelHours;
                                    model.NonEvidenceSumofServiceHours = nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfServiceHours;


                                    model.NonEvidencePrepHoursPercent = (nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfPrepHours) / 10;
                                    model.NonEvidenceTravelHoursPercent = (nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfTravelHours) / 10;
                                    model.NonEvidenceServiceHoursPercent = (nonEvidenceModel == null ? 0 : nonEvidenceModel.SumOfServiceHours) / 10;
                                    model.NonEvidenceBasedHours = nonEvidenceModel == null ? 0 : (nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours) / 10;


                                    model.TotalHours = Convert.ToString((nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours));
                                    model.TotalTargetHours = "0";
                                    model.NonEvidenceBasedHours = 0;
                                    model.TargetHours = 0;
                                    model.NonEvidenceRemainingHours = 0;
                                    model.TotalRemainingHours = "0";
                                    var targetHours = await context.tblStaffHours.Where(x => x.UserID == item.PKey && x.EvidenceBased == "2").FirstOrDefaultAsync();
                                    if (targetHours != null)
                                    {
                                        model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                                        model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                                        model.NonEvidenceRemainingHours = ((model.TargetHours + 1) - (model.NonEvidenceBasedHours));
                                        // model.NonEvidenceBasedHours = model.RemainingHours;
                                        var totalRemainingHours = Convert.ToDecimal(targetHours.Hours) - (nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours);
                                        model.NonEvidenceTotalRemainingHours = Convert.ToString(totalRemainingHours);
                                        //model.SumOfPrepHours = totalRemainingHours - (dataModel.SumOfTravelHours + dataModel.SumOfServiceHours);
                                        //model.SumOfTravelHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfServiceHours);
                                        //model.SumOfServiceHours = totalRemainingHours - (dataModel.SumOfPrepHours + dataModel.SumOfTravelHours); 


                                        model.AvgResult = Convert.ToString(Math.Round(((nonEvidenceModel.SumOfPrepHours + nonEvidenceModel.SumOfTravelHours + nonEvidenceModel.SumOfServiceHours) / Convert.ToDecimal(targetHours.Hours)) * 100, 2)) + "%";
                                    }
                                }






                                users.Add(model);
                            }
                            catch (Exception)
                            {
                                model.SumOfPrepHours = 0;
                                model.SumOfTravelHours = 0;
                                model.SumOfServiceHours = 0;
                                model.EvidenceBasedHours = 0;
                                model.TotalHours = "0";
                                model.TotalTargetHours = "0";
                                model.NonEvidenceBasedHours = 0;
                                model.TargetHours = 0;
                                model.RemainingHours = 0;
                                model.TotalRemainingHours = "0";
                                var targetHours = context.tblStaffHours.Where(x => x.UserID == item.PKey).FirstOrDefault();
                                if (targetHours != null)
                                {
                                    model.TotalTargetHours = Convert.ToString(targetHours.Hours);
                                    model.TargetHours = (Convert.ToDecimal(targetHours.Hours) / 10) - 1;
                                    model.RemainingHours = ((model.TargetHours + 1) - (model.EvidenceBasedHours));
                                    model.NonEvidenceBasedHours = model.RemainingHours;
                                    model.TotalRemainingHours = Convert.ToDecimal(targetHours.Hours).ToString();
                                    model.AvgResult = "0%";
                                }
                                users.Add(model);
                            }
                        }
                    }
                }



                PagerValue pagerValue = new PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<PagerValue>, List<ChartUserModel>> data = new Tuple<List<PagerValue>, List<ChartUserModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, users);
                return data;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        async private Task<HttpResponseMessage> UserTreatmentDashBoard1(string[] parameter)
        {
            List<BedCountModel> lstBedCount = new List<BedCountModel>();
            BedCountModel model;

            var userID = new Guid(parameter[0]);
            int locationId = Convert.ToInt32(parameter[1]);
            var strRole = parameter[2];
            var strState = parameter[3];

            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    var grantYear = dbContext.GrantYears.Where(x => x.IsActive == true).FirstOrDefault();
                    var startDate = grantYear.StartDate;
                    var endDate = grantYear.EndDate;
                    var query = "";

                    switch (strRole)
                    {

                        case "SIT3U":  //User

                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  left join tblLocations L
	                                on si.LocationID=L.PKey left join SurveySession ss on SI.SessionID=ss.SessionID
	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey='" + locationId + "'  and ss.UserID = '" + userID + "'  group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                            break;
                        case "SIT2A": //Admin

                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
	                                on si.LocationID=L.PKey 
	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey=" + locationId + " group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                            break;
                        case "STA1A": //State

                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
	                                on si.LocationID=L.PKey 
	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.StateAbbrev='" + strState + "' group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                            break;
                        case "Admin":
                            //Administrators can see everyone in the system
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ";

                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
	                                on si.LocationID=L.PKey
	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                            //model.TotalBeds = dbContext.tblLocations.Sum(x => x.BedCount);

                            break;
                        default:
                            break;

                    }



                    var dataResult = dbContext.Database.SqlQuery<BedCountModel>(query);
                    if (dataResult != null)
                    {
                        try
                        {
                            var dataModel = await dataResult.ToListAsync();
                            foreach (var item in dataModel)
                            {
                                model = new BedCountModel();
                                model.TotalAssignedBeds = item.TotalAssignedBeds;//Occupied
                                model.BedCount = item.BedCount;// model.TotalBeds == null ? 0 : model.TotalBeds;
                                if (item.BedCount - item.TotalAssignedBeds < 0)
                                { model.BedCount = 0; }
                                else
                                { model.BedCount = item.BedCount - item.TotalAssignedBeds; }  //Available
                                model.LocationId = item.LocationId;

                                if (item.FriendlyName_loc.Trim().Length > 20)
                                {
                                    string[] words = item.FriendlyName_loc.Split(' ');
                                    model.FriendlyName_loc = words[0];
                                }
                                else
                                    model.FriendlyName_loc = item.FriendlyName_loc;
                                lstBedCount.Add(model);
                            }

                            //model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;

                        }
                        catch (Exception ex)
                        {
                            //model.TotalAssignedBeds = 0;
                            //model.TotalBeds = model.TotalBeds == null ? 0 : model.TotalBeds;
                            //// model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount == null ? 0 : dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;
                            throw ex;

                        }
                    }

                    return this.Request.CreateResponse(HttpStatusCode.OK, lstBedCount);
                }
            }
            catch (Exception)
            {

                //using (datagadget_testEntities dbContext = new datagadget_testEntities())
                //{
                //    model.TotalAssignedBeds = 0;
                //    model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount == null ? 0 : dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;

                //}
                return this.Request.CreateResponse(HttpStatusCode.OK, lstBedCount);
            }
        }

        async private Task<HttpResponseMessage> UserTreatmentDashBoard(string[] parameter)
        {
            try
            {
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<BedCountModel>> data = await UserTreatmentDashBoardWithPagination(parameter);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task<Tuple<List<Pager.PagerValue>, List<Models.BedCountModel>>> UserTreatmentDashBoardWithPagination1(string[] parameter)
        {

            List<BedCountModel> lstBedCount = new List<BedCountModel>();

            BedCountModel model;

            var userID = new Guid(parameter[0]);
            int locationId = Convert.ToInt32(parameter[1]);
            var strRole = parameter[2];
            var strState = parameter[3];

            var pagerResult = new DataGadget.Web.Pager.Pager();

            int? pageId = Convert.ToInt32(parameter[4]);

            int take = 10;
            int segment = 3;

            int skip = (Convert.ToInt32(pageId) - 1) * take;
            DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
            Tuple<List<DataGadget.Web.Pager.PagerValue>, List<BedCountModel>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<BedCountModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstBedCount);

            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    // var grantYear = dbContext.GrantYears.Where(x => x.IsActive == true).FirstOrDefault();
                    var grantYear = dbContext.GrantYears.FirstOrDefault();

                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var query = "";
                    StringBuilder sb;

                    //pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();


                    #region "comments for roles"

                    //switch (strRole)
                    //{

                    //    case "SIT3U":  //User
                    //        // dbUsers = await context.TblUsers.Where(x => x.LocationID == locationId).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                    //        //{
                    //        //    int userCount = await context.TblUsers.Where(x => x.LocationID == locationId).CountAsync();
                    //        //    pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                    //        //}
                    //        //break;
                    //        //                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  left join tblLocations L
                    //        //	                                on si.LocationID=L.PKey left join SurveySession ss on SI.SessionID=ss.SessionID
                    //        //	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey='" + locationId + "'  and ss.UserID = '" + userID + "'  group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                    //        //                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  left join tblLocations L
                    //        //	                                on si.LocationID=L.PKey 
                    //        //	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey='" + locationId + "' group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                    //        sb = new StringBuilder();

                    //        sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //        sb.Append("isnull(bedcount,0) as AdultMale, ");
                    //        sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //        sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //        sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //        sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey='" + locationId + "' ");
                    //        sb.Append(" group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");

                    //        query = sb.ToString();


                    //        break;
                    //    case "SIT2A": //Admin

                    //        //                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
                    //        //	                                on si.LocationID=L.PKey 
                    //        //	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey=" + locationId + " group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                    //        sb = new StringBuilder();

                    //        sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //        sb.Append("isnull(bedcount,0) as AdultMale, ");
                    //        sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //        sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //        sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //        sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.PKey='" + locationId + "' ");
                    //        sb.Append(" group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");

                    //        query = sb.ToString();

                    //        break;
                    //    case "STA1A": //State

                    //        //                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
                    //        //	                                on si.LocationID=L.PKey 
                    //        //	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.StateAbbrev='" + strState + "' group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";
                    //        sb = new StringBuilder();

                    //        sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //        sb.Append("isnull(bedcount,0) as AdultMale, ");
                    //        sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //        sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //        sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //        sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 and  l.StateAbbrev='" + strState + "' ");
                    //        sb.Append(" group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");

                    //        query = sb.ToString();

                    //        break;
                    //    case "Admin":

                    //        //Administrators can see everyone in the system
                    //        //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                    //        //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ";

                    //        //                            query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
                    //        //	                                on si.LocationID=L.PKey
                    //        //	                                where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

                    //        sb = new StringBuilder();
                    //        sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //        sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //        sb.Append("isnull(bedcount,0) as AdultMale, ");
                    //        sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //        sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //        sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //        sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 ");
                    //        sb.Append("group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");

                    //        query = sb.ToString();


                    //        //model.TotalBeds = dbContext.tblLocations.Sum(x => x.BedCount);

                    //        break;
                    //    default:
                    //        break;

                    //}
                    #endregion


                    sb = new StringBuilder();
                    //sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //sb.Append("isnull(bedcount,0) as AdultMale, ");
                    //sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 ");
                    //sb.Append("group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");

                    //sb.Append("SELECT L.PKey,");
                    ////sb.Append("SELECT tp.programname,");

                    ////added new
                    ////sb.Append("(select  top(1) StateRegionName from tblStateRegions where StateRegionCode= L.StateRegionCode and StateAbbrev=l.StateAbbrev) + ' - ' +  L.FriendlyName_loc as  FriendlyName_loc, ");
                    //sb.Append("(select  top(1) StateRegionName from tblStateRegions where StateRegionCode= L.StateRegionCode and StateAbbrev=l.StateAbbrev) as  FriendlyName_loc, ");

                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //sb.Append("isnull(L.bedcount,0) as AdultMale, ");
                    //sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //sb.Append("FROM tblLocations L  left join SurveyItem SI  on si.LocationID=L.PKey ");

                    sb.Append("inner join dbo.treatmentprogram tp on tp.locationid = l.PKey ");
                    sb.Append("where  SI.SurveyID=1  ");
                    sb.Append(" and (l.BedCount <> 0 and l.BedCount is not null)  or (l.AdultFemale <> 0 and l.AdultFemale is not null) or (l.AdolescentMale <> 0 and l.AdolescentMale is not null) or (l.AdolescentFemale <> 0 and l.AdolescentFemale is not null)");
                    //sb.Append("where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SI.SurveyID=1 ");
                    sb.Append(" group by L.PKey,tp.ProgramName,L.bedcount,AdultFemale,AdolescentMale,AdolescentFemale,L.StateRegionCode,l.StateAbbrev order by  tp.ProgramName ");

                    //query = sb.ToString();


                    //sb = new StringBuilder();
                    //sb.Append("SELECT L.PKey,");
                    //sb.Append("(right(sr.StateRegionCode,2) + '-' + tp.ProgramName) FriendlyName_loc, ");

                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //sb.Append("isnull(L.bedcount,0) as AdultMale, ");
                    //sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    //sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    //sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    //sb.Append("from tblStateRegions sr inner join tblLocations l on sr.StateRegionCode=l.StateRegionCode ");
                    //sb.Append(" inner join TreatmentProgram tp on l.PKey=tp.LocationID left join SurveyItem si on si.LocationID=l.PKey where  SI.SurveyID=1 ");
                    //sb.Append("and (l.BedCount <> 0 and l.BedCount is not null)  or (l.AdultFemale <> 0 and l.AdultFemale is not null) or (l.AdolescentMale <> 0 and l.AdolescentMale is not null) or (l.AdolescentFemale <> 0 and l.AdolescentFemale is not null)");
                    //sb.Append("group by L.PKey,L.FriendlyName_loc,L.bedcount,AdultFemale,AdolescentMale,AdolescentFemale,L.StateRegionCode,l.StateAbbrev , sr.StateRegionCode,tp.ProgramName  ");
                    //sb.Append(" order by  FriendlyName_loc ");


                    //query = sb.ToString();



                    sb.Append("SELECT distinct tp.LocationID,");

                    sb.Append("(select right(l.stateregioncode,2) + '-' + tp.Programname)  as  FriendlyName_loc, ");

                    sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    sb.Append("isnull(tp.bedcount,0) as AdultMale, ");
                    sb.Append("isnull(tp.femalebeds,0) as AdultFemale, ");
                    sb.Append("isnull(tp.AdolescentMale,0) as AdolescentMale, ");
                    sb.Append("isnull(tp.AdolescentFemale,0) as AdolescentFemale ");

                    sb.Append("FROM TreatmentProgram tp ");

                    sb.Append("inner join tblLocations l on tp.locationid = l.PKey  ");
                    sb.Append("inner join SurveyItem SI  on si.LocationID=L.PKey ");
                    sb.Append("where  SI.SurveyID=1 and tp.IsActive = 1  ");
                    sb.Append(" and (tp.BedCount <> 0 and tp.BedCount is not null)  or (tp.femalebeds <> 0 and tp.femalebeds is not null) or (tp.AdolescentMale <> 0 and tp.AdolescentMale is not null) or (tp.AdolescentFemale <> 0 and tp.AdolescentFemale is not null)");

                    //sb.Append(" group by L.PKey,L.FriendlyName_loc,L.bedcount,AdultFemale,AdolescentMale,AdolescentFemale,L.StateRegionCode,l.StateAbbrev order by  FriendlyName_loc ");

                    query = sb.ToString();



                    var dataResult = dbContext.Database.SqlQuery<BedCountModel>(query);


                    if (dataResult != null)
                    {
                        try
                        {
                            List<BedCountModel> dataModel = await dataResult.ToListAsync();

                            //dataModel = dataModel.Where(x => x.AdultMale != 0 ||x.AdultFemale != 0 || x.AdolescentMale != 0 || x.AdolescentFemale != 0).ToList();

                            dataModel = dataModel.Skip(skip).Take(take).ToList();
                            int userCount = await dataResult.CountAsync();
                            pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                            foreach (var item in dataModel)
                            {
                                model = new BedCountModel();
                                //model.TotalAssignedBeds = item.TotalAssignedBeds;//Occupied
                                //model.BedCount = item.BedCount;// model.TotalBeds == null ? 0 : model.TotalBeds;

                                model.TotalAssignedBeds = item.TotalAdultMaleAssignedBeds + item.TotalAdultFemaleAssignedBeds + item.TotalAdolescentMaleAssignedBeds + item.TotalAdolescentFemaleAssignedBeds;//Occupied
                                model.BedCount = item.AdultMale + item.AdultFemale + item.AdolescentMale + item.AdolescentFemale;// model.TotalBeds == null ? 0 : model.TotalBeds;

                                if (model.BedCount - model.TotalAssignedBeds < 0)
                                { model.AvailableBedCount = 0; }
                                else
                                { model.AvailableBedCount = model.BedCount - model.TotalAssignedBeds; }  //Available
                                model.LocationId = item.PKey;

                                //if (item.FriendlyName_loc.Trim().Length > 20)
                                //{
                                //    string[] words = item.FriendlyName_loc.Split(' ');
                                //    model.FriendlyName_loc = words[0];
                                //}
                                //else
                                model.FriendlyName_loc = item.FriendlyName_loc;

                                //if (item.FriendlyName_loc.Trim().Length > 3)
                                //{
                                //    model.LocName = item.FriendlyName_loc.Substring(0, 3);
                                //}
                                //else
                                //{
                                //    model.LocName = item.FriendlyName_loc;
                                //}


                                model.TotalAdultMaleAssignedBeds = item.TotalAdultMaleAssignedBeds;
                                model.TotalAdultFemaleAssignedBeds = item.TotalAdultFemaleAssignedBeds;
                                model.TotalAdolescentMaleAssignedBeds = item.TotalAdolescentMaleAssignedBeds;
                                model.TotalAdolescentFemaleAssignedBeds = item.TotalAdolescentFemaleAssignedBeds;

                                model.AdultMale = item.AdultMale;
                                model.AdultFemale = item.AdultFemale;
                                model.AdolescentMale = item.AdolescentMale;
                                model.AdolescentFemale = item.AdolescentFemale;



                                //model.PersentagesBeds = Convert.ToString((Math.Round(Convert.ToDouble(model.BedCount / 100 * model.TotalAssignedBeds), 2)));

                                model.PersentagesBeds = Convert.ToString(Convert.ToDouble(model.BedCount * 0.01 * model.TotalAssignedBeds));


                                lstBedCount.Add(model);
                            }

                            //model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;

                            pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                            pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                            pagerValue.HasNextPage = pagerResult.HasNextPage;
                            pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                            pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                            pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                            pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                            pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;

                            return data;

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    return data;
                }
            }
            catch (Exception)
            {

                return data;
            }

        }


        private async Task<Tuple<List<Pager.PagerValue>, List<Models.BedCountModel>>> UserTreatmentDashBoardWithPagination(string[] parameter)
        {

            List<BedCountModel> lstBedCount = new List<BedCountModel>();

            BedCountModel model;

            var userID = new Guid(parameter[0]);
            int locationId = Convert.ToInt32(parameter[1]);
            var strRole = parameter[2];
            var strState = parameter[3];

            var pagerResult = new DataGadget.Web.Pager.Pager();

            int? pageId = Convert.ToInt32(parameter[4]);

            int val = Convert.ToInt32(parameter[5]);

            int take = 8;
            int segment = 3;

            int skip = (Convert.ToInt32(pageId) - 1) * take;
            DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
            Tuple<List<DataGadget.Web.Pager.PagerValue>, List<BedCountModel>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<BedCountModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstBedCount);

            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    // var grantYear = dbContext.GrantYears.Where(x => x.IsActive == true).FirstOrDefault();
                    var grantYear = dbContext.GrantYears.FirstOrDefault();

                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var query = "";
                    StringBuilder sb;

                    // sb = new StringBuilder();

                    // sb.Append("SELECT L.PKey,");

                    //// sb.Append("(select  top(1) StateRegionName from tblStateRegions where StateRegionCode= L.StateRegionCode and StateAbbrev=l.StateAbbrev) as  FriendlyName_loc, ");
                    // sb.Append("right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc, ");

                    // sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    // sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    // sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    // sb.Append("(select count(*)  from SurveyItem  where LocationID=L.PKey and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    // sb.Append("isnull(L.bedcount,0) as AdultMale, ");
                    // sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
                    // sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
                    // sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

                    // sb.Append("FROM tblLocations L  left join SurveyItem SI  on si.LocationID=L.PKey ");

                    // sb.Append("inner join dbo.treatmentprogram tp on tp.locationid = l.PKey ");
                    // sb.Append("where  SI.SurveyID=1 and l.IsActive = 1  ");
                    // sb.Append(" and (l.BedCount <> 0 and l.BedCount is not null)  or (l.AdultFemale <> 0 and l.AdultFemale is not null) or (l.AdolescentMale <> 0 and l.AdolescentMale is not null) or (l.AdolescentFemale <> 0 and l.AdolescentFemale is not null)");

                    // sb.Append(" group by L.PKey,tp.ProgramName,L.bedcount,AdultFemale,AdolescentMale,AdolescentFemale,L.StateRegionCode,l.StateAbbrev order by  FriendlyName_loc ");

                    // query = sb.ToString();



                    sb = new StringBuilder();

                    //sb.Append("SELECT distinct tp.LocationID as PKey,");

                    //sb.Append(" tp.RecNo,si.ProgramID,");

                    //sb.Append("right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc, ");

                    ////sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
                    ////sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
                    ////sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
                    ////sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and SurveyID=1 and DischargeDate is null  ) as TotalAdultMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and  SurveyID=1 and DischargeDate is null ) as TotalAdultFemaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male'  and SurveyID=1 and DischargeDate is null ) as TotalAdolescentMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tp.LocationID and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and SurveyID=1 and DischargeDate is null ) as TotalAdolescentFemaleAssignedBeds,");

                    //sb.Append("isnull(tp.MaleBeds,0) as AdultMale, ");
                    //sb.Append("isnull(tp.FeMaleBeds,0) as AdultFemale, ");
                    //sb.Append("isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                    //sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale ");

                    //sb.Append(" From TreatmentProgram tp inner join SurveyItem SI on tp.recno=si.ProgramID  ");

                    //sb.Append("inner join tblLocations Tl on si.LocationID=Tl.PKey ");
                    //sb.Append("inner join SurveyService SS on SI.ServiceID=ss.RecNo ");
                    //sb.Append("inner join LocationIOP LI on LI.LocationId=tp.LocationID   ");

                    //sb.Append(" where SI.SurveyID=1 and tp.IsActive = 1 and tl.IsActive=1 and ss.Description='Primary Residential'  ");
                    ////sb.Append(" and (tp.MaleBeds <> 0 and tp.MaleBeds is not null)  or (tp.FeMaleBeds <> 0 and tp.FeMaleBeds is not null) or (tp.AdolescentMaleBeds <> 0 and tp.AdolescentMaleBeds is not null) or (tp.AdolescentFeMaleBeds <> 0 and tp.AdolescentFeMaleBeds is not null)");

                    ////sb.Append(" group by L.PKey,tp.ProgramName,L.bedcount,AdultFemale,AdolescentMale,AdolescentFemale,L.StateRegionCode,l.StateAbbrev order by  FriendlyName_loc ");
                    //sb.Append("  order by  FriendlyName_loc ");


                    GetBedcountQuery(sb, val);

                    //sb.Append(" From TreatmentProgram tp inner join SurveyItem SI on tp.recno=si.ProgramID  ");

                    //sb.Append("inner join tblLocations Tl on si.LocationID=Tl.PKey ");
                    //sb.Append("inner join SurveyService SS on SI.ServiceID=ss.RecNo ");
                    //sb.Append("inner join LocationIOP LI on LI.LocationId=tp.LocationID   ");

                    //sb.Append(" where SI.SurveyID=1 and tp.IsActive = 1 and tl.IsActive=1 and SI.ServiceID=1  ");



                    //sb.Append(" and (tp.MaleBeds <> 0 and tp.MaleBeds is not null)  or (tp.FeMaleBeds <> 0 and tp.FeMaleBeds is not null) or (tp.AdolescentMaleBeds <> 0 and tp.AdolescentMaleBeds is not null) or (tp.AdolescentFeMaleBeds <> 0 and tp.AdolescentFeMaleBeds is not null)");

                    //sb.Append(" group by L.PKey,tp.ProgramName,L.bedcount,AdultFemale,AdolescentMale,AdolescentFemale,L.StateRegionCode,l.StateAbbrev order by  FriendlyName_loc ");


                    query = sb.ToString();

                    var dataResult = dbContext.Database.SqlQuery<BedCountModel>(query);

                    if (dataResult != null)
                    {
                        try
                        {
                            List<BedCountModel> dataModel = await dataResult.ToListAsync();
                            switch (val)
                            {
                                case 1:
                                    dataModel = dataModel.Where(x => x.TotalAdultMaleAssignedBeds >= 0).ToList();

                                    break;
                                case 2:
                                    dataModel = dataModel.Where(x => x.TotalAdultFemaleAssignedBeds >= 0).ToList();
                                    break;
                                case 3:
                                    dataModel = dataModel.Where(x => x.TotalAdolescentMaleAssignedBeds >= 0 || x.TotalAdolescentFemaleAssignedBeds >= 0).ToList();
                                    break;
                            }


                            //dataModel = dataModel.Where(x => x.AdultMale != 0 ||x.AdultFemale != 0 || x.AdolescentMale != 0 || x.AdolescentFemale != 0).ToList();
                            int userCount = dataModel.Count;
                            dataModel = dataModel.Skip(skip).Take(take).ToList();

                            pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                            foreach (var item in dataModel)
                            {
                                model = new BedCountModel();
                                //model.TotalAssignedBeds = item.TotalAssignedBeds;//Occupied
                                //model.BedCount = item.BedCount;// model.TotalBeds == null ? 0 : model.TotalBeds;

                                model.TotalAssignedBeds = item.TotalAdultMaleAssignedBeds + item.TotalAdultFemaleAssignedBeds + item.TotalAdolescentMaleAssignedBeds + item.TotalAdolescentFemaleAssignedBeds;//Occupied

                                switch (val)
                                {
                                    case 1:
                                        model.BedCount = item.AdultMale;
                                        break;
                                    case 2:
                                        model.BedCount = item.AdultFemale;
                                        break;
                                    case 3:
                                        model.BedCount = item.AdolescentMale + item.AdolescentFemale;
                                        break;
                                    default:
                                        model.BedCount = item.AdultMale + item.AdultFemale + item.AdolescentMale + item.AdolescentFemale;
                                        break;
                                }

                                // model.TotalBeds == null ? 0 : model.TotalBeds;

                                if (model.BedCount - model.TotalAssignedBeds < 0)
                                { model.AvailableBedCount = 0; }
                                else
                                { model.AvailableBedCount = model.BedCount - model.TotalAssignedBeds; }  //Available
                                model.LocationId = item.PKey;


                                model.FriendlyName_loc = item.FriendlyName_loc;



                                model.TotalAdultMaleAssignedBeds = item.TotalAdultMaleAssignedBeds;
                                model.TotalAdultFemaleAssignedBeds = item.TotalAdultFemaleAssignedBeds;
                                model.TotalAdolescentMaleAssignedBeds = item.TotalAdolescentMaleAssignedBeds;
                                model.TotalAdolescentFemaleAssignedBeds = item.TotalAdolescentFemaleAssignedBeds;

                                model.AdultMale = item.AdultMale;
                                model.AdultFemale = item.AdultFemale;
                                model.AdolescentMale = item.AdolescentMale;
                                model.AdolescentFemale = item.AdolescentFemale;



                                //model.PersentagesBeds = Convert.ToString((Math.Round(Convert.ToDouble(model.BedCount / 100 * model.TotalAssignedBeds), 2)));

                                model.PersentagesBeds = Convert.ToString(Convert.ToDouble(model.BedCount * 0.01 * model.TotalAssignedBeds));


                                lstBedCount.Add(model);
                            }

                            //model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;

                            pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                            pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                            pagerValue.HasNextPage = pagerResult.HasNextPage;
                            pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                            pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                            pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                            pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                            pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;

                            return data;

                        }
                        catch (Exception ex)
                        {
                            //WebLogger.Error("Exception In CommanController,UserTreatmentDashBoard Method  : " + ex.Message); 
                            throw (ex);
                        }
                    }

                    return data;
                }
            }
            catch (Exception ex)
            {
                // WebLogger.Error("Exception In CommanController,UserTreatmentDashBoard Method  : " + ex.Message); 

                //return data;
                throw (ex);
            }

        }

        private static void GetBedcountQuery(StringBuilder sb, int val)
        {
            sb.Append("SELECT distinct tl.PKey as PKey,");
            sb.Append(" tp.RecNo,");
            sb.Append("right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc, ");
            switch (val)
            {
                case 1:
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdultMaleAssignedBeds,");

                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdultMaleAssignedBeds,");
                    break;
                case 2:
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 ) as TotalAdultFemaleAssignedBeds,");

                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdultFemaleAssignedBeds,");
                    break;
                case 3:
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdolescentMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdolescentMaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdolescentFemaleAssignedBeds,");
                    break;
                default:

                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdultMaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3) or c.ClientStatusId is null) ) as TotalAdultMaleAssignedBeds,");
                   // sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 ) as TotalAdultFemaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdultFemaleAssignedBeds,");

                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdolescentMaleAssignedBeds,");
                    //sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 ) as TotalAdolescentFemaleAssignedBeds,");

                     sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdolescentMaleAssignedBeds,");
                    sb.Append("(select count(*)  from SurveyItem  SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo Inner Join Client C on sc.ClientID=c.RecNo  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and (c.ClientStatusId in (3,4) or c.ClientStatusId is null) ) as TotalAdolescentFemaleAssignedBeds,");
                    
                    break;
            }

            sb.Append("isnull(tp.MaleBeds,0) as AdultMale, ");
            sb.Append("isnull(tp.FeMaleBeds,0) as AdultFemale, ");
            sb.Append("isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
            sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale ");



            sb.Append(" From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
            sb.Append(" where tp.IsActive = 1 and tl.IsActive=1  ");
            sb.Append("  order by  FriendlyName_loc ");
        }

        async private Task<HttpResponseMessage> ContactsDashBoard1(string[] parameter)
        {
            PatientContacts model;
            List<PatientContacts> lstPatientContacts = new List<PatientContacts>();
            var userID = new Guid(parameter[0]);
            int locationId = Convert.ToInt32(parameter[1]);
            var strRole = parameter[2];
            var strState = parameter[3];

            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    var grantYear = dbContext.GrantYears.Where(x => x.IsActive == true).FirstOrDefault();
                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var query = "";

                    switch (strRole)
                    {

                        case "SIT3U":  //User
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID=" + locationId + "  and SurveyID=1 and ss.UserID = '" + userID + "' ";
                            //left join SurveyContact SCo on SI.RecNo=SCo.SurveyItemID
                            query = @"select SI.RecNo,C.PAT_ID,SI.SurveyAdminID,'Discharge' as surveyName from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";

                            break;
                        case "SIT2A": //Admin
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID=" + locationId + "  and SurveyID=1";
                            query = @"select SI.RecNo,C.PAT_ID,SI.SurveyAdminID,'Discharge' as surveyName from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";



                            break;
                        case "STA1A": //State
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "') and SurveyID=1 ";

                            query = @"select SI.RecNo,C.PAT_ID,SI.SurveyAdminID,'Discharge' as surveyName from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";

                            break;
                        case "Admin":

                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ";

                            query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,SI.AdminDate as SurveyDate,'Discharge' as SurveyName from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";



                            break;
                        default:
                            break;

                    }



                    var dataResult = dbContext.Database.SqlQuery<PatientContacts>(query);
                    if (dataResult != null)
                    {
                        try
                        {
                            var dataModel = await dataResult.ToListAsync();
                            foreach (PatientContacts r in dataModel)
                            {
                                model = new PatientContacts();
                                model.PatientId = r.PatientId;
                                model.SurveyAdmin = r.SurveyAdmin;
                                model.SurveyAdminName = dbContext.TblUsers.Where(x => x.UserGUID == r.SurveyAdmin).SingleOrDefault().FirstName;
                                model.SurveyDate = r.SurveyDate;
                                model.SurveyName = r.SurveyName;
                                model.SurveyItemId = r.SurveyItemId;
                                var datacontacts = dbContext.SurveyContacts.Where(x => x.SurveyItemID == r.SurveyItemId).ToList();

                                lstPatientContacts.Add(model);
                            }



                        }
                        catch (Exception)
                        {

                            // model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount == null ? 0 : dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;

                        }
                    }

                    return this.Request.CreateResponse(HttpStatusCode.OK, lstPatientContacts);
                }
            }
            catch (Exception)
            {

                //using (datagadget_testEntities dbContext = new datagadget_testEntities())
                //{
                //    model.TotalAssignedBeds = 0;
                //    model.TotalBeds = dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount == null ? 0 : dbContext.tblLocations.Where(x => x.PKey == locationId).SingleOrDefault().BedCount;

                //}
                return this.Request.CreateResponse(HttpStatusCode.OK, lstPatientContacts);
            }
        }

        async private Task<HttpResponseMessage> ContactsDashBoard(string[] p)
        {
            try
            {
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<PatientContacts>> data = await ContactsDashBoardWithPagination(p);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task<Tuple<List<Pager.PagerValue>, List<Models.PatientContacts>>> ContactsDashBoardWithPagination(string[] parameter)
        {
            PatientContacts model;
            List<PatientContacts> lstPatientContacts = new List<PatientContacts>();
            var userID = new Guid(parameter[0]);
            int locationId = Convert.ToInt32(parameter[1]);
            var strRole = parameter[2];
            var strState = parameter[3];

            var sortOrder = parameter[6];

            int days = 0;



            var pagerResult = new DataGadget.Web.Pager.Pager();

            int? pageId = Convert.ToInt32(parameter[4]);
            int? followUpId = Convert.ToInt32(parameter[5]);

            int take = 8;
            int segment = 3;

            int skip = (Convert.ToInt32(pageId) - 1) * take;
            DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
            Tuple<List<DataGadget.Web.Pager.PagerValue>, List<PatientContacts>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<PatientContacts>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstPatientContacts);

            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    var grantYear = dbContext.GrantYears.FirstOrDefault();
                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var query = "";

                    switch (strRole)
                    {

                        case "SIT3U":  //User
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID=" + locationId + "  and SurveyID=1 and ss.UserID = '" + userID + "' ";
                            //left join SurveyContact SCo on SI.RecNo=SCo.SurveyItemID
                            if (followUpId == 3)
                            {
                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      inner join SurveySession ss on si.SessionID= ss.SessionID  
                                //                                      where SurveyTypeID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'   And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = "
                                //                                                                                    + followUpId +
                                //                                                                                     " ) and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";

                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      inner join SurveySession ss on si.SessionID= ss.SessionID   
                                //                                      left join SurveyService sser on si.ServiceID=sser.RecNo 
                                //                                      where SurveyID=1    AND (EntryDate >= '" + startDate + "' and EntryDate <='"
                                //                                                                                + endDate + "') and si.DischargeId=2 and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'  and si.DischargeDate is not null  And (C.PAT_ID  not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= "
                                //                                + followUpId +
                                //                                " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  !=0)  ) order by SI.CreateDate desc";

                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      inner join SurveySession ss on si.SessionID= ss.SessionID   
                                      left join SurveyService sser on si.ServiceID=sser.RecNo 
                                      where SurveyID=1    AND (EntryDate >= '" + startDate + "' and EntryDate <='"
                                                                               + endDate + "') and si.DischargeId=2 and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'  and si.DischargeDate is not null And DATEDIFF (dd , DischargeDate , GETDATE() ) > 30  And (C.PAT_ID  not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= "
                               + followUpId +
                               " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  !=0)  ) order by SI.CreateDate desc";

                            }
                            else
                            {

                                if (followUpId == 1)
                                {
                                    days = 30;

                                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      inner join SurveySession ss on si.SessionID= ss.SessionID
                                      left join SurveyService sser on si.ServiceID=sser.RecNo  
                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'   And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) > =1)  and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";
                                }
                                else if (followUpId == 2)
                                {

                                    days = 60;

                                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      inner join SurveySession ss on si.SessionID= ss.SessionID
                                      left join SurveyService sser on si.ServiceID=sser.RecNo  
                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'   And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) > =2)  and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";
                                }
                                else
                                {
                                    days = 180;

                                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      inner join SurveySession ss on si.SessionID= ss.SessionID
                                      left join SurveyService sser on si.ServiceID=sser.RecNo  
                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'   And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) > =3)  and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";
                                }

                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      inner join SurveySession ss on si.SessionID= ss.SessionID
                                //                                      left join SurveyService sser on si.ServiceID=sser.RecNo  
                                //                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'   And C.PAT_ID   in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = "
                                //                                                                             + followUpId +
                                //                                                                             ")   and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";

                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,sser.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      inner join SurveySession ss on si.SessionID= ss.SessionID
                                //                                      left join SurveyService sser on si.ServiceID=sser.RecNo  
                                //                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " and SI.LocationID=" + locationId + " and  ss.UserID='" + userID + "'   And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) > =3)  and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";


                            }

                            break;
                        case "SIT2A": //Admin
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID=" + locationId + "  and SurveyID=1";
                            if (followUpId == 3)
                            {
                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > 30 and SI.LocationID=" + locationId +

                               " And C.PAT_ID  not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= " + followUpId +
                               " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)   order by SI.CreateDate desc";
                            }
                            else
                            {

                                if (followUpId == 1)
                                {
                                    days = 30;
                                    query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and SI.LocationID=" + locationId + "  And C.PAT_ID   Not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >=1) and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";
                                }
                                else if (followUpId == 2)
                                {
                                    days = 60;
                                    query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and SI.LocationID=" + locationId + "  And C.PAT_ID   Not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >=2) and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";
                                }
                                else
                                {
                                    days = 180;
                                    query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and SI.LocationID=" + locationId + "  And C.PAT_ID   Not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >=3) and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";
                                }

                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                //                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and SI.LocationID=" + locationId + "  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = "
                                //                                                                                + followUpId +
                                //                                                                                 " ) and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";


                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                //                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and SI.LocationID=" + locationId + "  And C.PAT_ID   Not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >=3) and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";

                            }


                            break;
                        case "STA1A": //State
                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "') and SurveyID=1 ";
                            if (followUpId == 3)
                            {
                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > 30  and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState +
                                                                                "')  And C.PAT_ID  not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= " + followUpId +
                                                                                 " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0)  ) order by SI.CreateDate desc";

                            }

                            else
                            {

                                if (followUpId == 1)
                                {
                                    days = 30;
                                    query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "')  And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 1 and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";
                                }
                                else if (followUpId == 2)
                                {
                                    days = 60;
                                    query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "')  And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 2 and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";
                                }
                                else
                                {
                                    days = 180;
                                    query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "')  And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 3 and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";
                                }

                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                //                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "')  And C.PAT_ID   in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = " + followUpId +
                                //                                                                                 " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";

                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                //                                      where SurveyID=1 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + "  and LocationID in (select LocationID from tblLocations where StateAbbrev='" + strState + "')  And C.PAT_ID   not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 3 and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";

                            }

                            break;
                        case "Admin":

                            //                            query = @"SELECT count(*) as TotalAssignedBeds  FROM SurveyItem SI inner join SurveySession ss on SI.SessionID=ss.SessionID
                            //                                  where (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and SurveyID=1 ";


                            //and si.DischargeId=2  max(HasSurveyDone) != 0)

                            if (followUpId == 3)
                            {
                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID ,isnull(tp.ProgramName,'') as ProgramName,isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days from  
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                //                                      left join TreatmentProgram tp on si.ProgramID=tp.RecNo   
                                //                                      where SurveyID=1     AND (EntryDate >= '" + startDate + "' and EntryDate <='"
                                //                                                                                + endDate + "') and si.DischargeId=2 and si.DischargeDate is not null  And (C.PAT_ID  not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= "
                                //                                + followUpId +
                                //                                " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  !=0)  ) order by SI.CreateDate desc";

                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID ,isnull(tp.ProgramName,'') as ProgramName,isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days from  
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo
                                      left join TreatmentProgram tp on si.ProgramID=tp.RecNo   
                                      where SurveyID=1     AND (EntryDate >= '" + startDate + "' and EntryDate <='"
                                                                               + endDate + "') and si.DischargeId=2 and si.DischargeDate is not null and DATEDIFF (dd , DischargeDate , GETDATE() ) > 30  And (C.PAT_ID  not in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= "
                               + followUpId +
                               " ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  !=0)  ) order by SI.CreateDate desc";




                            }

                            else
                            {

                                if (followUpId == 1)
                                {
                                    days = 30;
                                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID,isnull(tp.ProgramName,'') as ProgramName,isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo 
                                      left join TreatmentProgram tp on si.ProgramID=tp.RecNo   
                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " And C.PAT_ID   not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 1) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)   order by SI.CreateDate desc";
                                }
                                else if (followUpId == 2)
                                {
                                    days = 60;
                                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID,isnull(tp.ProgramName,'') as ProgramName,isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo 
                                      left join TreatmentProgram tp on si.ProgramID=tp.RecNo   
                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " And C.PAT_ID   not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 2) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)   order by SI.CreateDate desc";
                                }
                                else
                                {
                                    days = 180;
                                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID,isnull(tp.ProgramName,'') as ProgramName,isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo
                                      left join SurveyService ss on si.ServiceID=ss.RecNo 
                                      left join TreatmentProgram tp on si.ProgramID=tp.RecNo   
                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " And C.PAT_ID   not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 3) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)   order by SI.CreateDate desc";
                                }
                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      where SurveyTypeID=2   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  And C.PAT_ID   in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = "
                                //                                                                                + followUpId +
                                //                                                                                ")   and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) !=0)  ) order by SI.CreateDate desc";

                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      where SurveyTypeID=2   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  And C.PAT_ID   in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = "
                                //                                                                               + followUpId +
                                //                                                                               ")   and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";




                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo  
                                //                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2   And C.PAT_ID   in(select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) = "
                                //                                                                              + followUpId +
                                //                                                                              ")   and (C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)  ) order by SI.CreateDate desc";

                                //                                query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate, SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,IsNull(SI.ProgramID,0) as ProgramID,isnull(tp.ProgramName,'') as ProgramName,isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days from 
                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                                //                                      left join SurveyService ss on si.ServiceID=ss.RecNo 
                                //                                      left join TreatmentProgram tp on si.ProgramID=tp.RecNo   
                                //                                      where SurveyID=1   AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and si.DischargeDate is not null and si.DischargeId=2 and DATEDIFF (dd , DischargeDate , GETDATE() ) > " + days + " And C.PAT_ID   not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 3)   and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone)  != 0)   order by SI.CreateDate desc";

                            }



                            break;
                        default:
                            break;

                    }



                    var dataResult = dbContext.Database.SqlQuery<PatientContacts>(query).ToList();
                    //var dataResult = dbContext.Database.SqlQuery<PatientContacts>(query).Where(x => x.Is60days = true);


                    switch (days)
                    {
                        case 30:
                            //dataResult = dataResult.Where(x => x.EntryDate >= x.EntryDate.Value.AddDays(30)).ToList();
                            break;
                        case 60:
                            dataResult = dataResult.Where(x => x.Is60days == true).ToList();
                            break;
                        case 180:
                            dataResult = dataResult.Where(x => x.Is180days == true).ToList();
                            break;
                        default:
                            break;
                    }


                    if (dataResult != null)
                    {
                        try
                        {
                            //var dataModel = await dataResult.ToListAsync();
                            var dataModel = dataResult.ToList();

                            switch (sortOrder)
                            {
                                case "Asc":
                                    dataModel = dataModel.OrderBy(s => s.PatientId).ToList();
                                    break;
                                case "Desc":
                                    dataModel = dataModel.OrderByDescending(s => s.PatientId).ToList();
                                    break;
                                default:
                                    dataModel = dataModel.OrderByDescending(s => s.CreateDate).ToList();
                                    break;

                            }

                            dataModel = dataModel.Skip(skip).Take(take).ToList();
                            //int userCount = await dataResult.CountAsync();
                            int userCount = dataResult.Count();
                            pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                            foreach (PatientContacts r in dataModel)
                            {

                                model = new PatientContacts();
                                model.PatientId = r.PatientId;
                                model.SurveyAdmin = r.SurveyAdmin;
                                model.SurveyAdminName = dbContext.TblUsers.Where(x => x.UserGUID == r.SurveyAdmin).SingleOrDefault().FirstName;
                                model.EntryDate = r.EntryDate;
                                model.SurveyName = r.SurveyName;
                                model.SurveyItemId = r.SurveyItemId;
                                //var datacontacts = dbContext.SurveyContacts.Where(x => x.SurveyItemID == r.SurveyItemId).ToList();
                                var datacontacts = dbContext.FollowUpContacts.Where(x => x.PatientId == r.PatientId).ToList();
                                model.ContactCount = datacontacts.Count;
                                model.ClientGenderType = r.ClientGenderType;
                                model.CreateDate = r.CreateDate;
                                model.LocationID = r.LocationID;
                                model.ServiceID = r.ServiceID;
                                model.ProgramId = r.ProgramId;
                                model.DischargeDate = r.DischargeDate;
                                model.Description = r.Description;
                                switch (model.ContactCount)
                                {
                                    case 0:
                                        model.imagename = "../../Content/images/sm_round.png";
                                        break;
                                    case 1:
                                        model.imagename = "../../Content/images/sm_round1.png";
                                        break;
                                    case 2:
                                        //model.imagename = "../../Content/images/sm_round2.png";
                                        model.imagename = "../../Content/images/roundimages4.png";

                                        break;
                                    case 3:
                                        model.imagename = "../../Content/images/sm_round3.png";
                                        break;
                                    default:
                                        break;
                                }


                                lstPatientContacts.Add(model);
                            }

                            //DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                            pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                            pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                            pagerValue.HasNextPage = pagerResult.HasNextPage;
                            pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                            pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                            pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                            pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                            pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;

                            return data;



                        }
                        catch (Exception)
                        {

                        }
                    }


                    return data;

                }
            }
            catch (Exception)
            {

                return data;
            }
        }

        async private Task<HttpResponseMessage> SendEmailTreatmentUser(string[] p)
        {
            bool b = true;
            try
            {

                StringBuilder sb;
                MailPatientContacts model;
                List<MailPatientContacts> lstPatientContacts;
                var userID = new Guid(p[0]);
                int locationId = Convert.ToInt32(p[1]);
                var strRole = p[2];
                var strState = p[3];

                DateTime dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1));
                DateTime dLastDayOfLastMonth = dFirstDayOfThisMonth.AddDays(-1).Date;
                DateTime dFirstDayOfLastMonth = dFirstDayOfThisMonth.AddMonths(-1).Date;

                bool isMailsent = false;
                int mailCount = 0;
                string TreatmentMessage = string.Empty;
                string PreventionMessage = string.Empty;

                bool isTreatmentMail = false;
                bool isPreventionMail = false;

                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {

                    var date = DateTime.UtcNow.Date;



                    if (DateTime.Now.Day == 11)
                    {

                        var res = await dbContext.MailSettings.Where(x => x.IsActive == true).FirstOrDefaultAsync();
                        if (res != null)
                        {
                            TreatmentMessage = res.TreatmentMessage;
                            PreventionMessage = res.PreventionMessage;
                            isTreatmentMail = res.IsTreatment;
                            isPreventionMail = res.IsPrevention;
                        }

                        if (isTreatmentMail == false && isPreventionMail == false)
                        {
                            return this.Request.CreateResponse(HttpStatusCode.OK, b);
                        }

                        int TotalMailCount = dbContext.MailAuditTrails.Where(x => x.MailDate == date && (x.MailType == "Prevention" || x.MailType == "Treatment")).Count();
                        if (TotalMailCount > 0)
                        {
                            return this.Request.CreateResponse(HttpStatusCode.OK, b);
                        }


                        var query = "";

                        var users = new List<DataGadget.Web.Models.User>();
                        var dbUsers = new List<DataGadget.Web.DataModel.TblUser>();
                        //dbUsers = (from U in dbContext.TblUsers
                        //           join UTP in dbContext.UserCategories on U.UserGUID equals UTP.UserID
                        //           where U.IsActive == true
                        //           select U).ToList();

                        dbUsers = await dbContext.TblUsers.Where(x => x.IsActive == true).OrderBy(x => x.LocationID).ToListAsync();

                        users = dbUsers.Select(user => new DataGadget.Web.Models.User
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            EmailId = user.Email_addr,
                            UserId = user.UserGUID,
                            UserName = user.LoginID,
                            LocationId = user.LocationID,
                            AgencyName = user.tblLocation.FriendlyName_loc,
                            Password = user.PW,
                            Level = user.UserLevel,
                            Degree = user.Degree,
                            Experience = user.YrsPrevExp,
                            OfficePhoneNum = user.OfficePhone,
                            OfficeFaxNum = user.OfficeFax,
                            SelectedUserCategoryModel = dbContext.UserCategories.Where(x => x.UserID == user.UserGUID).ToList()

                        }).ToList();

                        foreach (var item in users)
                        {
                            sb = new StringBuilder();
                            string mailBody = string.Empty;
                            lstPatientContacts = new List<MailPatientContacts>();


                            if (item.SelectedUserCategoryModel != null)
                            {
                                foreach (var usercat in item.SelectedUserCategoryModel)
                                {

                                    isMailsent = false;
                                    mailCount = 0;

                                    if (usercat.CategoryID == 1)
                                    {
                                        query = @"select count(*) as surveyCount from SurveyItem inner join SurveySession on SurveyItem.SessionID=SurveySession.SessionID
                                        where SurveySession.UserID='" + item.UserId + "' and CreateDate Between '" + dFirstDayOfLastMonth + "' and '" + dLastDayOfLastMonth + "'  and SurveyItem.SurveyID=" + usercat.CategoryID + "";
                                    }
                                    else if (usercat.CategoryID == 2)
                                    {
                                        query = @"select count(*) as surveyCount from tbl2008_NOM_Entries
                                        where UserGUID='" + item.UserId + "' and Entrydate Between '" + dFirstDayOfLastMonth + "' and '" + dLastDayOfLastMonth + "'";
                                    }
                                    else
                                    {
                                        query = @"select count(*) as surveyCount from tbl2008_NOM_Entries where 1=2";
                                    }



                                    var dataResult = dbContext.Database.SqlQuery<int>(query);
                                    int dataModel = 0;
                                    if (dataResult != null)
                                    {
                                        dataModel = await dataResult.SingleOrDefaultAsync();
                                    }


                                    mailBody = "Hi " + item.FirstName + " " + item.LastName + ",<br><br>";// This email was sent automatically by DataGadget in response to your request to reset your password.<br><br>";



                                    if (dataModel == 0)
                                    {
                                        //getting admin email id

                                        var adminemail = await dbContext.TblUsers.Where(x => x.LocationID == item.LocationId && x.UserLevel == "SIT2A").Select(x => x.Email_addr).FirstOrDefaultAsync();
                                        if (adminemail == null)
                                            adminemail = "";
                                        if (usercat.CategoryID == 2 && isPreventionMail == true)
                                        {
                                            string addressFrom = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                                            string addressTo = item.EmailId;// "sudhakar@cattechnologies.com";

                                            string subject = "Prevention survey in last month";

                                            if (string.IsNullOrEmpty(PreventionMessage))
                                            {
                                                mailBody += "you are not done any  Prevention surveys in last month:<br><br>";
                                            }
                                            else
                                            {
                                                mailBody += PreventionMessage;
                                                mailBody += " <br><br>";
                                            }

                                            //mailBody += "you are not done any  Prevention surveys in last month:<br><br>";
                                            mailBody += "Best Regards,<br> Datagadget.";
                                            //check for mail already sent

                                            mailCount = dbContext.MailAuditTrails.Where(x => x.MailTo == addressTo && x.MailDate == date && x.MailType == "Prevention").Count();
                                            if (mailCount == 0)
                                            {
                                                isMailsent = Email.SendEmailWithCC(addressTo, addressFrom, subject, mailBody, adminemail);
                                                if (isMailsent)
                                                {
                                                    MailAuditTrail DTO = new MailAuditTrail();
                                                    DTO.Body = mailBody;
                                                    DTO.MailFrom = addressFrom;
                                                    DTO.MailTo = addressTo;
                                                    DTO.MailCC = adminemail;
                                                    DTO.MailType = "Prevention";
                                                    DTO.MailDate = System.DateTime.UtcNow.Date;
                                                    DTO.Subject = subject;
                                                    dbContext.MailAuditTrails.Add(DTO);
                                                    await dbContext.SaveChangesAsync();
                                                }
                                            }
                                           
                                        }
                                        else if (usercat.CategoryID == 1 && isTreatmentMail == true)
                                        {
                                            string addressFrom = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                                            string addressTo = item.EmailId; // "sudhakar@cattechnologies.com";

                                            string subject = "Treatment survey in last month";
                                            if (string.IsNullOrEmpty(TreatmentMessage))
                                            {
                                                mailBody += "you are not done any  Treatment surveys in last month:<br><br>";
                                            }
                                            else
                                            {
                                                mailBody += TreatmentMessage;
                                                mailBody += " <br><br>";
                                            }
                                            // mailBody += "you are not done any  Treatment surveys in last month:<br><br>";

                                            mailBody += "Best Regards,<br> Datagadget.";
                                            // Email.SendEmail(addressTo, addressFrom, subject, mailBody);
                                            mailCount = dbContext.MailAuditTrails.Where(x => x.MailTo == addressTo  && x.MailDate == date && x.MailType == "Treatment").Count();
                                            if (mailCount == 0)
                                            {
                                                isMailsent = Email.SendEmailWithCC(addressTo, addressFrom, subject, mailBody, adminemail);

                                                if (isMailsent)
                                                {
                                                    MailAuditTrail DTO = new MailAuditTrail();
                                                    DTO.Body = mailBody;
                                                    DTO.MailFrom = addressFrom;
                                                    DTO.MailTo = addressTo;
                                                    DTO.MailCC = adminemail;
                                                    DTO.MailType = "Treatment";
                                                    DTO.MailDate = System.DateTime.UtcNow.Date;
                                                    DTO.Subject = subject;
                                                    dbContext.MailAuditTrails.Add(DTO);
                                                    await dbContext.SaveChangesAsync();
                                                }
                                            }
                                           
                                        }
                                    }
                                }




                            }





                        }
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, b);

            }

            catch (Exception)
            {

                return this.Request.CreateResponse(HttpStatusCode.OK, b);
            }
        }

        private async Task<HttpResponseMessage> SendEmailTreatmentUser1(string[] p)
        {
            DateTime d = DateTime.Now;

            DateTime dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1));
            DateTime dLastDayOfLastMonth = dFirstDayOfThisMonth.AddDays(-1);
            DateTime dFirstDayOfLastMonth = dFirstDayOfThisMonth.AddMonths(-1);
            //DATEADD(mm, DATEDIFF(mm,0,getdate())-1, 0)
            // Dim days As Long = DateDiff(DateInterval.Day, date1, date2)
            //int days = Microsoft.VisualBasic.DateAndTime.DateDiff(DateInterval.Month,d,)
            // DateTime dd=dated
            DateTime stdate = d.AddMonths(1);

            System.DateTime dtTodayNoon = DateTime.Now;

            System.DateTime dtYestMidnight = DateTime.Now;

            System.TimeSpan diffResult = dtTodayNoon - dtYestMidnight;




            return this.Request.CreateResponse(HttpStatusCode.OK, true);
        }

        async private Task<HttpResponseMessage> GetWeekllyEmailData(string[] p)
        {
            bool b = true;
            StringBuilder sb;
            MailPatientContacts model;
            List<MailPatientContacts> lstPatientContacts;
            var userID = new Guid(p[0]);
            int locationId = Convert.ToInt32(p[1]);
            var strRole = p[2];
            var strState = p[3];
            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    var grantYear = dbContext.GrantYears.FirstOrDefault();
                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var query = "";

                    var record = dbContext.tblEmailTemplates.SingleOrDefault();

                    var users = new List<DataGadget.Web.Models.User>();
                    var dbUsers = new List<DataGadget.Web.DataModel.TblUser>();
                    dbUsers = await dbContext.TblUsers.Where(x => x.UserLevel == "SIT2A").OrderBy(x => x.LocationID).ToListAsync();

                    users = dbUsers.Select(user => new DataGadget.Web.Models.User
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        EmailId = user.Email_addr,
                        UserId = user.UserGUID,
                        UserName = user.LoginID,
                        LocationId = user.LocationID,
                        AgencyName = user.tblLocation.FriendlyName_loc,
                        Password = user.PW,
                        Level = user.UserLevel,
                        Degree = user.Degree,
                        Experience = user.YrsPrevExp,
                        OfficePhoneNum = user.OfficePhone,
                        OfficeFaxNum = user.OfficeFax,


                    }).ToList();

                    foreach (var item in users)
                    {
                        sb = new StringBuilder();
                        lstPatientContacts = new List<MailPatientContacts>();

                        query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate,FC.FollowupComment,FC.FollowupDate,FC.FollowupId  from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo left join FollowUpContacts FC on c.PAT_ID=FC.PatientId
                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID =" + item.LocationId + "  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.RecNo desc";

                        //                        switch (strRole)
                        //                        {
                        //                            case "SIT3U":  //User

                        //                                query = @"select SI.RecNo,C.PAT_ID,SI.SurveyAdminID,'Discharge' as surveyName
                        //                                      from 
                        //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                      left Join Client C on SC.ClientID=C.RecNo
                        //                                      
                        //                                      where SI.LocationID=" + item.LocationId + " and SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";

                        //                                break;
                        //                            case "SIT2A": //Admin

                        //                                query = @"select SI.RecNo,C.PAT_ID,SI.SurveyAdminID,'Discharge' as surveyName from 
                        //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                      inner Join Client C on SC.ClientID=C.RecNo
                        //                                      
                        //                                      where SI.LocationID=" + item.LocationId + " and SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";

                        //                                break;
                        //                            case "STA1A": //State

                        //                                query = @"select SI.RecNo,C.PAT_ID,SI.SurveyAdminID,'Discharge' as surveyName from 
                        //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                      inner Join Client C on SC.ClientID=C.RecNo
                        //                                     
                        //                                      where SI.LocationID=" + item.LocationId + " and SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";

                        //                                break;
                        //                            case "Admin":  //FC.FollowupDate ,Fc.FollowupComment  left join FollowUpContacts FC on C.PAT_ID=FC.PatientId

                        //                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,
                        //                                //                                        SI.AdminDate as SurveyDate,'Discharge' as SurveyName, from  
                        //                                //                                        SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                //                                        inner Join Client C on SC.ClientID=C.RecNo
                        //                                //                                        where SI.LocationID=" + item.LocationId + " and SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.CreateDate desc";

                        //                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,
                        //                                //                                    SI.AdminDate as SurveyDate,'Discharge' as SurveyName,FC.FollowupDate ,Fc.FollowupComment from  
                        //                                //                                    SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                //                                    inner Join Client C on SC.ClientID=C.RecNo
                        //                                //                                    left join FollowUpContacts FC on C.PAT_ID=FC.PatientId
                        //                                //                                    where SI.LocationID=31 and SurveyTypeID=2 AND (EntryDate >= '5/30/2014 6:30:00 PM' and EntryDate <='6/30/2015 6:30:00 PM')  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3)   order by SI.CreateDate desc";


                        //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate,FC.FollowupComment,FC.FollowupDate,FC.FollowupId  from 
                        //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                      inner Join Client C on SC.ClientID=C.RecNo left join FollowUpContacts FC on c.PAT_ID=FC.PatientId
                        //                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID =" + item.LocationId + "  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";

                        //                                //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate from 
                        //                                //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                //                                      inner Join Client C on SC.ClientID=C.RecNo
                        //                                //                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID =" + item.LocationId + "  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3 ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.CreateDate desc";


                        //                                break;
                        //                            default:
                        //                                break;

                        //                        }

                        var dataResult = dbContext.Database.SqlQuery<MailPatientContacts>(query);
                        if (dataResult != null)
                        {
                            var dataModel = await dataResult.ToListAsync();
                            foreach (MailPatientContacts r in dataModel)
                            {
                                model = new MailPatientContacts();
                                model.PatientId = r.PatientId;
                                model.SurveyAdmin = r.SurveyAdmin;
                                model.SurveyAdminName = dbContext.TblUsers.Where(x => x.UserGUID == r.SurveyAdmin).SingleOrDefault().FirstName;
                                model.SurveyDate = r.SurveyDate;
                                model.SurveyName = r.SurveyName;
                                model.SurveyItemId = r.SurveyItemId;
                                model.FollowupDate = r.FollowupDate;
                                model.FollowupComment = r.FollowupComment;
                                model.FollowupId = r.FollowupId;
                                lstPatientContacts.Add(model);
                            }
                        }


                        if (lstPatientContacts.Count > 0)
                        {
                            sb.Append("<table style='width:650px; border:1px solid #111111;font-family:calibri;font-size:14px; margin:0 auto;margin-left:15px'>");
                            sb.Append("<tr style='height:30px;'><th  width='100px' style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>PatientId</th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Followup Id </th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Followup Date</th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Followup Comment</th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Survey Admin Name </th></tr>");
                            foreach (MailPatientContacts m in lstPatientContacts)
                            {

                                sb.Append("<tr style='height:30px'><td align='center' width='100px' style='font-family:calibri;font-size:14px;'>" + m.PatientId + "</td>");
                                sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.FollowupId + "</td>");
                                sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.FollowupDate + "</td>");
                                sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.FollowupComment + "</td>");
                                sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.SurveyAdminName + "</td></tr>");

                            }

                            sb.Append("</table>");

                            string addressFrom = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                            string addressTo = "Sudhakar@cattechnologies.com";
                            string subject = "weekly mails";
                            string mailBody = record.Template + sb.ToString();
                            Email.SendEmail(addressTo, addressFrom, subject, mailBody);
                        }
                    }




                    return this.Request.CreateResponse(HttpStatusCode.OK, b);
                }
            }
            catch (Exception)
            {

                return this.Request.CreateResponse(HttpStatusCode.OK, b);
            }
        }

        //private HttpResponseMessage GetEventsCount(string[] p)
        //{
        //    try
        //    {

        //        var userID = new Guid(p[0]);
        //        int locationId = Convert.ToInt32(p[1]);
        //        var strRole = p[2];
        //        var strState = p[3];

        //        EventsCount EventsCount = new EventsCount();
        //        using (datagadget_testEntities context = new DataModel.datagadget_testEntities())
        //        {
        //            var grantYear = context.GrantYears.FirstOrDefault();
        //            var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
        //            var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));

        //            int pretestcount = context.SurveyItems.Where(x => x.SurveyID == 2 && x.SurveyTypeID == 1 && x.CreateDate >= grantYear.StartDate && x.CreateDate <= grantYear.EndDate).Count();
        //            int posttestcount = context.SurveyItems.Where(x => x.SurveyID == 2 && x.SurveyTypeID == 2 && x.CreateDate >= grantYear.StartDate && x.CreateDate <= grantYear.EndDate).Count();

        //            EventsCount.PretestCount = pretestcount;
        //            EventsCount.PosttestCount = posttestcount;

        //        }

        //        return this.Request.CreateResponse(HttpStatusCode.OK, EventsCount);




        //    }
        //    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        //    {
        //        Exception raise = dbEx;
        //        foreach (var validationErrors in dbEx.EntityValidationErrors)
        //        {
        //            foreach (var validationError in validationErrors.ValidationErrors)
        //            {
        //                string message = string.Format("{0}:{1}",
        //                    validationErrors.Entry.Entity.ToString(),
        //                    validationError.ErrorMessage);
        //                raise = new InvalidOperationException(message, raise);
        //            }
        //        }
        //        throw raise;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}

        async private Task<HttpResponseMessage> FollowUpContactsReport(string[] p)
        {
            try
            {
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<FollowUpReportModel>> data = await FollowUpContacts(p);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task<Tuple<List<Pager.PagerValue>, List<Models.FollowUpReportModel>>> FollowUpContacts(string[] parameter)
        {
            FollowUpReportModel model;
            List<FollowUpReportModel> lstPatientContacts = new List<FollowUpReportModel>();

            int locationId = Convert.ToInt32(parameter[0]);

            DateTime startDate = Convert.ToDateTime(parameter[1]);
            DateTime endDate = Convert.ToDateTime(parameter[2]);

            //var grantYear = dbContext.GrantYears.FirstOrDefault();
            //var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
            //var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));

            var pagerResult = new DataGadget.Web.Pager.Pager();

            int? pageId = 1;// Convert.ToInt32(parameter[4]);
            //int? followUpId = Convert.ToInt32(parameter[5]);

            int take = 8;
            int segment = 3;

            int skip = (Convert.ToInt32(pageId) - 1) * take;
            DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
            Tuple<List<DataGadget.Web.Pager.PagerValue>, List<FollowUpReportModel>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<FollowUpReportModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstPatientContacts);
            //IsNull(SI.ProgramID,0) as ProgramID ,isnull(tp.ProgramName,'') as ProgramName,
            //isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days
            //inner join TreatmentProgram tp on si.ProgramID=tp.RecNo  
            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {

                    var query = "";

                    //                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,
                    //                                'Discharge' as SurveyName,SI.EntryDate, 
                    //                                SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,ss.Description,
                    //                                fc.FollowupDate,fc.FollowupComment,fc.HasSurveyDone
                    //                                from  
                    //                                SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                    //                                inner Join Client C on SC.ClientID=C.RecNo
                    //                                inner join FollowUpContacts FC on c.PAT_ID=fc.PatientId  
                    //                                inner join SurveyService ss on si.ServiceID=ss.RecNo
                    //                                where SurveyID=1     
                    //                                AND (FollowupDate >= '" + startDate + "' and FollowupDate <='" + endDate + "')  order by fc.FollowupDate desc";


                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,
                                'Discharge' as SurveyName,SI.EntryDate, 
                                SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,
                                fc.FollowupDate,fc.FollowupComment,fc.HasSurveyDone
                               from FollowUpContacts fc inner join  client c on fc.PatientId=c.PAT_ID
                                inner join SurveyClient sc on SC.ClientID=C.RecNo
                                inner   join SurveyItem SI on SI.recno=FC.SurveyItemId
                                where SurveyID=1     
                                AND (FollowupDate >= '" + startDate + "' and FollowupDate <='" + endDate + "')  order by fc.FollowupDate desc";



                    var dataResult = dbContext.Database.SqlQuery<FollowUpReportModel>(query).ToList();

                    if (locationId > 0)
                    {
                        dataResult = dataResult.Where(x => x.LocationID == locationId).ToList();
                    }



                    if (dataResult != null)
                    {
                        try
                        {

                            var dataModel = dataResult.ToList();

                            //dataModel = dataModel.Skip(skip).Take(take).ToList();


                            //int userCount = dataResult.Count();
                            //pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                            foreach (FollowUpReportModel r in dataModel)
                            {

                                model = new FollowUpReportModel();
                                model.PatientId = r.PatientId;
                                model.SurveyAdmin = r.SurveyAdmin;
                                model.SurveyAdminName = dbContext.TblUsers.Where(x => x.UserGUID == r.SurveyAdmin).SingleOrDefault().FirstName;
                                model.EntryDate = r.EntryDate;
                                model.SurveyName = r.SurveyName;
                                model.SurveyItemId = r.SurveyItemId;
                                //var datacontacts = dbContext.SurveyContacts.Where(x => x.SurveyItemID == r.SurveyItemId).ToList();
                                //   var datacontacts = dbContext.FollowUpContacts.Where(x => x.PatientId == r.PatientId).ToList();
                                // model.ContactCount = datacontacts.Count;
                                model.ClientGenderType = r.ClientGenderType;
                                model.CreateDate = r.CreateDate;
                                model.LocationID = r.LocationID;
                                model.LocationName = dbContext.tblLocations.Where(x => x.PKey == r.LocationID).SingleOrDefault().FriendlyName_loc;
                                model.ServiceID = r.ServiceID;
                                // model.ProgramId = r.ProgramId;
                                model.DischargeDate = r.DischargeDate;
                                model.Description = r.Description;

                                model.FollowupComment = r.FollowupComment;
                                model.FollowupDate = r.FollowupDate;
                                model.FollowupId = r.FollowupId;



                                lstPatientContacts.Add(model);
                            }

                            //DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                            pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                            pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                            pagerValue.HasNextPage = pagerResult.HasNextPage;
                            pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                            pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                            pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                            pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                            pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;

                            return data;



                        }
                        catch (Exception)
                        {

                        }
                    }


                    return data;

                }
            }
            catch (Exception ex)
            {

                return data;
            }
        }


        /*Indigent report 88888888888888888888888888888*/

        async private Task<HttpResponseMessage> IndigentFundReport(string[] p)
        {
            try
            {
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<IndigentFuntReportModel>> data = await IndigentFund(p);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task<Tuple<List<Pager.PagerValue>, List<Models.IndigentFuntReportModel>>> IndigentFund(string[] parameter)
        {
            IndigentFuntReportModel model;
            List<IndigentFuntReportModel> lstPatientContacts = new List<IndigentFuntReportModel>();

            int locationId = Convert.ToInt32(parameter[0]);
            int ServiceId = Convert.ToInt32(parameter[3]);
            int ProgId = Convert.ToInt32(parameter[4]);
            double Dayamt = Convert.ToDouble(parameter[5]);

            DateTime startDate = Convert.ToDateTime(parameter[1]);
            DateTime endDate = Convert.ToDateTime(parameter[2]);



            var pagerResult = new DataGadget.Web.Pager.Pager();

            int? pageId = 1;// Convert.ToInt32(parameter[4]);

            int take = 8;
            int segment = 3;

            int skip = (Convert.ToInt32(pageId) - 1) * take;
            DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
            Tuple<List<DataGadget.Web.Pager.PagerValue>, List<IndigentFuntReportModel>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<IndigentFuntReportModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstPatientContacts);
            //IsNull(SI.ProgramID,0) as ProgramID ,isnull(tp.ProgramName,'') as ProgramName,
            //isnull(tp.Is60days,0) as Is60days,isnull(tp.Is180days,0) as Is180days
            //inner join TreatmentProgram tp on si.ProgramID=tp.RecNo  
            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {

                    var query = "";


                    //                    query = @"select distinct C.PAT_ID as PatientId, SI.RecNo as SurveyItemId, SI.SurveyAdminID as SurveyAdmin,
                    //                                'Discharge' as SurveyName,SI.EntryDate, 
                    //                                SI.ClientGenderType,SI.CreateDate,si.DischargeDate,si.LocationID,SI.ServiceID,
                    //                                fc.FollowupDate,fc.FollowupComment,fc.HasSurveyDone
                    //                               from FollowUpContacts fc inner join  client c on fc.PatientId=c.PAT_ID
                    //                                inner join SurveyClient sc on SC.ClientID=C.RecNo
                    //                                inner   join SurveyItem SI on SI.recno=FC.SurveyItemId
                    //                                where SurveyID=1     
                    //                                AND (FollowupDate >= '" + startDate + "' and FollowupDate <='" + endDate + "')  order by fc.FollowupDate desc";

                    //                    query = @"select Si.recNo as SurveyItemId,SC.RecNo as SurveyClientId,C.RecNo as ClientId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,
                    //                            Si.EntryDate,Si.DischargeDate,Si.ProgramID,Si.LocationId,SI.IsIndigent,SI.ServiceID,SI.ClientGenderType
                    //                            from SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                    //                            inner join Client C on SC.ClientId=C.RecNo 
                    //                            where SurveyID=1 and SI.IsIndigent =1
                    //                            AND (DischargeDate >= '" + startDate + "' and DischargeDate <='" + endDate + "')  order by DischargeDate desc";

                    query = @"select Si.recNo as SurveyItemId,SC.RecNo as SurveyClientId,C.RecNo as ClientId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,
                            Si.EntryDate,Si.DischargeDate,Si.ProgramID,Si.LocationId,SI.ServiceID,SI.ClientGenderType
                            from SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                            inner join Client C on SC.ClientId=C.RecNo 
                            inner join TreatmentSurveyIntakeValues TSI  on Si.RecNo=TSI.SurveyItemId
                            inner Join IntakeType IT on TSI.intakevalueid=it.Id
                            where SurveyID=1 and lower(it.IntakeTypeName)='indigent'
                            AND (DischargeDate >= '" + startDate + "' and DischargeDate <='" + endDate + "')  order by DischargeDate desc";



                    var dataResult = dbContext.Database.SqlQuery<IndigentFuntReportModel>(query).ToList();

                    if (locationId > 0)
                    {
                        dataResult = dataResult.Where(x => x.LocationID == locationId).ToList();
                    }
                    if (ServiceId > 0)
                    {
                        dataResult = dataResult.Where(x => x.ServiceID == ServiceId).ToList();
                    }
                    if (ProgId > 0)
                    {
                        dataResult = dataResult.Where(x => x.ProgramId == ProgId).ToList();
                    }



                    if (dataResult != null)
                    {
                        try
                        {

                            var dataModel = dataResult.ToList();

                            //dataModel = dataModel.Skip(skip).Take(take).ToList();


                            //int userCount = dataResult.Count();
                            //pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                            foreach (IndigentFuntReportModel r in dataModel)
                            {

                                model = new IndigentFuntReportModel();
                                model.PatientId = r.PatientId;
                                model.SurveyAdmin = r.SurveyAdmin;
                                model.SurveyAdminName = dbContext.TblUsers.Where(x => x.UserGUID == r.SurveyAdmin).SingleOrDefault().FirstName;
                                model.EntryDate = r.EntryDate;
                                model.SurveyName = r.SurveyName;
                                model.SurveyItemId = r.SurveyItemId;
                                //var datacontacts = dbContext.SurveyContacts.Where(x => x.SurveyItemID == r.SurveyItemId).ToList();
                                //   var datacontacts = dbContext.FollowUpContacts.Where(x => x.PatientId == r.PatientId).ToList();
                                // model.ContactCount = datacontacts.Count;
                                model.ClientGenderType = r.ClientGenderType;
                                model.CreateDate = r.CreateDate;
                                model.LocationID = r.LocationID;
                                model.LocationName = dbContext.tblLocations.Where(x => x.PKey == r.LocationID).SingleOrDefault().FriendlyName_loc;
                                model.ServiceID = r.ServiceID;
                                model.ServiceName = dbContext.SurveyServices.Where(x => x.RecNo == r.ServiceID).SingleOrDefault().Description;
                                // model.ProgramId = r.ProgramId;
                                model.DischargeDate = r.DischargeDate;
                                model.Description = r.Description;
                                model.ProgramId = r.ProgramId;
                                if (r.ProgramId > 0)
                                {
                                    model.ProgramName = dbContext.TreatmentPrograms.Where(x => x.RecNo == r.ProgramId).SingleOrDefault().ProgramName;
                                }
                                else
                                {
                                    model.ProgramName = "No Program";
                                }

                                //model.isIndigent = r.isIndigent;
                                if (r.DischargeDate != null)
                                {
                                    TimeSpan ts = r.DischargeDate.Value.Subtract(r.EntryDate.Value);
                                    model.Days = ts.Days;
                                    model.TotalAmount = ts.Days * Dayamt;// 35;
                                }
                                else
                                {
                                    DateTime newdate = DateTime.Now.AddDays(2);
                                    TimeSpan ts = newdate.Subtract(r.EntryDate.Value);
                                    model.Days = ts.Days;
                                    model.TotalAmount = ts.Days * Dayamt;// 35;
                                }

                                lstPatientContacts.Add(model);
                            }

                            //DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                            pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                            pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                            pagerValue.HasNextPage = pagerResult.HasNextPage;
                            pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                            pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                            pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                            pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                            pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;

                            return data;



                        }
                        catch (Exception)
                        {

                        }
                    }


                    return data;

                }
            }
            catch (Exception ex)
            {

                return data;
            }
        }

    }
}
