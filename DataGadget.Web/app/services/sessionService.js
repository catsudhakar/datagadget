﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('sessionService', sessionService);

    sessionService.$inject = ['$http'];

    function sessionService($http) {
        this.getData = getData;
        this.setData = setData;
        var event = {};
        function getData() {
            return event;
        }


        function setData(e) {
            event = e;
        }
    }
})();