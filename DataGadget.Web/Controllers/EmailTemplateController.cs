﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;

//using DataGadget.Web.DataModel;

namespace DataGadget.Web.Controllers
{
    public class EmailTemplateController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                    case "AllRecords":
                        return await AllRecords(value.Parameters);
                    case "CreateEmailTemplate":
                        return CreateEmailTemplate(value.Parameters);
                    case "GetTeplate":
                        return GetTeplate(value.Parameters);
                        

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));

            }
        }
        async private Task<HttpResponseMessage> AllRecords(string[] p)
        {
            try
            {
                List<EmailTemplateViewModel> res;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    res = new List<EmailTemplateViewModel>();
                    res = await context.tblEmailTemplates.
                        Select(x =>
                        new EmailTemplateViewModel
                        {
                            //EmailTemplateName = x.TemplateName,
                            //TemplateDesc = x.TemplateDescription,
                            ID = x.ID
                        }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private HttpResponseMessage GetTeplate(string[] p)
        {
            try
            {
                Models.EmailTemplateViewModel email = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.EmailTemplateViewModel>(p[0]);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //email.TemplateDesc = context.tblEmailTemplates.Where(x => x.ID == email.ID).SingleOrDefault().TemplateDescription;
                    
                    return this.Request.CreateResponse(HttpStatusCode.OK, email);
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, email);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private HttpResponseMessage CreateEmailTemplate(string[] p)
        {
            try
            {
                Models.EmailTemplateViewModel email = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.EmailTemplateViewModel>(p[0]);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var record = context.tblEmailTemplates.Where(x => x.ID == email.ID).SingleOrDefault();
                    //record.TemplateDescription = email.TemplateDesc;
                    context.SaveChanges();
                    return this.Request.CreateResponse(HttpStatusCode.OK, email);
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, email);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetNotificatons(string[] p)
        {
            try
            {
                Guid uid = new Guid(p[0]);
                string level = p[1];
                List<EmailTemplateViewModel> res;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    res = new List<EmailTemplateViewModel>();
                    res = await context.tblEmailTemplates.
                        Select(x =>
                        new EmailTemplateViewModel
                        {
                            //EmailTemplateName = x.TemplateName,
                            //TemplateDesc = x.TemplateDescription,
                            ID = x.ID
                        }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Put(int id, [FromBody]string value)
        {

        }
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

    }
}