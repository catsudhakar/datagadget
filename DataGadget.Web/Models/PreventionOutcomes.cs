﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class PreventionOutcomes
    {

        public string Agency_Name { get; set; }
        public string Survey_Entered_By { get; set; }
        //public Nullable<System.DateTime> Survey_Completed { get; set; }
        public string Survey_Completed { get; set; }
        public string Survey { get; set; }
        public string Participant_ID { get; set; }
        public int Survey_Type { get; set; }
        public string Program_Type { get; set; }
        public string Program_Name { get; set; }
        public string Group_Name { get; set; }
        //public int C1_Gender { get; set; }
        public int Gender { get; set; }

        //public string Gender { get; set; }
        //public int intGender { get; set; }

        public int C2_Hispanic_or_Latino { get; set; }
        //public int C3_Race { get; set; }

        public int C3_1_White { get; set; }
        public int C3_2_Black{ get; set; }
        public int C3_3_AmericanIndian { get; set; }

        public int C3_4_NativeHawaiian { get; set; }
        public string NativeHawaiian_Text { get; set; }
        public int C3_5_Asian { get; set; }
        public int C3_6_AlaskaNative { get; set; }
        public int C3_7_Other { get; set; }
        public string Other_Text { get; set; }
        public int C3_8_NoResponse { get; set; }

       
        


        //public string C4_Date_of_Birth { get; set; }
        //public string C4_1_Age { get; set; }

        public string C4_Day_of_Month_Born { get; set; }
        public string C4_1_Age { get; set; }

        public int C5_Military_Family { get; set; }
        public int C6_Smoke_Past_30_Days { get; set; }
        public int C7_Other_Tobacco_Past_30_Days { get; set; }
        public int C8_Alcohol_Past_30_Days { get; set; }
        public int C9_Marijuana_Past_30_Days { get; set; }
        public int C10_Prescription_Drugs_Past_30_Days { get; set; }
        public int C11_Synthetic_Drugs_Past_30_Days { get; set; }
        public int C12_Illegal_Drugs_Past_30_Days { get; set; }
        public int C13_Daily_Cigarettes_Disapproval { get; set; }
        public int C14_Daily_Cigarette_Pack_Disapproval { get; set; }
        public int C15_Trying_Marijuana_Disapproval { get; set; }
        public int C16_Using_Marijuana_Disapproval { get; set; }
        public int C17_Daily_Drinking_Disapproval { get; set; }
        public int C18_Binge_Drinking_Disapproval { get; set; }
        public int C19_Prescription_Drug_Disapproval { get; set; }
        public int C20_Cigarette_Pack_Risk { get; set; }
        public int C21_Marijuana_Risk { get; set; }
        public int C22_Binge_Drinking_Risk { get; set; }
        public int C23_Prescription_Drug_Risk { get; set; }
        public int C24_Drink_and_Drive { get; set; }
        public int C25_Drugs_and_Drive { get; set; }
        public int C26_Family_Talk_Frequency { get; set; }
        public int C27_Work_for_Drug_Testing_Employer { get; set; }
        public int C28_Hopeless { get; set; }
        public int C29_Restless { get; set; }
        public int C30_Suicide_Ideation { get; set; }
        public string C31_Satisfaction_Post_Test_Only { get; set; }
        public string C32_Client_Assessment_of_Program_Post_Test_Only { get; set; }
    }
}