﻿(function () {
    'use strict';
    var controllerId = 'managecomments';
    angular
        .module('app')
        .controller('managecomments', managecomments);

    managecomments.$inject = ['$scope', '$http', '$modal', '$location', '$window', 'common'];

    function managecomments($scope, $http, $modal, $location, $window, common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error')

        $scope.vm.OrganizationList = [];
        $scope.selectedUser = ''
        $scope.selectedMailUser = ''
        $scope.ManagecommentsSubmit = false;

        $scope.title = 'Manage Comments';
        var vm = this;
        vm.comment = {};
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];
        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }
        $scope.ismessage = "";
        vm.isNoRecords = false;
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        activate();
        function activate() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level != "Admin") {
                $location.path('/dashboard');
            }
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            getallusers();
            //$http({ method: 'POST', url: 'api/Comments', data: { method: "GetAllComments", parameters: [userInfo.pkey] } })
            //  .success(function (result) {
            //      vm.commentList = result;
            //      if (result.length == 0) {
            //          vm.isNoRecords = true;
            //      }
            //      else {
            //          vm.isNoRecords = false;
            //      }
            //  })
            //   .error(function (error) {
            //       console.log(error);
            //   });
            paging(userInfo.pkey,1);

        }
        vm.pagination = function (pageId, userId) {
            if (userId == undefined)
            {
                var userInfo = JSON.parse($window.localStorage["userInfo"]);
                userId = userInfo.pkey;
            }
            paging(userId,pageId);
        }
        function paging(userId,pageId) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $http({ method: 'POST', url: 'api/Comments', data: { method: "GetAllComments", parameters: [userId,pageId] } })
              .success(function (result) {
                $scope.ispaging = false;
                vm.pagerData = result.item1[0];
                if (parseInt(vm.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                vm.pageList = [];
                if (pageId > 3) {
                    vm.pageList.push(pageId - 2);
                    vm.pageList.push(pageId - 1);
                    vm.pageList.push(pageId);
                    if (pageId < vm.pagerData.numberOfPages) {
                        vm.pageList.push(pageId + 1);
                        if (pageId < (vm.pagerData.numberOfPages - 1)) {
                            vm.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);

                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        vm.pageList.push(pageId - 2);
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                if (userData.length == 0) {
                    vm.isNoRecords = true;
                }
                else {
                    vm.isNoRecords = false;
                }
                vm.commentList = userData;

            })
         .error(function (error) {
             console.log(error);
         })
        }

        function getallusers() {

            $http({ method: 'POST', url: 'api/Comments', data: { method: 'UsersList' } })
            .success(function (pass) {
                $scope.vm.OrganizationList = pass;
                //vm.editData.regionSiteCodes = '0_0';
            })
            .error(function (fail) {
                console.log(fail);
            });
        }

        vm.getcommentsByUser = function (userId) {
            // alert(userId);

          
            if (userId == "")
                userId = 0;

            vm.commentList = '';
            //$http({ method: 'POST', url: 'api/Comments', data: { method: "GetAllComments", parameters: [userId] } })
            // .success(function (result) {
            //     vm.commentList = result;
            //     if (result.length == 0) {
            //         vm.isNoRecords = true;
            //     }
            //     else {
            //         vm.isNoRecords = false;
            //     }
            // })
            //  .error(function (error) {
            //      console.log(error);
            //  });
            paging(userId, 1);

        }

        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                var userInfo = JSON.parse(localStorage.getItem('userInfo'));
                $http({ method: 'POST', url: 'api/Comments', data: { method: "SearchComments", parameters: [userInfo.pkey, search] } })
              .success(function (result) {
                  vm.commentList = result;
                  if (result.length == 0) {
                      vm.isNoRecords = true;
                  }
                  else {
                      vm.isNoRecords = false;
                  }
               })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    activate();
                }
            }
        }

        vm.deleteComment = function (singlecomment) {
            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/Comments', data: { method: "DeleteComment", parameters: [singlecomment.id] } })
            .success(function (result) {

                var i = vm.commentList.indexOf(singlecomment);
                if (i != -1) {
                    vm.commentList.splice(i, 1);
                }
                log("Comment deleted successfully");
            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {
            });
        };

        vm.respondComment = function (singlecomment) {
            var editModal = $modal.open({
                templateUrl: 'app/admin/newRespondComment.html',
                controller: 'newRespondComment',
                backdrop: 'static',
                resolve: {
                    respondInfo: function () {
                        return singlecomment;
                    }
                }
            });
            editModal.result.then(function (singlecomment) {
                activate();
            }, function () {

            });
        }

        vm.sendmail = function () {
            $scope.ManagecommentsSubmit = true;
            $scope.ismessage = '';
            var mailtext = vm.comment.commentText;
            var mailSubject = vm.comment.commentSubject;
            if (mailtext === undefined || mailSubject === undefined) {
                $scope.ismessage = 'Please enter subject and comment';
                return false;
            }
            var mailUserid = $scope.selectedMailUser;

            if (mailUserid == "")
                mailUserid = 0;

            $http({
                method: 'POST', url: 'api/Comments',
                data: { method: "SendCommentsAllUsers", parameters: [mailUserid, mailtext, mailSubject] }
            })
                 .success(function (result) {
                     log("Your comment has been sent.");
                     $location.path('/dashboard');
                     //vm.Comments = "";                    
                 })
                 .error(function (error) {
                     console.log(error);
                 })
        }
        vm.Filter = function () {
            vm.commentList = [];
            var sdt = vm.comment.beginDate;
            var edt = vm.comment.endDate;
            var userid = $scope.selectedUser;
            if (userid == "")
                userid = 0;

            $scope.ismessage = "";
            if (sdt === undefined && edt === undefined) {
                $scope.ismessage = 'Please enter start date and end date';
                return false;
            }
            if (sdt !== undefined && edt !== undefined) {
                var sday = sdt.getDate();
                var smonth = sdt.getMonth();
                var syear = sdt.getFullYear();

                var eday = edt.getDate();
                var emonth = edt.getMonth();
                var eyear = edt.getFullYear();

                var sdate = new Date();
                sdate.setFullYear(syear, smonth, sday);
                var edate = new Date();
                edate.setFullYear(eyear, emonth, eday);
                if (edate < sdate) {
                    $scope.ismessage = 'End date cannot be greater than begin date';
                    return false;
                }
            }
            var startingDate = GetformattedDate(new Date(vm.comment.beginDate));
            var endingDate = GetformattedDate(new Date(vm.comment.endDate));
            $scope.ispaging = false;
            $http({ method: 'POST', url: 'api/Comments', data: { method: "GetCommentsByDate", parameters: [startingDate,endingDate,userid] } })
              .success(function (result) {
                  vm.commentList = result;
                  if (result.length == 0) {
                      vm.isNoRecords = true;
                  }
                  else {
                      vm.isNoRecords = false;
                  }
              })
               .error(function (error) {
                   console.log(error);
               });
            
        }


        vm.loadConversations = function (e, singlecomment) {


            $(e.target).parent().parent().next().toggleClass("expand")
            $(e.target).toggleClass('nestedBtn nestedBtnExpand');
            getConversations(singlecomment);


        }

        function getConversations(singlecomment) {
            $http({ method: 'POST', url: 'api/Comments', data: { method: "GetCommentsByParentId", parameters: [singlecomment.parentId] } })
              .success(function (result) {

                  singlecomment.conversationList = result;
              })
            .error(function (error) {
                logError(error);
            })
        }
        function GetformattedDate(date) {

            var month = '' + (date.getMonth() + 1),
                   day = '' + date.getDate(),
                   year = date.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }
    }
})();
