//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.Web.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class IntakeType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public string IntakeTypeName { get; set; }
        public bool IsActive { get; set; }
    }
}
