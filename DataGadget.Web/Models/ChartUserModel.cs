﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataGadget.Web.Models
{
    [KnownTypeAttribute(typeof(OrganizationModel))]
    public class ChartUserModel
    {
        public string UserName { get; set; }
        public decimal SumOfPrepHours { get; set; }
        public decimal SumOfTravelHours { get; set; }
        public decimal SumOfServiceHours { get; set; }
        public decimal EvidenceBasedHours { get; set; }
        public decimal NonEvidenceBasedHours { get; set; }
        public decimal TargetHours { get; set; }
        public decimal RemainingHours { get; set; }
        public decimal NonEvidenceRemainingHours { get; set; }
        public string AvgResult { get; set; }
        public string TotalHours { get; set; }
        public string TotalRemainingHours { get; set; }
        public string NonEvidenceTotalRemainingHours { get; set; }
        public string TotalTargetHours { get; set; }

        public decimal PrepHoursPercent { get; set; }

        public decimal TravelHoursPercent { get; set; }

        public decimal ServiceHoursPercent { get; set; }

        public decimal NonEvidencePrepHoursPercent { get; set; }

        public decimal NonEvidenceTravelHoursPercent { get; set; }

        public decimal NonEvidenceServiceHoursPercent { get; set; }

        public decimal NonEvidenceSumofPrepHours { get; set; }
        public decimal NonEvidenceSumofTravelHours { get; set; }
        public decimal NonEvidenceSumofServiceHours { get; set; }

        //public string ErrMessage { get; set; }
    }
}