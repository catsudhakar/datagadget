﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_P13 : Report_Base
    {


        public Report_P13()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;


        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }

        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;

            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Form P13 - Number of Persons Served by Type of Intervention"));

            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);
            //PdfPCell cell;

            //table.WidthPercentage = 100;

            //float[] headerwidths = { 100 }; // percentage
            //table.SetWidths(headerwidths);

            //cell = new PdfPCell(ResultsTable(this.ReportSurveyID));
            //cell.Border = Rectangle.NO_BORDER;
            //table.AddCell(cell);


            this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataTable dtData = new DataTable();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_P13 ");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");
           

            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dtData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dtData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(3);
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase("", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Number of Persons Served by Individual- or Population-Based\nProgram or Strategy", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.Colspan = 2;
            table.AddCell(cell);



            cell = new PdfPCell(NewPhrase("Intervention Type", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            cell.VerticalAlignment = PdfCell.ALIGN_BOTTOM;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("A.\nIndividual-Based\nPrograms and Strategies", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_BOTTOM;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("B.\nPopulation-Based\nPrograms and Strategies", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_BOTTOM;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            table.HeaderRows = 2;

            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("1. Universal Direct"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Individual_Univ_Direct"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase("N/A"));
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Population_Univ_Direct"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("2. Universal Indirect"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase("N/A"));
            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Individual_Univ_InDirect"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Population_Univ_Indirect"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("3. Selective"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Individual_Selected"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("N/A"));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("4. Indicated"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Individual_Indicated"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("N/A"));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("5. Total"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Individual_Total"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Population_Total"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            return table;
        }



    }
}
