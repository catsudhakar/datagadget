﻿









(function () {
    'use strict';
    var controllerId = 'waitingData';
    angular
        .module('app')
        .controller(controllerId, waitingData);

    waitingData.$inject = ['$scope', '$http', '$location', 'common', '$modal','$route'];

    function waitingData($scope, $http, $location, common, $modal, $route) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'waitingData';
        var userid = localStorage.getItem('userId');
        var vm = this;
        vm.events = [];

        $scope.isdescending = false;
        $scope.isAescending = false;

        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        vm.pagination = function (pageId) {
            var strSort = document.getElementById('hdnSortType').value;
            paging(pageId, strSort);
        }
        function paging(pageId, sortType) {
            $http({ method: 'POST', url: 'api/discharges', data: { method: "GetWaitingData", parameters: [localStorage.getItem('userInfo'), pageId, sortType] } })
         .success(function (result) {
             $scope.ispaging = false;
             vm.pagerData = result.item1[0];
             if (parseInt(vm.pagerData.numberOfPages) > 1) {
                 $scope.ispaging = true;
             }
             vm.pageList = [];
             if (pageId > 3) {
                 vm.pageList.push(pageId - 2);
                 vm.pageList.push(pageId - 1);
                 vm.pageList.push(pageId);
                 if (pageId < vm.pagerData.numberOfPages) {
                     vm.pageList.push(pageId + 1);
                     if (pageId < (vm.pagerData.numberOfPages - 1)) {
                         vm.pageList.push(pageId + 2);
                     }
                 }
             }
             else {
                 if (pageId == 1) {
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                     if (pageId + 4 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 4);
                     }

                 }
                 if (pageId == 2) {
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);

                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                 }
                 if (pageId == 3) {
                     vm.pageList.push(pageId - 2);
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                 }

             }

             var userData = result.item2;
             vm.events = userData;
         })
         .error(function (error) {
             console.log(error);
         })
        }
        function activate() {
            if (document.getElementById('hdnSortType'))
                document.getElementById('hdnSortType').value = '';
            $scope.isdescending = true;
            //paging(1, 'Desc');
            paging(1, '');
        }

        vm.newEvent = function () {
            $location.path('/discharges');
        }



        vm.bedAllocation = function (event) {

            //alert(event);

            $http({ method: 'POST', url: 'api/discharges', data: { method: "updateWaitingData", parameters: [JSON.stringify(event)] } })
          //  $http({ method: 'POST', url: 'api/discharges', data: { method: "GetWaitingData", parameters: [localStorage.getItem('userInfo'), pageId, sortType] } })

              .success(function (result) {
                  if (result) {
                      if (result.returntype = 'No') {
                          alert('Unable allocate bed')
                      }
                      else {
                          $route.reload();
                      }
                  }

              })
              .error(function (error) {

                  console.log(error);
              });

        };


        $scope.today = function () {

            // $scope.today();

            //$scope.clear = function () {
            //    $scope.dt = null;
            //};

            //// Disable weekend selection
            //$scope.disabled = function (date, mode) {
            //    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            //};

            //$scope.toggleMin = function () {
            //    $scope.minDate = $scope.minDate ? null : new Date();
            //};
            //$scope.toggleMin();

            //$scope.open = function ($event, id) {

            //    $event.preventDefault();
            //    $event.stopPropagation();
            //    var dynamicTextCal = "opened" + id;
            //    alert(dynamicTextCal);
            //    // $scope.opened = true;

            //    $scope.dynamicTextCal = true;
            //};

            //$scope.dateOptions = {
            //    formatYear: 'yy',
            //    startingDay: 1
            //};

            //$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate', 'MM/dd/yyyy'];
            //$scope.format = $scope.formats[4];


            //$scope.onChange = function (event) {

            //   // alert('Hi');
            //    alert(event);

            //    $http({ method: 'POST', url: 'api/discharges', data: { method: "DateEvents", parameters: [event] } })
            //   // checkPassword($scope.user.password);
            //}




        }

        vm.ContactSorting = function (sortType) {



            if (sortType == "Desc") {
                document.getElementById('hdnSortType').value = "Desc";
                $scope.isdescending = false;
                $scope.isAescending = true;
            }
            else if (sortType == "Asc") {
                document.getElementById('hdnSortType').value = "Asc";
                $scope.isdescending = true;
                $scope.isAescending = false;
            }
            else {
                document.getElementById('hdnSortType').value = "Nothing";
                $scope.isdescending = true;
                $scope.isAescending = false;
            }


            paging(1, sortType);
        }




    }
})();










