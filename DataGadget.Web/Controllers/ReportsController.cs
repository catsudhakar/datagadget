﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DataGadget.Web.Reports;

namespace DataGadget.Web.Controllers
{
    public class ReportsController : ApiController
    {
        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                    case "GetReport12_A":
                        return GetReport12_A(value.Parameters);

                    case "InitializeUserForm":
                        return await InitializeUserForm();
                    case "DischargeInitializeUserForm":
                        return await DischargeInitializeUserForm(value.Parameters);

                    case "GetBaseStringDataForReport12_A":
                        return GetBaseStringDataForReport12_A(value.Parameters);

                    case "GetDataIndigentReport":
                        return await GetDataIndigentReport();
                    case "GetProgrmByLocId":
                        return await GetProgrmByLocId(value.Parameters);

                    case "GetData":
                        return await GetData();

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
                // WebLogger.Error("Exception In ReportsController : " + ex.Message); 
                //return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<List<Location>> GetLocationListAsync()
        {
            //            select stateRegionCode, siteCode, (stateRegionCode + '_' + siteCode) as regionsitecodes, 
            //friendlyName_loc from tblLocations where stateAbbrev = 'MS' order by friendlyName_loc 
            try
            {
                var locations = new List<Location>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.tblLocations.ToListAsync();
                    foreach (var item in dblocations)
                    {
                        locations.Add(
                            new Location
                            {
                                PKey = item.PKey,
                                StateRegionCode = item.StateRegionCode + '_' + item.SiteCode,
                                FriendlyName_loc = item.FriendlyName_loc
                            }
                            );
                    }
                }
                locations = locations.OrderBy(x => x.FriendlyName_loc).ToList();
                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetDataIndigentReport()
        {

            var reportData = new IndigentReportData
            {

                LocationList = await GetLocationListAsync(),
                ServiceLocations = await GetServiceListAsync(),
                Programmes = await GetProgramListAsync(0)

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, reportData);
        }

        async private Task<HttpResponseMessage> GetProgrmByLocId(string[] p)
        {

            int agencyId = Convert.ToInt32(p[0]);
            var reportData = new IndigentReportData
            {

                Programmes = await GetProgramListAsync(agencyId)

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, reportData);
        }


        async private Task<List<surveySerivesModel>> GetServiceListAsync()
        {

            try
            {
                var locations = new List<surveySerivesModel>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.SurveyServices.ToListAsync();
                    foreach (var item in dblocations)
                    {
                        locations.Add(
                            new surveySerivesModel
                            {
                                RecNo = item.RecNo,
                                Description = item.Description

                            }
                            );
                    }
                }
                locations = locations.OrderBy(x => x.Description).ToList();
                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<List<TreatmentProg>> GetProgramListAsync(int agencyId)
        {

            try
            {
                var locations = new List<TreatmentProg>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.TreatmentPrograms.ToListAsync();
                    foreach (var item in dblocations)
                    {
                        locations.Add(
                            new TreatmentProg
                            {
                                RecNo = item.RecNo,
                                ProgramName = item.ProgramName,
                                LocationID = item.LocationID
                            }
                            );
                    }
                }
                if (agencyId > 0)
                    locations = locations.Where(x => x.LocationID == agencyId).OrderBy(x => x.ProgramName).ToList();
                else
                    locations = locations.OrderBy(x => x.ProgramName).ToList();

                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetData()
        {

            var user = new IndigentReportData
            {

                LocationList = await GetLocationListAsync(),

                IntakeValues = await GetIntakevalues()

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, user);
        }

        async private Task<HttpResponseMessage> InitializeUserForm()
        {

            var user = new OneTimeEvent
            {

                LocationList = await GetLocationListAsync()

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, user);
        }

        async private Task<HttpResponseMessage> DischargeInitializeUserForm(string[] p)
        {
            try
            {
                User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }

                var locations = new List<Location>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.tblLocations.ToListAsync();
                    foreach (var item in dblocations)
                    {
                        locations.Add(
                            new Location
                            {
                                PKey = item.PKey,
                                StateRegionCode = item.StateRegionCode + '_' + item.SiteCode,
                                FriendlyName_loc = item.FriendlyName_loc
                            }
                            );
                    }
                }

                if (newUserId.Level == "SIT2A" || newUserId.Level == "SIT3U")
                    locations = locations.Where(x => x.PKey == newUserId.LocationId).ToList();

                locations = locations.OrderBy(x => x.FriendlyName_loc).ToList();

                var user = new OneTimeEvent
                {

                    LocationList = locations



                };

                return this.Request.CreateResponse(HttpStatusCode.OK, user);
                
               


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Convert an object to a byte array
        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();


        }
        private HttpResponseMessage GetReport12_A(string[] p)
        {
            try
            {
                byte[] s;
                var events = new OneTimeEvent();
                //var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    events = context.suretool_Report_12A(DateTime.Now.AddDays(-10), DateTime.Now, "000008", "000001")
                        .Select(x => new OneTimeEvent { Age0 = x.age0, Age12 = x.age12, Age5 = x.age5, Age15 = x.age15, Age18 = x.age18, Age21 = x.age21, Age25 = x.age25, Age45 = x.age45, Age65 = x.age65 }).SingleOrDefault();


                    //foreach (var oneevent in result)
                    //{
                    //    events.Add(
                    //        new OneTimeEvent { Age0 = oneevent.age0, Age12 = oneevent.age12, Age15 = oneevent.age15, Age18 = oneevent.age18, Age21 = oneevent.age21, Age25 = oneevent.age25 }
                    //        );

                    //}
                    s = ObjectToByteArray(events);

                    string base64String;

                    base64String = System.Convert.ToBase64String(s, 0, s.Length);

                    events.ProgramName = base64String;

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, events);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private HttpResponseMessage GetBaseStringDataForReport12_A(string[] p)
        {
            try
            {
                Models.reportModel oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.reportModel>(p[0]);

                //DateTime BeginDate = oneTime.BeginDate.AddDays(1);// DateTime.Now.AddDays(-100);//Convert.ToDateTime(p[0]);
                //DateTime EndDate = oneTime.EndDate.AddDays(1);//DateTime.Now.AddDays(1);//Convert.ToDateTime(p[1]);

                DateTime BeginDate = oneTime.StartDate;
                DateTime EndDate = oneTime.EndDate2;

                string RegionCode = oneTime.RegionCode;//"000008";// Convert.ToString(p[2]);
                string SiteCode = oneTime.SiteCode;//"000001";//Convert.ToString(p[3]);
                string OrgName = oneTime.OrganizationName;//"New Roads";//oneTime.Organization;//"New Roads";//Convert.ToString(p[4]);
                string ReportType = oneTime.ReportType;
                string ReportId = oneTime.BedUtilizationReportId;

                string base64String = string.Empty; ;
                System.IO.MemoryStream memStream = new System.IO.MemoryStream();
                Byte[] byteArray = null;


                switch (ReportType)
                {
                    case "12A":
                        Report_12A report12A = new Report_12A();
                        report12A.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "12B":
                        Report_12B report12B = new Report_12B();
                        report12B.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "P13":
                        Report_P13 reportP13 = new Report_P13();
                        reportP13.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "6A":
                        Report_6A report6A = new Report_6A();
                        report6A.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "P14":
                        Report_P14 reportp14 = new Report_P14();
                        reportp14.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "Goal2":
                        Report_Goal2 reportG12 = new Report_Goal2();
                        reportG12.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "ActivityReport":
                        Report_Activity reportAct = new Report_Activity();
                        reportAct.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "DataEntryReport":
                        Report_DataEntry reportData = new Report_DataEntry();
                        reportData.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "EvidenceSummary":
                        Report_EvidenceSummary reportEvidenceSummary = new Report_EvidenceSummary();
                        reportEvidenceSummary.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, "EvidenceSummary");
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;
                    case "Faith-Based":
                        Report_EvidenceSummary reportFaithBased = new Report_EvidenceSummary();
                        reportFaithBased.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, "Faith-Based");
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "Utilization":
                        Report_EvidenceSummary reportEvidenceSummary1 = new Report_EvidenceSummary();
                        reportEvidenceSummary1.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, "Utilization");
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "IndigentReport":
                    case "PREGNANT":
                    case "PANDP":
                    case "IVDRUG":
                    case "CO":
                    case "WITHDRAWAL":
                    case "ALCOHOL":
                    case "OPIOID":
                    case "METHAMPHETAMINE":
                    case "BENZODIAZAPINES":
                    case "OTHER":
                    case "SELFPAY":
                    case "PRIVATEINSURANCE":
                    case "MEDICAID":
                        Report_EvidenceSummary indigentSummary = new Report_EvidenceSummary();
                        EndDate = EndDate.AddSeconds(86399);
                        // indigentSummary.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, "IndigentReport");
                        indigentSummary.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, ReportType);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;


                    case "FollowUp":
                        FollowupReport reportFollowup = new FollowupReport();
                        reportFollowup.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;

                    case "BedUtilizationReport":
                        Report_EvidenceSummary BedUtilizationReport = new Report_EvidenceSummary();
                        EndDate = EndDate.AddSeconds(86399);
                        int Rid = Convert.ToInt32(ReportId);
                        string Name = string.Empty;
                        using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                        {
                            Name = context.IntakeTypes.Where(x => x.Id == Rid).Select(x => x.IntakeTypeName).FirstOrDefault();
                           
                        }

                        // indigentSummary.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, "IndigentReport");
                        BedUtilizationReport.GenerateToMemoryStream(memStream, BeginDate, EndDate, RegionCode, SiteCode, OrgName, ReportType, ReportId,Name);
                        byteArray = memStream.ToArray();
                        base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                        break;




                }

                memStream.Close();
                return this.Request.CreateResponse(HttpStatusCode.OK, base64String);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async private Task<List<IntakeTypeVlaues>> GetIntakevalues()
        {

            try
            {
                var intakeVlaues = new List<IntakeTypeVlaues>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.IntakeTypes.Where(x => x.IsActive == true).ToListAsync();
                    foreach (var item in dblocations)
                    {
                        intakeVlaues.Add(
                            new IntakeTypeVlaues
                            {

                                Id = item.Id,
                                TypeName = item.TypeName,
                                IntakeTypeName = item.IntakeTypeName
                            }
                            );
                    }
                }
                //intakeVlaues = locations.OrderBy(x => x.FriendlyName_loc).ToList();
                return intakeVlaues;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
