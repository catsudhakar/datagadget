﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class Location
    {
        public int PKey { get; set; }
        public string StateAbbrev { get; set; }
        public string StateRegionCode { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string FriendlyName_loc { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public int SIG { get; set; }
        public string FS_MH { get; set; }
        public string FedRegionCode { get; set; }
        public string FedRegionName { get; set; }
        public Nullable<int> BedCount { get; set; }
    }
}