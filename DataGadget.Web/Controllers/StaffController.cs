﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;

using DataGadget.Web.Reports;

//using DataGadget.Web.DataModel;

namespace DataGadget.Web.Controllers
{
    public class StaffController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                    case "AllRecords":
                        return await AllRecords(value.Parameters);
                    case "SearchRecords":
                        return await SearchRecords(value.Parameters);
                    case "CreateStaff":
                        return CreateStaff(value.Parameters);
                    case "GetStaff":
                        return await GetStaff(value.Parameters);
                    case "DeleteStaff":
                        return DeleteStaff(value.Parameters);
                    case "InitializeStaffForm":
                        return await InitializeStaffForm();
                    case "GetStaffForReport":
                        return await GetStaffForReport(value.Parameters);

                    case "GetBaseStringDataForReport":
                        return GetBaseStringDataForReport(value.Parameters);


                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
               // WebLogger.Error("Exception In StaffController : " + ex.Message); 
                //return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;


            }
        }

        private HttpResponseMessage GetBaseStringDataForReport(string[] p)
        {
            try
            {
                Models.StaffAdmin oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.StaffAdmin>(p[0]);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    if (oneTime.ProviderID > 0)
                    {
                        oneTime.ProviderName = context.tblProviders.Where(x => x.ID == oneTime.ProviderID).Select(x => x.Name).SingleOrDefault();
                    }
                    else
                    {

                        if (oneTime.ProviderName != "0")
                        {
                            int i = Convert.ToInt32(oneTime.ProviderName);
                            oneTime.ProviderName = context.tblProviders.Where(x => x.ID == i).Select(x => x.Name).SingleOrDefault();
                        }

                        else
                        {
                            oneTime.ProviderName="Other";
                        }

                    }
                }

               
                string base64String = string.Empty; ;
               
                System.IO.MemoryStream memStream = new System.IO.MemoryStream();
                Report_CurriculumSummary report = new Report_CurriculumSummary();

                report.GenerateToMemoryStreamForStaff(memStream, oneTime);
                Byte[] byteArray = memStream.ToArray();
                base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);
                memStream.Close();

                return this.Request.CreateResponse(HttpStatusCode.OK, base64String);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        async private Task<HttpResponseMessage> InitializeStaffForm()
        {

            var staff = new StaffAdmin
            {
                ProviderList = await GetProvidersListAsync()
            };

            return this.Request.CreateResponse(HttpStatusCode.OK, staff);
        }


        async private Task<List<Provider>> GetProvidersListAsync()
        {
            try
            {
                var providers = new List<Provider>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    providers = context.tblProviders.Where(x => x.IsActive == true).
                        Select(x =>
                            new Provider
                            {
                                ID = x.ID,
                                Name = x.Name
                            }).ToList();
                }
                Provider pr = new Provider();
                pr.ID = 0;
                pr.Name = "Other";
                providers.Add(pr);
                return providers;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> AllRecords(string[] p)
        {
            try
            {
                Guid uid = new Guid(p[0]);

                List<StaffAdmin> res;
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                   
                    int userID = context.TblUsers.Where(x => x.UserGUID == uid).Single().PKey;
                    res = new List<StaffAdmin>();

                    int rowCount = await context.tblStaffHours.Where(x => x.IsActive == true && x.UserID == userID).OrderByDescending(x=>x.EndDate).CountAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();


                    res = await context.tblStaffHours.Where(x => x.IsActive == true && x.UserID == userID).OrderByDescending(x=>x.EndDate).
                        Select(x =>
                        new StaffAdmin
                        {

                            InstructorName=x.InstructorName,
                            LearningObjectives=x.LearningObjectives,

                            CreatedOn = x.CreatedOn,
                            EducationCredits = x.EducationCredits,
                            EndDate = x.EndDate,
                            EvidenceBased = x.EvidenceBased,
                            Hours = x.Hours,
                            ID = x.ID,
                            IsActive = x.IsActive,
                            OtherProviderName = x.OtherProviderName,
                            ProviderName = x.tblProvider.Name,
                            ProviderID = x.ProviderID,
                            StartDate = x.StartDate,
                            TrainerDeatils = x.TrainerDeatils,
                            TrainingName = x.TrainingName,
                            UpdatedOn = x.UpdateOn,
                            UserName = context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().FirstName + " " + context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().LastName

                        }).Skip(skip).Take(take).ToListAsync();
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<StaffAdmin>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<StaffAdmin>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, res);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }
        async private Task<HttpResponseMessage> SearchRecords(string[] p)
        {
            try
            {
                Guid uid = new Guid(p[0]);

                List<StaffAdmin> res;
                string search = p[1];

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    int userID = context.TblUsers.Where(x => x.UserGUID == uid).Single().PKey;
                    res = new List<StaffAdmin>();

                    res = await context.tblStaffHours.Where(x => x.IsActive == true && x.UserID == userID).OrderByDescending(x => x.EndDate).
                        Select(x =>
                        new StaffAdmin
                        {

                            InstructorName = x.InstructorName,
                            LearningObjectives = x.LearningObjectives,

                            CreatedOn = x.CreatedOn,
                            EducationCredits = x.EducationCredits,
                            EndDate = x.EndDate,
                            EvidenceBased = x.EvidenceBased,
                            Hours = x.Hours,
                            ID = x.ID,
                            IsActive = x.IsActive,
                            OtherProviderName = x.OtherProviderName,
                            ProviderName = x.tblProvider.Name,
                            ProviderID = x.ProviderID,
                            StartDate = x.StartDate,
                            TrainerDeatils = x.TrainerDeatils,
                            TrainingName = x.TrainingName,
                            UpdatedOn = x.UpdateOn,
                            UserName = context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().FirstName + " " + context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().LastName

                        }).Where(x => x.UserName.Contains(search) || x.TrainingName.Contains(search) || x.ProviderName.Contains(search) || x.OtherProviderName.Contains(search) ).ToListAsync();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private HttpResponseMessage CreateStaff(string[] p)
        {
            try
            {
                Models.StaffAdmin staffAdmin = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.StaffAdmin>(p[0]);
                var staff = new DataModel.tblStaffHour();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    Guid uid = new Guid(staffAdmin.UserName);
                    int userid = context.TblUsers.Where(x => x.UserGUID == uid).Single().PKey;
                    if (staffAdmin.ID == 0 || staffAdmin.ID == null)
                    {
                        staff.CreatedOn = DateTime.Now;
                        staff.EndDate = staffAdmin.EndDate;
                        staff.Hours = staffAdmin.Hours;
                        staff.IsActive = true;
                        if (staffAdmin.ProviderName != "0")
                            staff.ProviderID = Convert.ToInt32(staffAdmin.ProviderName);
                        else
                            staff.ProviderID = null;
                        staff.StartDate = staffAdmin.StartDate;
                        staff.TrainingName = staffAdmin.TrainingName;
                        staff.UpdateOn = DateTime.Now;
                        staff.UserID = userid;
                        staff.OtherProviderName = staffAdmin.OtherProviderName;
                        staff.TrainerDeatils = staffAdmin.TrainerDeatils;
                        staff.EducationCredits = staffAdmin.EducationCredits;
                        staff.EvidenceBased = staffAdmin.EvidenceBased;

                        staff.InstructorName = staffAdmin.InstructorName;
                        staff.LearningObjectives = staffAdmin.LearningObjectives;
                        
                        context.tblStaffHours.Add(staff);
                        context.SaveChanges();
                    }
                    else   //updating staff
                    {
                        DataGadget.Web.DataModel.tblStaffHour U = new DataGadget.Web.DataModel.tblStaffHour();
                        staff = context.tblStaffHours.Where(p1 => p1.ID == staffAdmin.ID).SingleOrDefault();
                        staff.EndDate = staffAdmin.EndDate;
                        staff.Hours = staffAdmin.Hours;
                        staff.IsActive = true;
                        if (staffAdmin.ProviderName != "0")
                            staff.ProviderID = Convert.ToInt32(staffAdmin.ProviderName);
                        else
                            staff.ProviderID = null;
                        staff.StartDate = staffAdmin.StartDate;
                        staff.TrainingName = staffAdmin.TrainingName;
                        staff.UpdateOn = DateTime.Now;
                        staff.UserID = userid;
                        staff.OtherProviderName = staffAdmin.OtherProviderName;
                        staff.TrainerDeatils = staffAdmin.TrainerDeatils;
                        staff.EducationCredits = staffAdmin.EducationCredits;
                        staff.EvidenceBased = staffAdmin.EvidenceBased;

                        staff.InstructorName = staffAdmin.InstructorName;
                        staff.LearningObjectives = staffAdmin.LearningObjectives;

                        context.SaveChanges();
                    }
                    staffAdmin.ID = staff.ID;
                    return this.Request.CreateResponse(HttpStatusCode.OK, staffAdmin);
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, staff);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void Put(int id, [FromBody]string value)
        {

        }
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
        public HttpResponseMessage DeleteStaff(string[] p)
        {
            try
            {
                int sid = Convert.ToInt32(p[0]);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var res = context.tblStaffHours.Where(x => x.ID == sid).SingleOrDefault();
                    context.tblStaffHours.Remove(res);
                    context.SaveChanges();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);
            }
            catch (Exception)
            {

                throw;
            }

        }

        async private Task<HttpResponseMessage> GetStaff(string[] p)
        {

            try
            {
                StaffAdmin staff = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int sid = Convert.ToInt32(p[0]);
                    staff = await context.tblStaffHours.Where(x => x.ID == sid)
                        .Select(x =>
                            new StaffAdmin
                            {
                                InstructorName = x.InstructorName,
                                LearningObjectives = x.LearningObjectives,

                                CreatedOn = x.CreatedOn,
                                EducationCredits = x.EducationCredits,
                                EndDate = x.EndDate,
                                EvidenceBased = x.EvidenceBased,
                                Hours = x.Hours,
                                ID = x.ID,
                                IsActive = x.IsActive,
                                OtherProviderName = x.OtherProviderName,
                                ProviderName = x.tblProvider.Name,
                                ProviderID = x.ProviderID,
                                StartDate = x.StartDate,
                                TrainerDeatils = x.TrainerDeatils,
                                TrainingName = x.TrainingName,
                                UpdatedOn = x.UpdateOn,
                                UserName = x.UserID.ToString()
                            }).SingleAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, staff);
            }
            catch (Exception)
            {

                throw;
            }

        }

        async private Task<HttpResponseMessage> GetStaffForReport(string[] p)
        {
            try
            {
                int uid = Convert.ToInt32(p[0]);
                DateTime startDate = Convert.ToDateTime(p[1]);
                DateTime endDate = Convert.ToDateTime(p[2]);

                List<StaffAdmin> res;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                  
                    res = new List<StaffAdmin>();
                    if (uid > 0)
                    {
                        res = await context.tblStaffHours.Where(x => x.IsActive == true && x.UserID==uid && x.StartDate >= startDate && x.EndDate <= endDate).
                           Select(x =>
                           new StaffAdmin
                           {
                               InstructorName = x.InstructorName,
                               LearningObjectives = x.LearningObjectives,

                               CreatedOn = x.CreatedOn,
                               EducationCredits = x.EducationCredits,
                               EndDate = x.EndDate,
                               EvidenceBased = x.EvidenceBased,
                               Hours = x.Hours,
                               ID = x.ID,
                               IsActive = x.IsActive,
                               OtherProviderName = x.OtherProviderName,
                               ProviderName = x.tblProvider.Name,
                               ProviderID = x.ProviderID,
                               StartDate = x.StartDate,
                               TrainerDeatils = x.TrainerDeatils,
                               TrainingName = x.TrainingName,
                               UpdatedOn = x.UpdateOn,
                               UserName = context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().FirstName + " " + context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().LastName

                           }).ToListAsync();
                    }
                    else
                    {
                        res = await context.tblStaffHours.Where(x => x.IsActive == true && x.StartDate >= startDate && x.EndDate <= endDate).
                            Select(x =>
                            new StaffAdmin
                            {
                                InstructorName = x.InstructorName,
                                LearningObjectives = x.LearningObjectives,

                                CreatedOn = x.CreatedOn,
                                EducationCredits = x.EducationCredits,
                                EndDate = x.EndDate,
                                EvidenceBased = x.EvidenceBased,
                                Hours = x.Hours,
                                ID = x.ID,
                                IsActive = x.IsActive,
                                OtherProviderName = x.OtherProviderName,
                                ProviderName = x.tblProvider.Name,
                                ProviderID = x.ProviderID,
                                StartDate = x.StartDate,
                                TrainerDeatils = x.TrainerDeatils,
                                TrainingName = x.TrainingName,
                                UpdatedOn = x.UpdateOn,
                                UserName = context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().FirstName + " " + context.TblUsers.Where(usr => usr.PKey == x.UserID).FirstOrDefault().LastName

                            }).ToListAsync();
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}