﻿(function () {
    'use strict';
    var controllerId = 'staffDevelopment';
    angular
        .module('app')
        .controller(controllerId, staffDevelopment);

    staffDevelopment.$inject = ['$scope', '$http', '$modal', '$location', 'common'];

    function staffDevelopment($scope, $http, $modal, $location, common) {
        var userid = localStorage.getItem('userId');
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error')
        $scope.title = 'staffDevelopment';
        var vm = this;
        vm.staffs = [];
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        activate();       

        function activate() {
            paging(1);
        }

        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            $http({ method: 'POST', url: 'api/Staff', data: { method: "AllRecords", parameters: [userid,pageId] } })
             .success(function (result) {
                $scope.ispaging = false;
                vm.pagerData = result.item1[0];
                if (parseInt(vm.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                vm.pageList = [];
                if (pageId > 3) {
                    vm.pageList.push(pageId - 2);
                    vm.pageList.push(pageId - 1);
                    vm.pageList.push(pageId);
                    if (pageId < vm.pagerData.numberOfPages) {
                        vm.pageList.push(pageId + 1);
                        if (pageId < (vm.pagerData.numberOfPages - 1)) {
                            vm.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);

                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        vm.pageList.push(pageId - 2);
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                vm.staffs = userData;

            })
         .error(function (error) {
             console.log(error);
         })
        }

        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                var userInfo = JSON.parse(localStorage.getItem('userInfo'));
                $http({ method: 'POST', url: 'api/Staff', data: { method: "SearchRecords", parameters: [userid, search] } })
               .success(function (result) {
                   vm.staffs = result;
               })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    activate();
                }
            }
        }


        vm.viewReport = function (staff) {


            var modalInstance = $modal.open({
                templateUrl: 'app/admin/staffDevelopmentPreview.html',
                controller: 'staffDevelopmentPreview',
                backdrop: 'static',
                resolve: {
                    staffInfo: function () {
                        return staff;
                    }
                }
            });
        };

        vm.addStaffDevelopment = function () {
            var addStaffDevelopmentModal = $modal.open({
                templateUrl: 'app/admin/newStaffDevelopment.html',
                controller: 'newStaffDevelopment',
                backdrop: 'static',
                resolve: {
                    staffInfo: function () {
                        return undefined;
                    }
                }

            });
            addStaffDevelopmentModal.result.then(function (staff) {
                //vm.users.push(user);
                activate();

            }, function () {
            });
        }

        vm.editStaffDevelopment = function (staff) {
            var addStaffDevelopmentModal = $modal.open({
                templateUrl: 'app/admin/newStaffDevelopment.html',
                controller: 'newStaffDevelopment',
                backdrop: 'static',
                resolve: {
                    staffInfo: function () {
                        return staff;
                    }
                }
            });
            addStaffDevelopmentModal.result.then(function (staff) {
                activate();
            }, function () {
            });
        }
        vm.deleteStaffDevelopment = function (staff) {
            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {
                $http({ method: 'POST', url: 'api/Staff', data: { method: "DeleteStaff", parameters: [staff.id] } })
            .success(function (result) {
                var i = vm.staffs.indexOf(staff);
                if (i != -1) {
                    vm.staffs.splice(i, 1);
                }
                log("Staff Development Record deleted successfully");
            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {
            });
        };
    }
})();
