﻿(function () {
    'use strict';
    var controllerId = 'resetpassword';
    angular
        .module('app')
        .controller(controllerId, resetpassword);

    resetpassword.$inject = ['$scope', '$location', '$http', 'common', '$window'];

    function resetpassword($scope, $location, $http, common, $window) {
        var pathurl = $location.url();
        var passwordtoken = pathurl.split('=')[1];
        if (passwordtoken==undefined) {
            $location.path('');
        }
        $scope.title = 'Reset Password';
        $scope.errormsg = '';
        $scope.conpassword = '';
        $scope.newpassword = '';
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        var userInfo;//@rafeeq        
        activate();

        function activate() { }

        localStorage.setItem("isUserAuthentic", false);
        $scope.validateUser = function () {

            var htp = window.location.protocol;
            var hst = window.location.host;

            var url = htp + "//" + hst + "/";

           
          

            $scope.errormsg = '';
            $scope.isSubmit = true;
            if ($scope.loginForm.$valid) {
                if ($scope.newpassword == $scope.conpassword) {
                    var data = { method: 'ResetPassword', parameters: [passwordtoken, $scope.newpassword] };
                    $http.post('api/user', data).success(function (result) {
                        if (result != null) {
                            //$scope.errormsg = "password changed successfully.<a href='http://localhost:28386/'>Click here</a> to login. ";
                           $scope.errormsg = "password changed successfully.<a href='" + url + "'>Click here</a> to login. ";
                           

                        } else {
                            $scope.errormsg = 'Password already changed.';
                        }
                    }).error(function (error) {
                        console.log(error);
                    });
                }
                else {
                    $scope.errormsg = 'Please enter confirm password same as new password.';
                }
            }
            else {
                //$scope.errormsg = 'User Name Required';
            }
        }
                
    }
})();
