﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataGadget.Web.Models
{
    [KnownTypeAttribute(typeof(OrganizationModel))]
    public class OrganizationModel
    {
        public string Regionsitecodes { get; set; }
        public string FriendlyName_loc { get; set; }


        public string OrganizationName { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}