﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('followupModel', followupModel);

    followupModel.$inject = ['$scope', '$modalInstance', '$http', 'eventInfo','$location', '$route', 'common'];

    function followupModel($scope, $modalInstance, $http, eventInfo, $location, $route, common) {
        var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
        $scope.title = 'followupModel';

        $scope.FollowUps = {};
        $scope.FollowUpsList = {};

        $scope.lastDate = '';
        $scope.ismessage = '';


        $scope.isSubmit = false;

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(followupModel);
    

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

       

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }

        activate();

        function activate() {
            //var userInfo = JSON.parse($window.localStorage["userInfo"]);
           
            if (eventInfo) {

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                var today = mm + '/' + dd + '/' + yyyy;
               // document.getElementById("DATE").value = today;
                
                $scope.FollowUps.patientId = eventInfo.patientId

                $scope.FollowUps.followupDate = today;
               

                $http({
                    method: 'POST', url: 'api/Common',
                    data: {
                        method: "GetFollowUpdatesByPatId",
                        parameters: [eventInfo.patientId]
                    }
                })
                  .success(function (result) {
                     
                      $scope.FollowUpsList = result;
                      if (result.length > 0) {
                          var i = result.length;
                          $scope.lastDate = result[i-1].followupDate;
                          document.getElementById('hidId').value = i + 1;
                         
                          var element1 = document.getElementById("first");
                          var element2 = document.getElementById("Second");
                          var element3 = document.getElementById("Third");

                         

                          switch (i) {
                             
                              case 1:
                                  element1.classList.remove("active");
                                  element2.classList.add("active");
                                  element3.classList.remove("active");
                                  break;
                              case 2:
                                  element1.classList.remove("active");
                                  element2.classList.remove("active");
                                  element3.classList.add("active");
                                  break;
                              default:
                                  element1.classList.add("active");
                                  element2.classList.remove("active");
                                  element3.classList.remove("active");
                                  break;

                          }
                      }
                      else
                      {
                          document.getElementById('hidId').value = 1;
                      }
                  })
                  .error(function (error) {
                      console.log(error);
                  })

            }
            
        }

        //$scope.gettabId = function (Id) {
        //    document.getElementById('hidId').value=Id;
        //};

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function (frmResourceLink) {

            $scope.isSubmit = true;
            $scope.isdisplay = false;

            var sdt = new Date($scope.FollowUps.followupDate);
            var edt = new Date($scope.lastDate);
       
            if (sdt !== undefined && edt !== undefined) {
                var sday = sdt.getDate();
                var smonth = sdt.getMonth();
                var syear = sdt.getFullYear();

                var eday = edt.getDate();
                var emonth = edt.getMonth();
                var eyear = edt.getFullYear();

                //var dateOne = new Date(2010, 11, 25);
                var sdate = new Date(syear, smonth, sday);
                var edate = new Date(eyear, emonth, eday);
                if (edate > sdate) {
                    $scope.ismessage = 'Current follow up date should be greater then previous follow up date'
                    $scope.isSubmit = false;
                    $scope.isdisplay = true;
                }
            }

            if (frmResourceLink.$valid & $scope.isSubmit == true) {
                $scope.FollowUps.followupId = document.getElementById('hidId').value;
                $scope.FollowUps.surveyItemId = eventInfo.surveyItemId;
                var comm = document.getElementById('notes').value;
                if (comm == "")
                    $scope.FollowUps.followupComment = "";
                $http({
                    method: 'POST', url: 'api/Common',
                    data: {
                        method: "SetFollowUpdates",
                        parameters: [JSON.stringify($scope.FollowUps)]
                    }
                })
                    .success(function (result) {
                        $modalInstance.close(result);
                        log("Follow up contact date saved successfully");
                        $location.path('/dashboard');
                        $route.reload();

                    })
                    .error(function (error) {
                        console.log(error);
                    })


            }
        };
        $scope.onStartSurvey = function () {
            $modalInstance.dismiss('cancel');
            sessionStorage.setItem("patientId", eventInfo.patientId);
            sessionStorage.setItem('surveyAdmin', eventInfo.surveyAdmin);
            sessionStorage.setItem('gender', eventInfo.clientGenderType)
            sessionStorage.setItem('locationId', eventInfo.locationID)
            sessionStorage.setItem('serviceID', eventInfo.serviceID)
            sessionStorage.setItem('programId', eventInfo.programId)

            sessionStorage.setItem('entryDate', eventInfo.entryDate)
            sessionStorage.setItem('dischargeDate', eventInfo.dischargeDate)
            sessionStorage.setItem('from', 'FollowUp');
            $location.path('/treatmentSurveyTools');

        }
    }
})();
