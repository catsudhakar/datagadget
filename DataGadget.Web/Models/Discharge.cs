﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class Discharge
    {

        public string PAT_ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<System.DateTime> DischargeDate { get; set; }
        public string GroupName { get; set; }
        public string LoginID { get; set; }
        public int RecNo { get; set; }

        public List<DischargeCode> DischargeCodeList { get; set; }
        public int? DischargeId { get; set; }

        public string Description { get; set; }

        public string LocationName { get; set; }

        public int SurveyItemId { get; set; }

        public int LocationId { get; set; }
        public string GenderType { get; set; }

        public string returntype { get; set; }


    }
}