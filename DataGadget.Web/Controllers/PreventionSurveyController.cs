﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.DataModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
namespace DataGadget.Web.Controllers
{
    public class PreventionSurveyController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "GetSurveyData":
                        return await GetSurveyData(param.Parameters);
                    case "GetParticipantID":
                        return GetParticipantID(param.Parameters);
                    case "GetSurveyType":
                        return await GetSurveyType(param.Parameters);
                    case "GetProgramName":
                        return await GetProgramName(param.Parameters);
                    case "GetGroupName":
                        return GetGroupName(param.Parameters);
                    case "CreateSurveryTools":
                        return await CreateSurveryTools(param.Parameters);

                    case "GetSurveyCountByEventID":
                        return await GetSurveyCountByEventID(param.Parameters);

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
                //WebLogger.Error("Exception In PreventionSurveyController : " + ex.Message); 
                //return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<HttpResponseMessage> GetSurveyData(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }
                IEnumerable<SurveyList> data = (IEnumerable<SurveyList>)new List<SurveyList>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.Surveys.Where(x => x.SurveyCategoryID == 1).
                        Select(x =>
                            new SurveyList
                            {
                                RecNo = x.RecNo,
                                SurveyTitle = x.SurveyTitle
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetParticipantID(string[] p)
        {
            DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
            if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
            {
                return null;
            }
            string data = string.Empty;
            try
            {
                long i = 1;
                foreach (byte b in Guid.NewGuid().ToByteArray())
                {
                    i *= ((int)b + 1);
                }
                data = string.Format("{0:X}", i - DateTime.Now.Ticks);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetSurveyType(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }
                IEnumerable<NewSurveyType> dataSource = (IEnumerable<NewSurveyType>)new List<NewSurveyType>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    dataSource = await context.SurveyCategoryTypes.Where(x => x.SurveyCategoryID == 1)
                                    .Select(x => new NewSurveyType
                                    {
                                        SurveyTypeID = x.SurveyTypeID,
                                        DisplayName = x.DisplayName
                                    }).ToListAsync();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetProgramName(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[1].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }
                var type = p[0].Trim();
                IEnumerable<ProgramName> data = (IEnumerable<ProgramName>)new List<ProgramName>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.Programs.Where(x => x.Type == type).
                        Select(x =>
                            new ProgramName
                            {
                                Pid = x.ID,
                                Name = x.Name
                            }).OrderBy(x => x.Name).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private HttpResponseMessage GetGroupName(string[] p)
        {
            try
            {
                int Pid = Convert.ToInt32(p[0].Trim());
                IEnumerable<NewGroupName> data = (IEnumerable<NewGroupName>)new List<NewGroupName>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    string PName = context.Programs.Where(x => x.ID == Pid).
                       Select(x => x.Name).SingleOrDefault();
                    data = context.Database.SqlQuery<NewGroupName>("SELECT * FROM [dbo].[Group]").Where(x => x.ProgramName == PName).ToList();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        async private Task<HttpResponseMessage> CreateSurveryTools(string[] parameters)
        {
            try
            {

                var dataResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(parameters[0]);
                //var dynamicData = dataResult.Where(x => x.Key.Contains("question_")).ToList();
                var dynamicData = dataResult.Where(x => x.Key.Contains("question_") || x.Key.Contains("qestionchkNoResponse_")).ToList();
                SerializeJsonData model = new SerializeJsonData();
                model.GroupNameData = dataResult.FirstOrDefault(x => x.Key == "groupNameData").Value;
                model.SurveyEndDate = dataResult.FirstOrDefault(x => x.Key == "surveyEndDate").Value;
                model.Survey = dataResult.FirstOrDefault(x => x.Key == "survey").Value;
                model.SurveyID = dataResult.FirstOrDefault(x => x.Key == "surveyID").Value;
                model.ProgramNameData = dataResult.FirstOrDefault(x => x.Key == "programNameData").Value;
                model.ProgramID = dataResult.FirstOrDefault(x => x.Key == "programID").Value;
                model.UserID = dataResult.FirstOrDefault(x => x.Key == "UserID").Value;
                model.ParticipantID = dataResult.FirstOrDefault(x => x.Key == "participant").Value;
                model.SessionID = dataResult.FirstOrDefault(x => x.Key == "SessionID").Value;
                model.ClientSessionStart = dataResult.FirstOrDefault(x => x.Key == "ClientSessionStart").Value;

                model.EventId = dataResult.FirstOrDefault(x => x.Key == "eventId").Value;

                var otherResponses = dataResult.Where(x => x.Key.Contains("qestion_txtresponse_")).ToList();

                var chkNoResponses = dataResult.Where(x => x.Key.Contains("qestionchkNoResponse_")).ToList();


                foreach (var item in chkNoResponses)
                {
                    int noResponseId = Convert.ToInt32(item.Key.Split('_')[1].ToString());
                    if (chkNoResponses.Any(x => x.Value.Contains("False") && x.Key.Contains("qestionchkNoResponse_" + noResponseId.ToString())))
                    {
                        var record = chkNoResponses.FirstOrDefault(x => x.Key.Contains("qestionchkNoResponse_" + noResponseId.ToString()) && x.Value.Contains("False"));
                        dynamicData.Remove(record);
                    }
                    else if (dynamicData.Any(x => x.Key.Contains("question_" + noResponseId.ToString())))
                    {
                        var record = dynamicData.FirstOrDefault(x => x.Key.Contains("question_" + noResponseId.ToString()));
                        dynamicData.Remove(record);
                    }

                }

                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    #region comm
                    SurveySession surveySession = GetSurveySession(model, context);
                    if (surveySession.RecNo == 0) { context.SurveySessions.Add(surveySession); await context.SaveChangesAsync(); }
                    Client client = GetClient(model.ParticipantID, context);
                    if (client.RecNo == 0) { context.Clients.Add(client); await context.SaveChangesAsync(); }
                    SurveyClient surveyClient = GetSurveyClient(client, Convert.ToInt32(model.ProgramName), model.ClientSessionStart, context);
                    if (surveyClient.RecNo == 0) { context.SurveyClients.Add(surveyClient); await context.SaveChangesAsync(); }
                    SurveyItem surveyItem = GetSurveyItem(surveyClient, model, context);
                    if (surveyItem.RecNo == 0) { context.SurveyItems.Add(surveyItem); await context.SaveChangesAsync(); }

                    foreach (var qesans in dynamicData)
                    {

                        SurveyClientResponse clientResponse = new SurveyClientResponse();
                        clientResponse.SurveyItemID = surveyItem.RecNo;
                        int QuestionID = Convert.ToInt32(qesans.Key.Split('_')[1]);

                        var s = context.SurveyGroupQTNs.Where(x => x.RecNo == QuestionID).SingleOrDefault();
                        string aspcontroltype = s.SurveyASPControlType.ASPControlType.ToString();



                        int num1;
                        int surveyResponseItemId=0;
                        bool res = int.TryParse(qesans.Value, out num1);
                        if (aspcontroltype == "SingleLineTextBox" || aspcontroltype == "MultiLineTextBox")
                        {
                            res = false;
                        }
                        else if (aspcontroltype == "CheckBoxList")
                        {
                            if (qesans.Value == "False")
                            {
                                continue;
                            }
                        }

                        
                        //if (res)
                        //{
                        //    surveyResponseItemId = Convert.ToInt32(qesans.Value);
                        //    clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).SingleOrDefault();
                        //}

                        if (chkNoResponses.Any(x => x.Key.Contains("qestionchkNoResponse_" + QuestionID.ToString()) && x.Value.ToString() != "False"))
                        {
                            surveyResponseItemId = Convert.ToInt32(chkNoResponses.FirstOrDefault(x => x.Key.Contains("qestionchkNoResponse_" + QuestionID.ToString())).Value);
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).SingleOrDefault();

                        }
                        else if (res)
                        {
                            //if (QuestionID == 28)
                            //{
                            //    surveyResponseItemId = Convert.ToInt32(qesans.Key.Split('_')[2]);
                            //}
                            //else
                            //{
                               surveyResponseItemId = Convert.ToInt32(qesans.Value); 
                            //}
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).SingleOrDefault();
                        }
                        else
                        {
                            DateTime date;
                            if (DateTime.TryParse(qesans.Value, out date))
                            {

                                clientResponse.OtherResponse = date.ToShortDateString();
                            }
                            else
                            {
                                clientResponse.OtherResponse = qesans.Value;
                            }
                            //clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.SurveyGroupQTNID == QuestionID).SingleOrDefault();
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.SurveyGroupQTNID == QuestionID).FirstOrDefault();

                        }

                        ////var qvalue = context.SurveyResponses.Where(x => x.SurveyGroupQTNID == QuestionID).FirstOrDefault().SurveyResponseItemID;
                        //var qvalue1 = context.SurveyResponses.Where(x => x.SurveyGroupQTNID == QuestionID).FirstOrDefault();
                        //int qvalue = 0;
                        //if (qvalue1 != null)
                        //{
                        //    qvalue = qvalue1.SurveyResponseItemID;
                        //}
                        //if (res == false)
                        //{
                        //    clientResponse.OtherResponse = qesans.Value.ToString();
                        //    clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == qvalue).SingleOrDefault();
                        //}
                        //else
                        //{
                        //    clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == qvalue).SingleOrDefault();
                        //}



                        if (otherResponses.Any(x => x.Key.Contains("qestion_txtresponse_" + QuestionID.ToString() + "_" + surveyResponseItemId.ToString())))
                        {
                            clientResponse.OtherResponse = dataResult.FirstOrDefault(x => x.Key.Contains("qestion_txtresponse_" + QuestionID.ToString() + "_" + surveyResponseItemId.ToString())).Value;
                        }
                        context.SurveyClientResponses.Add(clientResponse);
                        await context.SaveChangesAsync();

                    }

                    if (model.EventId != null)
                    {
                        long id = Convert.ToInt64(model.EventId);
                        int? cnt = 0;
                        var Entries = context.tbl2008_NOM_Entries.Where(f => f.ID == id).FirstOrDefault();
                        if (Entries.SurveyCount != null)
                            cnt = Entries.SurveyCount;
                        else
                            cnt = 0;

                        Entries.SurveyCount = cnt + 1; ;

                        Entries.SurveysEntered = true;

                        context.SaveChanges();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return this.Request.CreateResponse(HttpStatusCode.OK, 1);
        }
        public SurveyItem GetSurveyItem(SurveyClient surveyClient, SerializeJsonData model, datagadget_testEntities context)
        {
            int eventId = 0;
            SurveyItem surveyItem = new SurveyItem();
            Guid uid = new Guid(model.UserID);
            surveyItem.SurveyAdminID = uid;
            surveyItem.AdminDate = Convert.ToDateTime(model.SurveyEndDate);
            surveyItem.LocationID = Convert.ToInt32(context.TblUsers.Where(x => x.UserGUID == uid).SingleOrDefault().LocationID);
            surveyItem.SessionID = model.SessionID;
            surveyItem.SurveyID = Convert.ToInt32(model.Survey);  //survey category id
            surveyItem.SurveyClient = surveyClient;
            surveyItem.ServiceID = null;
            surveyItem.ProgramID = Convert.ToInt32(model.ProgramNameData);
            surveyItem.SurveyTypeID = Convert.ToInt32(model.SurveyID);
            surveyItem.CreateDate = DateTime.Now;
            surveyItem.GroupName = model.GroupNameData;

            // Convert.ToInt32(context.tbl2008_NOM_Entries.Take(1).Where(x => x.Curriculum == 1).OrderByDescending(x => x.ID).SingleOrDefault().ID);
            //int dbUsers1 = context.tbl2008_NOM_Entries.Where(x => x.Curriculum == 1).OrderByDescending(x => x.ID).Take(1).SingleOrDefault().ID;

            if (model.EventId != null)
            { surveyItem.EventID = Convert.ToInt32(model.EventId); }
            else
            {
                var dbUsers1 = context.tbl2008_NOM_Entries.Where(x => x.Curriculum == 1).OrderByDescending(x => x.ID).FirstOrDefault();
                if (dbUsers1 != null)
                    eventId = dbUsers1.ID;

                surveyItem.EventID = eventId;
            }

            return surveyItem;
        }
        protected SurveyClient GetSurveyClient(Client client, int programID, string dischargeDate, datagadget_testEntities context)
        {
            SurveyClient surveyClient = new SurveyClient();
            if (client.RecNo != 0)
            {
                {
                    surveyClient = context.SurveyClients.Where(s => s.ClientID == client.RecNo).SingleOrDefault();
                    if (surveyClient != null)
                        return surveyClient;
                }
            }
            surveyClient = new SurveyClient();
            surveyClient.ClientID = client.RecNo;
            surveyClient.ClientType = 1;//prevension survey
            return surveyClient;
        }
        protected Client GetClient(string pAT_ID, datagadget_testEntities context)
        {
            Client client = new Client();
            {
                client = context.Clients.Where(Client => Client.PAT_ID == pAT_ID).SingleOrDefault();

                if (client == null)
                {
                    client = new Client();
                    client.PAT_ID = pAT_ID;
                }
            }
            return client;
        }
        public SurveySession GetSurveySession(SerializeJsonData model, datagadget_testEntities context)
        {
            SurveySession surveySession = new SurveySession();
            {
                surveySession = context.SurveySessions.Where(s => s.SessionID == model.SessionID).SingleOrDefault();
                if (surveySession == null)
                {
                    surveySession = new SurveySession();
                    surveySession.UserID = new Guid(model.UserID);
                    surveySession.SessionStart = Convert.ToDateTime(model.ClientSessionStart);
                    surveySession.SessionEnd = DateTime.Now;
                    surveySession.SessionID = model.SessionID;
                }
            }
            return surveySession;
        }


        async private Task<HttpResponseMessage> GetSurveyCountByEventID(string[] p)
        {
            try
            {
                long type = Convert.ToInt64(p[0]);

                int? i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    i = context.tbl2008_NOM_Entries.Where(x => x.ID == type).Select(x => x.SurveyCount).SingleOrDefault();

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, i);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
    [KnownTypeAttribute(typeof(SurveyList))]
    public class SurveyList
    {
        public int RecNo { get; set; }
        public string SurveyTitle { get; set; }
    }
    [KnownTypeAttribute(typeof(NewSurveyType))]
    partial class NewSurveyType
    {
        public int SurveyTypeID { get; set; }
        public string DisplayName { get; set; }
    }
    [KnownTypeAttribute(typeof(ProgramName))]
    public class ProgramName
    {
        public int Pid { get; set; }
        public string Name { get; set; }
    }
    [KnownTypeAttribute(typeof(NewGroupName))]
    public class NewGroupName
    {
        public string ProgramName { get; set; }
        public string GroupName { get; set; }
    }
    public partial class SerializeJsonData
    {
        public string SurveyEndDate { get; set; }

        public string SurveyID { get; set; }//serveyTypeID
        public string ProgramID { get; set; }
        public string ProgramNameData { get; set; }
        public string GroupNameData { get; set; }
        public string ParticipantID { get; set; }

        public string EventId { get; set; }
    }
    public class QuestionResponse
    {
        public int QuestionID { get; set; }
        public int ResponseID { get; set; }
        public string OtherResponse { get; set; }
    }
}
