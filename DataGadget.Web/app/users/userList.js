﻿(function () {
    'use strict';
    var controllerId = 'userList';
    angular
        .module('app')
        .controller(controllerId, userList);

    userList.$inject = ['$scope', '$http', '$modal', '$location', 'common'];

    function userList($scope, $http, $modal, $location, common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error')

        $scope.loginUserName = '';
        $scope.loginUserRole = '';

        $scope.title = 'userList';
        var vm = this;
        vm.users = [];
        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        function paging(pageId) {

            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
            loginUserInfo.pageId = pageId;
            $http({ method: 'POST', url: 'api/User', data: { method: "GetUsersByPage", parameters: [loginUserInfo.level, loginUserInfo.locationId, loginUserInfo.stateabbrev, loginUserInfo.pageId] } })
            .success(function (result) {
                $scope.ispaging = false;
                vm.pagerData = result.item1[0];
                if (parseInt(vm.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                vm.pageList = [];
                if (pageId > 3) {
                    vm.pageList.push(pageId - 2);
                    vm.pageList.push(pageId - 1);
                    vm.pageList.push(pageId);
                    if (pageId < vm.pagerData.numberOfPages) {
                        vm.pageList.push(pageId + 1);
                        if (pageId < (vm.pagerData.numberOfPages - 1)) {
                            vm.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);

                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        vm.pageList.push(pageId - 2);
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                vm.users = userData;
            })
            .error(function (error) {
                console.log(error);
            })
        }
        vm.pagination = function (pageId) {
            paging(pageId);
        }
        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
                loginUserInfo.searchUser = search;
                $http({ method: 'POST', url: 'api/User', data: { method: "SearchUser", parameters: [loginUserInfo.level, loginUserInfo.locationId, loginUserInfo.stateabbrev, loginUserInfo.searchUser] } })
                .success(function (result) {
                    vm.users = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    $scope.ispaging = false;
                    paging(1);
                }                
            }
        }
        function activate() {
            vm.searchCriteria = "";
            paging(1);
        }

        vm.addUser = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/users/newUser.html',
                controller: 'newUser',
                backdrop: 'static',
                resolve: {
                    userInfo: function () {
                        return undefined;
                    }
                }

            });
            addUserModal.result.then(function (user) {
                //vm.users.push(user);
                activate();

            }, function () {
            });
        }

        vm.editUser = function (user) {

            var addUserModal = $modal.open({
                templateUrl: 'app/users/newUser.html',
                controller: 'newUser',
                backdrop: 'static',
                resolve: {
                    userInfo: function () {
                        return user;
                    }
                }
            });

            addUserModal.result.then(function (user) {
                //var usr = _.findWhere(vm.users, { userId: user.userId });
                //var i = vm.users.indexOf(usr);
                //if (i != -1) {
                //    vm.users.splice(i, 1,user);                  
                //}
                activate();

            }, function () {

            });
        }

        vm.deleteUser = function (user) {


            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',

                backdrop: 'static',

            });


            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/User', data: { method: "DeleteUser", parameters: [user.userId] } })

            .success(function (result) {

                var i = vm.users.indexOf(user);
                if (i != -1) {
                    vm.users.splice(i, 1);
                }
                log("User deleted successfully");

            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {


            });

            //        $mdDialog.show(
            //  $mdDialog.alert()
            //    .title('This is an alert title')
            //    .content('You can specify some description text in here.')
            //    .ariaLabel('Password notification')
            //    .ok('Got it!')
            //    .targetEvent(ev)
            //);

            //var confirm = $mdDialog.confirm()
            //  .title('Would you like to delete this user?')
            //  .content('Delete User.')
            //  .ariaLabel('Lucky day')
            //  .ok('Yes')
            //  .cancel('No')
            //  .targetEvent(ev);

            //$mdDialog.show(confirm).then(function () {
            //    console.log( 'You decided to get rid of your debt.');
            //}, function () {
            //    console.log('You decided to keep your debt.');
            //});
        };

    }


})();
