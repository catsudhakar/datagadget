﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class TreatmentProg
    {
        public int RecNo { get; set; }
        public int LocationID { get; set; }
        public string ProgramName { get; set; }
        public Nullable<int> BedCount { get; set; }

        public bool IsSelected { get; set; }

        public Guid UserId { get; set; }
        public int ProgramId { get; set; }
    }

    public class UserIopModel
    {
        public int Id { get; set; }
        public System.Guid UserId { get; set; }
        public int IopId { get; set; }
        public bool IsSelected { get; set; }
    
    }
}