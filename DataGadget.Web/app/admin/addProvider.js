﻿

(function () {
    'use strict';
    var controllerId = 'addProvider';
    angular
        .module('app')
        .controller(controllerId, addProvider);

    addProvider.$inject = ['$scope', '$http', '$modalInstance', '$location', 'resourcesInfo', '$window', '$route', 'common'];

    function addProvider($scope, $http, $modalInstance, $location, resourcesInfo, $window, $route, common) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        $scope.title = 'Add Provider';

        if (resourcesInfo) {
            $scope.title = "Edit Provider";
        }
        else {
            $scope.title = "Add Provider";
        }
        $scope.statewide = {};
        $scope.isSubmit = false;

       
        activate();

        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (userInfo.level != "SIT3U" && userInfo.level != "SIT2A") {
                if (resourcesInfo) {
                    $scope.statewide.providerID = resourcesInfo.id;
                    $scope.statewide.providerName = resourcesInfo.name;
                    $scope.statewide.isActive = resourcesInfo.isActive;
                }
            }
            else {
                $location.path('/dashboard');
            }


        }


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function (frmProvider) {
            $scope.isSubmit = true;

            if (frmProvider.$valid) {
                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateNewProvider",
                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {
                        $modalInstance.close(result);
                        $location.path('/ProviderList');
                        $route.reload();
                        log("Provider saved successfully");

                    })
                    .error(function (error) {
                        console.log(error);
                    })

                $location.path('/ProviderList');
                $route.reload();
            }
        };
    }
})();

