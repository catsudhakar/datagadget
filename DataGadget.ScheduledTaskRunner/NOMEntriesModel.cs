﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataGadget.Web.Models
{
    
    public class NOMEntriesModel
    {
        public decimal SumOfPrepHours { get; set; }
        public decimal SumOfTravelHours { get; set; }
        public decimal SumOfServiceHours { get; set; }
        public decimal EvidenceBasedHours { get; set; }
        public decimal NonEvidenceBasedHours { get; set; }
        public decimal TargetHours { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string TyoeOfProgram_EA { get; set; }
    }
    
    public class BedCountModel
    {
        public int TotalAssignedBeds { get; set; }
        public int? BedCount { get; set; }

        public int TotalAdultMaleAssignedBeds { get; set; }
        public int? AdultMale { get; set; }

        public int TotalAdultFemaleAssignedBeds { get; set; }
        public int? AdultFemale { get; set; }

        public int TotalAdolescentMaleAssignedBeds { get; set; }
        public int? AdolescentMale { get; set; }

        public int TotalAdolescentFemaleAssignedBeds { get; set; }
        public int? AdolescentFemale { get; set; }

        public int? LocationId { get; set; }
        public string FriendlyName_loc { get; set; }
        public string LocName { get; set; }
        public DateTime? EntryDate { get; set; }

        public string PersentagesBeds { get; set; }

      


    }

    
    public class PatientContacts
    {
        public string  PatientId { get; set; }
        public int SurveyItemId{ get; set; }
        public Guid  SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }
        public string contact3 { get; set; }
        public string contact1 { get; set; }
        public string contact2 { get; set; }
        public DateTime? EntryDate { get; set; }

        public int ContactCount { get; set; }
        public string  imagename { get; set; }

        public DateTime SurveyDate { get; set; }

    }

    public class MailPatientContacts
    {
        public string PatientId { get; set; }
        public int SurveyItemId { get; set; }
        public Guid SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }
        
        public DateTime? SurveyDate { get; set; }

        public string  FollowupComment { get; set; }
       

        public DateTime? FollowupDate { get; set; }

        public int? FollowupId { get; set; }

        public string Description { get; set; }
       

    }

    public partial class FollowUpContactModel
    {
        public int Id { get; set; }
        public string PatientId { get; set; }
        public System.DateTime FollowupDate { get; set; }
        public string FollowupComment { get; set; }
        public int FollowupId { get; set; }
    }
}