﻿(function () {
    'use strict';
    var controllerId = 'admin';
    angular.module('app').controller(controllerId, admin);

    //admin.$inject = ['$scope', '$http', '$location', 'common', '$modal','$sce'];

    function admin($scope, $http, $location, common, $modal, $sce) {
        //var getLogFn = common.logger.getLogFn;
        //var log = getLogFn(controllerId);

        var vm = this;
        vm.onetime = {};
        vm.title = 'Admin';
        $scope.source = ''

        //vm.url = 'app/dashboard/dashboard.html';

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        $scope.isDisabled = false;

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }



        activate();

        function activate() {
            vm.onetime.locationList = "";
            //vm.onetime.beginDate = formattedDate();//new Date();
            //vm.onetime.endDate = formattedDate();//new Date();
            //vm.onetime.organization = "New Roads";
            vm.onetime.reportType = "12A";
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            vm.onetime.organization = userInfo.regionCode + "_" + userInfo.siteCode;
            if (userInfo.level != "") {
                if (userInfo.level == 'STA1A') {
                    //state level
                    $scope.isDisabled = false;

                } else if (userInfo.level == 'SIT2A') {
                    //admin level
                    $scope.isDisabled = true;

                } else if (userInfo.level == 'SIT3U') {
                    //user level
                    $scope.isDisabled = true;
                } else if (userInfo.level == 'Admin') {
                    //administrator level
                    $scope.isDisabled = false;
                }
                else
                    $scope.isDisabled = true;

            }
            else {
                $scope.isDisabled = true;
            }


            $http({ method: 'POST', url: 'api/Reports', data: { method: "InitializeUserForm", parameters: [] } })
           .success(function (result) {
               vm.onetime.locationList = result.locationList;
           });
        }


        vm.save1 = function () {
            var e = document.getElementById("selOrganization");
            var orgName = e.options[e.selectedIndex].text;

            var sdt = vm.onetime.beginDate;
            var edt = vm.onetime.endDate;
            $scope.ismessage = "";

            if (sdt === undefined && edt === undefined) {
                $scope.ismessage = 'Please enter start date and end date';
                return false;
            }
            if (sdt !== undefined && edt !== undefined) {
                var sday = sdt.getDate();
                var smonth = sdt.getMonth();
                var syear = sdt.getFullYear();

                var eday = edt.getDate();
                var emonth = edt.getMonth();
                var eyear = edt.getFullYear();

                var sdate = new Date();
                sdate.setFullYear(syear, smonth, sday);
                var edate = new Date();
                edate.setFullYear(eyear, emonth, eday);
                if (edate < sdate) {
                    $scope.ismessage = 'End date cannot be greater than begin date';
                    return false;
                }
            }

          

            vm.onetime.startDate = GetFormattedDate(vm.onetime.beginDate);
            vm.onetime.endDate2 = GetFormattedDate(vm.onetime.endDate);

            var res = vm.onetime.organization.split("_");
            vm.onetime.organizationName = orgName;
            if (res.length == 2) {
                vm.onetime.regionCode = res[0];
                vm.onetime.siteCode = res[1];

                $http({ method: 'POST', url: 'api/Reports', data: { method: "GetBaseStringDataForReport12_A", parameters: [JSON.stringify(vm.onetime)] } })
               .success(function (result) {
                   var reportData = "data:application/pdf;base64," + result;
                   $scope.source = $sce.trustAsResourceUrl(reportData);
               })
                .error(function (error) {
                    console.log(error);
                });
            }


        }

        vm.nextClick = function () {

            vm.url = "app/admin/admin.html";
        }

    }
})();