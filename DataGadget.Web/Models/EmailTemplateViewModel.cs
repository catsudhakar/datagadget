﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class EmailTemplateViewModel
    {
        public long ID { get; set; }
        public string EmailTemplateName { get; set; }
        public string TemplateDesc { get; set; }
    }

    public class MailTemplateViewModel
    {
        public long ID { get; set; }
        public string Template { get; set; }
       
    }
}