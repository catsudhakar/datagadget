﻿using DataGadget.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.Models;

namespace DataGadget.Web.Controllers
{
    public class SiteUsageController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                    case "AllSiteUsage":
                        return GetSiteUsage(value.Parameters);
                    case "AllSiteUsage1":
                        return GetSiteUsage1();
                    case "AllSiteUsage2":
                        return GetSiteUsage2();

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
               // WebLogger.Error("Exception In SiteUsageController : " + ex.Message); 
               // return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }


        private HttpResponseMessage GetSiteUsage(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var siteUS = new List<SiteUsage>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbUsers = context.suretool_SiteUsage();  //geting data from view
                    foreach (var statewide in dbUsers)
                    {
                        SiteUsage addSiteUsage = new SiteUsage();
                        addSiteUsage.FriendlyName = statewide;
                        siteUS.Add(addSiteUsage);
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, siteUS);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetSiteUsage1()
        {
            try
            {
                var siteUS = new List<SiteUsage>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbUsers = context.suretool_SiteUsage();  //geting data from view
                    if (dbUsers != null)
                    {
                        foreach (var statewide in dbUsers)
                        {
                            SiteUsage addSiteUsage = new SiteUsage();
                            addSiteUsage.FriendlyName = statewide;
                            siteUS.Add(addSiteUsage);
                        }
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, siteUS);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetSiteUsage2()
        {
            try
            {
                var siteUS = new List<SiteUsage>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbUsers = context.suretool_SiteUsage1();  //geting data from view
                    if (dbUsers != null)
                    {
                        foreach (var statewide in dbUsers)
                        {
                            SiteUsage addSiteUsage = new SiteUsage();
                            addSiteUsage.FriendlyName = statewide;
                            siteUS.Add(addSiteUsage);
                        }
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, siteUS);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //private HttpResponseMessage GetSiteUsage1()
        //{
        //    try
        //    {
        //        var siteUS = new List<SiteUsage>(); ;
        //        using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
        //        {
        //            var dbUsers = context.suretool_SiteUsage1();  //geting data from view
        //            foreach (var statewide in dbUsers)
        //            {
        //                SiteUsage addSiteUsage = new SiteUsage();
        //                addSiteUsage.FriendlyName = statewide;
        //                siteUS.Add(addSiteUsage);
        //            }
        //        }

        //        return this.Request.CreateResponse(HttpStatusCode.OK, siteUS);

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //private HttpResponseMessage GetSiteUsage2()
        //{
        //    try
        //    {
        //        var siteUS = new List<SiteUsage>(); ;
        //        using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
        //        {
        //            var dbUsers = context.suretool_SiteUsage2();  //geting data from view
        //            foreach (var statewide in dbUsers)
        //            {
        //                SiteUsage addSiteUsage = new SiteUsage();
        //                addSiteUsage.FriendlyName = statewide;
        //                siteUS.Add(addSiteUsage);
        //            }
        //        }

        //        return this.Request.CreateResponse(HttpStatusCode.OK, siteUS);

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}





    }
}
