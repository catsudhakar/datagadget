﻿(function () {
    'use strict';
    var controllerId = 'newUser';
    angular
        .module('app')
        .controller(controllerId, newUser);

    //newUser.$inject = ['$scope', '$http', 'common', '$routeParams','userInfo'];



    function newUser($scope, $http, $modalInstance, userInfo, $location, $route, common) {


        if (userInfo) {

            $scope.title = "Edit User";
            $scope.isExistingUser = true;
        }
        else {
            $scope.title = "Add User";
            $scope.isExistingUser = false;
        }
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var getLogFn1 = common.logger.getLogFn;
        var log1 = getLogFn1(controllerId, 'warn');
        $scope.user = {};
        $scope.sites = [];

        $scope.serSites = [];

        $scope.LocationCategory = [];
        $scope.Location = [];
        $scope.Program = [];
        $scope.treamProgramList = [];

        $scope.isShowTreatments = false;

        var locId = 31;
        $scope.LoginID = '';
        $scope.Password = '';
        $scope.prvEmailId = '';

        $scope.isSubmit = false;
        $scope.showtreat = false;

        //$scope.isStateLevel = false;
        //$scope.isAdminLevel = false;
        //$scope.isUserLevel = false;
        //$scope.isadministratorLevel = false;

        $scope.isStateLevel = true;
        $scope.isAdminLevel = true;
        $scope.isUserLevel = true;
        $scope.isadministratorLevel = true;

        $scope.isAgencyName = true;

        $scope.isvaliduser = false;  //for chcking alreday existing user
        var loginUserLocationId = ''

        activate();

        function activate() {

            var lid = '';
            var loginUserLocId = '';
            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
            $scope.isAdmin = loginUserInfo.level == "Admin";
            $scope.isUserName = false;


            loginUserLocationId = localStorage.getItem('locationId');

            if (loginUserInfo.level == 'STA1A') {
                //state level
                //$scope.isStateLevel = true;
                //$scope.isAdminLevel = false;
                //$scope.isUserLevel = false;
                //$scope.isAgencyName = false;
                //$scope.isadministratorLevel = true;

                $scope.isStateLevel = false;
                $scope.isAdminLevel = true;
                $scope.isUserLevel = true;
                $scope.isAgencyName = false;
                $scope.isadministratorLevel = false;
            } else if (loginUserInfo.level == 'SIT2A') {
                //admin level
                //$scope.isStateLevel = true;
                //$scope.isAdminLevel = true;
                //$scope.isUserLevel = false;
                //$scope.isAgencyName = true;
                //$scope.isadministratorLevel = true;

                $scope.isStateLevel = false;
                $scope.isAdminLevel = false;
                $scope.isUserLevel = true;
                $scope.isAgencyName = true;
                $scope.isadministratorLevel = false;
            } else if (loginUserInfo.level == 'SIT3U') {
                //user level
                $scope.isStateLevel = true;
                $scope.isAdminLevel = true;
                $scope.isUserLevel = true;
                $scope.isAgencyName = true;
                $scope.isadministratorLevel = true;
            } else if (loginUserInfo.level == 'Admin') {
                //administrator level
                //$scope.isStateLevel = false;
                //$scope.isAdminLevel = false;
                //$scope.isUserLevel = false;
                //$scope.isAgencyName = false;
                //$scope.isadministratorLevel = false;

                $scope.isStateLevel = true;
                $scope.isAdminLevel = true;
                $scope.isUserLevel = true;
                $scope.isAgencyName = false;
                $scope.isadministratorLevel = true;
            }
            else {
                //$scope.isStateLevel = true;
                //$scope.isAdminLevel = true;
                //$scope.isUserLevel = true;
                //$scope.isAgencyName = true;
                //$scope.isadministratorLevel = true;

                $scope.isStateLevel = false;
                $scope.isAdminLevel = false;
                $scope.isUserLevel = false;
                $scope.isAgencyName = true;
                $scope.isadministratorLevel = false;
            }

            if (userInfo) {
                loginUserLocId = parseInt(userInfo.agencyName.trim());
            }
            else {
                loginUserLocId = loginUserInfo.locationId;
                $scope.user.locationId = loginUserLocId;
            }

            $http({ method: 'POST', url: 'api/user', data: { method: 'InitializeUserForm', parameters: [loginUserLocId, loginUserLocationId] } })
            .success(function (pass) {
                $scope.treamProgramList = pass.treamProgramList;
                // $scope.user = pass;

                $scope.user.treamProgramList = pass.treamProgramList;
                $scope.user.locationList = pass.locationList;
                $scope.user.locationCategoryList = pass.locationCategoryList;
                $scope.user.surveySerivesList = pass.surveySerivesList;

                if (userInfo) {

                    $scope.isUserName = true; //for disable login id
                    $scope.isvaliduser = false;
                    $scope.user.userId = userInfo.userId;
                    $scope.user.userName = userInfo.userName.trim();

                    $scope.user.isActive = userInfo.isActive;

                    //$scope.user.password = 'password';
                    //$scope.user.confirmedPassword = 'password';

                    $scope.user.password = 'pas$w0rd';
                    $scope.user.confirmedPassword = 'pas$w0rd';

                    $scope.user.firstName = userInfo.firstName;
                    $scope.user.lastName = userInfo.lastName;
                    $scope.user.emailId = userInfo.emailId;
                    $scope.prvEmailId = userInfo.emailId;
                    $scope.user.officePhoneNum = userInfo.officePhoneNum;

                    $scope.user.hours = userInfo.hours;

                    $scope.user.officeFaxNum = userInfo.officeFaxNum;
                    $scope.user.experience = userInfo.experience;
                    $scope.user.degree = userInfo.degree;

                    $scope.user.locationId = parseInt(userInfo.agencyName.trim());
                    //lid = userInfo.agencyName;

                    $scope.user.level = userInfo.level;

                    $scope.user.selectedTreamProgramList = userInfo.selectedTreamProgramList

                    //angular.forEach($scope.user.treamProgramList, function (todo) {
                    //    var recNo = todo.recNo;
                    //    angular.forEach($scope.user.selectedTreamProgramList, function (todo1) {
                    //        var recNo1 = todo1.programId;
                    //        if (recNo == recNo1)
                    //            todo.isSelected = true;

                    //    });
                    //});

                    for (var i = 0; i < $scope.user.treamProgramList.length; i++) {
                        var recNo = $scope.user.treamProgramList[i].recNo;
                        for (var j = 0; j < $scope.user.selectedTreamProgramList.length; j++) {
                            var recNo1 = $scope.user.selectedTreamProgramList[j].programId;
                            if (recNo == recNo1) {
                                $scope.user.treamProgramList[i].isSelected = true;
                                break;
                            }

                        }

                    }

                    $scope.user.selectedLocationCategoryList = userInfo.selectedLocationCategoryList

                    //for (var j = 0; j < $scope.user.selectedLocationCategoryList.length; j++) {
                    //    $scope.sites.push($scope.user.selectedTreamProgramList[j].categoryID);
                    //}

                    angular.forEach($scope.user.locationCategoryList, function (todo) {
                        var recNo = todo.categoryID;

                        angular.forEach($scope.user.selectedLocationCategoryList, function (todo1) {
                            var recNo1 = todo1.categoryID;
                            if (recNo == recNo1) {
                                todo.isSelected = true;
                                $scope.sites.push(todo.categoryID);
                                if (todo1.categoryID == 2) {
                                    $scope.isShowTreatments = true;
                                }
                            }

                        });
                    });

                    // $scope.user.selectedTreamProgramList = userInfo.selectedTreamProgramList
                    $scope.user.selectedSurveySerivesList = userInfo.selectedSurveySerivesList

                    for (var i = 0; i < $scope.user.surveySerivesList.length; i++) {
                        var recNo = $scope.user.surveySerivesList[i].recNo;
                        for (var j = 0; j < $scope.user.selectedSurveySerivesList.length; j++) {
                            var recNo1 = $scope.user.selectedSurveySerivesList[j].iopId;
                            if (recNo == recNo1) {
                                $scope.user.surveySerivesList[i].isSerivesSelected = true;
                                $scope.serSites.push(recNo);
                                break;
                            }

                        }

                    }

                    //
                    //$scope.user.locationList = pass.locationList;
                    //if (lid !='')
                    //  $scope.user.locationId = parseInt(lid.trim());

                }
                else {
                    //$scope.user.userName = '';
                    //$scope.user.password = '';
                    //$scope.user.confirmedPassword = '';
                }
            })
            .error(function (fail) {

            });


        }

        $scope.ok = function (userForm) {
            $scope.isSubmit = true;
            $scope.isvalidemail = false;
            //var userName = document.getElementById("spnUserName").innerText;//document.getElementById("loginId").value;
            var userName = $scope.isvaliduser;
            if (userForm.$valid && userName == false) {

                var pwd = $scope.user.password;
                var cpwd = $scope.user.confirmedPassword;


                if (pwd == cpwd) {

                    var prespec = 0;
                    var state = "MS";
                    var stateCode = "000000";
                    var siteCode = "000000";
                    var fulltime = 0;
                    var locId = 15;

                    $http({
                        method: 'POST', url: 'api/User',
                        data: {
                            method: "checkuseremail", parameters: [$scope.user.emailId, $scope.prvEmailId]
                        }
                    })
                 .success(function (result) {

                     $scope.user.isActive = userInfo != undefined ? $scope.user.isActive : true;
                     if (result == false) {
                         $scope.isvalidemail = false;
                         $http({
                             method: 'POST', url: 'api/User',
                             data: {
                                 method: "CreateUser",
                                 parameters: [JSON.stringify($scope.user)]
                             }
                         })
                    .success(function (result) {

                        $modalInstance.close(result);
                        log("User saved successfully");
                        // $location.path('/userList');
                        //vm.users = result;

                    })
                    .error(function (error) {
                        if (error.message == "User Already Exists") {
                            document.getElementById("spnUserName").innerText = "User Already Exists";
                        }
                        //console.log(error);
                    })
                     }
                     else {
                         $scope.isvalidemail = true;
                         log("Email already exist")
                     }

                 })
                 .error(function (error) {
                     log("Please check the values");
                 })
                }
            }
            else {
                log1("Please enter the values");
            }



        };


        $scope.checkUser = function () {

            $scope.isvaliduser = false;
            // document.getElementById("spnUserName").innerText = "";
            var userName = document.getElementById("loginId").value;
            $http({
                method: 'POST', url: 'api/User',
                data: {
                    method: "CheckUser",
                    parameters: [userName]
                }
            })
            .success(function (result) {
                if (result.message == "User Already Exists") {
                    $scope.isvaliduser = true;
                    //document.getElementById("spnUserName").innerText = "User Already Exists";
                }

            })
            .error(function (error) {
                if (error.message == "User Already Exists") {
                    $scope.isvaliduser = true;
                    /// document.getElementById("spnUserName").innerText = "User Already Exists";
                }

                console.log(error);
            })

        };


        $scope.updateSite = function (site) {
            $scope.sites = $scope.sites || [];
            if (site.isSelected) {
                $scope.sites.push(site.categoryID);
                $scope.sites = _.uniq($scope.sites);
            }
            else {
                $scope.sites = _.without($scope.sites, site.categoryID);
            }

            if ($scope.sites.length == 0) {
                $scope.isShowTreatments = false;
                for (var i = 0; i < $scope.user.surveySerivesList.length; i++) {

                    $scope.user.surveySerivesList[i].isSerivesSelected = false;
                }

                for (var i = 0; i < $scope.user.treamProgramList.length; i++) {

                    $scope.user.treamProgramList[i].isSelected = false;
                }
            }
            else if ($scope.sites.length >= 2) {
                $scope.isShowTreatments = true;
            }
            else {
                if ($scope.sites[0] == 1) {
                    $scope.isShowTreatments = false;
                    for (var i = 0; i < $scope.user.surveySerivesList.length; i++) {

                        $scope.user.surveySerivesList[i].isSerivesSelected = false;
                    }

                    for (var i = 0; i < $scope.user.treamProgramList.length; i++) {

                        $scope.user.treamProgramList[i].isSelected = false;
                    }
                }
                else {
                    $scope.isShowTreatments = true;

                }
            }
        }



        $scope.updateServiceSite = function (site) {
            $scope.serSites = $scope.serSites || [];
            if (site.isSelected) {
                $scope.serSites.push(site.recNo);
                $scope.serSites = _.uniq($scope.serSites);
            }
            else {
                $scope.serSites = _.without($scope.serSites, site.recNo);
            }
        }


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.getTrtPrgByLocId = function (selLocid) {

            //alert(selLocid);

            loginUserLocationId = localStorage.getItem('locationId');

            $scope.user.treamProgramList = [];
            $scope.showtreat = false;
            $http({
                method: 'POST', url: 'api/user',
                // data: { method: 'GetTreatmentProgramListByLocId', parameters: [selLocid] }
                data: { method: 'InitializeUserForm', parameters: [selLocid, loginUserLocationId] }
            })
           .success(function (result) {
               //$scope.user.treamProgramList = result;
               $scope.user.treamProgramList = result.treamProgramList;
               //$scope.user.locationList = pass.locationList;
               // $scope.user.locationCategoryList = result.locationCategoryList;
               $scope.user.surveySerivesList = result.surveySerivesList;

               if ($scope.user.treamProgramList.length == 0) {
                   $scope.showtreat = true;
               }
           })
           .error(function (error) {
               console.log(error);
           })
        };


        $scope.isdisplay = function () {
            $scope.isvalidemail = false;
        };
    }
})();
