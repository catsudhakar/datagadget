﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('addGrantYear', addGrantYear);

   
    addGrantYear.$inject = ['$scope', '$http', '$modalInstance', '$location', 'resourcesInfo', '$route'];

    function addGrantYear($scope, $http, $modalInstance, $location, resourcesInfo, $route) {
        var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
        if (loginUserInfo.level == "SIT3U" || loginUserInfo.level == "SIT2A") {
            $location.path('/dashboard');
        }

        $scope.title = 'addGrantYear';

        //$scope.openBeginDateopened = true;

        if (resourcesInfo) {
            $scope.title = "Edit GrantYear";
        }
        else {
            $scope.title = "Add GrantYear";
        }
       
        $scope.statewide = {};
        $scope.isSubmit = false;

        $scope.isdisplay = false;
        $scope.ismessage = '';

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.open1 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened1 = true;
        };
        $scope.openBeginDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }


        activate();

        function activate() {
            if (resourcesInfo) {
                $scope.statewide.grantYearId = resourcesInfo.grantYearId;
                $scope.statewide.grantYear1 = resourcesInfo.grantYear1;
                $scope.statewide.startDate = resourcesInfo.startDate;
                $scope.statewide.endDate = resourcesInfo.endDate;
               
            }
            else
            {
                $scope.statewide.grantYear1 = "2014";
            }

        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };


        $scope.ok = function (frmResourceLink) {
             
            $scope.isSubmit = true;
            
            if (frmResourceLink.$valid) {

                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateNewGrantYear",
                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {
                        $modalInstance.close(result);
                       
                    })
                    .error(function (error) {
                        console.log(error);
                    })

              
           }



        };
    }
})();
