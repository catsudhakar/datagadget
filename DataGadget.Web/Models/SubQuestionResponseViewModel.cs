﻿using DataGadget.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataGadget.Web.Models
{
    public class SubQuestionResponseViewModel
    {
        public int SurveyGroupQTNRecNo { get; set; }
        public string QuetionText { get; set; }
        public string ControlType { get; set; }
        public string ResonseType { get; set; }
        public string SequenceNumber { get; set; }

        public List<SurveyRespons> SurveyQTNResponseList { get; set; }

        //public Nullable<int> ResponseSeqNum { get; set; }
        //public string Response { get; set; }
        //public int SurveyGroupQTNID { get; set; }
        //public int SurveyResponseItemID { get; set; }
        //public Nullable<int> ASPControlTypeID { get; set; }

    }
}
