﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('onetimeReportViewer', onetimeReportViewer);

    //reportViewerModal.$inject = ['$scope']; 

    function onetimeReportViewer($scope, $http, eventInfo, $modalInstance, $sce) {
        $scope.title = 'onetimeReportViewer';
        $scope.source = ''

        var vm = this;
        vm.onetime = {};

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');

        }

        activate();

        function activate() {
           
            vm.oneTimeEvent = {};
            if (eventInfo) {
                // vm.onGoingEvent.id = eventInfo.id;
               

                $http({
                    method: 'POST', url: 'api/oneTimeEvent',
                    data: { method: "GetBaseStringDataForReport", parameters: [JSON.stringify(eventInfo)] }
                })

                .success(function (result) {
                    // $scope.source = result;
                    var reportData = "data:application/pdf;base64," + result;
                    $scope.source = $sce.trustAsResourceUrl(reportData);
                })
                .error(function (error) {
                    console.log(error);
                })
            }
        }
    }
})();
