﻿
using System;
using System.Collections.Generic;

using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class FollowupReport : Report_Base
    {

        public FollowupReport()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;

        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }

        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;
            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Treatment FollowUp Report"));

            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);



            this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataTable dtData = new DataTable();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_Goal2");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");
           

            

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dtData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(3);
            PdfPCell cell;


            cell = new PdfPCell(NewPhrase("PatientId", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Survey Admin Name", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Entry Date", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase("FollowUp", Font.HELVETICA, 8, Font.BOLD));
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            //cell.BackgroundColor = cellHeaderColor;
            //table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase("FollowUp Date", Font.HELVETICA, 8, Font.BOLD));
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            //cell.BackgroundColor = cellHeaderColor;
            //table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase("Comments", Font.HELVETICA, 8, Font.BOLD));
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            //cell.BackgroundColor = cellHeaderColor;
            //table.AddCell(cell);

            table.HeaderRows = 1;

            int cCnt = 0;

            //---------------------------------------------------------------------------------------------------

            foreach (DataRow drData in dtData.Rows)
            {
                if (drData["Section"].ToString() == "1")
                    AddCategoryRow(table, drData["strategy"].ToString(), drData["timesUtilized"].ToString(), drData["participantCount"].ToString());
                else
                    AddCategorySubRow(table, drData["strategy"].ToString(), drData["timesUtilized"].ToString(), drData["participantCount"].ToString());

            }


            //---------------------------------------------------------------------------------------------------

            return table;
        }

        private void AddCategoryRow(PdfPTable table, string title, string numUtilized, string numParticipants)
        {
            PdfPCell cell;
            
            cell = new PdfPCell(NewPhrase(title, Font.HELVETICA, 8, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(numUtilized));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(numParticipants));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
        }

        private void AddCategorySubRow(PdfPTable table, string title, string numUtilized, string numParticipants)
        {
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase(title));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(numUtilized));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(numParticipants));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
        }




    }
}
