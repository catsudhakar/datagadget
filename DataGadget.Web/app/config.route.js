﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());

    // Configure the routes and route resolvers
    app.config(['$routeProvider', 'routes', '$locationProvider', routeConfigurator]);
    function routeConfigurator($routeProvider, routes, $locationProvider) {
        //$locationProvider.html5Mode(true);
        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/' });
    }

    // Define the routes 
    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: '/app/users/login.html',
                    title: 'login',
                    settings: {
                        nav: 1,

                    }
                }
            }, {
                url: '/dashboard',
                config: {
                    title: 'dashboard',
                    templateUrl: '/app/dashboard/dashboard.html',
                    settings: {
                        nav: 2,

                    }
                }
            }, {
                url: '/reports',
                config: {
                    title: 'reports',
                    templateUrl: '/app/admin/admin.html',
                    settings: {
                        nav: 3,

                    }
                }
            }, {
                url: '/ongoingEvents',
                config: {
                    title: 'onegointevents',
                    templateUrl: '/app/prevention/ongoingEvents.html',
                    settings: {
                        nav: 4,

                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }
                }
            }, {
                url: '/oneTimeEvents',
                config: {
                    title: 'oneTimeEvents',
                    templateUrl: 'app/prevention/oneTimeEvents.html',
                    settings: {
                        nav: 5,

                    },

                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }
                }
            }, {
                url: '/users',
                config: {
                    title: 'users',
                    templateUrl: 'app/users/userList.html',
                    settings: {
                        nav: 6,

                    },

                    resolve: {
                        permission: function (userAuthorizeService) {
                            userAuthorizeService.getPermission();
                        }
                    }
                }
            }, {
                url: '/newOngoingEvent',
                config: {
                    title: 'newOngoingEvent',
                    templateUrl: 'app/prevention/newOngoingEvent.html',
                    settings: {
                        nav: 7,

                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }
                }
            },
            {
                url: '/newOngoingEvent/:goingEvent',
                config: {
                    title: 'newOngoingEvent',
                    templateUrl: 'app/prevention/newOngoingEvent.html',
                    settings: {
                        nav: 2,

                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }
                }
            }, {
                url: '/newOneTimeEvent',
                config: {
                    title: 'newOneTimeEvent',
                    templateUrl: 'app/prevention/newOneTimeEvent.html',
                    settings: {
                        nav: 2,

                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }
                }
            }, {
                url: '/newOneTimeEvent/:event',
                config: {
                    title: 'newOneTimeEvent',
                    templateUrl: 'app/prevention/newOneTimeEvent.html',
                    settings: {
                        nav: 2,

                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }
                }
            }, {
                url: '/surveyTools',
                config: {
                    title: 'surveyTools',
                    templateUrl: 'PreventionSurveyTool/Index',
                    settings: {
                        nav: 9,
                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }

                }

            }, {
                url: '/surveyTools/:goingEventSurey',
                config: {
                    title: 'surveyTools',
                    templateUrl: 'PreventionSurveyTool/Index',
                    settings: {
                        nav: 9,
                    },
                    resolve: {
                        permission: function (preventionAuthorizeService) {
                            preventionAuthorizeService.getPermission();
                        }
                    }

                }

            },
            {
                url: '/discharges',
                config: {
                    title: 'discharges',
                    templateUrl: 'app/treatment/discharges.html',
                    settings: {
                        nav: 8,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },

            {
                url: '/waitingList',
                config: {
                    title: 'waitingList',
                    templateUrl: 'app/treatment/waitingData.html',
                    settings: {
                        nav: 88,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },

            {
                url: '/treatments',
                config: {
                    title: 'treatments',
                    templateUrl: 'app/treatment/treatments.html',
                    settings: {
                        nav: 89,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },

            {
                url: '/editTreatment',
                config: {
                    title: 'editTreatment',
                    templateUrl: 'Survey/EditTreatmentSurvey',
                    settings: {
                        nav: 90,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },

            {
                url: '/dischargesReport',
                config: {
                    title: 'dischargesReport',
                    templateUrl: 'app/treatment/dischargeReport.html',
                    settings: {
                        nav: 8,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },

            {
                url: '/followupReport',
                config: {
                    title: 'followupReport',
                    templateUrl: 'app/admin/followupReport.html',
                    settings: {
                        nav: 8,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },
            {
                url: '/bedUtilizationReport',
                config: {
                    title: 'bedUtilizationReport',
                    templateUrl: 'app/admin/bedUtilizationReport.html',
                    settings: {
                        nav: 8,

                    },
                    resolve: {
                        permission: function (treatmentAuthorizeService) {
                            treatmentAuthorizeService.getPermission();
                        }
                    }
                }
            },

             {
                 url: '/treatmentSurveyTools',
                 config: {
                     title: 'surveyTools',
                     templateUrl: 'Survey/TreatmentSurvey',
                     settings: {
                         nav: 10,

                     },
                     resolve: {
                         permission: function (treatmentAuthorizeService) {
                             treatmentAuthorizeService.getPermission();
                         }
                     }
                 }
             },
              {
                  url: '/StatewideInitiativesList',
                  config: {
                      title: 'StatewideInitiatives',
                      templateUrl: 'app/admin/StatewideInitiativesList.html',
                      settings: {
                          nav: 11,

                      }
                  }
              },
               {
                   url: '/programsandStrategiesStateList',
                   config: {
                       title: 'Programs and Strategies Maintenance - State',
                       templateUrl: 'app/admin/programsandStrategiesStateList.html',
                       settings: {
                           nav: 12,

                       }
                   }
               },
                {
                    url: '/programsandStrategiesNREPPList',
                    config: {
                        title: 'Programs and Strategies Maintenance - NREPP',
                        templateUrl: 'app/admin/programsandStrategiesNREPPList.html',
                        settings: {
                            nav: 13,

                        }
                    }
                },
               {
                   url: '/siteUsage',
                   config: {
                       title: 'Site Usage',
                       templateUrl: 'app/admin/siteUsage.html',
                       settings: {
                           nav: 14,

                       }
                   }
               },
               {
                   url: '/editData',
                   config: {
                       title: 'Edit Data',
                       templateUrl: 'app/admin/editData.html',
                       settings: {
                           nav: 13,
                       }
                   }
               },

                {
                    url: '/exportData',
                    config: {
                        title: 'exportData',
                        templateUrl: 'app/admin/exportData.html',
                        settings: {
                            nav: 17,

                        }
                    }
                },

               {
                   url: '/preventionOutcomes',
                   config: {
                       title: 'preventionOutcomes',
                       templateUrl: 'app/admin/preventionOutcomes.html',
                       settings: {
                           nav: 17,

                       }
                   }
               },

               {
                   url: '/treatmentOutcomes',
                   config: {
                       title: 'treatmentOutcomes',
                       templateUrl: 'app/admin/treatmentOutcomes.html',
                       settings: {
                           nav: 18,

                       }
                   }
               },

               {
                   url: '/utilizationRate',
                   config: {
                       title: 'utilizationRate',
                       templateUrl: 'app/admin/utilizationRate.html',
                       settings: {
                           nav: 19,

                       }
                   }
               },

               {
                   url: '/ExportUserList',
                   config: {
                       title: 'ExportUserList',
                       templateUrl: 'app/admin/ExportUserList.html',
                       settings: {
                           nav: 20,

                       }
                   }
               },

               {
                   url: '/editEventData',
                   config: {
                       title: 'editEventData',
                       templateUrl: 'app/Admin/editEventData.html',
                       settings: {
                           nav: 21,

                       }
                   }
               },
             {
                 url: '/resourceLinksList',
                 config: {
                     title: 'resourceLinksList',
                     templateUrl: 'app/admin/resourceLinksList.html',
                     settings: {
                         nav: 22,

                     }
                 }
             },

             {
                 url: '/setGrantYear',
                 config: {
                     title: 'setGrantYear',
                     templateUrl: 'app/admin/setGrantYear.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },
             {
                 url: '/comments',
                 config: {
                     title: 'comments',
                     templateUrl: 'app/comments/comments.html',
                     settings: {
                         nav: 23,

                     },

                     resolve: {
                         permission: function (commentAuthorizeService) {
                             commentAuthorizeService.getPermission();
                         }
                     }
                 }
             },
             {
                 url: '/ManageComments',
                 config: {
                     title: 'Manage Comments',
                     templateUrl: 'app/admin/managecomments.html',
                     settings: {
                         nav: 23,

                     }
                 }
             }, {
                 url: '/WeeklyMailTemplate',
                 config: {
                     title: 'Weekly Mail Template',
                     templateUrl: 'app/admin/weeklyMailTemplate.html',
                     settings: {

                     }
                 }
             },
             {
                 url: '/StaffDevelopment',
                 config: {
                     title: 'Staff Development',
                     templateUrl: 'app/admin/staffDevelopment.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },

             {
                 url: '/ProviderList',
                 config: {
                     title: 'Providers',
                     templateUrl: 'app/admin/ProviderList.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },

             {
                 url: '/bedCountList',
                 config: {
                     title: 'Bed Count',
                     templateUrl: 'app/admin/bedCountList.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },

             {
                 url: '/serviceLocations',
                 config: {
                     title: 'Service Locations',
                     templateUrl: 'app/admin/serviceLocations.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },
             {
                 url: '/forgotpassword',
                 config: {
                     title: 'Forgot Password',
                     templateUrl: 'app/users/forgotpassword.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },
             {
                 url: '/resetpassword',
                 config: {
                     title: 'Reset Password',
                     templateUrl: 'app/users/resetpassword.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },
             {
                 url: '/emailTemplates',
                 config: {
                     title: 'Manage Email Templates',
                     templateUrl: 'app/admin/emailTemplate.html',
                     settings: {
                         nav: 23,

                     }
                 }
             },

              {
                  url: '/staffDevelopmentReport',
                  config: {
                      title: 'staffDevelopmentReport',
                      templateUrl: 'app/admin/staffDevelopmentReport.html',
                      settings: {
                          nav: 19,

                      }
                  }
              },

              {
                  url: '/treatmentPrograms',
                  config: {
                      title: 'treatmentPrograms',
                      templateUrl: 'app/admin/treatmentProgramList.html',
                      settings: {
                          nav: 20,

                      }
                  }
              },

               {
                   url: '/locations',
                   config: {
                       title: 'treatmentPrograms',
                       templateUrl: 'app/admin/agencyList.html',
                       settings: {
                           nav: 20,

                       }
                   }
               },

               {
                   url: '/treatmentConfig',
                   config: {
                       title: 'treatmentConfig',
                       templateUrl: 'app/admin/treatmentConfig.html',
                       settings: {
                           nav: 20,

                       }
                   }
               },
                {
                    url: '/indigentFundReport',
                    config: {
                        title: 'indigentFundReport',
                        templateUrl: 'app/admin/indigentFundReport.html',
                        settings: {
                            nav: 31,
                        }
                    }
                },
                 {
                     url: '/mailConfig',
                     config: {
                         title: 'mailConfig',
                         templateUrl: 'app/admin/mailConfig.html',
                         settings: {
                             nav: 32,

                         }
                     }
                 },

        ];
    }
})();