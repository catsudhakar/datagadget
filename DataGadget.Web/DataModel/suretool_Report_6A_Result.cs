//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.Web.DataModel
{
    using System;
    
    public partial class suretool_Report_6A_Result
    {
        public int sequence { get; set; }
        public int Section { get; set; }
        public string strategy { get; set; }
        public Nullable<int> timesUtilized { get; set; }
        public Nullable<int> participantCount { get; set; }
    }
}
