//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.Web.DataModel
{
    using System;
    
    public partial class spResolveCurriculumCode_Result
    {
        public int id { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public string Type_MPE { get; set; }
    }
}
