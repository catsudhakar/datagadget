﻿(function () {
    'use strict';
    var controllerId = 'newprogramsandStrategiesForState';
    angular
        .module('app')
        .controller(controllerId, newprogramsandStrategiesForState);
    

    newprogramsandStrategiesForState.$inject = ['$scope', '$http', '$modalInstance', 'programInfo', 'psService', 'common'];
    function newprogramsandStrategiesForState($scope, $http, $modalInstance, programInfo, pssharedService, common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        if (programInfo) {
            $scope.title = "Edit Programs and Strategies Maintenance - State";
        }
        else {
            $scope.title = "Add Programs and Strategies Maintenance - State";
        }
        $scope.programs = {};
        $scope.isSubmit = false;
        activate();
        $scope.id = 0;
        $scope.name = "";
        function activate() {
                if (programInfo) {
                    $scope.programs.id = programInfo.id;
                    $scope.programs.name = programInfo.name;
                    $scope.programs.isActive = programInfo.isActive;
                }
                else {
                }
        }
        $scope.ok = function (programForm) {
            $scope.isSubmit = true;
            $scope.programs.type = "State";
            if (programForm.$valid) {
                if ($scope.programs.id != 0) {
                    $scope.id = $scope.programs.id;
                    $scope.name = $scope.programs.name;
                    if ($scope.programs.isActive !== undefined) {
                        $scope.isActive = $scope.programs.isActive;
                    } else
                    {
                        $scope.isActive = false;
                        $scope.programs.isActive = false;
                    }
                }
                $http({
                    method: 'POST', url: 'api/ProgramStategies',
                        data: {
                            method: "CreateProgramStategies",
                            parameters: [JSON.stringify($scope.programs)]
                        }
                    })
                    .success(function (result) {
                        $modalInstance.close(result);

                        if ($scope.id != 0) {
                            for (var i = 0; i < pssharedService.programs.length; i++) {
                                if (pssharedService.programs[i].id == $scope.id) {
                                    pssharedService.programs[i].name = $scope.name;
                                    pssharedService.programs[i].isActive = $scope.isActive;
                                }
                            }
                        }
                        log("Program Strategies saved successfully");

                    })
                    .error(function (error) {
                        console.log(error);
                    }) 
            } 

        };        
        $scope.updateSite = function (site) {
            $scope.sites = $scope.sites || [];
            if (site.isSelected) {
                $scope.sites.push(site.categoryID);
                $scope.sites = _.uniq($scope.sites);
            }
            else {
                $scope.sites = _.without($scope.sites, site.categoryID);
            }
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    
})();
