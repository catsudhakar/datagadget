//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.Web.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class UsersInCategory
    {
        public System.Guid UserGUID { get; set; }
        public string LoginID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string ProgramName { get; set; }
        public Nullable<int> BedCount { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public string Password { get; set; }
        public string RoleName { get; set; }
        public string Email_addr { get; set; }
        public string OfficePhone { get; set; }
        public string OfficeFax { get; set; }
        public string StateRegionCode { get; set; }
        public string SiteCode { get; set; }
    }
}
