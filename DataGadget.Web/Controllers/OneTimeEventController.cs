﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.Reports;

namespace DataGadget.Web.Controllers
{
    public class OneTimeEventController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {

                    case "GetServiceLocations":
                        return await GetServiceLocations(param.Parameters);
                    case "GetServiceLocations1":
                        return await GetServiceLocations1(param.Parameters);
                    case "AllEvents":
                        return GetAllEvents(param.Parameters);
                    case "GetProgramsAndStrategies":
                        return await GetProgramsAndStrategies(param.Parameters);

                    case "GetProgramIdByProgramName":
                        return await GetProgramIdByProgramName(param.Parameters);

                    case "GetStatewideInitiatives":
                        return await GetStatewideInitiatives(param.Parameters);
                    case "SaveEventDetails":
                        return SaveEventDetails(param.Parameters);
                    case "DeleteEvent":
                        return DeleteEvent(param.Parameters);
                    case "GetBaseStringDataForReport":
                        return GetBaseStringDataForReport(param.Parameters);
                    case "Search":
                        return Search(param.Parameters);

                    case "UpdateUserDetails":
                        return UpdateUserDetails(param.Parameters);

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

               // WebLogger.Error("Exception In OneTimeEventController : " + ex.Message);  
               // return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        private HttpResponseMessage UpdateUserDetails(string[] p)
        {


            try
            {

                Models.OneTimeEvent oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OneTimeEvent>(p[0]);

                bool b = false;
                int cnt = 0;

                var Entries = new DataModel.tbl2008_NOM_Entries();


                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    Entries = context.tbl2008_NOM_Entries.Where(ff => ff.ID == oneTime.ID).FirstOrDefault();
                    var record = context.TblUsers.Where(f => f.UserGUID == oneTime.UserGUID).FirstOrDefault();
                    Entries.RegionCode = record.StateRegionCode;//"000008";
                    Entries.SiteCode = record.SiteCode;//"000001";
                    Entries.FiscalYear = DateTime.Now.Year.ToString();// "2014";
                    Entries.OrgName = record.tblLocation.FriendlyName_loc;
                    Entries.StaffFname = record.FirstName;// "Mat";
                    Entries.StaffLName = record.LastName;//"Douglas";
                    Entries.Name = record.FirstName + record.LastName;//"Mat Douglas";
                    Entries.Degree = record.Degree;//"MAST";
                    Entries.UserGUID = record.UserGUID;

                    //context.tbl2008_NOM_Entries
                    cnt = context.SaveChanges();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, cnt);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage Search(string[] p)
        {
            try
            {
                User userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                int eventType = Convert.ToInt32(p[1]);
                string searchKey = Convert.ToString(p[2]);

                if (!userInfo.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }

                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    events = context.suretool_GetCurriculums(eventType, userInfo.UserId).Where(x => x.ProgramName.Contains(searchKey) || x.GroupName.Contains(searchKey) || x.OrgName.Contains(searchKey)).Select(x => new OneTimeEvent
                    {
                        ID = x.ID,
                        Program = x.ProgramName,
                        Organization = x.OrgName,
                        BeginDate = x.StartDate,
                        EndDate = x.EndDate,
                        Group = x.GroupName,
                        lstEvents = GetChildEvents(x.ID),
                        Age0 = x.Age0,
                        Age12 = x.Age12,
                        Age15 = x.Age15,
                        Age18 = x.Age18,
                        Age21 = x.Age21,
                        Age25 = x.Age25,
                        Age45 = x.Age45,
                        Age5 = x.Age5,
                        Age65 = x.Age65,
                        AvgCostPerParticipant = x.AvgCostPerParticipant,
                        closed = x.closed,
                        CoalitionMeeting = x.CoalitionMeeting,
                        CoalitionType = x.CoalitionType,
                        Curriculum = x.Curriculum,
                        D_HiRi_AbuVict = x.D_HiRi_AbuVict,
                        D_HiRi_ChildOfSA = x.D_HiRi_ChildOfSA,
                        D_HiRi_EconDis = x.D_HiRi_EconDis,
                        D_HiRi_FrLun = x.D_HiRi_FrLun,
                        D_HiRi_Homel = x.D_HiRi_Homel,
                        D_HiRi_K12DO = x.D_HiRi_K12DO,
                        D_HiRi_MentH = x.D_HiRi_MentH,
                        D_HiRi_NotAp = x.D_HiRi_NotAp,
                        D_HiRi_Other = x.D_HiRi_Other,
                        D_HiRi_OtherText = x.D_HiRi_OtherText,
                        D_HiRi_PhysDis = x.D_HiRi_PhysDis,
                        D_HiRi_PregUse = x.D_HiRi_PregUse,
                        D_HiRi_Using = x.D_HiRi_Using,
                        D_HiRi_Violent = x.D_HiRi_Violent,
                        Degree = x.Degree,
                        DirectServiceHours = x.DirectServiceHours,
                        DocOfActivities_Agenda = x.DocOfActivities_Agenda,
                        DocOfActivities_Brochure = x.DocOfActivities_Brochure,
                        DocOfActivities_ContactForm = x.DocOfActivities_ContactForm,
                        DocOfActivities_EvalForm = x.DocOfActivities_EvalForm,
                        DocOfActivities_Other = x.DocOfActivities_Other,
                        DocOfActivities_OtherText = x.DocOfActivities_OtherText,
                        DocOfActivities_PrePostTest = x.DocOfActivities_PrePostTest,
                        DocOfActivities_SignIn = x.DocOfActivities_SignIn,
                        EntryDate = x.EntryDate,
                        Females = x.Females,
                        FiscalYear = x.FiscalYear,
                        GroupName = x.GroupName,
                        Hispanic = x.Hispanic,
                        ID02_DirectoriesDistributed = x.ID02_DirectoriesDistributed,
                        ID04_BrochuresDistributed = x.ID04_BrochuresDistributed,
                        InterventionType = x.InterventionType,
                        Males = x.Males,
                        Name = x.Name,
                        NotHispanic = x.NotHispanic,
                        NumberOfEmployees = x.NumberOfEmployees,
                        NumberParticipants = x.NumberParticipants,
                        NumberProgHrsRecd = x.NumberProgHrsRecd,
                        OrgName = x.OrgName,
                        OverallComments = x.OverallComments,
                        ParentEntry = x.ParentEntry,
                        PrepHours = x.PrepHours,
                        ProgramName = x.ProgramName,
                        ProgramNameSource = x.ProgramNameSource,
                        ProgramType_IP = x.ProgramType_IP,
                        RaceAmIndian = x.RaceAmIndian,
                        RaceAsian = x.RaceAsian,
                        RaceBlack = x.RaceBlack,
                        RaceHawaii = x.RaceHawaii,
                        RaceMoreThanOne = x.RaceMoreThanOne,
                        RaceUnknownOther = x.RaceUnknownOther,
                        RaceWhite = x.RaceWhite,
                        RegionCode = x.RegionCode,
                        SessionName = x.SessionName,
                        SiteCode = x.SiteCode,
                        StaffFname = x.StaffFname,
                        StaffLName = x.StaffLName,
                        StartDate = x.StartDate,
                        Strat_Alt_21 = x.Strat_Alt_21,
                        Strat_Alt_22 = x.Strat_Alt_22,
                        Strat_Alt_23 = x.Strat_Alt_23,
                        Strat_Alt_24 = x.Strat_Alt_24,
                        Strat_Alt_25 = x.Strat_Alt_25,
                        Strat_Alt_26 = x.Strat_Alt_26,
                        Strat_Alt_27 = x.Strat_Alt_27,
                        Strat_Alt_27Text = x.Strat_Alt_27Text,
                        Strat_CBP_41 = x.Strat_CBP_41,
                        Strat_CBP_42 = x.Strat_CBP_42,
                        Strat_CBP_43 = x.Strat_CBP_43,
                        Strat_CBP_44 = x.Strat_CBP_44,
                        Strat_CBP_45 = x.Strat_CBP_45,
                        Strat_CBP_46 = x.Strat_CBP_46,
                        Strat_CBP_46Text = x.Strat_CBP_46Text,

                        Strat_CBP_Faith=x.Strat_CBP_Faith,

                        Strat_Ed_11 = x.Strat_Ed_11,
                        Strat_Ed_12 = x.Strat_Ed_12,
                        Strat_Ed_13 = x.Strat_Ed_13,
                        Strat_Ed_14 = x.Strat_Ed_14,
                        Strat_Ed_15 = x.Strat_Ed_15,
                        Strat_Ed_16 = x.Strat_Ed_16,
                        Strat_Ed_17 = x.Strat_Ed_17,
                        Strat_Ed_17Text = x.Strat_Ed_17Text,
                        Strat_Env_51 = x.Strat_Env_51,
                        Strat_Env_52 = x.Strat_Env_52,
                        Strat_Env_53 = x.Strat_Env_53,
                        Strat_Env_54 = x.Strat_Env_54,
                        Strat_Env_55 = x.Strat_Env_55,
                        Strat_Env_56_MW = x.Strat_Env_56_MW,
                        Strat_Env_57 = x.Strat_Env_57,
                        Strat_Env_57Text = x.Strat_Env_57Text,
                        Strat_ID_01 = x.Strat_ID_01,
                        Strat_ID_02 = x.Strat_ID_02,
                        Strat_ID_03 = x.Strat_ID_03,
                        Strat_ID_04 = x.Strat_ID_04,
                        Strat_ID_05 = x.Strat_ID_05,
                        Strat_ID_06 = x.Strat_ID_06,
                        Strat_ID_07 = x.Strat_ID_07,
                        Strat_ID_08 = x.Strat_ID_08,
                        Strat_ID_09 = x.Strat_ID_09,
                        Strat_ID_09Text = x.Strat_ID_09Text,
                        Strat_IR_31 = x.Strat_IR_31,
                        Strat_IR_32 = x.Strat_IR_32,
                        Strat_IR_34 = x.Strat_IR_34,
                        Strat_IR_34Text = x.Strat_IR_34Text,
                        SurveysEntered = x.SurveysEntered,
                        TargetedStatewideInitiative = x.TargetedStatewideInitiative,
                        TotalCostofProgram = x.TotalCostofProgram,
                        TravelHours = x.TravelHours,
                        TyoeOfProgram_EA = x.TyoeOfProgram_EA,
                        UniversalType_DI = x.UniversalType_DI,
                        UserGUID = x.UserGUID,
                        WithinCostBand_YN = x.WithinCostBand_YN
                    }).ToList();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, events);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private List<OneTimeEvent> GetChildEvents(int ParentEventID)
        {
            try
            {



                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbEvents = await context.tbl2008_NOM_Entries.Where(x => x.UserGUID == newUserId && x.Curriculum=0).ToListAsync();
                    events = context.suretool_GetChildCurriculums(ParentEventID).
                        Select(x => new OneTimeEvent
                        {
                            Program = x.ProgramName,
                            Organization = x.OrgName,
                            BeginDate = x.StartDate,
                            EndDate = x.EndDate,
                            Group = x.GroupName
                        }).ToList();
                }

                return events;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private HttpResponseMessage GetAllEvents(string[] p)
        {
            try
            {

                User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                int eventType = Convert.ToInt32(p[1]);

                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[2]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 1))
                {
                    return null;
                }

                var events = new List<OneTimeEvent>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = context.suretool_GetCurriculums(eventType, newUserId.UserId).Count();
                    events = context.suretool_GetCurriculums(eventType, newUserId.UserId).
                        Select(x => new OneTimeEvent
                        {
                            ID = x.ID,
                            Program = x.ProgramName,
                            Organization = x.OrgName,
                            BeginDate = x.StartDate,
                            EndDate = x.EndDate,
                            Group = x.GroupName,

                            ServiceLocationName = context.tbl2008_NOM_Entries.Where(y => y.ID == x.ID).FirstOrDefault().ServiceLocationName,
                            ServiceSchoolName = context.tbl2008_NOM_Entries.Where(y => y.ID == x.ID).FirstOrDefault().ServiceSchoolName,

                            lstEvents = GetChildEvents(x.ID),
                            Age0 = x.Age0,
                            Age12 = x.Age12,
                            Age15 = x.Age15,
                            Age18 = x.Age18,
                            Age21 = x.Age21,
                            Age25 = x.Age25,
                            Age45 = x.Age45,
                            Age5 = x.Age5,
                            Age65 = x.Age65,
                            AvgCostPerParticipant = x.AvgCostPerParticipant,
                            closed = x.closed,
                            CoalitionMeeting = x.CoalitionMeeting,
                            CoalitionType = x.CoalitionType,
                            Curriculum = x.Curriculum,
                            D_HiRi_AbuVict = x.D_HiRi_AbuVict,
                            D_HiRi_ChildOfSA = x.D_HiRi_ChildOfSA,
                            D_HiRi_EconDis = x.D_HiRi_EconDis,
                            D_HiRi_FrLun = x.D_HiRi_FrLun,
                            D_HiRi_Homel = x.D_HiRi_Homel,
                            D_HiRi_K12DO = x.D_HiRi_K12DO,
                            D_HiRi_MentH = x.D_HiRi_MentH,
                            D_HiRi_NotAp = x.D_HiRi_NotAp,
                            D_HiRi_Other = x.D_HiRi_Other,
                            D_HiRi_OtherText = x.D_HiRi_OtherText,
                            D_HiRi_PhysDis = x.D_HiRi_PhysDis,
                            D_HiRi_PregUse = x.D_HiRi_PregUse,
                            D_HiRi_Using = x.D_HiRi_Using,
                            D_HiRi_Violent = x.D_HiRi_Violent,
                            Degree = x.Degree,
                            DirectServiceHours = x.DirectServiceHours,
                            DocOfActivities_Agenda = x.DocOfActivities_Agenda,
                            DocOfActivities_Brochure = x.DocOfActivities_Brochure,
                            DocOfActivities_ContactForm = x.DocOfActivities_ContactForm,
                            DocOfActivities_EvalForm = x.DocOfActivities_EvalForm,
                            DocOfActivities_Other = x.DocOfActivities_Other,
                            DocOfActivities_OtherText = x.DocOfActivities_OtherText,
                            DocOfActivities_PrePostTest = x.DocOfActivities_PrePostTest,
                            DocOfActivities_SignIn = x.DocOfActivities_SignIn,
                            EntryDate = x.EntryDate,
                            Females = x.Females,
                            FiscalYear = x.FiscalYear,
                            GroupName = x.GroupName,
                            Hispanic = x.Hispanic,
                            ID02_DirectoriesDistributed = x.ID02_DirectoriesDistributed,
                            ID04_BrochuresDistributed = x.ID04_BrochuresDistributed,
                            InterventionType = x.InterventionType,
                            Males = x.Males,
                            Name = x.Name,
                            NotHispanic = x.NotHispanic,
                            NumberOfEmployees = x.NumberOfEmployees,
                            NumberParticipants = x.NumberParticipants,
                            NumberProgHrsRecd = x.NumberProgHrsRecd,
                            OrgName = x.OrgName,
                            OverallComments = x.OverallComments,
                            ParentEntry = x.ParentEntry,
                            PrepHours = x.PrepHours,
                            ProgramName = x.ProgramName,
                            ProgramNameSource = x.ProgramNameSource,
                            ProgramType_IP = x.ProgramType_IP,
                            RaceAmIndian = x.RaceAmIndian,
                            RaceAsian = x.RaceAsian,
                            RaceBlack = x.RaceBlack,
                            RaceHawaii = x.RaceHawaii,
                            RaceMoreThanOne = x.RaceMoreThanOne,
                            RaceUnknownOther = x.RaceUnknownOther,
                            RaceWhite = x.RaceWhite,
                            RegionCode = x.RegionCode,
                            SessionName = x.SessionName,
                            SiteCode = x.SiteCode,
                            StaffFname = x.StaffFname,
                            StaffLName = x.StaffLName,
                            StartDate = x.StartDate,
                            Strat_Alt_21 = x.Strat_Alt_21,
                            Strat_Alt_22 = x.Strat_Alt_22,
                            Strat_Alt_23 = x.Strat_Alt_23,
                            Strat_Alt_24 = x.Strat_Alt_24,
                            Strat_Alt_25 = x.Strat_Alt_25,
                            Strat_Alt_26 = x.Strat_Alt_26,
                            Strat_Alt_27 = x.Strat_Alt_27,
                            Strat_Alt_27Text = x.Strat_Alt_27Text,
                            Strat_CBP_41 = x.Strat_CBP_41,
                            Strat_CBP_42 = x.Strat_CBP_42,
                            Strat_CBP_43 = x.Strat_CBP_43,
                            Strat_CBP_44 = x.Strat_CBP_44,
                            Strat_CBP_45 = x.Strat_CBP_45,
                            Strat_CBP_46 = x.Strat_CBP_46,
                            Strat_CBP_46Text = x.Strat_CBP_46Text,

                            Strat_CBP_Faith=x.Strat_CBP_Faith,

                            Strat_Ed_11 = x.Strat_Ed_11,
                            Strat_Ed_12 = x.Strat_Ed_12,
                            Strat_Ed_13 = x.Strat_Ed_13,
                            Strat_Ed_14 = x.Strat_Ed_14,
                            Strat_Ed_15 = x.Strat_Ed_15,
                            Strat_Ed_16 = x.Strat_Ed_16,
                            Strat_Ed_17 = x.Strat_Ed_17,
                            Strat_Ed_17Text = x.Strat_Ed_17Text,
                            Strat_Env_51 = x.Strat_Env_51,
                            Strat_Env_52 = x.Strat_Env_52,
                            Strat_Env_53 = x.Strat_Env_53,
                            Strat_Env_54 = x.Strat_Env_54,
                            Strat_Env_55 = x.Strat_Env_55,
                            Strat_Env_56_MW = x.Strat_Env_56_MW,
                            Strat_Env_57 = x.Strat_Env_57,
                            Strat_Env_57Text = x.Strat_Env_57Text,
                            Strat_ID_01 = x.Strat_ID_01,
                            Strat_ID_02 = x.Strat_ID_02,
                            Strat_ID_03 = x.Strat_ID_03,
                            Strat_ID_04 = x.Strat_ID_04,
                            Strat_ID_05 = x.Strat_ID_05,
                            Strat_ID_06 = x.Strat_ID_06,
                            Strat_ID_07 = x.Strat_ID_07,
                            Strat_ID_08 = x.Strat_ID_08,
                            Strat_ID_09 = x.Strat_ID_09,
                            Strat_ID_09Text = x.Strat_ID_09Text,
                            Strat_IR_31 = x.Strat_IR_31,
                            Strat_IR_32 = x.Strat_IR_32,
                            Strat_IR_34 = x.Strat_IR_34,
                            Strat_IR_34Text = x.Strat_IR_34Text,
                            SurveysEntered = x.SurveysEntered,
                            TargetedStatewideInitiative = x.TargetedStatewideInitiative,
                            TotalCostofProgram = x.TotalCostofProgram,
                            TravelHours = x.TravelHours,
                            TyoeOfProgram_EA = x.TyoeOfProgram_EA,
                            UniversalType_DI = x.UniversalType_DI,
                            UserGUID = x.UserGUID,
                            WithinCostBand_YN = x.WithinCostBand_YN
                        }).Skip(skip).Take(take).ToList();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<OneTimeEvent>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<OneTimeEvent>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, events);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }


        private HttpResponseMessage GetBaseStringDataForReport(string[] p)
        {
            try
            {
                Models.OneTimeEvent oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OneTimeEvent>(p[0]);

                string base64String = string.Empty; ;
                //int eventId = Convert.ToInt32(p[0]);
                System.IO.MemoryStream memStream = new System.IO.MemoryStream();
                Report_CurriculumSummary report = new Report_CurriculumSummary();

                report.GenerateToMemoryStream(memStream, oneTime);
                Byte[] byteArray = memStream.ToArray();
                // base64String = "data:application/pdf;base64;";
                base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);

                memStream.Close();

                //Response.Clear();
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.AddHeader("Content-Disposition", "inline; filename=SystemActivityReport.pdf");
                //Response.AddHeader("Content-Length", byteArray.Length.ToString());
                //Response.ContentType = "application/pdf";
                //Response.BinaryWrite(byteArray);
                //Response.End();




                return this.Request.CreateResponse(HttpStatusCode.OK, base64String);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        async private Task<HttpResponseMessage> GetProgramsAndStrategies(string[] p)
        {
            try
            {
                var type = p[0].Trim();
                var ProgramsAndStrategies = new List<DataGadget.Web.DataModel.tblProgramsAndStrategy>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbProgramsAndStrategies = await context.tblProgramsAndStrategies.Where(x => x.Type == type && (x.IsActive==true || x.IsActive==null) && (x.LongDescription != null && x.LongDescription != "")).OrderBy(x=>x.LongDescription).ToListAsync();
                    foreach (var Programs in dbProgramsAndStrategies)
                    {
                        ProgramsAndStrategies.Add(
                            new DataGadget.Web.DataModel.tblProgramsAndStrategy { ID = Programs.ID, Type = Programs.Type, Name = Programs.Name, LongDescription = Programs.LongDescription }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, ProgramsAndStrategies);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetProgramIdByProgramName(string[] p)
        {
            try
            {
                var type = p[0].Trim();
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    i = context.tblProgramsAndStrategies.Where(x => x.Name == type).Select(x => x.ID).Single();

                   
                   
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, i);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetServiceLocations(string[] p)
        {
            try
            {
                //var type = p[0].Trim();
                var ProgramsAndStrategies = new List<DataGadget.Web.DataModel.TblServicelocation>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbProgramsAndStrategies = await context.TblServicelocations.Where(x=>x.IsActive==true).ToListAsync();
                    foreach (var Programs in dbProgramsAndStrategies)
                    {
                        ProgramsAndStrategies.Add(
                            new DataGadget.Web.DataModel.TblServicelocation { Id = Programs.Id, Servicelocation = Programs.Servicelocation,IsActive=Programs.IsActive }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, ProgramsAndStrategies);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetServiceLocations1(string[] p)
        {
            try
            {
                //var type = p[0].Trim();
                
                var ProgramsAndStrategies = new List<Models.ServiceLocationsModel>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbProgramsAndStrategies = await context.TblServicelocations.Where(x => x.IsActive == true).ToListAsync();
                    foreach (var Programs in dbProgramsAndStrategies)
                    {
                        ProgramsAndStrategies.Add(
                            new Models.ServiceLocationsModel { ID = Programs.Id,ServiceLocation = Programs.Servicelocation, IsActive = Programs.IsActive }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, ProgramsAndStrategies);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetStatewideInitiatives(string[] p)
        {
            try
            {

                var StatewideInitiatives = new List<DataGadget.Web.DataModel.statewideInitiative>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbProgramsAndStrategies = await context.statewideInitiatives.Where(x => x.IsActive == true).ToListAsync();
                    foreach (var Programs in dbProgramsAndStrategies)
                    {
                        StatewideInitiatives.Add(
                            new DataGadget.Web.DataModel.statewideInitiative { id = Programs.id, initiativeName = Programs.initiativeName, IsActive = Programs.IsActive }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, StatewideInitiatives);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        private HttpResponseMessage SaveEventDetails(string[] p)
        {


            try
            {

                Models.OneTimeEvent oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OneTimeEvent>(p[0]);

                bool b = false;
                int cnt = 0;
                #region "parameters"
                var Entries = new DataModel.tbl2008_NOM_Entries();
                Entries.SurveysEntered = false;


                // Entries.StartDate = System.DateTime.Now;//p[1]
                Entries.StartDate = oneTime.StartDate;
                Entries.EndDate = oneTime.EndDate;


                //Entries.ProgramType_IP = p[0];// "Individual";
                Entries.ProgramType_IP = oneTime.ProgramType_IP;


                // Entries.InterventionType = p[3];//"Universal";
                Entries.InterventionType = oneTime.InterventionType;
                //Entries.TyoeOfProgram_EA = p[4];// null;
                Entries.TyoeOfProgram_EA = oneTime.TyoeOfProgram_EA;
                //Entries.UniversalType_DI = p[6];// null;
                Entries.UniversalType_DI = oneTime.UniversalType_DI;
                // Entries.ProgramNameSource = p[5];// null;
                Entries.ProgramNameSource = oneTime.ProgramNameSource;
                Entries.ProgramName = oneTime.ProgramName; //p[7];// null;
                Entries.GroupName = oneTime.GroupName;// p[8];// null;

                //b = int.TryParse(p[9], out cnt);
                //Entries.NumberParticipants = cnt;// null;
                //b = false; cnt = 0;

                //b = int.TryParse(p[10], out cnt);
                //Entries.Males = cnt;// null;
                //b = false; cnt = 0;

                //b = int.TryParse(p[11], out cnt);
                //Entries.Females = cnt;// null;
                //b = false; cnt = 0;

                Entries.ServiceLocationName = oneTime.ServiceLocationName;

                Entries.ServiceSchoolName = oneTime.ServiceSchoolName;

                Entries.NumberParticipants = oneTime.NumberParticipants;
                Entries.Males = oneTime.Males;
                Entries.Females = oneTime.Females;



                Entries.Age0 = oneTime.Age0.HasValue ? oneTime.Age0 : 0;
                Entries.Age5 = oneTime.Age5.HasValue ? oneTime.Age5 : 0;
                Entries.Age12 = oneTime.Age12.HasValue ? oneTime.Age12 : 0;
                Entries.Age15 = oneTime.Age15.HasValue ? oneTime.Age15 : 0;
                Entries.Age18 = oneTime.Age18.HasValue ? oneTime.Age18 : 0;
                Entries.Age21 = oneTime.Age21.HasValue ? oneTime.Age21 : 0;
                Entries.Age25 = oneTime.Age25.HasValue ? oneTime.Age25 : 0;
                Entries.Age45 = oneTime.Age45.HasValue ? oneTime.Age45 : 0;
                Entries.Age65 = oneTime.Age65.HasValue ? oneTime.Age65 : 0;

                Entries.RaceWhite = oneTime.RaceWhite.HasValue ? oneTime.RaceWhite : 0;
                Entries.RaceBlack = oneTime.RaceBlack.HasValue ? oneTime.RaceBlack : 0;
                Entries.RaceHawaii = oneTime.RaceHawaii.HasValue ? oneTime.RaceHawaii : 0;
                Entries.RaceAsian = oneTime.RaceAsian.HasValue ? oneTime.RaceAsian : 0;
                Entries.RaceAmIndian = oneTime.RaceAmIndian.HasValue ? oneTime.RaceAmIndian : 0;
                Entries.RaceUnknownOther = oneTime.RaceUnknownOther.HasValue ? oneTime.RaceUnknownOther : 0;
                Entries.RaceMoreThanOne = oneTime.RaceMoreThanOne.HasValue ? oneTime.RaceMoreThanOne : 0;

                Entries.NotHispanic = oneTime.NotHispanic.HasValue ? oneTime.NotHispanic : 0;
                Entries.Hispanic = oneTime.Hispanic.HasValue ? oneTime.Hispanic : 0;

                // modification
                Entries.D_HiRi_ChildOfSA = oneTime.D_HiRi_ChildOfSA.HasValue ? oneTime.D_HiRi_ChildOfSA : 0; // Convert.ToInt32(p[33]);
                Entries.D_HiRi_PregUse = oneTime.D_HiRi_PregUse.HasValue ? oneTime.D_HiRi_PregUse : 0; //null;
                Entries.D_HiRi_K12DO = oneTime.D_HiRi_K12DO.HasValue ? oneTime.D_HiRi_K12DO : 0; //null;

                Entries.D_HiRi_Violent = oneTime.D_HiRi_Violent.HasValue ? oneTime.D_HiRi_Violent : 0; //Convert.ToInt32(p[34]);
                Entries.D_HiRi_MentH = oneTime.D_HiRi_MentH.HasValue ? oneTime.D_HiRi_MentH : 0; //Convert.ToInt32(p[38]);
                Entries.D_HiRi_EconDis = oneTime.D_HiRi_EconDis.HasValue ? oneTime.D_HiRi_EconDis : 0; //Convert.ToInt32(p[41]);
                Entries.D_HiRi_PhysDis = oneTime.D_HiRi_PhysDis.HasValue ? oneTime.D_HiRi_PhysDis : 0; //Convert.ToInt32(p[35]);
                Entries.D_HiRi_AbuVict = oneTime.D_HiRi_AbuVict.HasValue ? oneTime.D_HiRi_AbuVict : 0; //Convert.ToInt32(p[39]);
                Entries.D_HiRi_Using = oneTime.D_HiRi_Using.HasValue ? oneTime.D_HiRi_Using : 0; //null;
                Entries.D_HiRi_Homel = oneTime.D_HiRi_Homel.HasValue ? oneTime.D_HiRi_Homel : 0; //Convert.ToInt32(p[36]);

                Entries.D_HiRi_FrLun = oneTime.D_HiRi_FrLun.HasValue ? oneTime.D_HiRi_FrLun : 0; //null;
                Entries.D_HiRi_Other = oneTime.D_HiRi_Other.HasValue ? oneTime.D_HiRi_Other : 0; //null;             
                Entries.D_HiRi_NotAp = oneTime.D_HiRi_NotAp.HasValue ? oneTime.D_HiRi_NotAp : 0; //null;

                Entries.Strat_ID_01 = oneTime.Strat_ID_01.HasValue ? oneTime.Strat_ID_01 : 0; //Convert.ToBoolean(p[45]) ? 1 : 0;

                Entries.Strat_ID_02 = oneTime.Strat_ID_02.HasValue ? oneTime.Strat_ID_02 : 0; //Convert.ToBoolean(p[46]) ? 1 : 0;

                Entries.Strat_ID_03 = oneTime.Strat_ID_03.HasValue ? oneTime.Strat_ID_03 : 0;//Convert.ToBoolean(p[47]) ? 1 : 0;
                Entries.Strat_ID_04 = oneTime.Strat_ID_04.HasValue ? oneTime.Strat_ID_04 : 0;//Convert.ToBoolean(p[48]) ? 1 : 0;
                Entries.Strat_ID_05 = oneTime.Strat_ID_05.HasValue ? oneTime.Strat_ID_05 : 0;//Convert.ToBoolean(p[49]) ? 1 : 0;
                Entries.Strat_ID_06 = oneTime.Strat_ID_06.HasValue ? oneTime.Strat_ID_06 : 0; //Convert.ToBoolean(p[50]) ? 1 : 0;
                Entries.Strat_ID_07 = oneTime.Strat_ID_07.HasValue ? oneTime.Strat_ID_07 : 0; //Convert.ToBoolean(p[51]) ? 1 : 0;
                Entries.Strat_ID_08 = oneTime.Strat_ID_08.HasValue ? oneTime.Strat_ID_08 : 0; //Convert.ToBoolean(p[52]) ? 1 : 0;
                Entries.Strat_ID_09 = oneTime.Strat_ID_09.HasValue ? oneTime.Strat_ID_09 : 0; //Convert.ToBoolean(p[53]) ? 1 : 0;
                Entries.Strat_ID_09Text = oneTime.Strat_ID_09Text; //p[54];

                Entries.Strat_Ed_11 = oneTime.Strat_Ed_11.HasValue ? oneTime.Strat_Ed_11 : 0; //Convert.ToBoolean(p[55]) ? 1 : 0;
                Entries.Strat_Ed_12 = oneTime.Strat_Ed_12.HasValue ? oneTime.Strat_Ed_12 : 0; //Convert.ToBoolean(p[56]) ? 1 : 0;
                Entries.Strat_Ed_13 = oneTime.Strat_Ed_13.HasValue ? oneTime.Strat_Ed_13 : 0;//Convert.ToBoolean(p[57]) ? 1 : 0;
                Entries.Strat_Ed_14 = oneTime.Strat_Ed_14.HasValue ? oneTime.Strat_Ed_14 : 0;//Convert.ToBoolean(p[58]) ? 1 : 0;
                Entries.Strat_Ed_15 = oneTime.Strat_Ed_15.HasValue ? oneTime.Strat_Ed_15 : 0;//Convert.ToBoolean(p[59]) ? 1 : 0;
                Entries.Strat_Ed_16 = oneTime.Strat_Ed_16.HasValue ? oneTime.Strat_Ed_16 : 0;//Convert.ToBoolean(p[60]) ? 1 : 0;
                Entries.Strat_Ed_17 = oneTime.Strat_Ed_17.HasValue ? oneTime.Strat_Ed_17 : 0;//Convert.ToBoolean(p[61]) ? 1 : 0;
                Entries.Strat_Ed_17Text = oneTime.Strat_Ed_17Text; //p[62];

                Entries.Strat_Alt_21 = oneTime.Strat_Alt_21.HasValue ? oneTime.Strat_Alt_21 : 0; //Convert.ToBoolean(p[63]) ? 1 : 0;
                Entries.Strat_Alt_22 = oneTime.Strat_Alt_22.HasValue ? oneTime.Strat_Alt_22 : 0;//Convert.ToBoolean(p[64]) ? 1 : 0;
                Entries.Strat_Alt_23 = oneTime.Strat_Alt_23.HasValue ? oneTime.Strat_Alt_23 : 0;//Convert.ToBoolean(p[65]) ? 1 : 0;
                Entries.Strat_Alt_24 = oneTime.Strat_Alt_24.HasValue ? oneTime.Strat_Alt_24 : 0;//Convert.ToBoolean(p[66]) ? 1 : 0;
                Entries.Strat_Alt_25 = oneTime.Strat_Alt_25.HasValue ? oneTime.Strat_Alt_25 : 0;//Convert.ToBoolean(p[67]) ? 1 : 0;
                Entries.Strat_Alt_26 = oneTime.Strat_Alt_26.HasValue ? oneTime.Strat_Alt_26 : 0;//Convert.ToBoolean(p[68]) ? 1 : 0;
                Entries.Strat_Alt_27 = oneTime.Strat_Alt_27.HasValue ? oneTime.Strat_Alt_27 : 0; //Convert.ToBoolean(p[69]) ? 1 : 0;
                Entries.Strat_Alt_27Text = oneTime.Strat_Alt_27Text; //p[70];

                Entries.Strat_IR_31 = oneTime.Strat_IR_31.HasValue ? oneTime.Strat_IR_31 : 0; ; //Convert.ToBoolean(p[71]) ? 1 : 0;
                Entries.Strat_IR_32 = oneTime.Strat_IR_32.HasValue ? oneTime.Strat_IR_32 : 0; ; //Convert.ToBoolean(p[72]) ? 1 : 0;
                Entries.Strat_IR_34 = oneTime.Strat_IR_34.HasValue ? oneTime.Strat_IR_34 : 0; ; //Convert.ToBoolean(p[73]) ? 1 : 0;
                Entries.Strat_IR_34Text = oneTime.Strat_IR_34Text; //p[74];

                Entries.Strat_CBP_41 = oneTime.Strat_CBP_41.HasValue ? oneTime.Strat_CBP_41 : 0;
                Entries.Strat_CBP_42 = oneTime.Strat_CBP_42.HasValue ? oneTime.Strat_CBP_42 : 0;
                Entries.Strat_CBP_43 = oneTime.Strat_CBP_43.HasValue ? oneTime.Strat_CBP_43 : 0;
                Entries.Strat_CBP_44 = oneTime.Strat_CBP_44.HasValue ? oneTime.Strat_CBP_44 : 0;
                Entries.Strat_CBP_45 = oneTime.Strat_CBP_45.HasValue ? oneTime.Strat_CBP_45 : 0;
                Entries.Strat_CBP_46 = oneTime.Strat_CBP_46.HasValue ? oneTime.Strat_CBP_46 : 0;
                Entries.Strat_CBP_46Text = oneTime.Strat_CBP_46Text;

                Entries.Strat_CBP_Faith = oneTime.Strat_CBP_Faith.HasValue ? oneTime.Strat_CBP_Faith : 0;

                Entries.Strat_Env_51 = oneTime.Strat_Env_51.HasValue ? oneTime.Strat_Env_51 : 0;
                Entries.Strat_Env_52 = oneTime.Strat_Env_52.HasValue ? oneTime.Strat_Env_52 : 0;
                Entries.Strat_Env_53 = oneTime.Strat_Env_53.HasValue ? oneTime.Strat_Env_53 : 0;
                Entries.Strat_Env_54 = oneTime.Strat_Env_54.HasValue ? oneTime.Strat_Env_54 : 0;
                Entries.Strat_Env_55 = oneTime.Strat_Env_55.HasValue ? oneTime.Strat_Env_55 : 0;
                Entries.Strat_Env_56_MW = oneTime.Strat_Env_56_MW.HasValue ? oneTime.Strat_Env_56_MW : 0;
                Entries.Strat_Env_57 = oneTime.Strat_Env_57.HasValue ? oneTime.Strat_Env_57 : 0;
                Entries.Strat_Env_57Text = oneTime.Strat_Env_57Text;

                Entries.NumberProgHrsRecd = oneTime.NumberProgHrsRecd.HasValue ? oneTime.NumberProgHrsRecd : 0;
                Entries.TotalCostofProgram = oneTime.TotalCostofProgram.HasValue ? oneTime.TotalCostofProgram : 0;
                Entries.AvgCostPerParticipant = oneTime.AvgCostPerParticipant.HasValue ? oneTime.AvgCostPerParticipant : 0;
                Entries.WithinCostBand_YN = oneTime.WithinCostBand_YN.HasValue ? oneTime.WithinCostBand_YN : 0;
                Entries.EntryDate = DateTime.Now; //oneTime.EntryDate; 

                Entries.Curriculum = 0;
                Entries.ParentEntry = 0;
                Entries.SessionName = "";
                Entries.closed = 1;

                Entries.CoalitionMeeting = oneTime.CoalitionMeeting;
                Entries.CoalitionType = oneTime.CoalitionType;

                Entries.TargetedStatewideInitiative = "";

                if (oneTime.lstStatewideInitiative.Count > 0)
                {

                    foreach (var item in oneTime.lstStatewideInitiative)
                    {
                        if (item.IsSelected == true)
                        {
                            if (Entries.TargetedStatewideInitiative != "")
                                Entries.TargetedStatewideInitiative = Entries.TargetedStatewideInitiative + "|" + item.initiativeName;
                            else
                                Entries.TargetedStatewideInitiative = item.initiativeName;

                        }
                    }

                }

                //Entries.TargetedStatewideInitiative = "Marijuana use by adolescents|Underage Drinking|";

                Entries.NumberOfEmployees = oneTime.NumberOfEmployees;
                Entries.TravelHours = oneTime.TravelHours; ;
                Entries.DirectServiceHours = oneTime.DirectServiceHours;
                Entries.PrepHours = oneTime.PrepHours;

                Entries.DocOfActivities_Agenda = oneTime.DocOfActivities_Agenda != null ? oneTime.DocOfActivities_Agenda : 0;
                Entries.DocOfActivities_EvalForm = oneTime.DocOfActivities_EvalForm != null ? oneTime.DocOfActivities_EvalForm : 0;
                Entries.DocOfActivities_PrePostTest = oneTime.DocOfActivities_PrePostTest != null ? oneTime.DocOfActivities_PrePostTest : 0;
                Entries.DocOfActivities_ContactForm = oneTime.DocOfActivities_ContactForm != null ? oneTime.DocOfActivities_ContactForm : 0;
                Entries.DocOfActivities_SignIn = oneTime.DocOfActivities_SignIn != null ? oneTime.DocOfActivities_SignIn : 0;
                Entries.DocOfActivities_Brochure = oneTime.DocOfActivities_Brochure != null ? oneTime.DocOfActivities_Brochure : 0;
                Entries.DocOfActivities_Other = oneTime.DocOfActivities_Other;
                Entries.DocOfActivities_OtherText = "";// oneTime.DocOfActivities_OtherText;
                Entries.OverallComments = oneTime.OverallComments != null ? oneTime.OverallComments : "";

                Guid g = new Guid("A7170971-8583-4C84-A6BF-AE4DC2C6CAAA");

                Entries.UserGUID = oneTime.UserGUID;//g;
                Entries.ID02_DirectoriesDistributed = oneTime.ID02_DirectoriesDistributed;
                Entries.ID04_BrochuresDistributed = oneTime.ID04_BrochuresDistributed;

                #endregion

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var record = context.TblUsers.Where(f => f.UserGUID == oneTime.UserGUID).FirstOrDefault();
                    Entries.RegionCode = record.StateRegionCode;//"000008";
                    Entries.SiteCode = record.SiteCode;//"000001";
                    Entries.FiscalYear = DateTime.Now.Year.ToString();// "2014";
                    Entries.OrgName = record.tblLocation.FriendlyName_loc;
                    Entries.StaffFname = record.FirstName;// "Mat";
                    Entries.StaffLName = record.LastName;//"Douglas";
                    Entries.Name = record.FirstName + record.LastName;//"Mat Douglas";
                    Entries.Degree = record.Degree;//"MAST";

                    context.tbl2008_NOM_Entries.Add(Entries);
                    context.SaveChanges();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, Entries);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public HttpResponseMessage DeleteEvent(string[] p)
        {

            try
            {
                int EventId = Convert.ToInt32(p[0]);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    i = context.suretool_DeleteCurriculum(EventId);
                    context.SaveChanges();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }

            catch (Exception ex)
            {

                throw ex;
            }

        }


    }
}