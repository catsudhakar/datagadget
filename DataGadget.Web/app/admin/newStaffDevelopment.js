﻿(function () {
    'use strict';
    var controllerId = 'newStaffDevelopment';
    angular
        .module('app')
        .controller(controllerId, newStaffDevelopment);
    //newUser.$inject = ['$scope', '$http', 'common', '$routeParams','staffInfo'];
    function newStaffDevelopment($scope, $http, $modalInstance, staffInfo, $location, $route, common, $modal) {
        var userid = localStorage.getItem('userId');
        if (staffInfo) {
            $scope.title = "Edit Staff Development";
        }
        else {
            $scope.title = "Add Staff Development";
        }
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var getLogFn1 = common.logger.getLogFn;
        var log1 = getLogFn1(controllerId, 'warn');
        $scope.staff = {};
        $scope.providers = [];
        $scope.staff.providerName = 1;
        $scope.staff.evidenceBased = 1;
        $scope.IsOtherDiv = false;
        activate();
        function activate() {
            $scope.today = function () {
                $scope.dt = new Date();
            };
            $scope.today();
            $scope.clear = function () {
                $scope.dt = null;
            };
            // Disable weekend selection
            $scope.disabled = function (date, mode) {
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            };
            $scope.toggleMin = function () {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.toggleMin();
            $scope.open = function ($event) {
                $scope.staff.openBeginDateopened = false;
                $event.preventDefault();
                $event.stopPropagation();
                $scope.staff.opened = true;
            };
            $scope.openBeginDate = function ($event) {
                $scope.staff.opened = false;
                $event.preventDefault();
                $event.stopPropagation();
                $scope.staff.openBeginDateopened = true;
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
            $scope.format = $scope.formats[4];
            function formattedDate() {
                var d = new Date(Date.now()),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [month, day, year].join('/');
            }
            $scope.ismessage = "";
            $scope.isintmessage = "";

            var loginstaffInfo = JSON.parse(localStorage.getItem('staffInfo'));

            $http({ method: 'POST', url: 'api/Staff', data: { method: 'InitializeStaffForm', parameters: [] } })
            .success(function (pass) {
                //$scope.treamProgramList = pass.treamProgramList;
                $scope.providerList = pass.providerList;
                if (staffInfo) {
                    $scope.staff.trainingName = staffInfo.trainingName;

                    $scope.staff.startDate = staffInfo.startDate;
                    $scope.staff.endDate = staffInfo.endDate;

                    //   var sdate = new Date(staffInfo.startDate);
                    //var month1 = sdate.getMonth();
                    //var day1 = sdate.getDate();
                    //var year1 = sdate.getFullYear();

                    //// $scope.staff.startDate = new Date(staffInfo.startDate).toDateString("MM/dd/YYYY");
                    //$scope.staff.startDate = new Date(year1, month1, day1);;
                    //$scope.staff.endDate = new Date(staffInfo.endDate);

                    $scope.staff.hours = staffInfo.hours;
                    if (staffInfo.providerID != null) {
                        $scope.staff.providerName = staffInfo.providerID;
                    }
                    else {
                        $scope.staff.providerName = 0;
                        $scope.IsOtherDiv = true;
                        $scope.staff.otherProviderName = staffInfo.otherProviderName;
                    }
                    $scope.staff.trainerDeatils = staffInfo.trainerDeatils;
                    //$scope.staff.educationCredits = staffInfo.educationCredits;
                    $scope.staff.evidenceBased = staffInfo.evidenceBased;
                    $scope.staff.id = staffInfo.id;

                    $scope.staff.instructorName = staffInfo.instructorName;
                    $scope.staff.learningObjectives = staffInfo.learningObjectives;
                }
                else {
                }
            })
            .error(function (fail) {
            });
        }
        $scope.IsOther = function (providerid) {
            if (providerid == "0") {
                $scope.IsOtherDiv = true;
            }
            else {
                $scope.IsOtherDiv = false;
            }
        }
        $scope.ok = function (staffForm) {
          
            $scope.staff.userName = userid;
            $scope.isSubmit = true;
            $scope.isintmessage = "";
            if (staffForm.$valid) {

                var iHours = parseInt($scope.staff.hours);
                if (isNaN(iHours)) {
                    $scope.isintmessage = "Please enter integer values for hours";
                    return false;
                }

                var sdt = Date.parse($scope.staff.startDate);
                var edt = Date.parse($scope.staff.endDate);
                $scope.ismessage = "";
                if (sdt === undefined || edt === undefined) {
                    $scope.ismessage = 'Please enter start date and end date';
                    return false;
                }
                if (sdt !== undefined && edt !== undefined) {

                    sdt = Date.parse($scope.staff.startDate);
                    edt = Date.parse($scope.staff.endDate);

                    //var sday = sdt.getDate();
                    //var smonth = sdt.getMonth();
                    //var syear = sdt.getFullYear();
                    //var eday = edt.getDate();
                    //var emonth = edt.getMonth();
                    //var eyear = edt.getFullYear();
                    //var sdate = new Date();
                    //sdate.setFullYear(syear, smonth, sday);
                    //var edate = new Date();
                    //edate.setFullYear(eyear, emonth, eday);
                    if (edt < sdt) {
                        $scope.ismessage = 'End date cannot be greater than begin date';
                        return false;
                    }
                }
             
                $scope.staff.startDate = GetFormattedDate(new Date($scope.staff.startDate));
                $scope.staff.endDate = GetFormattedDate(new Date($scope.staff.endDate));


                if ($scope.staff.providerName != "0") {
                    $scope.staff.otherProviderName = "";
                }
                

                $http({
                    method: 'POST', url: 'api/Staff',
                    data: {
                        method: "CreateStaff",
                        parameters: [JSON.stringify($scope.staff)]
                    }
                })
                .success(function (result) {

                    var addStaffDevelopmentModal1 = $modal.open({
                        templateUrl: 'app/admin/staffDevelopmentPreview.html',
                        controller: 'staffDevelopmentPreview',
                        backdrop: 'static',
                        resolve: {
                            staffInfo: function () {
                                return result;
                            }
                        }
                    });

                    $modalInstance.close(result);
                    log("Staff Development Record saved successfully");




                })
                .error(function (error) {
                    console.log(error);
                    $modalInstance.close(result);
                    log("Staff Development Record saved successfully");
                })
            }
        };

        $scope.updateSite = function (site) {
            $scope.sites = $scope.sites || [];
            if (site.isSelected) {
                $scope.sites.push(site.categoryID);
                $scope.sites = _.uniq($scope.sites);
            }
            else {
                $scope.sites = _.without($scope.sites, site.categoryID);
            }
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
})();
