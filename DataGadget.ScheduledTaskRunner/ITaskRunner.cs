﻿using System.Threading.Tasks;
namespace DataGadget.ScheduledTaskRunner
{
    public interface ITaskRunner
    {
        Task Run(params string[] args);
    }
}
