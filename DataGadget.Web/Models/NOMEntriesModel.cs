﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataGadget.Web.Models
{
    [KnownTypeAttribute(typeof(OrganizationModel))]
    public class NOMEntriesModel
    {
        public decimal SumOfPrepHours { get; set; }
        public decimal SumOfTravelHours { get; set; }
        public decimal SumOfServiceHours { get; set; }
        public decimal EvidenceBasedHours { get; set; }
        public decimal NonEvidenceBasedHours { get; set; }
        public decimal TargetHours { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string TyoeOfProgram_EA { get; set; }
    }
    [KnownTypeAttribute(typeof(OrganizationModel))]
    public class BedCountModel
    {
        public int TotalAssignedBeds { get; set; }
        public int? BedCount { get; set; }
        public int? AvailableBedCount { get; set; }

        public int TotalAdultMaleAssignedBeds { get; set; }
        public int? AdultMale { get; set; }

        public int TotalAdultFemaleAssignedBeds { get; set; }
        public int? AdultFemale { get; set; }

        public int TotalAdolescentMaleAssignedBeds { get; set; }
        public int? AdolescentMale { get; set; }

        public int TotalAdolescentFemaleAssignedBeds { get; set; }
        public int? AdolescentFemale { get; set; }

        public int? LocationId { get; set; }

        public int PKey { get; set; }

        public string FriendlyName_loc { get; set; }
        public string LocName { get; set; }
        public DateTime? EntryDate { get; set; }

        public string PersentagesBeds { get; set; }

        public string ProgramName { get; set; }


    }

    [KnownTypeAttribute(typeof(OrganizationModel))]
    public class PatientContacts
    {
        public string PatientId { get; set; }
        public int SurveyItemId { get; set; }
        public Guid SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }

        public DateTime? EntryDate { get; set; }

        public int ContactCount { get; set; }
        public string imagename { get; set; }

        public DateTime SurveyDate { get; set; }

        public int ProgramId { get; set; }

        public int LocationID { get; set; }

        public int ServiceID { get; set; }

        public string ClientGenderType { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime DischargeDate { get; set; }

        public string Description { get; set; }

        public string ProgramName { get; set; }
        public bool Is60days { get; set; }
        public bool Is180days { get; set; }

    }

    public class MailPatientContacts
    {
        public string PatientId { get; set; }
        public int SurveyItemId { get; set; }
        public Guid SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }

        public DateTime? SurveyDate { get; set; }

        public string FollowupComment { get; set; }


        public DateTime FollowupDate { get; set; }

        public int FollowupId { get; set; }

    }

    public partial class FollowUpContactModel
    {
        public int Id { get; set; }
        public string PatientId { get; set; }
        public System.DateTime FollowupDate { get; set; }
        public string FollowupComment { get; set; }
        public int FollowupId { get; set; }
        public int SurveyItemId { get; set; }
    }

    public partial class FollowUpReportModel
    {
      
        public System.DateTime FollowupDate { get; set; }
        public string FollowupComment { get; set; }
        public int FollowupId { get; set; }

        public string PatientId { get; set; }
        public int SurveyItemId { get; set; }
        public Guid SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }

        public DateTime? EntryDate { get; set; }

        //public int ContactCount { get; set; }
        //public string imagename { get; set; }

        public DateTime SurveyDate { get; set; }

       // public int ProgramId { get; set; }

        public int LocationID { get; set; }

        public string LocationName { get; set; }

        public int ServiceID { get; set; }

        public string ClientGenderType { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime DischargeDate { get; set; }

        public string Description { get; set; }

        //public string ProgramName { get; set; }
        //public bool Is60days { get; set; }
        //public bool Is180days { get; set; }
        
        
    }

    public partial class IndigentFuntReportModel
    {

      

        public string PatientId { get; set; }
        public int SurveyItemId { get; set; }
        public Guid SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }

        public DateTime? EntryDate { get; set; }

       

        public DateTime SurveyDate { get; set; }

        public int ProgramId { get; set; }
        public string ProgramName { get; set; }

        public int LocationID { get; set; }

        public string LocationName { get; set; }

        public int ServiceID { get; set; }
        public string  ServiceName { get; set; }

        public string ClientGenderType { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ? DischargeDate { get; set; }

        public string Description { get; set; }

        public bool isIndigent { get; set; }

        public int Days { get; set; }

        public double TotalAmount { get; set; }

       


    }

    public partial class bedutilizationReportModel
    {

        public System.DateTime BedUtilizationDate { get; set; }
        public string FollowupComment { get; set; }
        public int FollowupId { get; set; }

        public string PatientId { get; set; }
        public int SurveyItemId { get; set; }
        public Guid SurveyAdmin { get; set; }
        public string SurveyAdminName { get; set; }
        public string SurveyName { get; set; }

        public DateTime? EntryDate { get; set; }

        //public int ContactCount { get; set; }
        //public string imagename { get; set; }

        public DateTime SurveyDate { get; set; }

        // public int ProgramId { get; set; }

        public int LocationID { get; set; }

        public string LocationName { get; set; }

        public int ServiceID { get; set; }

        public string ClientGenderType { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime DischargeDate { get; set; }

        public string Description { get; set; }

        //public string ProgramName { get; set; }
        //public bool Is60days { get; set; }
        //public bool Is180days { get; set; }


    }
}