﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('printsummary', printsummary);

    // printsummary.$inject = ['$scope', '$modalInstance'];

    function printsummary($scope, $http, eventInfo, $modalInstance, $sce, $location, sessionService, $modal) {
        $scope.title = 'printsummary';

        var vm = this;
        vm.onetime = {};

        activate();

        function activate() {
            vm.onGoingEvent = {};
            if (eventInfo) {
                // vm.onGoingEvent.id = eventInfo.id;
                $http({
                    method: 'POST', url: 'api/OngoingEvents',
                    data: { method: "GetBaseStringDataForReport", parameters: [JSON.stringify(eventInfo)] }
                })
                .success(function (result) {
                    // $scope.source = result;
                    var reportData = "data:application/pdf;base64," + result;
                    $scope.source = $sce.trustAsResourceUrl(reportData);
                })
                .error(function (error) {
                    console.log(error);
                })
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
            if (eventInfo.curriculum == 1) {
                //if (eventInfo.tyoeOfProgram_EA == "Evidence" && eventInfo.programNameSource == "NREPP") {
                if (eventInfo.tyoeOfProgram_EA == "Evidence" && eventInfo.programNameSource == "NREPP" && (eventInfo.closed == 1 || eventInfo.parentEntry == 0)) {
                    if (eventInfo.closed == 0) {
                        eventInfo.age0 = 1; //Surevytype pretest or post test
                    }
                    else {
                        eventInfo.age0 = 2;//posttest
                    }

                    eventInfo.surveyCount = 0;


                    sessionService.setData(eventInfo);

                    var modalInstance1 = $modal.open({
                        templateUrl: '/app/common/confirmationSurveyDialog.html',
                        controller: 'confirmationSurveyDialogCtrl',
                        backdrop: 'static',
                    });
                    modalInstance1.result.then(function () {
                        $location.path('/surveyTools');
                        // $route.reload();
                    }, function () {
                        $location.path('/ongoingEvents');
                    });
                    // $location.path('/surveyTools');

                }
                else {
                    $location.path('/ongoingEvents');
                }

            }
            else {

                $location.path('/oneTimeEvents');
            }

        }

        $scope.print = function () {
            $modalInstance.dismiss('cancel');
            $location.path('/ongoingEvents');

        }
    }
})();
