﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;
using DREAMTool;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

//using DataGadget.Web.DataModel;

namespace DataGadget.Web.Controllers
{
    [RequireHttps]
    public class UserController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {

                    case "GetMessage":
                        return await GetUserMessage(value.Parameters);
                    case "GetMonthlyMailDetails":
                        return await GetMonthlyMailDetails(value.Parameters);

                    case "CreateMonthlyMessages":
                        return CreateMonthlyMessages(value.Parameters);

                    case "CreateLoginMessages":
                        return CreateLoginMessages(value.Parameters);

                    case "GetActiveUserMessage":
                        return await GetActiveUserMessage(value.Parameters);

                    case "GetUsers":
                        return await GetUsers(value.Parameters);
                    case "AllUsers":
                        return await GetAllUsers(value.Parameters);
                    case "ValidateUser":
                        return await ValidateUser(value.Parameters);
                    case "ForgotPassword":
                        return await ForgotPassword(value.Parameters);
                    case "ResetPassword":
                        return await ResetPassword(value.Parameters);
                    case "CreateUser":
                        return CreateUser(value.Parameters);
                    case "GetUser":
                        return await GetUser(value.Parameters);

                    case "DeleteUser":
                        return DeleteUser(value.Parameters);
                    case "CheckUser":
                        return CheckUser(value.Parameters);
                    case "GetTreatmentProgramListByLocId":
                        return await GetTreatmentProgramListByLocIdAsync(value.Parameters);
                    case "InitializeUserForm":
                        return await InitializeUserForm(value.Parameters);
                    case "GetUsersByPage":
                        return await GetUsersByPage(value.Parameters);
                    case "SearchUser":
                        return await SearchUser(value.Parameters);
                    case "checkuseremail":
                        return checkuseremail(value.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //weblogger.Error("Exception In UserController : " + ex.Message); 

                return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));

            }
        }




        async private Task<HttpResponseMessage> InitializeUserForm(string[] p)
        {

            var user = new User
            {
                LocationCategoryList = await GetLocationCategoryListAsync(p),
                LocationList = await GetLocationListAsync(),
                TreamProgramList = await GetTreatmentProgramListAsync(p),
                surveySerivesList = await GetSurveySericesListAsync(p)
            };

            return this.Request.CreateResponse(HttpStatusCode.OK, user);
        }


        async private Task<List<LocCategory>> GetLocationCategoryListAsync(string[] p)
        {
            try
            {
                var locations = new List<LocCategory>();
                var dblocations = new List<DataModel.LocationCategory>();
                // int locationId = Convert.ToInt32(p[0]);
                int locationId = Convert.ToInt32(p[1]);
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    dblocations = await context.LocationCategories.Where(x => x.LocationID == locationId).ToListAsync();

                    if (dblocations.Count > 0)
                    {
                        foreach (var location in dblocations)
                        {
                            locations.Add(
                                new LocCategory { RecNo = location.RecNo, LocationID = location.LocationID, CategoryID = location.CategoryID, SurveyCategory = location.SurveyCategory }
                                );
                        }
                    }
                    else
                    {
                        int? lid = context.TblUsers.Where(x => x.UserLevel == "Admin").Select(x => x.LocationID).FirstOrDefault();

                        dblocations = await context.LocationCategories.Where(x => x.LocationID == lid).ToListAsync();


                        foreach (var location in dblocations)
                        {
                            locations.Add(
                                new LocCategory { RecNo = location.RecNo, LocationID = location.LocationID, CategoryID = location.CategoryID, SurveyCategory = location.SurveyCategory }
                                );
                        }

                    }
                }

                return locations;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetLocationCategoryListAsync Method : " + ex.Message); 

                throw ex;
            }
        }

        async private Task<List<Location>> GetLocationListAsync()
        {
            try
            {
                var locations = new List<Location>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.tblLocations.Where(x => x.IsActive == true).OrderBy(x => x.FriendlyName_loc).ToListAsync();
                    foreach (var item in dblocations)
                    {

                        locations.Add(
                            new Location { PKey = item.PKey, FriendlyName_loc = item.FriendlyName_loc.Trim() }
                            );
                    }
                }

                return locations;
            }
            catch (Exception ex)
            {

                //weblogger.Error("Exception In UserController,GetLocationListAsync Method : " + ex.Message); 

                throw ex;
            }
        }

        async private Task<List<TreatmentProg>> GetTreatmentProgramListAsync(string[] p)
        {
            try
            {
                int locationId = Convert.ToInt32(p[0]);
                var program = new List<TreatmentProg>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.TreatmentPrograms.Where(x => x.LocationID == locationId && x.IsActive == true).ToListAsync();
                    foreach (var location in dblocations)
                    {
                        program.Add(
                            new TreatmentProg { RecNo = location.RecNo, LocationID = location.LocationID, ProgramName = location.ProgramName }
                            );
                    }
                }

                return program;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetTreatmentProgramListAsync Method : " + ex.Message); 

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetTreatmentProgramListByLocIdAsync(string[] p)
        {
            try
            {
                int locationId = Convert.ToInt32(p[0]);
                var program = new List<TreatmentProg>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.TreatmentPrograms.Where(x => x.LocationID == locationId && x.IsActive == true).ToListAsync();
                    foreach (var location in dblocations)
                    {
                        program.Add(
                            new TreatmentProg { RecNo = location.RecNo, LocationID = location.LocationID, ProgramName = location.ProgramName }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, program);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetTreatmentProgramListByLocIdAsync Method : " + ex.Message); 

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> ForgotPassword(string[] p)
        {
            try
            {




                User user = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var email = p[0];
                    var res = await context.TblUsers.FirstOrDefaultAsync(x => x.Email_addr == email);
                    if (res != null)
                    {
                        string token = GetMD5HashData(res.PKey.ToString());
                        res.PasswordToken = token;
                        await context.SaveChangesAsync();
                        //string addressFrom = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];

                        string addressFrom = System.Configuration.ConfigurationManager.AppSettings["generalEmailFromAddress"];

                        string weburl = System.Configuration.ConfigurationManager.AppSettings["weburl"];
                        string addressTo = res.Email_addr;
                        string subject = "Forgot Password";

                        string mailBody = "Hi " + res.FirstName + " " + res.LastName + ",<br><br> This email was sent automatically by DataGadget in response to your request to reset your password.<br><br>";

                        mailBody += "To reset your password, click on the below link (expires in 24 hours):<br><br>";

                        mailBody += "<a href='" + weburl + "#/resetpassword?id=" + token + "'>Reset Password</a> <br><br>";

                        mailBody += "Best Regards,<br> Datagadget.";

                        // string mailBody = "Hi " + res.FirstName + " " + res.LastName + ",<br><br> You can reset your password by using the below link.<br><br>";
                        //mailBody += "<a href='" + weburl + "#/resetpassword?id=" + token + "'>Reset Password</a> <br><br>";
                        //mailBody += "Please ignore this email, if you haven't made this request. You can continue to use your current password.<br><br>";
                        //mailBody += "Best Regards,<br> Datagadget.";
                        bool b = Email.SendEmail(addressTo, addressFrom, subject, mailBody);
                        if (b == true)
                            return this.Request.CreateResponse(HttpStatusCode.OK, "0");
                        else
                            return this.Request.CreateResponse(HttpStatusCode.OK, "1");
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,ForgotPassword Method : " + ex.Message); 
                throw ex;
            }
        }
        async private Task<HttpResponseMessage> ResetPassword(string[] p)
        {
            try
            {
                User user = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    string token = p[0];
                    string newpassword = p[1];
                    var res = await context.TblUsers.FirstOrDefaultAsync(x => x.PasswordToken == token);
                    if (res != null)
                    {
                        res.PW = GetMD5HashData(newpassword);
                        res.PasswordToken = null;
                        await context.SaveChangesAsync();
                        return this.Request.CreateResponse(HttpStatusCode.OK, "0");
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,ResetPassword Method : " + ex.Message); 
                throw ex;
            }
        }

        async private Task<HttpResponseMessage> ValidateUser(string[] p)
        {
            try
            {
                User user = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var uname = p[0].ToLowerInvariant().Trim();

                    string pass = GetMD5HashData(p[1]);

                    //from view
                    //var res = await context.Users.FirstOrDefaultAsync(x => x.LoweredUserName == uname && x.PW == pass);
                    //var res = from u in context.TblUsers
                    //          join uc in context.UserCategories on u.UserGUID equals uc.UserID
                    //          where u.LoginID == uname && u.PW == pass
                    //          select u;

                    //var res = await context.TblUsers.FirstOrDefaultAsync(x => x.LoginID == uname && x.PW == pass);
                    var res = await context.TblUsers.FirstOrDefaultAsync(x => x.LoginID == uname && x.PW == pass && x.IsActive == true);
//                    var clientQuery = @"select *  from client
//                                        where Convert(date, LastUpdated,101)=CONVERT(date, getdate(), 101)";

//                    var clientres = await context.Database.SqlQuery<DataGadget.Web.DataModel.Client>(clientQuery).ToListAsync();
                    if (res != null)
                    {

                        UpdateGrantYearDetails();

                        //Update client status in cleint table based delivered date

                        //if (clientres.Count == 0)
                        //{

                            var dateQuery = @"select SI.RecNo,SI.SurveyClientID,C.RecNo as ClientID,C.PAT_ID as ClientName,c.ClientStatusId as ClientStatusID
                                        from SurveyItem SI  inner join SurveyClient SC on si.SurveyClientID=sc.RecNo
                                        inner join Client c on sc.ClientID=c.RecNo  
                                        where Convert(date, DischargeDate,101)=CONVERT(date, getdate(), 101) and SI.SurveyID=1
                                        and (c.ClientStatusId  != 4  or c.ClientStatusId is null)
                                        order by si.RecNo desc";

                            var deschareUsers = await context.Database.SqlQuery<SurveyItemVM>(dateQuery).ToListAsync();

                            // var deschareUsers = await context.SurveyItems.Where(x => Convert.to(x.DischargeDate == DateTime.UtcNow.ToShortDateString()).ToListAsync();
                            if (deschareUsers.Count > 0)
                            {
                                foreach (var item in deschareUsers)
                                {
                                    var clientdata = await context.Clients.Where(x => x.RecNo == item.ClientID).SingleOrDefaultAsync();
                                    if (clientdata != null)
                                    {
                                        clientdata.LastUpdated = DateTime.UtcNow;
                                        clientdata.ClientStatusId = 4; //discharged
                                        await context.SaveChangesAsync();

                                    }
                                }
                            }
                        //}


                        List<LocCategory> UserCategoryList = new List<LocCategory>();
                        LocCategory UserCategory;
                        var UserCategory1 = await context.UserCategories.Where(x => x.UserID == res.UserGUID).ToListAsync();

                        foreach (var item in UserCategory1)
                        {

                            UserCategory = new LocCategory();
                            UserCategory.CategoryID = item.CategoryID;

                            UserCategory.SurveyCategory = item.SurveyCategory.SurveyCategory1;

                            UserCategoryList.Add(UserCategory);

                        }

                        List<LocCategory> CategoryList = new List<LocCategory>();
                        LocCategory Category;
                        var Category1 = await context.LocationCategories.Where(x => x.LocationID == res.LocationID).ToListAsync();

                        foreach (var item in Category1)
                        {

                            Category = new LocCategory();
                            Category.CategoryID = item.CategoryID;
                            Category.SurveyCategory = item.SurveyCategory;
                            CategoryList.Add(Category);

                        }



                        user = new User
                        {
                            Pkey = res.PKey,
                            UserId = res.UserGUID,// res.UserId,
                            FirstName = res.FirstName,
                            LastName = res.LastName,
                            UserName = res.FirstName,//res.UserName,
                            EmailId = res.Email_addr,
                            Level = res.UserLevel,
                            RegionCode = res.StateRegionCode,
                            SiteCode = res.SiteCode,
                            LocationId = res.LocationID,
                            Stateabbrev = res.StateAbbrev,
                            LocationCategoryList = CategoryList,
                            SelectedLocationCategoryList = UserCategoryList,
                            Hours = res.Hours,


                        };
                        return this.Request.CreateResponse(HttpStatusCode.OK, user);
                    }

                }
                return this.Request.CreateResponse(HttpStatusCode.Unauthorized, user);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,ValidateUser Method : " + ex.Message); 

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetAllUsers(string[] p)
        {
            try
            {
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<User>> data = await AllUsersWithPagination(p);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetAllUsers Method : " + ex.Message); 

                throw ex;
            }
        }

        private async Task<Tuple<List<Pager.PagerValue>, List<Models.User>>> AllUsersWithPagination(string[] p)
        {
            var users = new List<User>();
            var pagerResult = new DataGadget.Web.Pager.Pager();

            string strRole = string.Empty;
            strRole = p[0];
            string strStateAbbrev = string.Empty;
            strStateAbbrev = p[2];
            int locationId = Convert.ToInt32(p[1]);
            int? pageId = Convert.ToInt32(p[3]);

            int take = 10;
            int segment = 3;

            int skip = (Convert.ToInt32(pageId) - 1) * take;

            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                //var dbUsers = await context.Users.ToListAsync();  //geting data from view
                var dbUsers = new List<DataGadget.Web.DataModel.TblUser>();
                switch (strRole)
                {
                    case "SIT3U": //User
                    ////Users cannot get to this page
                    //dbUsers = await context.TblUsers.Where(x => x.PKey == 0).ToListAsync();  //geting data from table
                    //users = null;
                    //break;
                    case "SIT2A": //Admin
                        dbUsers = await context.TblUsers.Where(x => x.LocationID == locationId).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                        {
                            int userCount = await context.TblUsers.Where(x => x.LocationID == locationId).CountAsync();
                            pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                        }
                        break;
                    case "STA1A": //State
                        dbUsers = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev).OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                        {
                            int userCount = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev).CountAsync();
                            pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                        }
                        break;
                    case "Admin":
                        //Administrators can see everyone in the system
                        dbUsers = await context.TblUsers.OrderBy(x => x.UserGUID).Skip(skip).Take(take).ToListAsync();
                        {
                            int userCount = await context.TblUsers.CountAsync();
                            pagerResult = DataGadget.Web.Pager.Pager.Items(userCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                        }
                        break;
                    default:
                        break;

                }
                users = dbUsers.Select(user => new User
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            EmailId = user.Email_addr,
                            UserId = user.UserGUID,
                            UserName = user.LoginID,
                            AgencyName = user.LocationID.ToString(),
                            Password = user.PW,
                            // Password= Decrypt(user.PW, true),
                            Level = user.UserLevel,
                            Degree = user.Degree,
                            Experience = user.YrsPrevExp,
                            OfficePhoneNum = user.OfficePhone,
                            OfficeFaxNum = user.OfficeFax,
                            SelectedTreamProgramList = GetUserTreatmentProgramList(user.UserGUID),
                            SelectedLocationCategoryList = GetUserCategoryList(user.UserGUID),
                            Hours = user.Hours,
                            IsActive = user.IsActive,
                            SelectedSurveySerivesList = GetUserServicesList(user.UserGUID)

                        }).ToList();
            }

            DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
            pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
            pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
            pagerValue.HasNextPage = pagerResult.HasNextPage;
            pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
            pagerValue.LastPageIndex = pagerResult.LastPageIndex;
            pagerValue.NextPageIndex = pagerResult.NextPageIndex;
            pagerValue.NumberOfPages = pagerResult.NumberOfPages;
            pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
            Tuple<List<DataGadget.Web.Pager.PagerValue>, List<User>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<User>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, users);
            return data;

        }

        async private Task<HttpResponseMessage> GetUsersByPage(string[] p)
        {
            try
            {

                //if (p[0] == "SIT3U")
                //{
                //    return null;
                //}

                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<User>> data = await AllUsersWithPagination(p);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetUsersByPage Method : " + ex.Message); 

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> SearchUser(string[] p)
        {
            try
            {
                var users = new List<User>();

                string strRole = p[0];
                int locationId = Convert.ToInt32(p[1]);
                string strStateAbbrev = p[2];
                string searchKey = p[3];
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbUsers = await context.Users.ToListAsync();  //geting data from view
                    var dbUsers = new List<DataGadget.Web.DataModel.TblUser>();
                    switch (strRole)
                    {
                        case "SIT3U": //User
                            //Users cannot get to this page
                            dbUsers = await context.TblUsers.Where(x => x.PKey == 0 && x.IsActive == true).ToListAsync();  //geting data from table
                            users = null;
                            break;
                        case "SIT2A": //Admin
                            dbUsers = await context.TblUsers.Where(x => x.LocationID == locationId && x.IsActive == true && (x.FirstName.Contains(searchKey) || x.LastName.Contains(searchKey) || x.LoginID.Contains(searchKey)) || x.Email_addr.Contains(searchKey)).OrderBy(x => x.UserGUID).ToListAsync();
                            break;
                        case "STA1A": //State
                            dbUsers = await context.TblUsers.Where(x => x.StateAbbrev == strStateAbbrev && x.IsActive == true && (x.FirstName.Contains(searchKey) || x.LastName.Contains(searchKey) || x.LoginID.Contains(searchKey) || x.Email_addr.Contains(searchKey))).OrderBy(x => x.UserGUID).ToListAsync();
                            break;
                        case "Admin":
                            //Administrators can see everyone in the system
                            dbUsers = await context.TblUsers.Where((x => x.FirstName.Contains(searchKey) || x.LastName.Contains(searchKey) || x.LoginID.Contains(searchKey) || x.Email_addr.Contains(searchKey) && x.IsActive == true)).OrderBy(x => x.UserGUID).ToListAsync();
                            break;
                        default:
                            break;

                    }
                    users = dbUsers.Select(user => new User
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        EmailId = user.Email_addr,
                        UserId = user.UserGUID,
                        UserName = user.LoginID,
                        AgencyName = user.LocationID.ToString(),
                        Password = user.PW,
                        Level = user.UserLevel,
                        Degree = user.Degree,
                        Experience = user.YrsPrevExp,
                        OfficePhoneNum = user.OfficePhone,
                        OfficeFaxNum = user.OfficeFax,
                        SelectedTreamProgramList = GetUserTreatmentProgramList(user.UserGUID),
                        SelectedLocationCategoryList = GetUserCategoryList(user.UserGUID),
                        Hours = user.Hours,
                        IsActive = user.IsActive,
                        SelectedSurveySerivesList = GetUserServicesList(user.UserGUID)

                    }).ToList();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,SearchUser Method : " + ex.Message); 

                throw ex;
            }
        }
        private List<TreatmentProg> GetUserTreatmentProgramList(Guid id)
        {
            List<TreatmentProg> UserTreatmentProgramList = new List<TreatmentProg>();
            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                UserTreatmentProgramList = context.UserTreatmentPrograms.Where(x => x.UserID == id).
                    Select(x => new TreatmentProg
                    {
                        UserId = x.UserID,
                        ProgramId = x.TreatmentProgramID
                    }).ToList();
            }
            return UserTreatmentProgramList;
        }

        //get user services

        private List<UserIopModel> GetUserServicesList(Guid id)
        {
            List<UserIopModel> UserIopList = new List<UserIopModel>();
            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                UserIopList = context.UserIOPs.Where(x => x.UserId == id).
                    Select(x => new UserIopModel
                    {
                        UserId = x.UserId,
                        IopId = x.IopId
                    }).ToList();
            }
            return UserIopList;
        }

        private List<LocCategory> GetUserCategoryList(Guid id)
        {
            List<LocCategory> UserCategoryList = new List<LocCategory>();
            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                UserCategoryList = context.UserCategories.Where(x => x.UserID == id).
                    Select(x => new LocCategory
                    {
                        CategoryID = x.CategoryID
                    }).ToList();
            }
            return UserCategoryList;
        }

        private HttpResponseMessage CreateUser(string[] p)
        {

            // MembershipUser newUser = Membership.CreateUser();
            try
            {
                Models.User usr = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.User>(p[0]);
                var user = new DataModel.TblUser();

                bool istreatment = false;

                //user.UserGUID = Guid.NewGuid();
                //MembershipCreateStatus mstatus;
                // MembershipUser newUser = Membership.CreateUser(users.LoginID, users.PW, users.Email_addr);
                //MembershipUser newUser = Membership.CreateUser(users.LoginID, users.PW, users.Email_addr, "testing", "testing", true, out mstatus);
                /// Roles.AddUserToRole(users.LoginID, "SIT2A");
                //users.UserGUID = (Guid)newUser.ProviderUserKey;


                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var dblocations = context.tblLocations.Where(location => location.PKey == usr.LocationId).SingleOrDefault();
                    if (dblocations != null)
                    {
                        user.StateAbbrev = dblocations.StateAbbrev;
                        user.StateRegionCode = dblocations.StateRegionCode;
                        user.SiteCode = dblocations.SiteCode;
                    }
                    if (usr.UserId == Guid.Empty)
                    {
                        user.UserGUID = Guid.NewGuid();
                        user.LoginID = usr.UserName;
                        user.PW = GetMD5HashData(usr.Password);
                        user.LastName = usr.LastName;
                        user.FirstName = usr.FirstName;
                        user.UserLevel = usr.Level;
                        user.PrevSpec = 0;
                        user.Email_addr = usr.EmailId;
                        user.Degree = usr.Degree;
                        user.YrsPrevExp = usr.Experience;
                        user.OfficePhone = usr.OfficePhoneNum;
                        user.OfficeFax = usr.OfficeFaxNum;
                        user.fulltime = 0;
                        user.LocationID = usr.LocationId;
                        user.TreatmentProgramID = null;// usr.TreatmentId;
                        user.Hours = usr.Hours;
                        user.IsActive = usr.IsActive;
                        context.TblUsers.Add(user);
                        context.SaveChanges();
                    }
                    else   //updating user
                    {
                        DataGadget.Web.DataModel.TblUser U = new DataGadget.Web.DataModel.TblUser();
                        U = context.TblUsers.Where(p1 => p1.UserGUID == usr.UserId).SingleOrDefault();

                        U.LoginID = usr.UserName;
                        //if (!string.IsNullOrEmpty(usr.Password) && usr.Password != "password")
                        //    U.PW = GetMD5HashData(usr.Password);

                        if (!string.IsNullOrEmpty(usr.Password) && usr.Password != "pas$w0rd")
                            U.PW = GetMD5HashData(usr.Password);

                        U.LastName = usr.LastName;
                        U.FirstName = usr.FirstName;
                        U.UserLevel = usr.Level;
                        U.PrevSpec = 0;
                        U.Email_addr = usr.EmailId;
                        U.Degree = usr.Degree;
                        U.YrsPrevExp = usr.Experience;

                        U.StateAbbrev = user.StateAbbrev;
                        U.StateRegionCode = user.StateRegionCode;
                        U.SiteCode = user.SiteCode;

                        U.OfficePhone = usr.OfficePhoneNum;
                        U.OfficeFax = usr.OfficeFaxNum;
                        U.fulltime = 0;
                        U.LocationID = usr.LocationId;
                        U.TreatmentProgramID = null;// usr.TreatmentId;
                        U.Hours = usr.Hours;
                        U.IsActive = usr.IsActive;

                        context.SaveChanges();




                        List<DataGadget.Web.DataModel.UserTreatmentProgram> UsrTrtPrglist = new List<DataGadget.Web.DataModel.UserTreatmentProgram>();
                        UsrTrtPrglist = context.UserTreatmentPrograms.Where(program => program.UserID == usr.UserId).ToList();
                        if (UsrTrtPrglist != null)
                        {
                            context.UserTreatmentPrograms.RemoveRange(UsrTrtPrglist);
                            try { context.SaveChanges(); }
                            catch { }
                        }

                        List<DataGadget.Web.DataModel.UserCategory> UsrCatList = new List<DataGadget.Web.DataModel.UserCategory>();
                        UsrCatList = context.UserCategories.Where(cat => cat.UserID == usr.UserId).ToList();
                        if (UsrCatList != null)
                        {
                            context.UserCategories.RemoveRange(UsrCatList);
                            try { context.SaveChanges(); }
                            catch { }
                        }

                        //survey services
                        //need to get userid
                        List<DataGadget.Web.DataModel.UserIOP> UsrServiceList = new List<DataGadget.Web.DataModel.UserIOP>();
                        UsrServiceList = context.UserIOPs.Where(cat => cat.UserId == usr.UserId).ToList();
                        if (UsrServiceList != null)
                        {
                            context.UserIOPs.RemoveRange(UsrServiceList);
                            try { context.SaveChanges(); }
                            catch { }
                        }


                    }



                    List<DataGadget.Web.DataModel.UserTreatmentProgram> UserTreatmentProgramList = new List<DataModel.UserTreatmentProgram>();
                    DataGadget.Web.DataModel.UserTreatmentProgram usertreatprg;
                    if (usr.TreamProgramList.Count > 0)
                    {
                        foreach (var item in usr.TreamProgramList)
                        {
                            if (item.IsSelected)
                            {
                                usertreatprg = new DataModel.UserTreatmentProgram();
                                //usertreatprg.UserID = user.UserGUID;
                                if (usr.UserId == Guid.Empty)
                                    usertreatprg.UserID = user.UserGUID;
                                else
                                    usertreatprg.UserID = usr.UserId;

                                usertreatprg.TreatmentProgramID = item.RecNo;

                                UserTreatmentProgramList.Add(usertreatprg);
                                istreatment = true;
                            }
                        }
                        InsertTreatmentPrograms(UserTreatmentProgramList, usr.UserId);
                    }


                    List<DataGadget.Web.DataModel.UserCategory> UserCategoryList = new List<DataModel.UserCategory>();
                    DataGadget.Web.DataModel.UserCategory UserCategory;
                    if (usr.LocationCategoryList.Count > 0)
                    {
                        foreach (var item in usr.LocationCategoryList)
                        {
                            if (item.IsSelected)
                            {
                                UserCategory = new DataModel.UserCategory();
                                //UserCategory.UserID = user.UserGUID;
                                if (usr.UserId == Guid.Empty)
                                    UserCategory.UserID = user.UserGUID;
                                else
                                    UserCategory.UserID = usr.UserId;

                                UserCategory.CategoryID = item.CategoryID;

                                UserCategoryList.Add(UserCategory);
                            }
                        }
                        InsertUserCategories(UserCategoryList, usr.UserId);
                    }

                    //adding servives 

                    List<DataGadget.Web.DataModel.UserIOP> UserIOPList = new List<DataModel.UserIOP>();
                    DataGadget.Web.DataModel.UserIOP userIOP;
                    if (usr.surveySerivesList.Count > 0)
                    {
                        foreach (var item in usr.surveySerivesList)
                        {
                            if (item.IsSerivesSelected)
                            {
                                userIOP = new DataModel.UserIOP();
                                //usertreatprg.UserID = user.UserGUID;
                                if (usr.UserId == Guid.Empty)
                                    userIOP.UserId = user.UserGUID;
                                else
                                    userIOP.UserId = usr.UserId;

                                userIOP.IopId = item.RecNo;

                                UserIOPList.Add(userIOP);
                            }
                        }

                        //if (istreatment)
                        //{
                        //    userIOP = new DataModel.UserIOP();
                        //    if (usr.UserId == Guid.Empty)
                        //        userIOP.UserId = user.UserGUID;
                        //    else
                        //        userIOP.UserId = usr.UserId;

                        //    userIOP.IopId = 1;

                        //    UserIOPList.Add(userIOP);
                        //}
                        InsertUserIops(UserIOPList, usr.UserId);
                    }



                }

                return this.Request.CreateResponse(HttpStatusCode.OK, usr);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,CreateUser Method : " + ex.Message); 

                throw ex;
            }
        }

        protected void InsertTreatmentPrograms(List<DataGadget.Web.DataModel.UserTreatmentProgram> UserTreatmentProgramList, Guid pUserID)
        {
            try
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    DataGadget.Web.DataModel.UserTreatmentProgram U = new DataGadget.Web.DataModel.UserTreatmentProgram();
                    //if (pUserID == Guid.Empty)
                    context.UserTreatmentPrograms.AddRange(UserTreatmentProgramList);

                    context.SaveChanges();
                }
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,insertTreatmentProgram Method : " + ex.Message); 

                throw ex;
            }
        }

        protected void InsertUserCategories(List<DataGadget.Web.DataModel.UserCategory> UserCategoryList, Guid pUserID)
        {
            try
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    // if (pUserID == Guid.Empty)
                    context.UserCategories.AddRange(UserCategoryList);


                    context.SaveChanges();
                }
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,InsertUserCategories Method : " + ex.Message); 

                throw ex;
            }
        }

        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public HttpResponseMessage DeleteUser(string[] p)
        {

            try
            {
                Guid userId = new Guid(p[0]);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //  i = context.suretool_DeleteUser(userId);
                    DataGadget.Web.DataModel.TblUser U = new DataGadget.Web.DataModel.TblUser();
                    U = context.TblUsers.Where(p1 => p1.UserGUID == userId).SingleOrDefault();
                    U.IsActive = false;
                    context.SaveChanges();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,DeleteUser Method : " + ex.Message); 

                throw;
            }

        }

        async private Task<HttpResponseMessage> GetUser(string[] p)
        {

            try
            {
                User user = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    Guid userId = new Guid(p[0]);
                    var res = await context.Users.FirstOrDefaultAsync(x => x.UserId == userId);
                    if (res != null)
                    {
                        user = new User { UserId = res.UserId, FirstName = res.FirstName, LastName = res.LastName, UserName = res.UserName, EmailId = res.Email_addr };
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetUser Method : " + ex.Message); 

                throw ex;
            }

        }

        private HttpResponseMessage CheckUser(string[] p)
        {
            string Username = p[0];
            int AlreadyUser = 0;
            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                //var AlreadyUser = context.TblUsers.Where(x => x.LoginID.ToLower() == Username.ToLower()).SingleOrDefault();
                AlreadyUser = context.TblUsers.Where(x => x.LoginID.ToLower() == Username.ToLower()).Count();
                if (AlreadyUser > 0) //if (AlreadyUser != null)
                {
                    //if (AlreadyUser.LoginID.Trim() == Username.Trim())
                    return this.Request.CreateErrorResponse(HttpStatusCode.Ambiguous, "User Already Exists");
                }

            }

            return this.Request.CreateErrorResponse(HttpStatusCode.OK, Username);

        }

        private HttpResponseMessage checkuseremail(string[] p)
        {
            string Username = p[0];
            string prvEmail = p[1];
            int AlreadyUser = 0;
            bool isuser = false;
            if (Username != prvEmail)
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    AlreadyUser = context.TblUsers.Where(x => x.Email_addr.ToLower() == Username.ToLower()).Count();
                    if (AlreadyUser > 0)
                    {
                        isuser = true;
                        return this.Request.CreateResponse(HttpStatusCode.OK, isuser);
                    }

                }
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, isuser);

        }

        private string GetMD5HashData(string data)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            //convert the input text to array of bytes
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(data));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            return returnValue.ToString();

        }

        async private Task<HttpResponseMessage> GetUsers(string[] parameters)
        {
            try
            {
                IEnumerable<NewUser> dataSource = (IEnumerable<NewUser>)new List<NewUser>();
                IEnumerable<NewUser> users = (IEnumerable<NewUser>)new List<NewUser>();


                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    dataSource = await context.TblUsers
                        .OrderBy(surveyAdmin => surveyAdmin.FirstName)
                        .Select(x =>
                            new NewUser
                            {
                                ID = x.PKey.ToString(),
                                Name = x.FirstName + " " + x.LastName
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetUsers Method : " + ex.Message); 

                throw ex;
            }
        }


        private void UpdateGrantYearDetails()
        {
            //grantYear gy = new grantYear();
            //            select @GrantYear = datepart(year,@InputDate);
            //select @GrantMonth = datepart(month,@InputDate);

            //if @GrantMonth >= 7
            //   set @grantyear = @grantyear + 1;

            //RETURN(@grantyear);

            //            --2015        July 1, 2014 -       June 30, 2015
            //--2016      July 1, 2015 - June 30, 2016

            int month = System.DateTime.Now.Month;
            int year = System.DateTime.Now.Year;
            int currentYear = System.DateTime.Now.Year;

            if (month >= 7)
                year = year + 1;

            //gy.grantYear1 = year.ToString();
            //gy.startDate = new DateTime(year, 7, 1);
            //gy.startDate = new DateTime(year + 1, 6, 30);

            try
            {
                DataGadget.Web.DataModel.GrantYear gy = new DataGadget.Web.DataModel.GrantYear();

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var result = context.GrantYears.SingleOrDefault();

                    if (result != null)
                    {
                        if (result.GrantYear1 != year.ToString())
                        {
                            result.GrantYear1 = year.ToString();
                            result.StartDate = new DateTime(currentYear, 7, 1);
                            result.EndDate = new DateTime(year, 6, 30);
                            result.IsActive = true;
                            context.SaveChanges();
                        }

                    }
                    else
                    {
                        gy.GrantYear1 = year.ToString();
                        gy.StartDate = new DateTime(currentYear, 7, 1); ;
                        gy.EndDate = new DateTime(year, 6, 30);
                        gy.IsActive = true;
                        context.GrantYears.Add(gy);
                        context.SaveChanges();
                    }


                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        async private Task<List<surveySerivesModel>> GetSurveySericesListAsync(string[] p)
        {
            try
            {
                int locationId = Convert.ToInt32(p[0]);
                var lstsurveySerives = new List<surveySerivesModel>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dblocations = await context.SurveyServices.OrderBy(x => x.Description).ToListAsync();

                    //var dblocations = await context.LocationIOPs.Where(x => x.LocationId == locationId && x.IopId != 1).ToListAsync();
                    var dblocations = await context.LocationIOPs.Where(x => x.LocationId == locationId).ToListAsync();
                    foreach (var item in dblocations)
                    {

                        lstsurveySerives.Add(
                            new surveySerivesModel
                            {
                                RecNo = item.IopId,
                                Description = item.SurveyService.Description.Trim()
                            }
                            );
                    }
                }

                return lstsurveySerives;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetSurveySericesListAsync Method : " + ex.Message); 
                throw ex;
            }
        }

        protected void InsertUserIops(List<DataGadget.Web.DataModel.UserIOP> UserIOPList, Guid pUserID)
        {
            try
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    DataGadget.Web.DataModel.UserIOP U = new DataGadget.Web.DataModel.UserIOP();
                    //if (pUserID == Guid.Empty)
                    context.UserIOPs.AddRange(UserIOPList);

                    context.SaveChanges();
                }
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,InsertUsrIops Method : " + ex.Message); 
                throw ex;
            }
        }


        async private Task<HttpResponseMessage> GetUserMessage(string[] p)
        {

            try
            {
                LoginMessageModel user = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var res = await context.LoginMessages.FirstOrDefaultAsync();
                    if (res != null)
                    {
                        user = new LoginMessageModel { ID = res.ID, Message = res.Message, IsActive = res.IsActive };
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetUserMessage Method : " + ex.Message); 

                throw ex;
            }

        }

        async private Task<HttpResponseMessage> GetMonthlyMailDetails(string[] p)
        {

            try
            {
                MonthlyMessageModel message = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var res = await context.MailSettings.FirstOrDefaultAsync();
                    if (res != null)
                    {
                        message = new MonthlyMessageModel
                        {
                            ID = res.Id,
                            TreatmentMessage = res.TreatmentMessage,
                            PreventionMessage=res.PreventionMessage,
                            IsTreatment=res.IsTreatment,
                            IsPrevention=res.IsPrevention,
                            IsActive = res.IsActive
                        };
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetUserMessage Method : " + ex.Message); 

                throw ex;
            }

        }


        async private Task<HttpResponseMessage> GetActiveUserMessage(string[] p)
        {

            try
            {
                LoginMessageModel user = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var res = await context.LoginMessages.Where(x => x.IsActive == true).FirstOrDefaultAsync();
                    if (res != null)
                    {
                        user = new LoginMessageModel { ID = res.ID, Message = res.Message, IsActive = res.IsActive };
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,GetActiveUserMessage Method : " + ex.Message); 
                throw ex;
            }

        }




        private HttpResponseMessage CreateLoginMessages(string[] p)
        {

            try
            {
                Models.LoginMessageModel state = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.LoginMessageModel>(p[0]);
                DataGadget.Web.DataModel.LoginMessage statewide = new DataGadget.Web.DataModel.LoginMessage();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (state.ID == 0)
                    {

                        statewide.Message = state.Message;
                        statewide.IsActive = state.IsActive;
                        context.LoginMessages.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        DataGadget.Web.DataModel.LoginMessage U = new DataGadget.Web.DataModel.LoginMessage();
                        U = context.LoginMessages.Where(p1 => p1.ID == state.ID).SingleOrDefault();
                        U.ID = state.ID;
                        U.Message = state.Message;
                        U.IsActive = state.IsActive;
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,CreateLoginMessages Method : " + ex.Message); 
                throw ex;
            }
        }

        private HttpResponseMessage CreateMonthlyMessages(string[] p)
        {

            try
            {
                Models.MonthlyMessageModel message = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.MonthlyMessageModel>(p[0]);
                DataGadget.Web.DataModel.MailSetting mailsettings = new DataGadget.Web.DataModel.MailSetting();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (message.ID == 0)
                    {

                        mailsettings.TreatmentMessage = message.TreatmentMessage;
                        mailsettings.PreventionMessage = message.PreventionMessage;
                        mailsettings.IsPrevention = message.IsPrevention;
                        mailsettings.IsTreatment = message.IsTreatment;
                        mailsettings.IsPrevention = message.IsPrevention;
                        mailsettings.UpdatedBy = message.UpdatedBy;
                        mailsettings.UpdatedDate = System.DateTime.UtcNow;
                        mailsettings.IsActive = true;
                        context.MailSettings.Add(mailsettings);
                        context.SaveChanges();
                    }
                    else   
                    {
                    
                        mailsettings = context.MailSettings.Where(p1 => p1.Id == message.ID).SingleOrDefault();
                        mailsettings.TreatmentMessage = message.TreatmentMessage;
                        mailsettings.PreventionMessage = message.PreventionMessage;
                        mailsettings.IsPrevention = message.IsPrevention;
                        mailsettings.IsTreatment = message.IsTreatment;
                        mailsettings.IsPrevention = message.IsPrevention;
                        mailsettings.UpdatedBy = message.UpdatedBy;
                        mailsettings.UpdatedDate = System.DateTime.UtcNow;
                        mailsettings.IsActive = true;// message.IsActive;
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, message);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                //weblogger.Error("Exception In UserController,CreateLoginMessages Method : " + ex.Message); 
                throw ex;
            }
        }

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray = null;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //Dim settingsReader As System.Configuration.AppSettingsReader = New AppSettingsReader
            //'Get your key from config file to open the lock!
            //Dim key As String = DirectCast(settingsReader.GetValue("SecurityKey", GetType([String])), String)

            string key = "Test";

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

    }
}