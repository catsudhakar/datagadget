﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net.Mail;

namespace DREAMTool
{
    public class Email
    {

        public static bool SendEmail(string mailTo, string mailFrom, string subject, string body)
        {
            try
            {
                //MailMessage mail = new MailMessage();
                //mail.Body = body;
                //mail.IsBodyHtml = true;
                //mail.From = new MailAddress(mailFrom);
                //mail.Subject = subject;
                //mail.To.Add(mailTo);
                //string addressFrom = System.Configuration.ConfigurationManager.AppSettings["generalEmailFromAddress"];
                //string password = System.Configuration.ConfigurationManager.AppSettings["emailPassword"];
                //SmtpClient smtp = new SmtpClient();
                //smtp.Host = System.Configuration.ConfigurationManager.AppSettings["host"]; // "smtp.gmail.com";
                //smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["port"]); //587;
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new System.Net.NetworkCredential("rammohan.test1@gmail.com", "cattesting");// Enter seders User name and password
                //smtp.Credentials = new System.Net.NetworkCredential(addressFrom, password);// Enter seders User name and password
                //smtp.EnableSsl = true;
                //smtp.Send(mail);


                using (SmtpClient client = new SmtpClient())
                using (MailMessage message = new MailMessage())
                {
                    message.To.Add(mailTo); // BREAKPOINT will be here
                    message.IsBodyHtml = true;
                    message.Subject = subject;
                    message.Body = body;
                    

                    try
                    {
                        // send the email
                        client.Send(message);
                    }
                    catch (SmtpException ex)
                    {
                        // log exception
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendForgotPasswordEmail(string mailTo, string mailFrom, string subject, string body)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.From = new MailAddress(mailFrom);
                mail.Subject = subject;
                mail.To.Add(mailTo);
                string addressFrom = System.Configuration.ConfigurationManager.AppSettings["generalEmailFromAddress"];
                string password = System.Configuration.ConfigurationManager.AppSettings["emailPassword"];
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new System.Net.NetworkCredential("rammohan.test1@gmail.com", "cattesting");// Enter seders User name and password
                smtp.Credentials = new System.Net.NetworkCredential(addressFrom, password);// Enter seders User name and password
                smtp.EnableSsl = true;
                smtp.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static bool SendEmailWithCC(string mailTo, string mailFrom, string subject, string body,string ccmail)
        {
            try
            {
                //MailMessage mail = new MailMessage();
                //mail.Body = body;
                //mail.IsBodyHtml = true;
                //mail.From = new MailAddress(mailFrom);
                //mail.Subject = subject;
                //mail.To.Add(mailTo);
                //string addressFrom = System.Configuration.ConfigurationManager.AppSettings["generalEmailFromAddress"];
                //string password = System.Configuration.ConfigurationManager.AppSettings["emailPassword"];
                //SmtpClient smtp = new SmtpClient();
                //smtp.Host = System.Configuration.ConfigurationManager.AppSettings["host"]; // "smtp.gmail.com";
                //smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["port"]); //587;
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new System.Net.NetworkCredential("rammohan.test1@gmail.com", "cattesting");// Enter seders User name and password
                //smtp.Credentials = new System.Net.NetworkCredential(addressFrom, password);// Enter seders User name and password
                //smtp.EnableSsl = true;
                //smtp.Send(mail);


                using (SmtpClient client = new SmtpClient())
                using (MailMessage message = new MailMessage())
                {
                    message.To.Add(mailTo); // BREAKPOINT will be here
                    message.IsBodyHtml = true;
                    message.Subject = subject;
                    message.Body = body;
                    if (ccmail !="")
                       message.CC.Add(ccmail);


                    try
                    {
                        // send the email
                        client.Send(message);
                       
                    }
                    catch (SmtpException ex)
                    {
                        // log exception
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
