﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('treatmentProgram', treatmentProgram);

    treatmentProgram.$inject = ['$scope', '$modalInstance', '$http', '$location', '$route', 'resourcesInfo', 'common'];

    function treatmentProgram($scope, $modalInstance, $http, $location, $route, resourcesInfo, common) {
        

        if (resourcesInfo) {
            $scope.title = "Edit Treatment Program";
            $scope.isLocationEnable = false;
        }
        else {
            $scope.title = 'Treatment Program';
            $scope.isLocationEnable = true;
        }

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(treatmentProgram);

        $scope.treatmentProgram = {};

        $scope.programs = [];

       



        activate();

        function activate() {

            var lid = '';
            var loginUserLocId = '';
            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));

            // var userInfo = JSON.parse($window.localStorage["userInfo"]);

            loginUserLocId = loginUserInfo.locationId;

            if (loginUserInfo.level == "SIT3U" || loginUserInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }



            $http({
                method: 'POST', url: 'api/TreatmentProgram',
                data: { method: "GetLocationList" }
            })
                .success(function (result) {
                    $scope.treatmentProgram.locationList = result;
                    if (resourcesInfo) {
                        $scope.treatmentProgram.id = resourcesInfo.id;
                        $scope.treatmentProgram.name = resourcesInfo.name;
                        $scope.treatmentProgram.locationId = parseInt(resourcesInfo.locationId);
                        $scope.treatmentProgram.isActive = resourcesInfo.isActive;

                        $scope.treatmentProgram.maleBeds = resourcesInfo.maleBeds;
                        $scope.treatmentProgram.femaleBeds = resourcesInfo.femaleBeds;
                        $scope.treatmentProgram.adolescentMaleBeds = resourcesInfo.adolescentMaleBeds;
                        $scope.treatmentProgram.adolescentFemaleBeds = resourcesInfo.adolescentFemaleBeds;

                        $scope.treatmentProgram.is60days = resourcesInfo.is60days;
                        $scope.treatmentProgram.is180days = resourcesInfo.is180days;



                    }
                    
                })
                .error(function (error) {
                    error(error);
                })

            


        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function (frmtreatprog) {
            
            $scope.isSubmit = true;

            if (frmtreatprog.$valid) {
                //$scope.treatmentProgram.locationId = 7;
                $http({
                    method: 'POST', url: 'api/TreatmentProgram',
                    data: {
                        method: "UpdateTreatmentProgram",
                        parameters: [JSON.stringify($scope.treatmentProgram)]
                    }
                })
                    .success(function (result) {
                        $modalInstance.close(result);
                        log("TreatmentProgram saved successfully");
                        $location.path('/treatmentPrograms');
                        $route.reload();

                    })
                    .error(function (error) {
                        console.log(error);
                        

                    })

              
            }
        };

    }
})();
