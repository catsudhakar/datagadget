﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.DataModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web.SessionState;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataGadget.Web.Reports;
using DataGadget.Web.ViewModels;

namespace DataGadget.Web.Controllers
{
    public class TreatmentSurveyToolController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "AdminStateByRole":
                        return await AdminStateByRole(param.Parameters);

                    case "BindUsersBySerciveId":
                        return await BindUsersBySerciveId(param.Parameters);

                    case "BindSurveyType":
                        return await BindSurveyType(param.Parameters);
                    case "BindSurveyData":
                        return await BindSurveyData(param.Parameters);
                    case "BindSurveyService":
                        return await BindSurveyService(param.Parameters);
                    case "BindProgramData":
                        return await BindProgramData(param.Parameters);
                    case "NewBindProgramData":
                        return await NewBindProgramData(param.Parameters);
                    case "BindDynamicData":
                        return await BindDynamicData();
                    case "CreateSurveryTools":
                        return await CreateSurveryTools(param.Parameters);
                    case "BindAgencies":
                        return await BindAgencies(param.Parameters);

                    case "GetBaseStringDataForReport":
                        return await GetBaseStringDataForReport(param.Parameters);

                    case "BindIntakeData":
                        return await BindIntakeData(param.Parameters);

                    case "BindSurveyItemById":
                        return await BindSurveyItemById(param.Parameters);

                    case "BindSelectedIntakeValues":
                        return await BindSelectedIntakeValues(param.Parameters);
                    case "BindSelectedSurveyClientResponses":
                        return await BindSelectedSurveyClientResponses(param.Parameters);
                    case "EditSurveryTools":
                        return await EditSurveryTools(param.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //WebLogger.Error("Exception In TreatmentSurveyToolController : " + ex.Message); 

                //return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<HttpResponseMessage> AdminStateByRole(string[] parameters)
        {
            try
            {


                Guid newUserId = new Guid(parameters[0]);
                int agencyId = Convert.ToInt32(parameters[1]);
                IEnumerable<NewUser> dataSource = (IEnumerable<NewUser>)new List<NewUser>();
                IEnumerable<NewUser> users = (IEnumerable<NewUser>)new List<NewUser>();
                IEnumerable<UserTreatmentProgram> userTxPrograms = (IEnumerable<UserTreatmentProgram>)new List<UserTreatmentProgram>();
                bool isInTxProgram = (userTxPrograms.Count() > 0);
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    dataSource = await context.TblUsers.Where(user => user.LocationID == agencyId && user.IsActive == true)
                        .OrderBy(surveyAdmin => surveyAdmin.FirstName)
                        .Select(x =>
                            new NewUser
                            {
                                ID = x.UserGUID.ToString(),
                                Name = x.FirstName + " " + x.LastName
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        async private Task<HttpResponseMessage> BindUsersBySerciveId(string[] parameters)
        {
            try
            {


                int serId = Convert.ToInt32(parameters[0]);
                int agencyId = Convert.ToInt32(parameters[1]);
                IEnumerable<NewUser> dataSource = (IEnumerable<NewUser>)new List<NewUser>();
                IEnumerable<NewUser> users = (IEnumerable<NewUser>)new List<NewUser>();
                IEnumerable<UserTreatmentProgram> userTxPrograms = (IEnumerable<UserTreatmentProgram>)new List<UserTreatmentProgram>();
                List<NewUser> lstusers = new List<NewUser>();
                bool isInTxProgram = (userTxPrograms.Count() > 0);
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    //dataSource = await context.TblUsers.Where(user => user.LocationID == agencyId && user.IsActive == true)
                    //    .OrderBy(surveyAdmin => surveyAdmin.FirstName)
                    //    .Select(x =>
                    //        new NewUser
                    //        {
                    //            ID = x.UserGUID.ToString(),
                    //            Name = x.FirstName + " " + x.LastName
                    //        }).ToListAsync();

                    var dbusers = await context.UserIOPs.Where(user => user.IopId == serId).ToListAsync();


                    foreach (UserIOP item in dbusers)
                    {
                        var d = context.TblUsers.Where(user => user.UserGUID == item.UserId && user.LocationID == agencyId).SingleOrDefault();
                        if (d != null)
                        {
                            //if(d.LocationID==agencyId)
                            //{
                            NewUser u = new NewUser();
                            u.Name = d.FirstName + " " + d.LastName;
                            u.ID = item.UserId.ToString();
                            lstusers.Add(u);
                            //}
                        }
                    }

                    dataSource = lstusers;

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        async private Task<HttpResponseMessage> BindSurveyType(string[] p)
        {
            try
            {

                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }
                IEnumerable<NewSurveyType> dataSource = (IEnumerable<NewSurveyType>)new List<NewSurveyType>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    dataSource = await context.SurveyCategoryTypes.Where(x => x.SurveyCategoryID == 2)//SurveyCategoryID == 2 for treatment survey
                                    .Select(x => new NewSurveyType
                                    {
                                        ID = x.SurveyTypeID.ToString(),
                                        Name = x.DisplayName
                                    }).ToListAsync();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindIntakeData(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }
                IEnumerable<IntakeTypeVlaues> data = (IEnumerable<IntakeTypeVlaues>)new List<IntakeTypeVlaues>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {

                    data = await context.IntakeTypes.Where(x => x.IsActive == true).
                        Select(x =>
                            new IntakeTypeVlaues
                            {
                                Id = x.Id,
                                TypeName = x.TypeName,
                                IntakeTypeName = x.IntakeTypeName,
                                IsActive = x.IsActive,
                                IsSelect = false,
                                IsDisable = false
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindSurveyData(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }
                IEnumerable<NewSurvey> data = (IEnumerable<NewSurvey>)new List<NewSurvey>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.Surveys.Where(x => x.SurveyCategoryID == 2).
                        Select(x =>
                            new NewSurvey
                            {
                                RecNo = x.RecNo,
                                SurveyTitle = x.SurveyTitle
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindSurveyService_old(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }
                IEnumerable<NewSurveyService> data = (IEnumerable<NewSurveyService>)new List<NewSurveyService>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.SurveyServices.
                        Select(x =>
                            new NewSurveyService
                            {
                                RecNo = x.RecNo,
                                Description = x.Description
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindSurveyService(string[] p)
        {
            try
            {
                DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                int agencyId = Convert.ToInt32(p[1]);

                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }
                IEnumerable<NewSurveyService> data = (IEnumerable<NewSurveyService>)new List<NewSurveyService>();
                //using (datagadget_testEntities context = new datagadget_testEntities())
                //{
                //    data = await context.LocationIOPs.Where(x => x.LocationId == agencyId).
                //        Select(x =>
                //            new NewSurveyService
                //            {
                //                //RecNo = x.RecNo,
                //                //Description = x.Description
                //                RecNo = x.IopId,
                //                Description = x.SurveyService.Description
                //            }).ToListAsync();
                //}

                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.UserIOPs.Where(x => x.UserId == newUserId.UserId).
                        Select(x =>
                            new NewSurveyService
                            {
                                //RecNo = x.RecNo,
                                //Description = x.Description
                                RecNo = x.IopId,
                                Description = x.SurveyService.Description
                            }).ToListAsync();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindProgramData(string[] parameters)
        {
            try
            {
                Guid surveyAdminID = new Guid(parameters[0]);
                IEnumerable<NewProgram> dataSource = (IEnumerable<NewProgram>)new List<NewProgram>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    dataSource = await context.UserTreatmentPrograms.Where(x => x.UserID == surveyAdminID).
                        Select(x => new NewProgram { RecNo = x.TreatmentProgram.RecNo, Name = x.TreatmentProgram.ProgramName }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> NewBindProgramData(string[] parameters)
        {
            try
            {
                Guid surveyAdminID = new Guid(parameters[0]);
                int agencyId = Convert.ToInt32(parameters[1]);
                IEnumerable<NewProgram> dataSource = (IEnumerable<NewProgram>)new List<NewProgram>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    //dataSource = await context.UserTreatmentPrograms.Where(x => x.UserID == surveyAdminID).
                    //    Select(x => new NewProgram { RecNo = x.TreatmentProgram.RecNo, Name = x.TreatmentProgram.ProgramName }).ToListAsync();

                    dataSource = await (from UTP in context.UserTreatmentPrograms
                                        join TP in context.TreatmentPrograms on UTP.TreatmentProgramID equals TP.RecNo
                                        join L in context.tblLocations on TP.LocationID equals L.PKey
                                        join U in context.TblUsers on UTP.UserID equals U.UserGUID
                                        where UTP.UserID == surveyAdminID && TP.IsActive == true && L.PKey == agencyId
                                        select new NewProgram
                                        {
                                            RecNo = TP.RecNo,
                                            Name = TP.ProgramName
                                        }).ToListAsync();


                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindProgramData1(string[] parameters)
        {
            try
            {
                Guid surveyAdminID = new Guid(parameters[0]);
                //IEnumerable<NewProgram> dataSource = (IEnumerable<NewProgram>)new List<NewProgram>();
                List<NewProgram> dataSource = new List<NewProgram>();
                IEnumerable<TreatmentProgram> dataSource1 = (IEnumerable<TreatmentProgram>)new List<TreatmentProgram>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    //    dataSource = await context.UserTreatmentPrograms.Where(x => x.UserID == surveyAdminID).
                    //        Select(x => new NewProgram { RecNo = x.TreatmentProgram.RecNo, Name = x.TreatmentProgram.ProgramName }).ToListAsync();



                    dataSource1 = from TP in context.TreatmentPrograms
                                  join UTP in context.UserTreatmentPrograms on TP.RecNo equals UTP.TreatmentProgramID
                                  where UTP.UserID == surveyAdminID && TP.IsActive == true
                                  select TP;
                    foreach (TreatmentProgram tp in dataSource1)
                    {
                        NewProgram p = new NewProgram();
                        p.RecNo = tp.RecNo;
                        p.Name = tp.ProgramName;
                        dataSource.Add(p);

                    }

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindDynamicData()
        {
            try
            {
                string data = "";
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    int questionNumber = 1;
                    var dataResult = await context.SurveyGroupQuestions.Where(x => x.SurveyID == 1).OrderBy(x => x.GroupSeqNum).ToListAsync();
                    foreach (var item in dataResult)
                    {
                        switch (item.ASPControlType)
                        {
                            case "DropDownList":
                                data += "<div><p><label>" + questionNumber + ". " + item.SurveyQTN + "</label></p>";
                                var quesionResponseDrp = await context.Database.SqlQuery<SurveyQTNResponse>("select * from  SurveyQTNResponse where RecNo in (select SurveyQTNResponseID from SurveyResponseItems where SurveyGroupQTNID=" + item.RecNo + ");").ToListAsync();
                                data += "<select ng-required='true' ng-model='vm.surveytype' id='" + item.RecNo + "'>";
                                foreach (var contrl in quesionResponseDrp)
                                {
                                    data += "<option value='" + contrl.RecNo + "'>" + contrl.SurveyResponse + "</option>";
                                }
                                data += "</select></div>";
                                break;

                            case "RadioButtonList":
                                data += "<div><p><label>" + questionNumber + ". " + item.SurveyQTN + "</label></p>";
                                var quesionResponse = await context.Database.SqlQuery<SurveyQTNResponse>("select * from  SurveyQTNResponse where RecNo in (select SurveyQTNResponseID from SurveyResponseItems where SurveyGroupQTNID=" + item.RecNo + ");").ToListAsync();
                                foreach (var contrl in quesionResponse)
                                {
                                    string otherResponse = string.Empty;
                                    string itemValue = contrl.RecNo + "$" + item.RecNo;
                                    if (contrl.IsOtherResponse)
                                        otherResponse = "<input ng-model='vm" + questionNumber + "' class='form-control ng-pristine ng-invalid ng-invalid-required' id='txtOtherResponse" + questionNumber + "' name='txtOtherResponse" + questionNumber + "'   type='text' placeholder='Please Specify' style='width:200px;'>";
                                    data += "<p><input type='radio' onchange='pushValue(&#39;" + itemValue + "&#39;)' name='rd_questionresonse" + item.RecNo + "' ng-model='vm.rd" + contrl.RecNo + "' value='" + itemValue + "' />  " + contrl.SurveyResponse + " " + otherResponse + "</p>";
                                }
                                data += "</div>";
                                break;

                            case "CheckBoxList":
                                data += "<div><p><label>" + questionNumber + ". " + item.SurveyQTN + "</label></p>";
                                var quesionResponseCheck = await context.Database.SqlQuery<SurveyQTNResponse>("select * from  SurveyQTNResponse where RecNo in (select SurveyQTNResponseID from SurveyResponseItems where SurveyGroupQTNID=" + item.RecNo + ");").ToListAsync();
                                foreach (var contrl in quesionResponseCheck)
                                {
                                    string otherResponse = string.Empty;
                                    if (contrl.IsOtherResponse)
                                        otherResponse = "<input ng-model='vm" + questionNumber + "' class='form-control ng-pristine ng-invalid ng-invalid-required' id='txtOtherResponse" + questionNumber + "' name='txtOtherResponse" + questionNumber + "'   type='text' placeholder='Please Specify' style='width:200px;'>";
                                    data += "<p><input type='checkbox' name='ch_questionresonse" + item.RecNo + "' ng-model='vm.chk" + contrl.RecNo + "' value='" + contrl.RecNo + "' />  " + contrl.SurveyResponse + " " + otherResponse + "</p>";
                                }
                                data += "</div>";
                                break;

                            case "MultiLineTextBox":
                                data += "<div><p><label>" + questionNumber + ". " + item.SurveyQTN + "</label></p>";
                                var quesionResponseText = await context.Database.SqlQuery<SurveyQTNResponse>("select * from  SurveyQTNResponse where RecNo in (select SurveyQTNResponseID from SurveyResponseItems where SurveyGroupQTNID=" + item.RecNo + ");").ToListAsync();
                                foreach (var contrl in quesionResponseText)
                                {
                                    data += "<p><textarea rows='5' cols='60' name='txt_questionresonse" + item.RecNo + "' ng-model='vm.txt" + contrl.RecNo + "'></textarea>  " + contrl.SurveyResponse + "</p>";
                                }
                                data += "</div>";
                                break;

                        }
                        questionNumber++;
                    }

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private List<TreatmentSurveyIntakeValue> GetSelectedIntakevalues(List<IntakeTypeVlaues> intakevalues, int surveyId)
        {
            List<TreatmentSurveyIntakeValue> selectedValues = new List<TreatmentSurveyIntakeValue>();
            foreach (IntakeTypeVlaues v in intakevalues)
            {
                if (v.IsSelect)
                {
                    TreatmentSurveyIntakeValue tsintakevalue = new TreatmentSurveyIntakeValue();
                    tsintakevalue.SurveyItemId = surveyId;
                    tsintakevalue.IntakeValueId = v.Id;
                    selectedValues.Add(tsintakevalue);
                }
            }
            return selectedValues;
        }

        async private Task<HttpResponseMessage> CreateSurveryTools(string[] parameters)
        {
            SerializeJsonData resultModel = new SerializeJsonData();
            List<IntakeTypeVlaues> lstValues = new List<IntakeTypeVlaues>();
            List<TreatmentSurveyIntakeValue> selectedValies = new List<TreatmentSurveyIntakeValue>();
            try
            {
                var dataResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(parameters[0]);
                lstValues = JsonConvert.DeserializeObject<List<IntakeTypeVlaues>>(parameters[2]);

                var dynamicData = dataResult.Where(x => x.Key.Contains("question_")).ToList();
                SerializeJsonData model = new SerializeJsonData();

                string from = parameters[1];

                model.Administered = dataResult.FirstOrDefault(x => x.Key == "Administered").Value;
                model.SurveryCompletedDate = dataResult.FirstOrDefault(x => x.Key == "SurveryCompletedDate").Value;
                model.Survey = dataResult.FirstOrDefault(x => x.Key == "Survey").Value;
                model.SurveyType = dataResult.FirstOrDefault(x => x.Key == "SurveyType").Value;
                model.ServiceProvide = dataResult.FirstOrDefault(x => x.Key == "ServiceProvide").Value;
                model.ProgramName = dataResult.FirstOrDefault(x => x.Key == "ProgramName").Value;
                model.CaseNumber = dataResult.FirstOrDefault(x => x.Key == "CaseNumber").Value;
                model.ClientEntryDate = dataResult.FirstOrDefault(x => x.Key == "ClientEntryDate").Value;
                model.ClientDischargeDate = dataResult.FirstOrDefault(x => x.Key == "ClientDischargeDate").Value;

                model.ClientGenderType = dataResult.FirstOrDefault(x => x.Key == "ClientGenderType").Value;


                model.Comments = dataResult.FirstOrDefault(x => x.Key == "Comments").Value;
                model.UserID = dataResult.FirstOrDefault(x => x.Key == "UserID").Value;
                model.SessionID = dataResult.FirstOrDefault(x => x.Key == "SessionID").Value;
                model.ClientSessionStart = dataResult.FirstOrDefault(x => x.Key == "ClientSessionStart").Value;
                model.Agency = dataResult.FirstOrDefault(x => x.Key == "Agency").Value;
                //model.IsIndigent = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsIndigent").Value);
                //model.IsWithdrawal = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsWithdrawal").Value);
                //model.IsPregnant = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsPregnant").Value);
                //model.IsIVDrugUser = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsIVDrugUser").Value);
                //model.IsPregnantParenting = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsPregnantParenting").Value);

                //model.IsOpioid = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsOpioid").Value);
                //model.IsCooccurring = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsCooccurring").Value);

                //model.IsAlcohol = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsAlcohol").Value);
                //model.IsMethamphetamine = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsMethamphetamine").Value);
                //model.IsBenzodiazapines = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsBenzodiazapines").Value);
                //model.IsMarijuana = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsMarijuana").Value);
                //model.IsOtherSubstances = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsOtherSubstances").Value);
                //model.IsSelfPay = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsSelfPay").Value);
                //model.IsPrivateInsurance = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsPrivateInsurance").Value);
                //model.IsMedicaid = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsMedicaid").Value);

                int locId = Convert.ToInt32(model.Agency);
                Guid uID = new Guid(model.UserID);


                var otherResponses = dataResult.Where(x => x.Key.Contains("qestion_txtresponse_")).ToList();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    #region comm
                    SurveySession surveySession = GetSurveySession(model, context);
                    if (surveySession.RecNo == 0) { context.SurveySessions.Add(surveySession); await context.SaveChangesAsync(); }
                    //Client client = GetClient(model.CaseNumber, context);
                    Client client = GetClient(model.CaseNumber, Convert.ToInt32(model.ProgramName), model.ClientGenderType, context);
                    if (client.ClientStatusId == 1)
                        resultModel.BedStatus = "In Waiting";
                    else if (client.ClientStatusId == 3)
                        resultModel.BedStatus = "Allocated";
                    else
                        resultModel.BedStatus = "In Waiting";

                    if (client.RecNo == 0) { context.Clients.Add(client); await context.SaveChangesAsync(); }
                    SurveyClient surveyClient = GetSurveyClient(client, Convert.ToInt32(model.ProgramName), model.ClientDischargeDate, context);
                    if (surveyClient.RecNo == 0) { context.SurveyClients.Add(surveyClient); await context.SaveChangesAsync(); }
                    SurveyItem surveyItem = GetSurveyItem(surveyClient, model);


                    if (surveyItem.RecNo == 0) { context.SurveyItems.Add(surveyItem); await context.SaveChangesAsync(); }

                    //saving data in new table getintakevalues selected intake valies
                    selectedValies = GetSelectedIntakevalues(lstValues, surveyItem.RecNo);
                    context.TreatmentSurveyIntakeValues.AddRange(selectedValies);
                    await context.SaveChangesAsync();

                    var questionsList = await context.SurveyGroupQuestions.Where(x => x.SurveyID == 1).OrderBy(x => x.GroupSeqNum).ToListAsync();

                    foreach (var qesans in dynamicData)
                    {
                        int num1;
                        SurveyClientResponse clientResponse = new SurveyClientResponse();
                        clientResponse.SurveyItemID = surveyItem.RecNo;
                        int QuestionID = Convert.ToInt32(qesans.Key.Split('_')[1].ToString());


                        int surveyResponseItemId;
                        bool res = int.TryParse(qesans.Value, out num1);
                        if (res)
                        {
                            surveyResponseItemId = Convert.ToInt32(qesans.Value);
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).SingleOrDefault();
                        }
                        else
                        {
                            DateTime date;
                            if (DateTime.TryParse(qesans.Value, out date))
                            {

                                clientResponse.OtherResponse = date.ToShortDateString();
                            }
                            else
                            {
                                clientResponse.OtherResponse = qesans.Value;
                            }
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.SurveyGroupQTNID == QuestionID).SingleOrDefault();

                        }

                        // bool res = int.TryParse(qesans.Value.ToString(), out num1);
                        //var answer = await context.SurveyQTNResponses.FirstOrDefaultAsync(x=>x.RecNo == num1);
                        // var quest =  questionsList.FirstOrDefault(x => x.RecNo == QuestionID);
                        // resultModel.QuestAnsPair.Add(quest == null ? "":quest.SurveyQTN,answer.SurveyResponse);
                        // int qvalue = context.SurveyResponses.FirstOrDefault(x => x.SurveyGroupQTNID == QuestionID).SurveyResponseItemID;
                        // if (res == false)
                        // {
                        //     clientResponse.OtherResponse = qesans.Value.ToString();
                        //     clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == qvalue).Single();
                        // }
                        // else
                        // {
                        //     clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == qvalue).Single();
                        // }

                        //clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).Single();

                        if (otherResponses.Any(x => x.Key.Contains("qestion_txtresponse_" + QuestionID.ToString())))
                            clientResponse.OtherResponse = dataResult.FirstOrDefault(x => x.Key.Contains("qestion_txtresponse_" + QuestionID.ToString())).Value;
                        context.SurveyClientResponses.Add(clientResponse);
                    }

                    var followup = context.FollowUpContacts.FirstOrDefault(x => x.PatientId == model.CaseNumber);
                    if (followup != null)
                    {
                        followup.HasSurveyDone = 1;

                    }
                    else if (from == "FollowUp")
                    {

                        context.FollowUpContacts.Add(new FollowUpContact
                        {
                            PatientId = model.CaseNumber,
                            FollowupDate = DateTime.Now,
                            HasSurveyDone = 1,
                            FollowupComment = "survey has been done",
                            FollowupId = 1
                        });

                    }
                    resultModel.Administered = model.Administered;
                    resultModel.Agency = context.tblLocations.Where(x => x.PKey == locId).Select(x => x.FriendlyName_loc).SingleOrDefault();
                    resultModel.UserID = context.TblUsers.Where(x => x.UserGUID == uID).Select(x => x.FirstName).SingleOrDefault();
                    resultModel.SurveryCompletedDate = model.SurveryCompletedDate;
                    resultModel.SurveyType = model.SurveyType;
                    resultModel.ServiceProvide = model.ServiceProvide;
                    resultModel.CaseNumber = model.CaseNumber;
                    resultModel.ProgramName = model.ProgramName;
                    resultModel.ClientGenderType = model.ClientGenderType;
                    resultModel.ClientEntryDate = model.ClientEntryDate;
                    resultModel.ClientDischargeDate = model.ClientDischargeDate;
                    resultModel.Comments = model.Comments;
                    resultModel.SurveyID = surveyItem.RecNo.ToString();
                    //resultModel.IsIndigent = model.IsIndigent;
                    //resultModel.IsIVDrugUser = model.IsIVDrugUser;
                    //resultModel.IsPregnant = model.IsPregnant;
                    //resultModel.IsWithdrawal = model.IsWithdrawal;
                    //resultModel.IsPregnantParenting = model.IsPregnantParenting;
                    //resultModel.IsOpioid = model.IsOpioid;
                    //resultModel.IsCooccurring = model.IsCooccurring;

                    //resultModel.IsAlcohol = model.IsAlcohol;
                    //resultModel.IsMethamphetamine = model.IsMethamphetamine;
                    //resultModel.IsBenzodiazapines = model.IsBenzodiazapines;
                    //resultModel.IsMarijuana = model.IsMarijuana;
                    //resultModel.IsOtherSubstances = model.IsOtherSubstances;
                    //resultModel.IsSelfPay = model.IsSelfPay;
                    //resultModel.IsPrivateInsurance = model.IsPrivateInsurance;
                    //resultModel.IsMedicaid = model.IsMedicaid;

                    resultModel.lstIntakevalues = lstValues;

                    await context.SaveChangesAsync();
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, resultModel);
        }
        public SurveyItem GetSurveyItem(SurveyClient surveyClient, SerializeJsonData model)
        {
            SurveyItem surveyItem = new SurveyItem();
            surveyItem.SurveyAdminID = new Guid(model.Administered);
            surveyItem.LocationID = Convert.ToInt32(model.Agency);
            surveyItem.AdminDate = Convert.ToDateTime(model.SurveryCompletedDate);
            surveyItem.SessionID = model.SessionID;
            surveyItem.SurveyID = Convert.ToInt32(model.Survey);
            surveyItem.SurveyClientID = surveyClient.RecNo;
            surveyItem.ServiceID = Convert.ToInt32(model.ServiceProvide);
            surveyItem.EntryDate = Convert.ToDateTime(model.ClientEntryDate);

            if (model.ClientDischargeDate != null)
                surveyItem.DischargeDate = Convert.ToDateTime(model.ClientDischargeDate);
            else
                surveyItem.DischargeDate = null;

            //surveyItem.ProgramID = null;
            surveyItem.ProgramID = Convert.ToInt32(model.ProgramName);

            surveyItem.SurveyTypeID = Convert.ToInt32(model.SurveyType);
            if (surveyItem.SurveyTypeID == 2)
            {
                surveyItem.DischargeId = 2;
            }
            surveyItem.CreateDate = DateTime.Now;
            surveyItem.Notes = model.Comments;

            surveyItem.ClientGenderType = model.ClientGenderType;
            //surveyItem.IsIndigent = model.IsIndigent;
            //surveyItem.IsIVDrugUser = model.IsIVDrugUser;
            //surveyItem.IsPregnant = model.IsPregnant;
            //surveyItem.IsWithdrawal = model.IsWithdrawal;
            //surveyItem.IsPregnantParenting = model.IsPregnantParenting;
            //surveyItem.IsOpioid = model.IsOpioid;
            //surveyItem.IsCooccurring = model.IsCooccurring;

            //surveyItem.IsAlcohol = model.IsAlcohol;
            //surveyItem.IsMethamphetamine = model.IsMethamphetamine;
            //surveyItem.IsBenzodiazapines = model.IsBenzodiazapines;
            //surveyItem.IsMarijuana = model.IsMarijuana;
            //surveyItem.IsOtherSubstances = model.IsOtherSubstances;
            //surveyItem.IsSelfPay = model.IsSelfPay;
            //surveyItem.IsPrivateInsurance = model.IsPrivateInsurance;
            //surveyItem.IsMedicaid = model.IsMedicaid;

            return surveyItem;
        }
        protected SurveyClient GetSurveyClient(Client client, int programID, string dischargeDate, datagadget_testEntities context)
        {
            SurveyClient surveyClient = new SurveyClient();
            if (client.RecNo != 0)
            {
                {
                    surveyClient = context.SurveyClients.Where(s => s.ClientID == client.RecNo).SingleOrDefault();
                    if (surveyClient != null)
                        return surveyClient;
                }
            }
            surveyClient = new SurveyClient();
            surveyClient.Client = client;
            surveyClient.ClientType = 2;//treatment survey
            surveyClient.TreatmentProgramID = programID;
            surveyClient.TreatmentStatus = dischargeDate == "" ? 0 : 1; //0 = active patient, 1 = discharged
            return surveyClient;
        }
        protected Client GetClientback(string pAT_ID, datagadget_testEntities context)
        {
            Client client = new Client();
            {
                client = context.Clients.Where(Client => Client.PAT_ID == pAT_ID).SingleOrDefault();

                if (client == null)
                {
                    client = new Client();
                    client.PAT_ID = pAT_ID;
                }
            }
            return client;
        }

        protected Client GetClient(string pAT_ID, int TreatmentProgId, string GenderType, datagadget_testEntities context)
        {
            Client client = new Client();
            BedsCount bedCout = new BedsCount();

            try
            {
                bedCout = context.GetAvailabilityCount(TreatmentProgId, GenderType).Select(x => new BedsCount
                {
                    AllocatedBeds = x.Allocated_Beds,
                    TotalBeds = x.TotalBeds,
                    WaitingClients = x.WaitingClients

                }).SingleOrDefault(); ;

                client = context.Clients.Where(Client => Client.PAT_ID == pAT_ID).SingleOrDefault();

                if (client == null)
                {
                    client = new Client();
                    if (bedCout.WaitingClients == 0)
                    {
                        if (bedCout.AllocatedBeds <= bedCout.TotalBeds)
                        {
                            client.ClientStatusId = 3;//InWaiting-1,Rejected -2,Allocated-3,Discharged-4
                        }
                        else
                        {
                            client.ClientStatusId = 1;
                        }

                    }
                    else
                    {
                        client.ClientStatusId = 1;
                    }


                    client.PAT_ID = pAT_ID;
                }
                else
                {
                    Random rand = new Random(DateTime.Now.Millisecond);
                    client = new Client();
                    client.PAT_ID = pAT_ID + "@DG" + rand.Next();
                    if (bedCout.WaitingClients == 0)
                    {
                        if (bedCout.AllocatedBeds <= bedCout.TotalBeds)
                        {
                            client.ClientStatusId = 3;//InWaiting-1,Rejected -2,Allocated-3,Discharged-4
                        }
                        else
                        {
                            client.ClientStatusId = 1;
                        }

                    }
                    else
                    {
                        client.ClientStatusId = 1;
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return client;
        }
        public SurveySession GetSurveySession(SerializeJsonData model, datagadget_testEntities context)
        {
            SurveySession surveySession = new SurveySession();
            {
                surveySession = context.SurveySessions.Where(s => s.SessionID == model.SessionID).SingleOrDefault();
                if (surveySession == null)
                {
                    surveySession = new SurveySession();
                    surveySession.UserID = new Guid(model.UserID);
                    surveySession.SessionStart = Convert.ToDateTime(model.ClientSessionStart);
                    surveySession.SessionEnd = DateTime.Now;
                    surveySession.SessionID = model.SessionID;
                }
            }
            return surveySession;
        }
        async private Task<HttpResponseMessage> BindAgencies(string[] p)
        {
            try
            {

                IEnumerable<AgencyType> dataSource = (IEnumerable<AgencyType>)new List<AgencyType>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    dataSource = await context.tblLocations.Where(x => x.IsActive == true).OrderBy(x => x.FriendlyName_loc).
                        Select(x =>
                            new AgencyType
                            {
                                ID = x.PKey.ToString(),
                                Name = x.FriendlyName_loc
                            }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        private async Task<HttpResponseMessage> GetBaseStringDataForReport(string[] p)
        {

            ClientQuestionsResponse clq;
            List<ClientQuestionsResponse> lstClq = new List<ClientQuestionsResponse>();

            try
            {
                //SerializeJsonData resultModel = new SerializeJsonData();
                SerializeJsonData resultModel = Newtonsoft.Json.JsonConvert.DeserializeObject<SerializeJsonData>(p[0]);


                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    //TODO:
                    //administered by
                    //survey type
                    //service provided
                    //program name


                    var user = (await context.TblUsers.FirstAsync(x => x.UserGUID.ToString() == resultModel.Administered));
                    resultModel.Administered = string.Format("{0} {1}", user.FirstName, user.LastName);
                    resultModel.SurveyType = (await context.SurveyCategoryTypes.FirstAsync(x => x.SurveyTypeID.ToString() == resultModel.SurveyType)).DisplayName;
                    var spId = Convert.ToInt32(resultModel.ServiceProvide);
                    resultModel.ServiceProvide = (await context.SurveyServices.FirstAsync(x => x.RecNo == spId)).Description;
                    var pId = Convert.ToInt32(resultModel.ProgramName);
                    //resultModel.ProgramName = (await context.UserTreatmentPrograms.FirstAsync(x => x.TreatmentProgramID == pId))..ProgramName;

                    // resultModel.ProgramName = (context.TreatmentPrograms.SingleOrDefault(x => x.RecNo == pId)).ProgramName;

                    var db1 = context.TreatmentPrograms.Where(x => x.RecNo == pId).SingleOrDefault();
                    if (db1 != null)
                    {
                        resultModel.ProgramName = db1.ProgramName;
                    }
                    else
                    {
                        resultModel.ProgramName = "";
                    }
                }

                string base64String = string.Empty;
                //int eventId = Convert.ToInt32(p[0]);
                System.IO.MemoryStream memStream = new System.IO.MemoryStream();
                Report_CurriculumSummary report = new Report_CurriculumSummary();

                report.GenerateTreatmentMemoryStream(memStream, resultModel, lstClq);
                Byte[] byteArray = memStream.ToArray();
                // base64String = "data:application/pdf;base64;";
                base64String = System.Convert.ToBase64String(byteArray, 0, byteArray.Length);

                memStream.Close();





                return this.Request.CreateResponse(HttpStatusCode.OK, base64String);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async private Task<HttpResponseMessage> EditSurveryTools(string[] parameters)
        {
            SerializeJsonData resultModel = new SerializeJsonData();
            List<IntakeTypeVlaues> lstValues = new List<IntakeTypeVlaues>();
            List<TreatmentSurveyIntakeValue> selectedValies = new List<TreatmentSurveyIntakeValue>();
            try
            {
                var dataResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(parameters[0]);
                lstValues = JsonConvert.DeserializeObject<List<IntakeTypeVlaues>>(parameters[2]);

                var dynamicData = dataResult.Where(x => x.Key.Contains("question_")).ToList();
                SerializeJsonData model = new SerializeJsonData();

                string from = parameters[1];

                model.Administered = dataResult.FirstOrDefault(x => x.Key == "Administered").Value;
                model.SurveryCompletedDate = dataResult.FirstOrDefault(x => x.Key == "SurveryCompletedDate").Value;
                model.Survey = dataResult.FirstOrDefault(x => x.Key == "Survey").Value;
                model.SurveyType = dataResult.FirstOrDefault(x => x.Key == "SurveyType").Value;
                model.ServiceProvide = dataResult.FirstOrDefault(x => x.Key == "ServiceProvide").Value;
                model.ProgramName = dataResult.FirstOrDefault(x => x.Key == "ProgramName").Value;
                model.CaseNumber = dataResult.FirstOrDefault(x => x.Key == "CaseNumber").Value;
                model.ClientEntryDate = dataResult.FirstOrDefault(x => x.Key == "ClientEntryDate").Value;
                model.ClientDischargeDate = dataResult.FirstOrDefault(x => x.Key == "ClientDischargeDate").Value;

                model.ClientGenderType = dataResult.FirstOrDefault(x => x.Key == "ClientGenderType").Value;


                model.Comments = dataResult.FirstOrDefault(x => x.Key == "Comments").Value;
                model.UserID = dataResult.FirstOrDefault(x => x.Key == "UserID").Value;
                model.SessionID = dataResult.FirstOrDefault(x => x.Key == "SessionID").Value;
                model.ClientSessionStart = dataResult.FirstOrDefault(x => x.Key == "ClientSessionStart").Value;
                model.Agency = dataResult.FirstOrDefault(x => x.Key == "Agency").Value;
                //model.IsIndigent = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsIndigent").Value);
                //model.IsWithdrawal = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsWithdrawal").Value);
                //model.IsPregnant = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsPregnant").Value);
                //model.IsIVDrugUser = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsIVDrugUser").Value);
                //model.IsPregnantParenting = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsPregnantParenting").Value);

                //model.IsOpioid = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsOpioid").Value);
                //model.IsCooccurring = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsCooccurring").Value);

                //model.IsAlcohol = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsAlcohol").Value);
                //model.IsMethamphetamine = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsMethamphetamine").Value);
                //model.IsBenzodiazapines = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsBenzodiazapines").Value);
                //model.IsMarijuana = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsMarijuana").Value);
                //model.IsOtherSubstances = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsOtherSubstances").Value);
                //model.IsSelfPay = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsSelfPay").Value);
                //model.IsPrivateInsurance = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsPrivateInsurance").Value);
                //model.IsMedicaid = Convert.ToBoolean(dataResult.FirstOrDefault(x => x.Key == "IsMedicaid").Value);

                int locId = Convert.ToInt32(model.Agency);
                Guid uID = new Guid(model.UserID);


                var otherResponses = dataResult.Where(x => x.Key.Contains("qestion_txtresponse_")).ToList();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    #region comm
                    SurveySession surveySession = GetSurveySession(model, context);
                    if (surveySession.RecNo == 0) { context.SurveySessions.Add(surveySession); await context.SaveChangesAsync(); }
                    //Client client = GetClient(model.CaseNumber, context);
                    Client client = GetClient(model.CaseNumber, Convert.ToInt32(model.ProgramName), model.ClientGenderType, context);
                    if (client.RecNo == 0) { context.Clients.Add(client); await context.SaveChangesAsync(); }
                    SurveyClient surveyClient = GetSurveyClient(client, Convert.ToInt32(model.ProgramName), model.ClientDischargeDate, context);
                    if (surveyClient.RecNo == 0) { context.SurveyClients.Add(surveyClient); await context.SaveChangesAsync(); }
                    SurveyItem surveyItem = GetSurveyItem(surveyClient, model);


                    if (surveyItem.RecNo == 0) { context.SurveyItems.Add(surveyItem); await context.SaveChangesAsync(); }

                    //saving data in new table getintakevalues selected intake valies
                    selectedValies = GetSelectedIntakevalues(lstValues, surveyItem.RecNo);
                    context.TreatmentSurveyIntakeValues.AddRange(selectedValies);
                    await context.SaveChangesAsync();

                    var questionsList = await context.SurveyGroupQuestions.Where(x => x.SurveyID == 1).OrderBy(x => x.GroupSeqNum).ToListAsync();

                    foreach (var qesans in dynamicData)
                    {
                        int num1;
                        SurveyClientResponse clientResponse = new SurveyClientResponse();
                        clientResponse.SurveyItemID = surveyItem.RecNo;
                        int QuestionID = Convert.ToInt32(qesans.Key.Split('_')[1].ToString());


                        int surveyResponseItemId;
                        bool res = int.TryParse(qesans.Value, out num1);
                        if (res)
                        {
                            surveyResponseItemId = Convert.ToInt32(qesans.Value);
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).SingleOrDefault();
                        }
                        else
                        {
                            DateTime date;
                            if (DateTime.TryParse(qesans.Value, out date))
                            {

                                clientResponse.OtherResponse = date.ToShortDateString();
                            }
                            else
                            {
                                clientResponse.OtherResponse = qesans.Value;
                            }
                            clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.SurveyGroupQTNID == QuestionID).SingleOrDefault();

                        }

                        // bool res = int.TryParse(qesans.Value.ToString(), out num1);
                        //var answer = await context.SurveyQTNResponses.FirstOrDefaultAsync(x=>x.RecNo == num1);
                        // var quest =  questionsList.FirstOrDefault(x => x.RecNo == QuestionID);
                        // resultModel.QuestAnsPair.Add(quest == null ? "":quest.SurveyQTN,answer.SurveyResponse);
                        // int qvalue = context.SurveyResponses.FirstOrDefault(x => x.SurveyGroupQTNID == QuestionID).SurveyResponseItemID;
                        // if (res == false)
                        // {
                        //     clientResponse.OtherResponse = qesans.Value.ToString();
                        //     clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == qvalue).Single();
                        // }
                        // else
                        // {
                        //     clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == qvalue).Single();
                        // }

                        //clientResponse.SurveyResponseItem = context.SurveyResponseItems.Where(item => item.RecNo == surveyResponseItemId).Single();

                        if (otherResponses.Any(x => x.Key.Contains("qestion_txtresponse_" + QuestionID.ToString())))
                            clientResponse.OtherResponse = dataResult.FirstOrDefault(x => x.Key.Contains("qestion_txtresponse_" + QuestionID.ToString())).Value;
                        context.SurveyClientResponses.Add(clientResponse);
                    }

                    var followup = context.FollowUpContacts.FirstOrDefault(x => x.PatientId == model.CaseNumber);
                    if (followup != null)
                    {
                        followup.HasSurveyDone = 1;

                    }
                    else if (from == "FollowUp")
                    {

                        context.FollowUpContacts.Add(new FollowUpContact
                        {
                            PatientId = model.CaseNumber,
                            FollowupDate = DateTime.Now,
                            HasSurveyDone = 1,
                            FollowupComment = "survey has been done",
                            FollowupId = 1
                        });

                    }
                    resultModel.Administered = model.Administered;
                    resultModel.Agency = context.tblLocations.Where(x => x.PKey == locId).Select(x => x.FriendlyName_loc).SingleOrDefault();
                    resultModel.UserID = context.TblUsers.Where(x => x.UserGUID == uID).Select(x => x.FirstName).SingleOrDefault();
                    resultModel.SurveryCompletedDate = model.SurveryCompletedDate;
                    resultModel.SurveyType = model.SurveyType;
                    resultModel.ServiceProvide = model.ServiceProvide;
                    resultModel.CaseNumber = model.CaseNumber;
                    resultModel.ProgramName = model.ProgramName;
                    resultModel.ClientGenderType = model.ClientGenderType;
                    resultModel.ClientEntryDate = model.ClientEntryDate;
                    resultModel.ClientDischargeDate = model.ClientDischargeDate;
                    resultModel.Comments = model.Comments;
                    resultModel.SurveyID = surveyItem.RecNo.ToString();
                    //resultModel.IsIndigent = model.IsIndigent;
                    //resultModel.IsIVDrugUser = model.IsIVDrugUser;
                    //resultModel.IsPregnant = model.IsPregnant;
                    //resultModel.IsWithdrawal = model.IsWithdrawal;
                    //resultModel.IsPregnantParenting = model.IsPregnantParenting;
                    //resultModel.IsOpioid = model.IsOpioid;
                    //resultModel.IsCooccurring = model.IsCooccurring;

                    //resultModel.IsAlcohol = model.IsAlcohol;
                    //resultModel.IsMethamphetamine = model.IsMethamphetamine;
                    //resultModel.IsBenzodiazapines = model.IsBenzodiazapines;
                    //resultModel.IsMarijuana = model.IsMarijuana;
                    //resultModel.IsOtherSubstances = model.IsOtherSubstances;
                    //resultModel.IsSelfPay = model.IsSelfPay;
                    //resultModel.IsPrivateInsurance = model.IsPrivateInsurance;
                    //resultModel.IsMedicaid = model.IsMedicaid;

                    resultModel.lstIntakevalues = lstValues;

                    await context.SaveChangesAsync();
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, resultModel);
        }


        async private Task<HttpResponseMessage> BindSurveyItemById(string[] p)//joj
        {
            try
            {
                //DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                //int agencyId = Convert.ToInt32(p[1]);

                //if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                //{
                //    return null;
                //}

                int id = Convert.ToInt32(p[0]);
                IEnumerable<SurveyItemVM> data = new List<SurveyItemVM>();
                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.SurveyItems.Where(x => x.RecNo == id).
                        Select(x =>
                            new SurveyItemVM
                            {
                                //RecNo = x.RecNo,
                                //Description = x.Description
                                RecNo = x.RecNo,
                                SurveyID = x.SurveyID,
                                SurveyClientID = x.SurveyClientID,
                                EntryDate = x.EntryDate,
                                DischargeDate = x.DischargeDate,
                                SurveyTypeID = x.SurveyTypeID,
                                SurveyAdminID = x.SurveyAdminID,
                                ServiceID = x.ServiceID,
                                LocationID = x.LocationID,
                                SessionID = x.SessionID,
                                ProgramID = x.ProgramID,
                                ClientGenderType = x.ClientGenderType,
                                Notes = x.Notes,
                                AdminDate = x.AdminDate

                            }).ToListAsync();
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        async private Task<HttpResponseMessage> BindSelectedIntakeValues(string[] p)//joj
        {
            try
            {
                //DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                //int agencyId = Convert.ToInt32(p[1]);

                //if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                //{
                //    return null;
                //}

                int id = Convert.ToInt32(p[0]);
                IEnumerable<TreatmentSurveyIntakeValueVM> data = new List<TreatmentSurveyIntakeValueVM>();

                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.TreatmentSurveyIntakeValues.Where(x => x.SurveyItemId == id).
                    Select(x => new TreatmentSurveyIntakeValueVM
                    {
                        SurveyItemId = x.SurveyItemId,
                        IntakeValueId = x.IntakeValueId
                    }).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> BindSelectedSurveyClientResponses(string[] p)//joj
        {
            try
            {
                //DataGadget.Web.Models.User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.Models.User>(p[0].ToString());
                //int agencyId = Convert.ToInt32(p[1]);

                //if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                //{
                //    return null;
                //}

                int id = Convert.ToInt32(p[0]);
                IEnumerable<SurveyClientResponsesVM> data = new List<SurveyClientResponsesVM>();

                using (datagadget_testEntities context = new datagadget_testEntities())
                {
                    data = await context.SurveyClientResponses.Where(x => x.SurveyItemID == id).
                    Select(x => new SurveyClientResponsesVM
                    {
                        RecNo = x.RecNo,
                        SurveyItemID = x.SurveyItemID,
                        SurveyResponseItemID = x.SurveyResponseItemID,
                        OtherResponse = x.OtherResponse,
                    }).OrderBy(m => m.SurveyResponseItemID).ToListAsync();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        //protected int GetLocationID(string[] parameters)
        //{
        //    try
        //    {
        //        IEnumerable<AdminType> dataSource = (IEnumerable<AgencyType>)new List<AdminType>();
        //        using (datagadget_testEntities context = new datagadget_testEntities())
        //        {
        //            string[] roles = Roles.GetRolesForUser();
        //            string role = roles[0];
        //            int locationID = GetLocationID();
        //            string stateAbbrev = DataContext.tblLocations.Where(location => location.PKey == locationID).
        //                Select(location => location.StateAbbrev).Single();
        //            Guid userID = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());

        //            ListItem selectedLocation, selectedSurveyAdmin, selectedTxProgram;
        //            DropDownList ddlLocation = (DropDownList)fvClientSessions.FindControl("ddlLocationTx");
        //            DropDownList ddlSurveyAdmin = (DropDownList)fvClientSessions.FindControl("ddlSurveyAdminTx");
        //            IEnumerable<UserTreatmentProgram> userTxPrograms = DataContext.UserTreatmentPrograms.
        //                Where(userProgram => userProgram.UserID == userID);
        //            bool isInTxProgram = (userTxPrograms.Count() > 0);

        //            selectedLocation = (postBackControl == "Page") ?
        //                SetAgencyStateByRole(role, locationID, stateAbbrev, Page.IsPostBack) : ddlLocation.SelectedItem;

        //            selectedSurveyAdmin = (postBackControl == "Page" || postBackControl == "Agency") ?
        //                SetAdminStateByRole(role, userID, isInTxProgram, stateAbbrev, userTxPrograms, Convert.ToInt32(selectedLocation.Value), Page.IsPostBack) :
        //                ddlSurveyAdmin.SelectedItem;

        //            selectedTxProgram = SetTxProgramStateByRole(role, userID, new Guid(selectedSurveyAdmin.Value),
        //                isInTxProgram, userTxPrograms, Convert.ToInt32(selectedLocation.Value));
        //        }
        //        return this.Request.CreateResponse(HttpStatusCode.OK, dataSource);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


    }


    [KnownTypeAttribute(typeof(NewUser))]
    public class NewUser
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    [KnownTypeAttribute(typeof(NewSurveyType))]
    partial class NewSurveyType
    {
        /// <summary>
        /// name = displayname from SurveyCategoryType table
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ID = surveytypeid from SurveyCategoryType table
        /// </summary>
        public string ID { get; set; }
    }

    [KnownTypeAttribute(typeof(NewSurvey))]
    public class NewSurvey
    {
        public int RecNo { get; set; }
        public string SurveyTitle { get; set; }
    }

    [KnownTypeAttribute(typeof(NewSurveyService))]
    public class NewSurveyService
    {
        public int RecNo { get; set; }
        public string Description { get; set; }
    }

    [KnownTypeAttribute(typeof(NewProgram))]
    public class NewProgram
    {
        public int RecNo { get; set; }
        public string Name { get; set; }
    }

    public partial class SerializeJsonData
    {
        public SerializeJsonData()
        {
            QuestAnsPair = new Dictionary<string, string>();
        }
        public string Agency { get; set; }
        public string UserID { get; set; }
        public string Administered { get; set; }
        public string SurveryCompletedDate { get; set; }
        public string Survey { get; set; }
        public string SurveyType { get; set; }
        public string ServiceProvide { get; set; }
        public string ProgramName { get; set; }
        public string CaseNumber { get; set; }
        public string ClientEntryDate { get; set; }
        public string ClientDischargeDate { get; set; }
        public string Comments { get; set; }
        public string SessionID { get; set; }
        public string ClientSessionStart { get; set; }
        //public bool IsIndigent { get; set; }
        //public bool IsWithdrawal { get; set; }
        //public bool IsPregnant { get; set; }
        //public bool IsIVDrugUser { get; set; }
        //public bool IsPregnantParenting { get; set; }

        //public bool IsOpioid { get; set; }
        //public bool IsCooccurring { get; set; }

        //public bool IsAlcohol { get; set; }
        //public bool IsMethamphetamine { get; set; }
        //public bool IsBenzodiazapines { get; set; }
        //public bool IsMarijuana { get; set; }
        //public bool IsOtherSubstances { get; set; }
        //public bool IsSelfPay { get; set; }
        //public bool IsPrivateInsurance { get; set; }
        //public bool IsMedicaid { get; set; }

        public Dictionary<string, string> QuestAnsPair
        { get; set; }
        public string ClientGenderType { get; set; }

        public List<IntakeTypeVlaues> lstIntakevalues { get; set; }

        public string BedStatus { get; set; }


    }
    [KnownTypeAttribute(typeof(AgencyType))]
    public class AgencyType
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    [KnownTypeAttribute(typeof(AdminType))]
    public class AdminType
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    [KnownTypeAttribute(typeof(ClientQuestionsResponse))]
    public class ClientQuestionsResponse
    {
        public int SurveyItemID { get; set; }
        public string Response { get; set; }
        public string Question { get; set; }
    }

    [KnownTypeAttribute(typeof(IntakeTypeVlaues))]
    public class IntakeTypeVlaues
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public string IntakeTypeName { get; set; }
        public bool IsActive { get; set; }
        public bool IsSelect { get; set; }
        public bool IsDisable { get; set; }
    }


}