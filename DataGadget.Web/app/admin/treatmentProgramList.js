﻿(function () {
    'use strict';
    var controllerId = "treatmentProgramList";
    angular
        .module('app')
        .controller(controllerId, treatmentProgramList);

    treatmentProgramList.$inject = ['$scope', '$http', 'common', '$modal', '$location'];

    function treatmentProgramList($scope, $http, common, $modal, $location) {

        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var warning = getLogFn(controllerId, 'warn');
        var error = getLogFn(controllerId, 'error');
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        activate();

        function activate() {
            paging(1);
        }

        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
          
            $http({ method: 'POST', url: 'api/TreatmentProgram', data: { method: "GetAlltreatmentProgramms", parameters: [pageId] } })
            .success(function (result) {
                $scope.ispaging = false;
                vm.pagerData = result.item1[0];
                if (parseInt(vm.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                vm.pageList = [];
                if (pageId > 3) {
                    vm.pageList.push(pageId - 2);
                    vm.pageList.push(pageId - 1);
                    vm.pageList.push(pageId);
                    if (pageId < vm.pagerData.numberOfPages) {
                        vm.pageList.push(pageId + 1);
                        if (pageId < (vm.pagerData.numberOfPages - 1)) {
                            vm.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);

                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        vm.pageList.push(pageId - 2);
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                vm.programs = userData;

            })
         .error(function (error) {
             console.log(error);
         })
        }

        vm.userSearch = function (search) {
            
            if (search.length >= 3) {
                $scope.ispaging = false;
                $http({ method: 'POST', url: 'api/TreatmentProgram', data: { method: "SearchTreatmentProgramms", parameters: [search] } })
               .success(function (result) {
                    vm.programs = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    activate();
                }
            }
        }


        vm.add = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/treatmentProgram.html',
                controller: 'treatmentProgram',
                resolve: {
                    resourcesInfo: function () {
                        return undefined;
                    }
                }

            });

            addUserModal.result.then(function (prog) {
                vm.programs.push(prog);

            }, function () {
            });
        }



        vm.edit = function (prog) {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/treatmentProgram.html',
                controller: 'treatmentProgram',
                resolve: {
                    resourcesInfo: function () {
                        return prog;
                    }
                }
            });

            addUserModal.result.then(function () {

            }, function () {
            });
        }


        vm.delete = function (prog) {

            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {
                $http({ method: 'POST', url: 'api/TreatmentProgram', data: { method: "DeleteServiceLocation", parameters: [prog.id] } })
            .success(function (result) {

                log("Deactivated successfully");
                activate();
            })
            .error(function (error) {
                error(error);
            })
            }, function () {
            });
        };



        //vm.delete = function (prog) {

        //    var modalInstance = $modal.open({
        //        templateUrl: '/app/common/confirmationDialog.html',
        //        controller: 'confirmationDialogCtrl',
        //        backdrop: 'static',
        //    });
        //    modalInstance.result.then(function () {
        //        $http.delete("api/TreatmentProgram/"+prog.id)//, {id : })//({ method: 'DELETE', url: 'api/TreatmentProgram', data: {id: prog.id } })
        //    .success(function (result) {

        //        log("Deleted successfully");
        //        activate();
        //    })
        //    .error(function (error) {
        //        console.log(error);
        //    })
        //    }, function () {
        //    });
        //};

        

    }
})();
