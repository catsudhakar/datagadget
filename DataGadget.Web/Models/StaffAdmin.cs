﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class StaffAdmin
    {
        public int ID { get; set; }
        public string ProviderName { get; set; }
        public string UserName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TrainingName { get; set; }
        public string Hours { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public List<Provider> ProviderList { get; set; }
        public int? ProviderID { get; set; }

        public string OtherProviderName { get; set; }
        public string TrainerDeatils { get; set; }
        public string EducationCredits { get; set; }
        public string EvidenceBased { get; set; }

        public string InstructorName { get; set; }
        public string LearningObjectives { get; set; }

    }
}