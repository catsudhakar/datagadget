﻿(function () {
    'use strict';
    var controllerId = 'newRespondComment';
    angular
        .module('app')
        .controller(controllerId, newRespondComment);
    //newUser.$inject = ['$scope', '$http', 'common', '$routeParams','staffInfo'];
    function newRespondComment($scope, $http, $modalInstance, respondInfo, $location, $route, common, $window) {
        var userid = localStorage.getItem('userId');

        if (respondInfo) {
            $scope.title = "Comment Reply";
        }
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var getLogFn1 = common.logger.getLogFn;
        var log1 = getLogFn1(controllerId, 'warn');
        $scope.comment = {};
        activate();
        function activate() {
            $scope.timeStamp = Date.now();
            if (respondInfo) {

                $scope.comment.email = respondInfo.toEmail;
                $scope.comment.subject = respondInfo.subject;
                $scope.comment.userID = respondInfo.userID;
                $scope.comment.toUserID = respondInfo.toUserID;
                $scope.comment.parentId = respondInfo.parentId;

            }
        }
        $scope.ok = function (commentForm) {
            commentForm.$submitted = true;
            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (commentForm.$valid) {
                $http({ method: 'POST', url: 'api/Comments', data: { method: "ReplyToComment", parameters: [$scope.comment.email, $scope.comment.Comments, $scope.comment.parentId, userInfo.pkey, $scope.comment.userID, $scope.comment.toUserID, $scope.comment.subject] } })
                   .success(function (result) {

                       $modalInstance.close(result);
                       log("Reply sent successfully");
                   })
                    .error(function (error) {
                        console.log(error);
                    });
            }
        };
        $scope.updateSite = function (site) {
            $scope.sites = $scope.sites || [];
            if (site.isSelected) {
                $scope.sites.push(site.categoryID);
                $scope.sites = _.uniq($scope.sites);
            }
            else {
                $scope.sites = _.without($scope.sites, site.categoryID);
            }
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
})();
