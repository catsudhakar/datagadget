﻿using DataGadget.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class QuestionResponseViewModel
    {
        public QuestionResponseViewModel()
        {
            this.SubQuestion = new SubQuestionResponseViewModel();
        }
        public SurveyGroupQuestion SurveyGroupQuestion { get; set; }
        public List<SurveyRespons> SurveyQTNResponseList { get; set; }
        public string UserSessionID { get; set; }
        public DateTime ClientSessionStart { get; set; }
        public SubQuestionResponseViewModel SubQuestion { get; set; }
    }
}