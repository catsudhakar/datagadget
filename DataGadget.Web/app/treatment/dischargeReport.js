﻿




(function () {
    'use strict';
    var controllerId = 'dischargeReport';
    angular
    .module('app')
    .controller(controllerId, dischargeReport);

    dischargeReport.$inject = ['$scope', '$http', '$location', 'common', '$window'];

    function dischargeReport($scope, $http, $location, common, $window) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error');
        $scope.title = 'dischargeReport';
        var vm = this;
        $scope.vm.editData = {};
        $scope.vm.isList = false;
        $scope.vm.Locations = [];
        $scope.vm.editDataList = [];
        vm.onetime = {};
        vm.events = [];

        $scope.isdisplay = false;
        $scope.ismessage = '';

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }
        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        vm.pagination = function (pageId) {
            var strSort = document.getElementById('hdnSortType').value;
            paging(pageId, strSort);
        }


        function paging(pageId, sortType) {
            $http({ method: 'POST', url: 'api/discharges', data: { method: "DischargesReport", parameters: [localStorage.getItem('userInfo'), pageId, sortType, vm.editData.regionSiteCodes, vm.editData.startingDate, vm.editData.endingDate] } })
         .success(function (result) {
             $scope.ispaging = false;
             vm.pagerData = result.item1[0];
             if (parseInt(vm.pagerData.numberOfPages) > 1) {
                 $scope.ispaging = true;
             }
             vm.pageList = [];
             if (pageId > 3) {
                 vm.pageList.push(pageId - 2);
                 vm.pageList.push(pageId - 1);
                 vm.pageList.push(pageId);
                 if (pageId < vm.pagerData.numberOfPages) {
                     vm.pageList.push(pageId + 1);
                     if (pageId < (vm.pagerData.numberOfPages - 1)) {
                         vm.pageList.push(pageId + 2);
                     }
                 }
             }
             else {
                 if (pageId == 1) {
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                     if (pageId + 4 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 4);
                     }

                 }
                 if (pageId == 2) {
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);

                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                 }
                 if (pageId == 3) {
                     vm.pageList.push(pageId - 2);
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                 }

             }
             if (result.item2.length > 0) {
                 $scope.vm.isList = true;
                 var userData = result.item2;

                 vm.events = userData;
             } else {
                 $scope.vm.isList = true;
                 $scope.emptyMessage = "No records found.";
                 vm.events = [];

             }

         })
        .error(function (error) {
            console.log(error);
        })
        }
        function activate() {

            vm.editData.regionSiteCodes = '0';
            //$http({ method: 'POST', url: 'api/discharges', data: { method: "AllDischarges", parameters: [localStorage.getItem('userInfo'), pageId, sortType] } })

            $http({
                method: 'POST', url: 'api/Reports', data: {
                    method: "DischargeInitializeUserForm", parameters: [localStorage.getItem('userInfo')]
                     
                }
            })
             .success(function (result) {
                 vm.onetime.locationList = result.locationList;
             })
              .error(function (fail) {
                  console.log(fail);
              });
            if (document.getElementById('hdnSortType'))
                document.getElementById('hdnSortType').value = '';
            $scope.isdescending = true;
            // paging(1, '','','','');
        }


        $scope.applyFilter = function (editData) {
            var sdt = vm.editData.startDate;
            var edt = vm.editData.endDate;
            var issubmit = true;
            if (sdt !== undefined && edt !== undefined) {
                var sday = sdt.getDate();
                var smonth = sdt.getMonth();
                var syear = sdt.getFullYear();

                var eday = edt.getDate();
                var emonth = edt.getMonth();
                var eyear = edt.getFullYear();

                //var dateOne = new Date(2010, 11, 25);
                var sdate = new Date(syear, smonth, sday);
                var edate = new Date(eyear, emonth, eday);
                if (sdate > edate) {
                    // $scope.ismessage = 'Please enter end date should be greater then start date'
                    issubmit = false;
                }
            }
            var startingDate = GetFormattedDate(new Date(vm.editData.startDate));
            var endingDate = GetFormattedDate(new Date(vm.editData.endDate));

            if ($scope.editData.$valid && issubmit) {
                $scope.isdisplay = false;
                $scope.ismessage = '';

                vm.editData.startingDate = startingDate;
                vm.editData.endingDate = endingDate;

                //$http({
                //    method: 'POST', url: 'api/Staff',
                //    data: { method: "GetStaffForReport", parameters: [vm.editData.regionSiteCodes, startingDate, endingDate] }
                //})
                //.success(function (result) {

                //    if (result != null) {
                //        $scope.vm.isList = true;
                //        $scope.vm.editDataList = result;
                //    }

                //})
                //.error(function (error) {
                //    console.log(error);
                //})

                paging(1, '');
            }
            else {
                if (sdt === undefined && edt === undefined) {
                    $scope.ismessage = 'Please enter start date and end date'
                }
                else if (sdt === undefined) {
                    $scope.ismessage = 'Please enter start date '
                }
                else if (edt === undefined) {
                    $scope.ismessage = 'Please enter end date '
                }
                else {
                    var sday = sdt.getDate();
                    var smonth = sdt.getMonth();
                    var syear = sdt.getFullYear();

                    var eday = edt.getDate();
                    var emonth = edt.getMonth();
                    var eyear = edt.getFullYear();

                    //var dateOne = new Date(2010, 11, 25);
                    var sdate = new Date(syear, smonth, sday);
                    var edate = new Date(eyear, emonth, eday);
                    if (sdate > edate) {
                        $scope.ismessage = 'End date should be greater than start date'
                    }
                }
                $scope.isdisplay = true;

            }
        };




    }
})();


