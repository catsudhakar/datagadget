﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('treatmentAuthorizeService', treatmentAuthorizeService);

    treatmentAuthorizeService.$inject = ['$http', '$q', '$rootScope', '$location', '$route'];

    function treatmentAuthorizeService($http, $q, $rootScope, $location, $route) {
        var service = {
            getPermission: getData
        };

        return service;

        function getData() {
            var deferred = $q.defer();
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            var isPermitted = _.findWhere(userInfo.selectedLocationCategoryList, { categoryID: 2 });
            if (!isPermitted) {
                $location.path('/dashboard');
                $rootScope.$on('$locationChangeSuccess', function (next, current) {
                    deferred.resolve();
                });

            }
            else {
                deferred.resolve();
            }

            return deferred.promise;
        }
    }
})();