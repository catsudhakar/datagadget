﻿using System;
using System.Collections.Generic;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_DataEntry : Report_Base
    {
        
        public Report_DataEntry()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;


        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }



        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;	
            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Summary Report"));

            if(this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));


            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);



            this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataSet dsData = new DataSet();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_DataEntry ");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");
           

            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;
            //        sqlCmd.CommandTimeout = 300;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dsData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dsData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;

            int cCnt = 0;

            AddGeneralTableRow(table, "Participants Served", dsData.Tables[0].Rows[0]["NumberParticipants"].ToString(), cCnt++);
            AddGeneralTableRow(table, "     " + "Males", dsData.Tables[0].Rows[0]["Males"].ToString(), cCnt++);
            AddGeneralTableRow(table, "     " + "Females", dsData.Tables[0].Rows[0]["Females"].ToString(), cCnt++);

            AddGeneralTableRow(table, " ", " ", cCnt++);
            AddGeneralTableRow(table, "     " + "Age 0-4", dsData.Tables[0].Rows[0]["Age0"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 5-11", dsData.Tables[0].Rows[0]["Age5"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 12-14", dsData.Tables[0].Rows[0]["Age12"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 15-17", dsData.Tables[0].Rows[0]["Age15"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 18-20", dsData.Tables[0].Rows[0]["Age18"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 21-24", dsData.Tables[0].Rows[0]["Age21"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 25-44", dsData.Tables[0].Rows[0]["Age25"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 45-64", dsData.Tables[0].Rows[0]["Age45"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Age 65 & over", dsData.Tables[0].Rows[0]["Age65"].ToString(), cCnt++);

            AddGeneralTableRow(table, " ", " ", cCnt++);
            AddGeneralTableRow(table, "     " + "White", dsData.Tables[0].Rows[0]["RaceWhite"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Black", dsData.Tables[0].Rows[0]["RaceBlack"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Hawaiian", dsData.Tables[0].Rows[0]["RaceHawaii"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Asian", dsData.Tables[0].Rows[0]["RaceAsian"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "American Indian", dsData.Tables[0].Rows[0]["RaceAmIndian"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Unknown/Other Race", dsData.Tables[0].Rows[0]["RaceUnknownOther"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "More Than One Race", dsData.Tables[0].Rows[0]["RaceMoreThanOne"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Not Hispanic", dsData.Tables[0].Rows[0]["NotHispanic"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Hispanic", dsData.Tables[0].Rows[0]["Hispanic"].ToString(), cCnt++);

            AddGeneralTableRow(table, " ", " ", cCnt++);
            AddGeneralTableRow(table, "     " + "Children of Substance Abusers", dsData.Tables[0].Rows[0]["D_HiRi_ChildOfSA"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Pregnant Women/Teens", dsData.Tables[0].Rows[0]["D_HiRi_PregUse"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Drop-outs", dsData.Tables[0].Rows[0]["D_HiRi_K12DO"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Violent and Delinquent Behavior", dsData.Tables[0].Rows[0]["D_HiRi_Violent"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Mental Health Problems", dsData.Tables[0].Rows[0]["D_HiRi_MentH"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Economically Disadvantaged", dsData.Tables[0].Rows[0]["D_HiRi_EconDis"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Physically Disabled", dsData.Tables[0].Rows[0]["D_HiRi_PhysDis"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Abuse Victims", dsData.Tables[0].Rows[0]["D_HiRi_AbuVict"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Already Using Substances", dsData.Tables[0].Rows[0]["D_HiRi_Using"].ToString(), cCnt++); 
            AddGeneralTableRow(table, "     " + "Homeless and/or Runaway Youth", dsData.Tables[0].Rows[0]["D_HiRi_Homel"].ToString(), cCnt++);
            AddGeneralTableRow(table, "     " + "Free Lunch", dsData.Tables[0].Rows[0]["D_HiRi_FrLun"].ToString(), cCnt++);
            AddGeneralTableRow(table, "     " + "Other", dsData.Tables[0].Rows[0]["D_HiRi_Other"].ToString(), cCnt++); 
            //AddGeneralTableRow(table, "D_HiRi_NotAp", dsData.Tables[0].Rows[0]["D_HiRi_NotAp"].ToString(), cCnt++);



            AddGeneralTableRow(table, " ", " ", cCnt++);
            AddGeneralTableRow(table, "Number of Evidence-based Direct Service Hours", dsData.Tables[0].Rows[0]["NumberOfEvidenceBasedHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Number of Evidence-based Travel Hours", dsData.Tables[0].Rows[0]["NumberOfEvidenceBasedTravelHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Number of Evidence-based Prep Hours", dsData.Tables[0].Rows[0]["NumberOfEvidenceBasedPrepHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Total Evidence-based Hours", dsData.Tables[0].Rows[0]["TotalEvidenceBasedHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, " ", " ", cCnt++);
            
            AddGeneralTableRow(table, "Number of Non-Evidence-based Direct Service Hours", dsData.Tables[0].Rows[0]["NumberOfNonEvidenceBasedHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Number of Non-Evidence-based Travel Hours", dsData.Tables[0].Rows[0]["NumberOfNonEvidenceBasedTravelHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Number of Non-Evidence-based Prep Hours", dsData.Tables[0].Rows[0]["NumberOfNonEvidenceBasedPrepHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Total Non-Evidence-based Hours", dsData.Tables[0].Rows[0]["TotalNonEvidenceBasedHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, " ", " ", cCnt++);
            
            AddGeneralTableRow(table, "Total Direct Service Hours", dsData.Tables[0].Rows[0]["DirectServiceHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Total Travel Hours", dsData.Tables[0].Rows[0]["TravelHours"].ToString(), cCnt++);
            AddGeneralTableRow(table, "Total Prep Hours", dsData.Tables[0].Rows[0]["PrepHours"].ToString(), cCnt++);

            AddGeneralTableRow(table, " ", " ", cCnt++);

            AddGeneralTableRow(table, "02-Resource Directories (number distributed)", dsData.Tables[0].Rows[0]["ID02_DirectoriesDistributed"].ToString(), cCnt++);
            AddGeneralTableRow(table, "04-Brochures (number distributed)", dsData.Tables[0].Rows[0]["ID04_BrochuresDistributed"].ToString(), cCnt++); 


            AddGeneralTableRow(table, " ", " ", cCnt++);

            AddGeneralTableRow(table, "Number of Coalition Meetings", dsData.Tables[0].Rows[0]["CoalitionMeeting"].ToString(), cCnt++);
            foreach (DataRow dataRow in dsData.Tables[1].Rows)
            {
                AddGeneralTableRow(table, "     " + dataRow["CoalitionType"].ToString(), dataRow["CoalitionTypeCount"].ToString(), cCnt++);
            }


            AddGeneralTableRow(table, " ", " ", cCnt++);

            AddGeneralTableRow(table, "Statewide initiatives", "", cCnt++);
            foreach (DataRow dataRow in dsData.Tables[2].Rows)
            {
                AddGeneralTableRow(table, "     " + dataRow["TargetedStatewideInitiative"].ToString(), dataRow["SWInitiativeTypeCount"].ToString(), cCnt++);
            }

            AddGeneralTableRow(table, " ", " ", cCnt++);

            if (dsData.Tables[3].Rows.Count > 0)
            {
                AddGeneralTableRow(table, "Evidence-Based Programs", " ", cCnt++);

                foreach (DataRow dataRow in dsData.Tables[3].Rows)
                {
                    AddGeneralTableRow(table, " ", dataRow["ProgramName"].ToString(), cCnt++);
                }
            }

            //---------------------------------------------------------------------------------------------------

            return table;
        }


        private void AddGeneralTableRow(PdfPTable table, string c1, string c2, int cCnt)
        {
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase(c1, Font.HELVETICA, 8, Font.BOLD));
            if (Convert.ToBoolean(cCnt % 2))
                cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(c2, Font.HELVETICA, 8, Font.NORMAL));
            cell.HorizontalAlignment = Cell.ALIGN_RIGHT;
            if (Convert.ToBoolean(cCnt % 2))
                cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

        }



    }
}
