﻿(function () {
    'use strict';
    var controllerId = 'report12a';
    angular
        .module('app')
        .controller(controllerId, report12a);

    report12a.$inject = ['$scope']; 

    function report12a($scope) {
        $scope.title = 'report12a';

        activate();

        function activate() { }
    }
})();
