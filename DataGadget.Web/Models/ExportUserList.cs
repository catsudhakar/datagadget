﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class ExportUserList
    {
        public string Organization { get; set; }
        public string  UserName { get; set; }
        public string Email { get; set; }
        public string  LastName { get; set; }
        public string FirstName { get; set; }
        public string OfficePhone { get; set; }
        public string OfficeFax { get; set; }
    }
}