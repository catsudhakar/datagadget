﻿using DataGadget.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.Models;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace DataGadget.Web.Controllers
{
    public class ProgramStategiesController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }
        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                    case "GetAllProgramStategies":
                        return await GetAllProgramStategies(value.Parameters);
                    case "GetAllProgramStategiesState":
                        return await GetAllProgramStategiesState(value.Parameters);
                    case "CreateProgramStategies":
                        return CreateProgramStategies(value.Parameters);
                    case "GetProgramStategies":
                        return await GetProgramStategies(value.Parameters);
                    case "DeleteProgramStategies":
                        return DeleteProgramStategies(value.Parameters);
                    case "InitializeProgramStategiesForm":
                        return await InitializeProgramStategiesForm();
                    case "Search":
                        return await Search(value.Parameters);

                    case "SearchStatewideProgramStategies":
                        return await SearchStatewideProgramStategies(value.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
              ///  WebLogger.Error("Exception In ProgramStategiesController : " + ex.Message); 
               // return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }
        async private Task<HttpResponseMessage> Search(string[] p)
        {
            try
            {

                if (p[1] == "SIT2A" || p[1] == "SIT3U")
                {
                    return null;
                }
                string type = p[0];
                var statewides = new List<ProgramStrategy>();
                string searchKey = Convert.ToString(p[2]);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var resData = context.suretool_GetProgramsAndStrategies(type);//.Where(x => x.Name.Contains(searchKey)).ToList();

                    var resData = context.tblProgramsAndStrategies.Where(x => x.Type == type && x.Name.Contains(searchKey)).ToList();
                    foreach (var programData in resData)
                    {
                        bool ? isa = programData.IsActive.HasValue ? programData.IsActive : true;

                        statewides.Add(
                            new ProgramStrategy
                            {
                                ID = programData.ID,
                                LongDescription = programData.LongDescription,
                                Name = programData.Name,
                                Type = programData.Type,
                                IsActive = (bool)isa
                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewides);
            }
            catch (Exception)
            {

                throw;
            }
        }
        async private Task<HttpResponseMessage> InitializeProgramStategiesForm()
        {

            var statewide = new statewideInitiative
            {

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
        }
        async private Task<HttpResponseMessage> SearchStatewideProgramStategies(string[] p)
        {
            try
            {

                if (p[1] == "SIT2A" || p[1] == "SIT3U")
                {
                    return null;
                }
                string type = p[0];
                string searchKey = p[2].ToLower();
                var statewides = new List<ProgramStrategy>();

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var resData = context.suretool_GetProgramsAndStrategies1(type).Where(x => x.Name.Contains(searchKey)).ToList();
                    var resData = context.suretool_GetProgramsAndStrategies1(type).Where(x=>x.Name.ToLower().Contains(searchKey)).OrderBy(x=>x.Name).ToList();
                    foreach (var programData in resData)
                    {

                        statewides.Add(
                            new ProgramStrategy
                            {
                                ID = programData.ID,
                                LongDescription = programData.LongDescription,
                                Name = programData.Name,
                                Type = programData.Type,IsActive=programData.IsActive
                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewides);
            }
            catch (Exception)
            {

                throw;
            }
        }
        async private Task<HttpResponseMessage> GetAllProgramStategies(string[] p)
        {
            try
            {

                if (p[1] == "SIT2A" || p[1] == "SIT3U")
                {
                    return null;
                }
                string type = p[0];
                var statewides = new List<ProgramStrategy>();

                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[2]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //int rowCount = context.suretool_GetProgramsAndStrategies(type).Count();
                    //var resData = context.suretool_GetProgramsAndStrategies(type).Skip(skip).Take(take).ToList();

                    int rowCount = context.suretool_GetProgramsAndStrategies1(type).Count();
                    var resData = context.suretool_GetProgramsAndStrategies1(type).OrderBy(x=>x.Name).Skip(skip).Take(take).ToList();

                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var programData in resData)
                    {

                        statewides.Add(
                            new ProgramStrategy
                            {
                                ID = programData.ID,
                                LongDescription = programData.LongDescription,
                                Name = programData.Name,
                                Type = programData.Type,
                                IsActive=programData.IsActive
                            }
                            );
                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<ProgramStrategy>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<ProgramStrategy>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, statewides);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }

        async private Task<HttpResponseMessage> GetAllProgramStategiesState(string[] p)
        {
            try
            {

                if (p[1] == "SIT2A" || p[1] == "SIT3U")
                {
                    return null;
                }
                string type = p[0];
                var statewides = new List<ProgramStrategy>();
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[2]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;
               

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = context.suretool_GetProgramsAndStrategies1(type).OrderBy(x => x.Name).Count();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                    var resData = context.suretool_GetProgramsAndStrategies1(type).OrderBy(x=>x.Name).Skip(skip).Take(take).ToList();

                    

                    foreach (var programData in resData)
                    {

                        statewides.Add(
                            new ProgramStrategy
                            {
                                ID = programData.ID,
                                LongDescription = programData.LongDescription,
                                Name = programData.Name,
                                Type = programData.Type,
                                IsActive = programData.IsActive
                            }
                            );
                    }
                   
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<ProgramStrategy>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<ProgramStrategy>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, statewides);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }
        private HttpResponseMessage CreateProgramStategies(string[] p)
        {
            try
            {
                var resData = 0;
                Models.ProgramStrategy program = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.ProgramStrategy>(p[0]);
                tblProgramsAndStrategy programStrategy = new tblProgramsAndStrategy();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    if (program.ID == null || program.ID == 0)
                    {
                        programStrategy.ID = program.ID;
                        programStrategy.Name = program.Name;
                        programStrategy.LongDescription = program.Name;
                        programStrategy.Type = program.Type;
                        programStrategy.IsActive = program.IsActive;
                        context.tblProgramsAndStrategies.Add(programStrategy);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        tblProgramsAndStrategy U = new tblProgramsAndStrategy();
                        U = context.tblProgramsAndStrategies.Where(p1 => p1.ID == program.ID).SingleOrDefault();
                        U.ID = program.ID;
                        U.Name = program.Name;
                        U.LongDescription = program.Name;
                        U.Type = program.Type;
                        U.IsActive = program.IsActive;
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, programStrategy);


                //var resData=0;
                //Models.ProgramStrategy program = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.ProgramStrategy>(p[0]);
                //using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                //{
                //    if (program.ID == 0 || program.ID==null)
                //    {
                //        ObjectParameter objParam = new ObjectParameter("id", typeof(int));
                //        resData = context.suretool_InsertProgram(program.Type, program.Name, program.Name, objParam);
                //        context.SaveChanges();
                //    }
                //    else   //updating 
                //    {
                //        resData = context.suretool_UpdateProgram(program.ID, program.Name, program.Name);
                //        context.SaveChanges();
                //    }
                //}
                //return this.Request.CreateResponse(HttpStatusCode.OK, resData);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
        public HttpResponseMessage DeleteProgramStategies(string[] p)
        {
            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //i = context.suretool_DeleteProgram(id);
                    DataGadget.Web.DataModel.tblProgramsAndStrategy dpProg = new DataGadget.Web.DataModel.tblProgramsAndStrategy();
                    dpProg = context.tblProgramsAndStrategies.Where(x => x.ID == id).SingleOrDefault();
                    if (dpProg !=null)
                    {
                        dpProg.IsActive = false;
                        context.SaveChanges();
                    }
                    

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);
            }
            catch (Exception)
            {
                throw;
            }
        }
        async private Task<HttpResponseMessage> GetProgramStategies(string[] p)
        {
            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                ProgramStrategy program = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var res = await context.tblProgramsAndStrategies.Select(x => new ProgramStrategy
                    {
                        ID = x.ID,
                        LongDescription = x.LongDescription,
                        Name = x.Name,
                        Type = x.Type,IsActive=x.IsActive.HasValue?x.IsActive:true
                    }).FirstOrDefaultAsync(x => x.ID == id);
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, program);
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
