﻿(function () {
    'use strict';
    var controllerId = 'oneTimeEvents';
    angular
        .module('app')
        .controller(controllerId, oneTimeEvents);

    oneTimeEvents.$inject = ['$scope', '$http', 'common', '$modal', '$location', '$route', '$window', 'sessionService'];

    function oneTimeEvents($scope, $http, common, $modal, $location, $route, $window, sessionService) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'oneTimeEvents';
        var userid = localStorage.getItem('userId');
        var eventType = 0;
        var vm = this;
        vm.events = [];
        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;

        $scope.CurrentDate = new Date();
      
        vm.pagination = function (pageId) {
            paging(pageId);
        }
        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                $http({ method: 'POST', url: 'api/oneTimeEvent', data: { method: "Search", parameters: [localStorage.getItem('userInfo'), eventType, search] } })
                .success(function (result) {
                    vm.events = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    $scope.ispaging = false;
                    paging(1);
                }
            }
        }
        function paging(pageId) {
            $http({ method: 'POST', url: 'api/oneTimeEvent', data: { method: "AllEvents", parameters: [localStorage.getItem('userInfo'), eventType, pageId] } })
         .success(function (result) {
             
             $scope.ispaging = false;
             vm.pagerData = result.item1[0];
             if (parseInt(vm.pagerData.numberOfPages) > 1) {
                 $scope.ispaging = true;
             }
             vm.pageList = [];
             if (pageId > 3) {
                 vm.pageList.push(pageId - 2);
                 vm.pageList.push(pageId - 1);
                 vm.pageList.push(pageId);
                 if (pageId < vm.pagerData.numberOfPages) {
                     vm.pageList.push(pageId + 1);
                     if (pageId < (vm.pagerData.numberOfPages - 1)) {
                         vm.pageList.push(pageId + 2);
                     }
                 }
             }
             else {
                 if (pageId == 1) {
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                     if (pageId + 4 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 4);
                     }

                 }
                 if (pageId == 2) {
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);

                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                 }
                 if (pageId == 3) {
                     vm.pageList.push(pageId - 2);
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                 }

             }

             var userData = result.item2;
             vm.events = userData;
             for (var i = 0; i < vm.events.length; i++) {
                 vm.events[i].compareDate = new Date(vm.events[i].endDate);
             }
         })
         .error(function (error) {
             console.log(error);
         })
        }

        function activate() {
            $scope.isuserLevel = true;
            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            //if (userInfo.level == "SIT3U") {

            //    $scope.isuserLevel = false;
                
            //}
            paging(1);
        }

        vm.newEvent = function () {
            $location.path('/newOneTimeEvent');
        }

        vm.editEvent = function (event) {
            var p = '/newOneTimeEvent/' + JSON.stringify(event);
            $location.path(p);

        }

        vm.viewReport = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/prevention/onetimeReportViewer.html',
                controller: 'onetimeReportViewer',
                backdrop: 'static',
                resolve: {
                    eventInfo: function () {
                        return event;
                    }
                }
            });
        };


        vm.Delegate = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/prevention/userListModal.html',
                controller: 'userListModal',
                backdrop: 'static',
                resolve: {
                    eventInfo: function () {
                        return event;
                    }
                }
            });
        };

        $scope.dynamicPopover = function (event){
            content: 'Hello, World!';
            templateUrl: '/app/prevention/userListModal.html';
            title: 'Title'
        };

        vm.deleteEvent = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',

            });


            modalInstance.result.then(function () {
                //vm.events.pop();
                $http({ method: 'POST', url: 'api/oneTimeEvent', data: { method: "DeleteEvent", parameters: [event.id] } })
               .success(function (result) {
                   log("Event deleted successfully");
                   $location.path('/oneTimeEvents');
                   $route.reload();
               })
               .error(function (error) {
                   console.log(error);
               })
            }, function () {

            });
        };

        vm.editDataListOpen = function (data) {

            data.from = 'oneTime';
            sessionService.setData(data);
            $location.path('/editEventData');

        }
    }

})();
