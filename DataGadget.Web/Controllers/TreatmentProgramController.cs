﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.ViewModels;
using System.Threading.Tasks;
using DataGadget.Web.Models;

namespace DataGadget.Web.Controllers
{
    public class TreatmentProgramController : ApiController
    {

        [HttpDelete()]
        public async void Delete(int id)
        {
            try
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    //var res = await context.Database.ExecuteSqlCommandAsync("delete treatmentprogram where recno = {0}", new object[] { id });

                    var res = await context.Database.ExecuteSqlCommandAsync("update treatmentprogram set isActive=0 where recno = {0}", new object[] { id });

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "GetAlltreatmentProgramms":
                        return await GetAlltreatmentProgramms(param.Parameters);
                    case "SearchTreatmentProgramms":
                        return await SearchTreatmentProgramms(param.Parameters);
                    case "GetLocationList":
                        return await GetLocationListAsync();
                    case "UpdateTreatmentProgram":
                        return UpdateTreatmentProgram(param.Parameters);
                    case "DeleteServiceLocation":
                        return DeleteServiceLocation(param.Parameters);

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
                //WebLogger.Error("Exception In TreatmentProgramController : " + ex.Message); 
                //return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        public HttpResponseMessage DeleteServiceLocation(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    DataGadget.Web.DataModel.TreatmentProgram U = new DataGadget.Web.DataModel.TreatmentProgram();
                    U = context.TreatmentPrograms.Where(p1 => p1.RecNo == id).SingleOrDefault();

                    U.IsActive = false;
                    context.SaveChanges();


                }

                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private HttpResponseMessage UpdateTreatmentProgram(string[] p)
        {


            try
            {

                TreatmentProgramViewModel state = Newtonsoft.Json.JsonConvert.DeserializeObject<TreatmentProgramViewModel>(p[0]);

                var statewide = new DataModel.TreatmentProgram();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {


                    if (state.Id == 0)
                    {

                        statewide.RecNo = Convert.ToInt32(state.Id);
                        statewide.LocationID = state.LocationId;
                        statewide.ProgramName = state.Name;
                        statewide.ProgramTypeID = 1;
                        statewide.IsActive = state.IsActive;

                        statewide.MaleBeds = state.MaleBeds;
                        statewide.FemaleBeds = state.FemaleBeds;
                        statewide.AdolescentFemaleBeds = state.AdolescentFemaleBeds;
                        statewide.AdolescentMaleBeds = state.AdolescentMaleBeds;

                        statewide.Is60days = state.Is60days == null ? false : state.Is60days;
                        statewide.Is180days = state.Is180days == null ? false : state.Is180days;

                        context.TreatmentPrograms.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        DataGadget.Web.DataModel.TreatmentProgram U = new DataGadget.Web.DataModel.TreatmentProgram();
                        U = context.TreatmentPrograms.Where(p1 => p1.RecNo == state.Id).SingleOrDefault();
                        U.ProgramTypeID = 1;
                        U.RecNo = state.Id;
                        U.LocationID = state.LocationId;
                        U.ProgramName = state.Name;
                        U.IsActive = state.IsActive;

                        U.MaleBeds = state.MaleBeds;
                        U.FemaleBeds = state.FemaleBeds;
                        U.AdolescentFemaleBeds = state.AdolescentFemaleBeds;
                        U.AdolescentMaleBeds = state.AdolescentMaleBeds;

                        U.Is60days = state.Is60days == null ? false : state.Is60days;
                        U.Is180days = state.Is180days == null ? false : state.Is180days;


                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetAlltreatmentProgramms(string[] p)
        {


            try
            {
                var lstTreatmentProg = new List<TreatmentProgramViewModel>();
                var lstTreatmentProg1 = new List<TreatmentProgramViewModel>();

                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[0]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.TreatmentPrograms.OrderBy(x => x.ProgramName).CountAsync();
                    var dbProviders = await context.TreatmentPrograms.OrderBy(x => x.ProgramName).Skip(skip).Take(take).ToListAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();


                    foreach (var rlink in dbProviders)
                    {

                        lstTreatmentProg.Add(
                            new TreatmentProgramViewModel
                            {
                                Is60days = rlink.Is60days == null ? false : rlink.Is60days,
                                Is180days = rlink.Is180days == null ? false : rlink.Is180days,

                                Id = rlink.RecNo,
                                Name = rlink.ProgramName,
                                LocationId = rlink.LocationID,
                                IsActive = rlink.IsActive,
                                MaleBeds = rlink.MaleBeds,
                                FemaleBeds = rlink.FemaleBeds,
                                AdolescentFemaleBeds = rlink.AdolescentFemaleBeds,
                                AdolescentMaleBeds = rlink.AdolescentMaleBeds,
                                Location = rlink.tblLocation.FriendlyName_loc //(await context.tblLocations.FirstAsync(x=>x.PKey == rlink.LocationID)).FriendlyName_loc
                            }
                            );
                    }
                }
                if (lstTreatmentProg.Count > 0)
                    lstTreatmentProg1 = lstTreatmentProg.OrderBy(x => x.Location).ToList();

                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<TreatmentProgramViewModel>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<TreatmentProgramViewModel>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstTreatmentProg1);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> SearchTreatmentProgramms(string[] p)
        {


            try
            {
                var lstTreatmentProg = new List<TreatmentProgramViewModel>();
                string search = p[0];
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    lstTreatmentProg = await context.TreatmentPrograms.
                    Select(x => new TreatmentProgramViewModel
                        {
                            Is60days = x.Is60days == null ? false : x.Is60days,
                            Is180days = x.Is180days == null ? false : x.Is180days,

                            Id = x.RecNo,
                            Name = x.ProgramName,
                            LocationId = x.LocationID,
                            IsActive = x.IsActive,
                            MaleBeds = x.MaleBeds,
                            FemaleBeds = x.FemaleBeds,
                            AdolescentFemaleBeds = x.AdolescentFemaleBeds,
                            AdolescentMaleBeds = x.AdolescentMaleBeds,
                            Location = x.tblLocation.FriendlyName_loc
                        }).
                   Where(x => x.Name.Contains(search) || x.Location.Contains(search)).OrderBy(x => x.Name).ToListAsync();

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstTreatmentProg);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        async private Task<HttpResponseMessage> GetLocationListAsync()
        {
            try
            {
                var locations = new List<Location>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.tblLocations.OrderBy(x => x.FriendlyName_loc).ToListAsync();
                    foreach (var item in dblocations)
                    {

                        locations.Add(
                            new Location { PKey = item.PKey, FriendlyName_loc = item.FriendlyName_loc.Trim() }
                            );
                    }
                    //var query = from loc in context.tblLocations
                    //            join prog in context.TreatmentPrograms
                    //                on loc.PKey equals prog.LocationID into lp
                    //            from prog in lp.DefaultIfEmpty()

                    //            select new  { PKey = loc.PKey,   FriendlyName_loc = loc.FriendlyName_loc , LocProg = prog};




                    //foreach (var item in query)
                    //{

                    //    if (item.LocProg==null)
                    //    {
                    //        locations.Add(new Location { PKey = item.PKey, FriendlyName_loc = item.FriendlyName_loc}); 
                    //    }
                    //    else
                    //    {
                    //        locations.Add(new Location { PKey = item.PKey, FriendlyName_loc = item.FriendlyName_loc, IsDisable = true }); 
                    //    }
                    //}
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, locations);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }







    }
}