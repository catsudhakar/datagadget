﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('dischargeModal', dischargeModal);

    dischargeModal.$inject = ['$scope', '$http', 'dischargeDate', '$modalInstance', '$location', '$route'];

    function dischargeModal($scope, $http, dischargeDate, $modalInstance, $location, $route) {
        $scope.title = 'dischargeModal';
        $scope.dt = dischargeDate;

        $scope.ismessage = false;
        $scope.dischargeCodeList = [];

        var dateDischarge = $scope.dt.dischargeDate;

        var did = $scope.dt.dischargeId;
        activate();

        function activate() {

            $http({ method: 'POST', url: 'api/discharges', data: { method: "InitializeStaffForm" } })
             .success(function (result) {
                 //$scope.dischargeCodeList = result;
                 $scope.dt.dischargeCodeList = result;
                 $scope.dt.dischargeId = did;
             })
             .error(function (error) {

                 console.log(error);
             });

        }

        $scope.cancel = function () {
            dischargeDate.dischargeDate = dateDischarge;
            $modalInstance.dismiss('cancel');

        }

        $scope.ok = function () {

            $scope.ismessage = false;

            var i = $scope.dt.dischargeId

            var sdt = $scope.dt.entryDate;
            var edt = $scope.dt.dischargeDate;

            if (sdt !== undefined && edt !== undefined) {

                sdt = Date.parse($scope.dt.entryDate);
                edt = Date.parse($scope.dt.dischargeDate);
                if (edt != NaN) {
                    if (edt < sdt) {
                        $scope.ismessage = true;
                        return false;
                    }
                }
            }


            $http({ method: 'POST', url: 'api/discharges', data: { method: "DateEvents", parameters: [$scope.dt.recNo, $scope.dt.dischargeDate, i] } })

            .success(function (result) {
                $modalInstance.close('done');
                $location.path('/discharges');
                $route.reload();

            })
            .error(function (error) {
                console.log(error);
                $modalInstance.close('done');
                $location.path('/discharges');
                $route.reload();
            })

          
        }
    }
})();
