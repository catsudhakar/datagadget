﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using DataGadget.Web.DataModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web.SessionState;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DREAMTool;

namespace DataGadget.Web.Controllers
{
    public class CommentsController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "SendComments":
                        return await SendComments(param.Parameters);
                    case "GetCommentsByDate":
                        return await GetCommentsByDate(param.Parameters);
                    case "GetCommentsByParentId":
                        return await GetCommentsByParentId(param.Parameters);
                    case "GetAllComments":
                        return await GetAllComments(param.Parameters);
                    case "SearchComments":
                        return await SearchComments(param.Parameters);

                    case "DeleteComment":
                        return DeleteComment(param.Parameters);
                    case "ReplyToComment":
                        return await ReplyToComment(param.Parameters);

                    case "UsersList":
                        return await UsersList();

                    case "SendCommentsAllUsers":
                        return await SendCommentsAllUsers(param.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

               // WebLogger.Error("Exception In CommentsController : " + ex.Message);   
               // return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<HttpResponseMessage> UsersList()
        {
            try
            {
                var users = new List<DataGadget.Web.Models.User>();
                var dbUsers = new List<DataGadget.Web.DataModel.TblUser>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    dbUsers = await context.TblUsers.OrderBy(x => x.LoginID).OrderBy(x => x.FirstName).ToListAsync();

                }

                users = dbUsers.Select(user => new DataGadget.Web.Models.User
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    EmailId = user.Email_addr,
                    UserId = user.UserGUID,
                    UserName = user.LoginID,
                    AgencyName = user.LocationID.ToString(),
                    Password = user.PW,
                    Level = user.UserLevel,
                    Degree = user.Degree,
                    Experience = user.YrsPrevExp,
                    OfficePhoneNum = user.OfficePhone,
                    OfficeFaxNum = user.OfficeFax,
                    LocationId = user.PKey


                }).ToList();
                return this.Request.CreateResponse(HttpStatusCode.OK, users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> ReplyToComment(string[] p)
        {
            try
            {
                string addressTo = p[0].Trim();
                string addressFrom = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                string subject = p[6].Trim();
                string mailBody = p[1].Trim();
                StringBuilder sb;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int userId = Convert.ToInt32(p[4]) == Convert.ToInt32(p[3]) ? Convert.ToInt32(p[5]) : Convert.ToInt32(p[4]);
                    var userDetails = context.TblUsers.Where(x => x.PKey == userId).SingleOrDefault();
                    sb = new StringBuilder();
                    sb.Append("Hi " + userDetails.FirstName + ",\n");
                    sb.Append(mailBody + "\n");
                    sb.Append("Thanks <br>");
                    sb.Append("DataGadget Team");

                    bool isMailSent = Email.SendEmail(addressTo, addressFrom, subject, sb.ToString());
                    if (isMailSent)
                    {
                        tblComment DTO = new tblComment();
                        DTO.Comment = mailBody;
                        DTO.UserID = Convert.ToInt32(p[3]);
                        DTO.CreatedOn = DateTime.Now;
                        DTO.UpdatedOn = DateTime.Now;
                        DTO.IsActive = true;
                        DTO.ToUserID = Convert.ToInt32(p[4]) == Convert.ToInt32(p[3]) ? Convert.ToInt32(p[5]) : Convert.ToInt32(p[4]);
                        DTO.ParentId = Convert.ToInt32(p[2]);
                        DTO.Subject = subject;
                        context.tblComments.Add(DTO);
                        await context.SaveChangesAsync();
                    }

                }



                return this.Request.CreateResponse(HttpStatusCode.OK, "success");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> SendComments(string[] parameters)
        {
            try
            {
                string addressFrom = parameters[0];
                string addressTo = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                string subject = parameters[3].Trim();
                string mailBody = parameters[1].Trim();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    Guid UID = new Guid(parameters[2]);
                    var userDetails = context.TblUsers.Where(x => x.UserGUID == UID).SingleOrDefault();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Hi " + userDetails.FirstName + ",\n");
                    sb.Append(mailBody + "\n");
                    sb.Append("Thanks <br>");
                    sb.Append("DataGadget Team");

                    bool isMailSent = Email.SendEmail(addressTo, addressFrom, subject, sb.ToString());
                    if (isMailSent)
                    {
                        tblComment DTO = new tblComment();
                        DTO.Comment = mailBody;
                        DTO.UserID = userDetails.PKey;
                        DTO.CreatedOn = DateTime.Now;
                        DTO.UpdatedOn = DateTime.Now;
                        DTO.IsActive = true;
                        DTO.ToUserID = 1;
                        DTO.Subject = subject;
                        context.tblComments.Add(DTO);
                        context.SaveChanges();
                        DTO.ParentId = Convert.ToInt32(DTO.ID);
                        await context.SaveChangesAsync();
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, "success");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> SendCommentsAllUsers(string[] parameters)
        {
            try
            {
                StringBuilder sb;

                string addressFrom = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                string addressTo = System.Configuration.ConfigurationManager.AppSettings["commentsEmailAddress"];
                string subject = parameters[2].Trim();
                string mailBody = parameters[1].Trim();
                int userId = 0;
                userId = Convert.ToInt32(parameters[0].Trim());

                if (userId == 0)
                {
                    using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                    {
                        var dbusers = context.TblUsers.Where(x => x.LoginID.ToUpper() != "ADMINISTRATOR" && x.Email_addr != null).ToList();
                        foreach (var u in dbusers)
                        {
                            addressTo = u.Email_addr;
                            sb = new StringBuilder();

                            sb.Append("Hi " + u.FirstName + ",<br><br>");
                            sb.Append(mailBody + "<br><br>");

                            sb.Append("Thanks <br>");
                            sb.Append("DataGadget Team");

                            bool b = Email.SendEmail(addressTo, addressFrom, subject, sb.ToString());
                            if (b)
                            {

                                tblComment DTO = new tblComment();
                                DTO.Comment = mailBody;
                                DTO.UserID = 1;
                                DTO.CreatedOn = DateTime.Now;
                                DTO.UpdatedOn = DateTime.Now;
                                DTO.IsActive = true;
                                DTO.ToUserID = u.PKey;
                                DTO.Subject = subject;
                                context.tblComments.Add(DTO);
                                context.SaveChanges();
                                DTO.ParentId = Convert.ToInt32(DTO.ID);

                                await context.SaveChangesAsync();
                            }
                        }
                    }
                }
                else
                {
                    using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                    {

                        // string  emiladdress = context.TblUsers.Where(x => x.PKey == userId).Select(x => x.Email_addr).Single();
                        var userDetails = context.TblUsers.Where(x => x.PKey == userId).SingleOrDefault();
                        if (userDetails != null)
                        {
                            sb = new StringBuilder();
                            addressTo = userDetails.Email_addr; //"sudhakar@cattechnologies.com";//

                            sb.Append("Hi " + userDetails.FirstName + ",<br><br>");
                            sb.Append(mailBody + "<br><br>");


                            sb.Append("Thanks <br>");
                            sb.Append("DataGadget Team");

                            bool b = Email.SendEmail(addressTo, addressFrom, subject, sb.ToString());
                            if (b)
                            {
                                tblComment DTO = new tblComment();
                                DTO.Comment = mailBody;
                                DTO.UserID = 1;
                                DTO.CreatedOn = DateTime.Now;
                                DTO.UpdatedOn = DateTime.Now;
                                DTO.IsActive = true;
                                DTO.ToUserID = userId;
                                DTO.Subject = subject;
                                context.tblComments.Add(DTO);
                                context.SaveChanges();
                                DTO.ParentId = Convert.ToInt32(DTO.ID);
                                await context.SaveChangesAsync();
                            }
                        }

                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, "success");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> GetCommentsByDate(string[] parameters)
        {
            try
            {
                DateTime startDate = Convert.ToDateTime(parameters[0]);
                DateTime endDate = Convert.ToDateTime(parameters[1]);
                endDate = endDate.Add(new TimeSpan(23, 59, 59));

                int UserId = 0;
                UserId = Convert.ToInt32(parameters[2]);
                var tempEvents = new List<DataGadget.Web.DataModel.tblComment>();
                List<Comments> listData = new List<Comments>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    

                    if (UserId > 0)
                    {
                        tempEvents = await context.tblComments.
                            Where(x => x.UserID == UserId && (DbFunctions.TruncateTime(x.CreatedOn) >= startDate)).ToListAsync();
                    }
                    else
                    {
                        tempEvents = await context.tblComments.
                            Where(x => (DbFunctions.TruncateTime(x.CreatedOn) >= startDate)).ToListAsync();
                    }


                    //tempEvents = tempEvents
                    //   .Where(x => (DbFunctions.TruncateTime(x.CreatedOn) >= startDate.Date)).ToList();

                    listData = tempEvents
                       .Where(x => (x.CreatedOn >= startDate) && (x.CreatedOn <= endDate)).OrderByDescending(x => x.ID).
                        Select(x =>
                            new Comments
                            {
                                Comment = x.Comment,
                                CreatedOn = x.CreatedOn,
                                Email = x.TblUser.Email_addr,
                                FirstName = x.TblUser.FirstName,
                                LastName = x.TblUser.LastName,
                                UserID = x.UserID,
                                ID = x.ID,
                                ParentId = x.ParentId,
                                ToUserID = x.ToUserID,
                                Subject = x.Subject,
                                ToEmail = x.UserID == UserId ? x.TblUser1.Email_addr : x.TblUser.Email_addr
                            }).ToList();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, listData);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetCommentsByParentId(string[] parameters)
        {
            try
            {
                List<Comments> listData = new List<Comments>();
                int parentId = 0;
                parentId = Convert.ToInt32(parameters[0]);


                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    listData = await context.tblComments.Where(x => x.ParentId == parentId).OrderBy(x => x.ID).
                    Select(x =>
                                 new Comments
                                 {
                                     Comment = x.Comment,
                                     CreatedOn = x.CreatedOn,
                                     Email = x.TblUser.Email_addr,
                                     FirstName = x.TblUser.FirstName,
                                     LastName = x.TblUser.LastName,
                                     UserID = x.UserID,
                                     ID = x.ID,
                                     ParentId = x.ParentId,
                                     ToUserID = x.ToUserID

                                 }).ToListAsync();





                }
                return this.Request.CreateResponse(HttpStatusCode.OK, listData);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> GetAllComments(string[] parameters)
        {
            try
            {
                List<Comments> listData = new List<Comments>();
                int UserId = 0;
                UserId = Convert.ToInt32(parameters[0]);
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(parameters[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.tblComments.GroupBy(x => x.ParentId).Select(g => g.OrderByDescending(x => x.ID).FirstOrDefault()).Where(x => x.UserID == UserId || x.ToUserID == UserId).OrderByDescending(x => x.ID).CountAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    listData = await context.tblComments.GroupBy(x => x.ParentId).Select(g => g.OrderByDescending(x => x.ID).FirstOrDefault()).Where(x => x.UserID == UserId || x.ToUserID == UserId).OrderByDescending(x => x.ID).
                           Select(x =>
                            new Comments
                            {
                                Comment = x.Comment,
                                CreatedOn = x.CreatedOn,
                                Email =x.TblUser.Email_addr,
                                FirstName =x.TblUser.FirstName,
                                LastName =x.TblUser.LastName,
                                UserID = x.UserID,
                                ID = x.ID,
                                ParentId = x.ParentId,
                                ToUserID = x.ToUserID,
                                Subject = x.Subject,
                                ToEmail = x.UserID == UserId ?x.TblUser1.Email_addr :x.TblUser.Email_addr

                            }).Skip(skip).Take(take).ToListAsync();



                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Comments>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Comments>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, listData);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> SearchComments(string[] parameters)
        {
            try
            {
                List<Comments> listData = new List<Comments>();
                int UserId = 0;
                UserId = Convert.ToInt32(parameters[0]);
                string search=parameters[1];

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                   
                    listData = await context.tblComments.GroupBy(x => x.ParentId).Select(g => g.OrderByDescending(x => x.ID).FirstOrDefault()).Where(x => (x.UserID == UserId || x.ToUserID == UserId)).OrderByDescending(x => x.ID).
                           Select(x =>
                            new Comments
                            {
                                Comment = x.Comment,
                                CreatedOn = x.CreatedOn,
                                Email =x.TblUser.Email_addr,
                                FirstName = x.TblUser.FirstName,
                                LastName = x.TblUser.LastName,
                                FullName = x.TblUser.FirstName + " " + x.TblUser.LastName,
                                UserID = x.UserID,
                                ID = x.ID,
                                ParentId = x.ParentId,
                                ToUserID = x.ToUserID,
                                Subject = x.Subject,
                                ToEmail = x.UserID == UserId ?x.TblUser1.Email_addr : x.TblUser.Email_addr

                            }).Where(x => x.Comment.Contains(search) || x.Subject.Contains(search) || x.Email.Contains(search) || x.FirstName.Contains(search) || x.LastName.Contains(search) || x.FullName.Contains(search)).ToListAsync();

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, listData);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        public HttpResponseMessage DeleteComment(string[] p)
        {

            try
            {
                long CID = Convert.ToInt64(p[0]);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var res = context.tblComments.Where(x => x.ID == CID).SingleOrDefault();
                    context.tblComments.Remove(res);
                    context.SaveChanges();
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



    }
}