﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;
using System.Data.Entity.Core.Objects;

using System.Data.SqlClient;

namespace DataGadget.Web.Controllers
{
    public class ExportDataController : ApiController
    {
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {


                    case "InitializeUserForm":
                        return await InitializeUserForm(value.Parameters);
                    case "GetData":
                        return await GetData(value.Parameters);
                    case "GetPreventionData":
                        return GetPreventionData(value.Parameters);
                    case "GetTreatmentData":
                        return GetTreatmentData(value.Parameters);
                    case "GetutilizationRateData":
                        return GetutilizationRateData(value.Parameters);

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {
                //WebLogger.Error("Exception In ExportDataController : " + ex.Message);   
                //return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }

        async private Task<HttpResponseMessage> InitializeUserForm(string[] p)
        {

            if (p[1] == "SIT2A" || p[1] == "SIT3U")
            {
                return null;
            }
            string reporttype = p[0];
            var user = new reportModel();
            switch (reporttype)
            {

                case "PreventionOutcomes":
                case "TreatmentOutcomes":
                case "UtilizationRate":
                    string role = p[1];
                    string StateAbbrev = p[3];
                    int Locid = Convert.ToInt32(p[2]);
                    user.LocationList = await GetLocationListPreventionAsync(role, Locid, StateAbbrev);
                    break;
                case "ExportData":
                    user.LocationList = await GetLocationListAsync();
                    break;
                default:
                    break;
            }


            return this.Request.CreateResponse(HttpStatusCode.OK, user);
        }

        async private Task<List<Location>> GetLocationListAsync()
        {

            try
            {
                var locations = new List<Location>();
                locations.Add(new Location { PKey = 0, StateRegionCode = "0_0", FriendlyName_loc = "All" });

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    // var dblocations = await context.tblLocations.Where(x => x.StateAbbrev == "MS").OrderBy(x => x.FriendlyName_loc).ToListAsync();
                    var dblocations = await context.tblLocations.Where(x => x.IsActive == true).OrderBy(x => x.FriendlyName_loc).ToListAsync();
                    foreach (var item in dblocations)
                    {
                        locations.Add(
                            new Location
                            {
                                PKey = item.PKey,
                                StateRegionCode = item.StateRegionCode + '_' + item.SiteCode,
                                FriendlyName_loc = item.FriendlyName_loc
                            }
                            );
                    }

                }

                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<List<Location>> GetLocationListPreventionAsync(string strUserLevel, int intLocationId, string strStateAbbrev)
        {
            try
            {
                DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();
                var locations = new List<Location>();
                locations.Add(new Location { PKey = 0, StateRegionCode = "0_0", FriendlyName_loc = "All" });
                IEnumerable<DataGadget.Web.DataModel.tblLocation> dataSource = new List<DataGadget.Web.DataModel.tblLocation>();
                switch (strUserLevel)
                {
                    case "SIT3U": //User
                    case "coordinator":
                    case "SIT2A": //Admin
                        //Show the user's location only
                        dataSource = await context.tblLocations.Where(location => location.PKey == intLocationId).OrderBy(x => x.FriendlyName_loc).ToListAsync();
                        break;
                    case "STA1A": //State
                        //Show all locations in the state
                        dataSource = await context.tblLocations.Where(location => location.StateAbbrev == strStateAbbrev).OrderBy(x => x.FriendlyName_loc).ToListAsync();
                        break;
                    case "administrator":
                    case "admin":
                        //Show all locations in the system
                        dataSource = await context.tblLocations.OrderBy(x => x.FriendlyName_loc).ToListAsync();
                        break;
                    default:
                        dataSource = await context.tblLocations.OrderBy(x => x.FriendlyName_loc).ToListAsync();
                        break;
                }

                foreach (var item in dataSource)
                {
                    locations.Add(
                        new Location
                        {
                            PKey = item.PKey,
                            StateRegionCode = item.StateRegionCode + '_' + item.SiteCode,
                            FriendlyName_loc = item.FriendlyName_loc
                        }
                        );
                }

                // locations.Add(new Location { PKey = 0, StateRegionCode = "0_0", FriendlyName_loc = "All" });


                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        async private Task<HttpResponseMessage> GetData(string[] p)
        {


            try
            {
                Models.reportModel oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.reportModel>(p[0]);

                var EventsData = new List<ExportEvents>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    //var tempEvents = await context.tbl2008_NOM_Entries
                    //    .Where(x => ( oneTime.BeginDate.Date >= DbFunctions.TruncateTime( x.StartDate)))
                    //    .Where(x=> oneTime.EndDate.Date <= DbFunctions.TruncateTime( x.EndDate)).ToListAsync();
                    // && x.RegionCode == oneTime.RegionCode && x.SiteCode == oneTime.SiteCode).ToListAsync();

                    var tempEvents = await context.tbl2008_NOM_Entries
                        .Where(x => (DbFunctions.TruncateTime(x.StartDate) >= oneTime.BeginDate.Date)).ToListAsync();
                    tempEvents = tempEvents
                       .Where(x => (x.EndDate >= oneTime.BeginDate) && (x.EndDate <= oneTime.EndDate.Date)).ToList();
                    // && x.RegionCode == oneTime.RegionCode && x.SiteCode == oneTime.SiteCode).ToListAsync();

                    if (oneTime.SiteCode != "0" && oneTime.RegionCode != "0")
                    {
                        tempEvents = tempEvents.Where(x => x.SiteCode == oneTime.SiteCode && x.RegionCode == oneTime.RegionCode).ToList();
                    }
                    foreach (var oneevent in tempEvents)
                    {
                        EventsData.Add(
                            new ExportEvents
                            {
                                ID = oneevent.ID,

                                Age0 = oneevent.Age0,
                                Age12 = oneevent.Age12,
                                Age15 = oneevent.Age15,
                                Age18 = oneevent.Age18,
                                Age21 = oneevent.Age21,
                                Age25 = oneevent.Age25,
                                Age45 = oneevent.Age45,
                                Age5 = oneevent.Age5,
                                Age65 = oneevent.Age65,
                                AvgCostPerParticipant = oneevent.AvgCostPerParticipant,
                                closed = oneevent.closed,
                                CoalitionMeeting = oneevent.CoalitionMeeting,
                                CoalitionType = oneevent.CoalitionType,
                                Curriculum = oneevent.Curriculum,
                                D_HiRi_AbuVict = oneevent.D_HiRi_AbuVict,
                                D_HiRi_ChildOfSA = oneevent.D_HiRi_ChildOfSA,
                                D_HiRi_EconDis = oneevent.D_HiRi_EconDis,
                                D_HiRi_FrLun = oneevent.D_HiRi_FrLun,
                                D_HiRi_Homel = oneevent.D_HiRi_Homel,
                                D_HiRi_K12DO = oneevent.D_HiRi_K12DO,
                                D_HiRi_MentH = oneevent.D_HiRi_MentH,
                                D_HiRi_NotAp = oneevent.D_HiRi_NotAp,
                                D_HiRi_Other = oneevent.D_HiRi_Other,
                                D_HiRi_OtherText = oneevent.D_HiRi_OtherText,
                                D_HiRi_PhysDis = oneevent.D_HiRi_PhysDis,
                                D_HiRi_PregUse = oneevent.D_HiRi_PregUse,
                                D_HiRi_Using = oneevent.D_HiRi_Using,
                                D_HiRi_Violent = oneevent.D_HiRi_Violent,
                                Degree = oneevent.Degree,
                                DirectServiceHours = oneevent.DirectServiceHours,
                                DocOfActivities_Agenda = oneevent.DocOfActivities_Agenda,
                                DocOfActivities_Brochure = oneevent.DocOfActivities_Brochure,
                                DocOfActivities_ContactForm = oneevent.DocOfActivities_ContactForm,
                                DocOfActivities_EvalForm = oneevent.DocOfActivities_EvalForm,
                                DocOfActivities_Other = oneevent.DocOfActivities_Other,
                                DocOfActivities_OtherText = oneevent.DocOfActivities_OtherText,
                                DocOfActivities_PrePostTest = oneevent.DocOfActivities_PrePostTest,
                                DocOfActivities_SignIn = oneevent.DocOfActivities_SignIn,

                                Females = oneevent.Females,
                                FiscalYear = oneevent.FiscalYear,
                                GroupName = oneevent.GroupName,
                                Hispanic = oneevent.Hispanic,
                                //ID = oneevent.ID,
                                ID02_DirectoriesDistributed = oneevent.ID02_DirectoriesDistributed,
                                ID04_BrochuresDistributed = oneevent.ID04_BrochuresDistributed,
                                InterventionType = oneevent.InterventionType,
                                Males = oneevent.Males,
                                Name = oneevent.Name,
                                NotHispanic = oneevent.NotHispanic,
                                NumberOfEmployees = oneevent.NumberOfEmployees,
                                NumberParticipants = oneevent.NumberParticipants,
                                NumberProgHrsRecd = oneevent.NumberProgHrsRecd,
                                OrgName = oneevent.OrgName,
                                OverallComments = oneevent.OverallComments,
                                ParentEntry = oneevent.ParentEntry,
                                PrepHours = oneevent.PrepHours,
                                ProgramName = oneevent.ProgramName,
                                ProgramNameSource = oneevent.ProgramNameSource,
                                ProgramType_IP = oneevent.ProgramType_IP,
                                RaceAmIndian = oneevent.RaceAmIndian,
                                RaceAsian = oneevent.RaceAsian,
                                RaceBlack = oneevent.RaceBlack,
                                RaceHawaii = oneevent.RaceHawaii,
                                RaceMoreThanOne = oneevent.RaceMoreThanOne,
                                RaceUnknownOther = oneevent.RaceUnknownOther,
                                RaceWhite = oneevent.RaceWhite,
                                RegionCode = oneevent.RegionCode,
                                SessionName = oneevent.SessionName,
                                SiteCode = oneevent.SiteCode,
                                StaffFname = oneevent.StaffFname,
                                StaffLName = oneevent.StaffLName,

                                Strat_Alt_21 = oneevent.Strat_Alt_21,
                                Strat_Alt_22 = oneevent.Strat_Alt_22,
                                Strat_Alt_23 = oneevent.Strat_Alt_23,
                                Strat_Alt_24 = oneevent.Strat_Alt_24,
                                Strat_Alt_25 = oneevent.Strat_Alt_25,
                                Strat_Alt_26 = oneevent.Strat_Alt_26,
                                Strat_Alt_27 = oneevent.Strat_Alt_27,
                                Strat_Alt_27Text = oneevent.Strat_Alt_27Text,
                                Strat_CBP_41 = oneevent.Strat_CBP_41,
                                Strat_CBP_42 = oneevent.Strat_CBP_42,
                                Strat_CBP_43 = oneevent.Strat_CBP_43,
                                Strat_CBP_44 = oneevent.Strat_CBP_44,
                                Strat_CBP_45 = oneevent.Strat_CBP_45,
                                Strat_CBP_46 = oneevent.Strat_CBP_46,
                                Strat_CBP_46Text = oneevent.Strat_CBP_46Text,
                                Strat_Ed_11 = oneevent.Strat_Ed_11,
                                Strat_Ed_12 = oneevent.Strat_Ed_12,
                                Strat_Ed_13 = oneevent.Strat_Ed_13,
                                Strat_Ed_14 = oneevent.Strat_Ed_14,
                                Strat_Ed_15 = oneevent.Strat_Ed_15,
                                Strat_Ed_16 = oneevent.Strat_Ed_16,
                                Strat_Ed_17 = oneevent.Strat_Ed_17,
                                Strat_Ed_17Text = oneevent.Strat_Ed_17Text,
                                Strat_Env_51 = oneevent.Strat_Env_51,
                                Strat_Env_52 = oneevent.Strat_Env_52,
                                Strat_Env_53 = oneevent.Strat_Env_53,
                                Strat_Env_54 = oneevent.Strat_Env_54,
                                Strat_Env_55 = oneevent.Strat_Env_55,
                                Strat_Env_56_MW = oneevent.Strat_Env_56_MW,
                                Strat_Env_57 = oneevent.Strat_Env_57,
                                Strat_Env_57Text = oneevent.Strat_Env_57Text,
                                Strat_ID_01 = oneevent.Strat_ID_01,
                                Strat_ID_02 = oneevent.Strat_ID_02,
                                Strat_ID_03 = oneevent.Strat_ID_03,
                                Strat_ID_04 = oneevent.Strat_ID_04,
                                Strat_ID_05 = oneevent.Strat_ID_05,
                                Strat_ID_06 = oneevent.Strat_ID_06,
                                Strat_ID_07 = oneevent.Strat_ID_07,
                                Strat_ID_08 = oneevent.Strat_ID_08,
                                Strat_ID_09 = oneevent.Strat_ID_09,
                                Strat_ID_09Text = oneevent.Strat_ID_09Text,
                                Strat_IR_31 = oneevent.Strat_IR_31,
                                Strat_IR_32 = oneevent.Strat_IR_32,
                                Strat_IR_34 = oneevent.Strat_IR_34,
                                Strat_IR_34Text = oneevent.Strat_IR_34Text,
                                SurveysEntered = oneevent.SurveysEntered,
                                TargetedStatewideInitiative = oneevent.TargetedStatewideInitiative,
                                TotalCostofProgram = oneevent.TotalCostofProgram,
                                TravelHours = oneevent.TravelHours,
                                TyoeOfProgram_EA = oneevent.TyoeOfProgram_EA,
                                UniversalType_DI = oneevent.UniversalType_DI,
                                UserGUID = oneevent.UserGUID,
                                WithinCostBand_YN = oneevent.WithinCostBand_YN,
                                //StartDate = oneevent.StartDate,
                                //EntryDate = oneevent.EntryDate,

                                StartDate = oneevent.StartDate.HasValue ? oneevent.StartDate.Value.ToString("MM/dd/yyyy") : "",
                                EntryDate = oneevent.EntryDate.HasValue ? oneevent.EntryDate.Value.ToString("MM/dd/yyyy") : "",
                                EndDate = oneevent.EndDate.HasValue ? oneevent.EndDate.Value.ToString("MM/dd/yyyy") : ""

                            }
                            );
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, EventsData);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetPreventionData(string[] p)
        {


            try
            {
                Models.reportModel oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.reportModel>(p[0]);
                oneTime.EndDate = oneTime.EndDate.Add(new TimeSpan(23, 59, 59));
                var PreventionData = new List<PreventionOutcomes>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    context.Database.CommandTimeout = 240;

                    var dblocations = context.suretool_GetPreventionOutcomesReport1().
                    Where(report => report.Survey_Completed >= oneTime.BeginDate).
                    Where(report => report.Survey_Completed <= oneTime.EndDate);
                    if (oneTime.Organization != "All")
                    {
                        dblocations = dblocations.
                            Where(report => report.Agency_Name == oneTime.OrganizationName);
                    }
                    dblocations.OrderBy(report => report.Agency_Name).ThenBy(report => report.Program_Name).
                    ThenBy(report => report.Participant_ID);
                    foreach (var oneevent in dblocations)
                    {
                        PreventionData.Add(
                            new PreventionOutcomes
                            {
                                Agency_Name = oneevent.Agency_Name,
                                Survey_Entered_By = oneevent.Survey_Entered_By,
                                //Survey_Completed = oneevent.Survey_Completed,

                                Survey_Completed = oneevent.Survey_Completed.HasValue ? oneevent.Survey_Completed.Value.ToString("MM/dd/yyyy") : "",

                                Survey = oneevent.Survey,
                                Participant_ID = oneevent.Participant_ID,
                                Survey_Type = oneevent.Survey_Type,
                                Program_Type = oneevent.Program_Type,
                                Program_Name = oneevent.Program_Name,
                                Group_Name = oneevent.Group_Name,
                                //intGender = oneevent.C1_Gender,
                                Gender = oneevent.C1_Gender,//==1?"Male":"Female",
                                C2_Hispanic_or_Latino = oneevent.C2_Hispanic_or_Latino,
                                //C3_Race = oneevent.C3_Race,

                                C3_1_White = oneevent.White > 0 ? 1 : 0,
                                C3_2_Black = oneevent.Black > 0 ? 1 : 0,
                                C3_3_AmericanIndian = oneevent.American_Indian > 0 ? 1 : 0,

                                C3_4_NativeHawaiian = oneevent.Native_Hawaiian > 0 ? 1 : 0,
                                NativeHawaiian_Text = oneevent.Other_Native_Text,
                                C3_5_Asian = oneevent.Asian > 0 ? 1 : 0,
                                C3_6_AlaskaNative = oneevent.Alaska_Native > 0 ? 1 : 0,
                                C3_7_Other = oneevent.other > 0 ? 1 : 0,
                                Other_Text = oneevent.Other_Text,
                                C3_8_NoResponse = oneevent.No_Response ,//> 0 ? 1 : 0,

                                
                               

                                

                                //C4_Date_of_Birth = oneevent.C4_Date_of_Birth,
                                C4_Day_of_Month_Born = oneevent.C4_Date_of_Birth,

                                C4_1_Age = oneevent.C4_1_Age,
                                C5_Military_Family = oneevent.C5_Military_Family,
                                C6_Smoke_Past_30_Days = oneevent.C6_Smoke_Past_30_Days,
                                C7_Other_Tobacco_Past_30_Days = oneevent.C7_Other_Tobacco_Past_30_Days,
                                C8_Alcohol_Past_30_Days = oneevent.C8_Alcohol_Past_30_Days,
                                C9_Marijuana_Past_30_Days = oneevent.C9_Marijuana_Past_30_Days,
                                C10_Prescription_Drugs_Past_30_Days = oneevent.C10_Prescription_Drugs_Past_30_Days,
                                C11_Synthetic_Drugs_Past_30_Days = oneevent.C11_Synthetic_Drugs_Past_30_Days,
                                C12_Illegal_Drugs_Past_30_Days = oneevent.C12_Illegal_Drugs_Past_30_Days,
                                C13_Daily_Cigarettes_Disapproval = oneevent.C13_Daily_Cigarettes_Disapproval,
                                C14_Daily_Cigarette_Pack_Disapproval = oneevent.C14_Daily_Cigarette_Pack_Disapproval,
                                C15_Trying_Marijuana_Disapproval = oneevent.C15_Trying_Marijuana_Disapproval,
                                C16_Using_Marijuana_Disapproval = oneevent.C16_Using_Marijuana_Disapproval,
                                C17_Daily_Drinking_Disapproval = oneevent.C17_Daily_Drinking_Disapproval,
                                C18_Binge_Drinking_Disapproval = oneevent.C18_Binge_Drinking_Disapproval,
                                C19_Prescription_Drug_Disapproval = oneevent.C19_Prescription_Drug_Disapproval,
                                C20_Cigarette_Pack_Risk = oneevent.C20_Cigarette_Pack_Risk,
                                C21_Marijuana_Risk = oneevent.C21_Marijuana_Risk,
                                C22_Binge_Drinking_Risk = oneevent.C22_Binge_Drinking_Risk,
                                C23_Prescription_Drug_Risk = oneevent.C23_Prescription_Drug_Risk,
                                C24_Drink_and_Drive = oneevent.C24_Drink_and_Drive,
                                C25_Drugs_and_Drive = oneevent.C25_Drugs_and_Drive,
                                C26_Family_Talk_Frequency = oneevent.C26_Family_Talk_Frequency,
                                C27_Work_for_Drug_Testing_Employer = oneevent.C27_Work_for_Drug_Testing_Employer,
                                C28_Hopeless = oneevent.C28_Hopeless,
                                C29_Restless = oneevent.C29_Restless,
                                C30_Suicide_Ideation = oneevent.C30_Suicide_Ideation,
                                C31_Satisfaction_Post_Test_Only = oneevent.C31_Satisfaction_Post_Test_Only != 0 ? Convert.ToString(oneevent.C31_Satisfaction_Post_Test_Only) : "",
                                C32_Client_Assessment_of_Program_Post_Test_Only = oneevent.C32_Client_Assessment_of_Program_Post_Test_Only
                            }
                            );
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, PreventionData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private HttpResponseMessage GetTreatmentData(string[] p)
        {
            try
            {
                Models.reportModel oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.reportModel>(p[0]);
                var TreatmentData = new List<TreatmentOutcomes>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    context.Database.CommandTimeout = 240;
                    string begindate = oneTime.BeginDate.ToString("MM/dd/yyyy");
                    string enddate = oneTime.EndDate.ToString("MM/dd/yyyy");
                    TreatmentData = context.suretool_GetTreatmentOutcomeReport1(begindate, enddate).Select(x => new TreatmentOutcomes
                    {
                        //Agency_Name = oneevent.Agency_Name,
                        //Survey_Administered_By = oneevent.Survey_Administered_By,
                        //Survey_Completed = oneevent.Survey_Completed,
                        //Survey = oneevent.Survey,
                        //Survey_Type = oneevent.Survey_Type,
                        //Service_Provided = oneevent.Service_Provided,
                        //Program_Name = oneevent.Program_Name,
                        //Case_Number = oneevent.Case_Number,
                        //Client_Entry_Date = oneevent.Client_Entry_Date,
                        //Comments = oneevent.Comments

                        Agency_Name = x.Agency_Name,
                        C1_1_Employment_Status = x.C1_1_Employment_Status,
                        C1_Employment_Status = x.C1_Employment_Status,
                        C2_2_Living_Situation = x.C2_2_Living_Situation,
                        C2_Living_Situation = x.C2_Living_Situation,
                        C3_Arrested_Past_Year = x.C3_Arrested_Past_Year,
                        C4_Arrested_Past_30_Days = x.C4_Arrested_Past_30_Days,
                        C5_Alcohol_Use_Past_30_Days = x.C5_Alcohol_Use_Past_30_Days,
                        C6_Illegal_Drug_Use_Past_30_Days = x.C6_Illegal_Drug_Use_Past_30_Days,
                        C7_Self_Help_Group = x.C7_Self_Help_Group,
                        Case_Number = x.Case_Number,
                        Client_Entry_Date = x.Client_Entry_Date,//          DbFunctions.TruncateTime((x.Client_Entry_Date),

                        //Client_Entry_Date = SqlFunctions.DateName("day", p.Client_Entry_Date) + "/" + SqlFunctions.DateName("month", p.Client_Entry_Date) + "/" + SqlFunctions.DateName("year", p.Client_Entry_Date),

                        //Client_Entry_Date = x.Client_Entry_Date.HasValue ? x.Client_Entry_Date.Value.ToString("MM/dd/yyyy") : "",

                        //Comments = x.Comments, 
                        Program_Name = x.Program_Name,
                        Service_Provided = x.Service_Provided,
                        Survey = x.Survey,
                        Survey_Administered_By = x.Survey_Administered_By,
                        Survey_Completed = x.Survey_Completed,
                        Survey_Type = x.Survey_Type
                    }).OrderBy(report => report.Agency_Name).ThenBy(report => report.Program_Name).
                        ThenBy(report => report.Case_Number).ToList();
                    //var dbData1 = dbData.AsEnumerable();
                    //TreatmentData = dbData1.
                    //    Where(report => Convert.ToDateTime(report.Client_Entry_Date) >= begindate).
                    //    Where(report => Convert.ToDateTime(report.Client_Entry_Date) <= enddate).
                    //    OrderBy(report => report.Agency_Name).ThenBy(report => report.Program_Name).
                    //    ThenBy(report => report.Case_Number).ToList();
                    if (oneTime.Organization != "All") //if (oneTime.Organization != "")
                    {
                        TreatmentData = TreatmentData.
                            Where(report => report.Agency_Name == oneTime.OrganizationName).ToList();
                    }
                    //Where(report => report.Client_Entry_Date >= oneTime.BeginDate).
                    //Where(report => report.Client_Entry_Date <= oneTime.EndDate);
                    //TreatmentData.OrderBy(report => report.Agency_Name).ThenBy(report => report.Program_Name).
                    //ThenBy(report => report.Case_Number);
                }


                return this.Request.CreateResponse(HttpStatusCode.OK, TreatmentData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private HttpResponseMessage GetutilizationRateData(string[] p)
        {
            try
            {
                Models.reportModel oneTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.reportModel>(p[0]);
                var TreatmentData = new List<UtilizationRate>();
                //var dblocationsCount=new List<Location>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = context.suretool_GetUtilizationRateReport(oneTime.BeginDate, oneTime.EndDate);
                    if (oneTime.Organization != "" && oneTime.Organization != "All")
                    {
                        dblocations = dblocations.
                            Where(report => report.Agency_Name == oneTime.OrganizationName);
                    }
                    dblocations.OrderBy(report => report.Agency_Name).ThenBy(report => report.Program_Facility_Name).
                    ThenBy(report => report.Client_ID).ToList();
                    foreach (var oneevent in dblocations)
                    {
                        //var dblocationsCount = context.tblLocations.Where(x => x.PKey == oneevent.Location_ID).OrderBy(x => x.FriendlyName_loc).SingleOrDefault();
                        //int? bcount = dblocationsCount.BedCount + dblocationsCount.AdultFemale + dblocationsCount.AdolescentMale + dblocationsCount.AdolescentFemale;
                        TreatmentData.Add(
                            new UtilizationRate
                            {

                                Report_Start_Date = oneevent.Report_Start_Date,
                                Report_End_Date = oneevent.Report_End_Date,
                                Agency_Name = oneevent.Agency_Name,
                                Location_ID = oneevent.Location_ID,
                                Program_Facility_Name = oneevent.Program_Facility_Name,
                                Client_ID = oneevent.Client_ID,
                                Client_Intake_Date = oneevent.Client_Intake_Date,
                                Client_Release_Date = oneevent.Client_Release_Date,
                                Duration_of_Client_Stay = oneevent.Duration_of_Client_Stay,
                                Number_of_Days_in_Reporting_Period = oneevent.Number_of_Days_in_Reporting_Period,
                                Capacity = oneevent.Capacity,
                                //Capacity=bcount.ToString(),
                            }
                            );
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, TreatmentData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
