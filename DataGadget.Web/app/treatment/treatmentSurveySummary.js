﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('treatmentSurveySummary', treatmentSurveySummary);

    //treatmentSurveySummary.$inject = ['$scope'];


    function treatmentSurveySummary($scope, $http, eventInfo, $modalInstance, $sce, $location) {
        $scope.title = 'treatmentSurveySummary';

        var vm = this;
        vm.onetime = {};

        activate();

        function activate() {

            vm.onGoingEvent = {};
            if (eventInfo) {
                // vm.onGoingEvent.id = eventInfo.id;
                $http({
                    method: 'POST', url: 'api/TreatmentSurveyTool',
                    data: { method: "GetBaseStringDataForReport", parameters: [JSON.stringify(eventInfo)] }
                })
                .success(function (result) {
                    // $scope.source = result;
                    var reportData = "data:application/pdf;base64," + result;
                    $scope.source = $sce.trustAsResourceUrl(reportData);
                })
                .error(function (error) {
                    console.log(error);
                })
            }
        }

        $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            $location.path('/dashboard');

        }
    }
})();
