﻿(function () {
    'use strict';
    var controllerId = 'statewideInitiativesList';
    angular
        .module('app')
        .controller(controllerId, statewideInitiativesList);

    statewideInitiativesList.$inject = ['$scope', '$http', '$modal', '$location', 'common', 'mySharedService', '$window'];

    function statewideInitiativesList($scope, $http, $modal, $location, common, sharedService, $window) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error')

        $scope.title = 'statewideInitiativesList';
        var vm = this;
        $scope.vm.statewides = sharedService.statewides;
        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "AllStateWideInitiatives", parameters: [userInfo.level, pageId] } })
         .success(function (result) {
             $scope.ispaging = false;
             vm.pagerData = result.item1[0];
             if (parseInt(vm.pagerData.numberOfPages) > 1) {
                 $scope.ispaging = true;
             }
             vm.pageList = [];
             if (pageId > 3) {
                 vm.pageList.push(pageId - 2);
                 vm.pageList.push(pageId - 1);
                 vm.pageList.push(pageId);
                 if (pageId < vm.pagerData.numberOfPages) {
                     vm.pageList.push(pageId + 1);
                     if (pageId < (vm.pagerData.numberOfPages - 1)) {
                         vm.pageList.push(pageId + 2);
                     }
                 }
             }
             else {
                 if (pageId == 1) {
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                     if (pageId + 4 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 4);
                     }

                 }
                 if (pageId == 2) {
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);

                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                 }
                 if (pageId == 3) {
                     vm.pageList.push(pageId - 2);
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                 }

             }

             var userData = result.item2;
             $scope.vm.statewides = userData;
             sharedService.statewides = userData;
         })
         .error(function (error) {
             console.log(error);
         })
        }
        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (userInfo.level != "SIT3U" && userInfo.level != "SIT2A") {
                paging(1);
            }
            else {
                $location.path('/dashboard');
            }
        }

        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                var userInfo = JSON.parse($window.localStorage["userInfo"]);
                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "SearchStateWideInitiatives", parameters: [userInfo.level, search] } })
                .success(function (result) {
                    $scope.vm.statewides = result;
                    sharedService.statewides = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    activate();
                }
            }
        }

        $scope.isSubmit = false;

        $scope.Add = function (statewideForm) {
            $scope.isSubmit = true;

            if (statewideForm.$valid) {

                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateStateWideInitiatives",
                        parameters: [JSON.stringify($scope.statewide)]
                    }
                })
                    .success(function (result) {
                        // $scope.vm.statewides.push(result);
                        sharedService.statewides.push(result);
                        $scope.vm.statewides = sharedService.statewides;
                    })
                    .error(function (error) {
                        console.log(error);
                    })
            }



        };

        vm.addUser = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/newStatewideInitiative.html',
                controller: 'newStatewideInitiative',
                resolve: {
                    statewideInfo: function () {
                        return undefined;
                    }
                }

            });
            addUserModal.result.then(function (user) {
                vm.statewides.push(user);

            }, function () {
            });
        }
        $scope.vm.edituser = function (user) {

            var addUserModal = $modal.open({
                templateUrl: 'app/admin/newStatewideInitiative.html',
                controller: 'newStatewideInitiative',
                resolve: {
                    statewideInfo: function () {
                        return user;
                    }
                }
            });

            addUserModal.result.then(function () {

            }, function () {
            });
        }



        $scope.vm.deleteuser = function (user) {

            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',

                backdrop: 'static',

            });


            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "DeleteStateWideInitiative", parameters: [user.id] } })

            .success(function (result) {

                //var i = vm.statewides.indexOf(user);
                //if (i != -1) {
                //    vm.statewides.splice(i, 1);
                //}

                activate();
                log("StateWideInitative deleted successfully");

            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {


            });


        };

    }


})();
