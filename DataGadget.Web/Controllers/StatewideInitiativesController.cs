﻿using DataGadget.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Security;
using DataGadget.Web.Models;

namespace DataGadget.Web.Controllers
{
    public class StatewideInitiativesController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {



                    case "GetSurveySericesListAsync":
                        return await GetSurveySericesListAsync();

                    case "GetSurveySericesListByLocId":
                        return await GetSurveySericesListByLocId(value.Parameters);

                    case "AllStateWideInitiatives":
                        return await GetAllStateWideInitiatives(value.Parameters);
                    case "SearchStateWideInitiatives":
                        return await SearchStateWideInitiatives(value.Parameters);
                    case "ValidateStateWideInitiatives":
                        return await ValidateStateWideInitiatives(value.Parameters);
                    case "CreateStateWideInitiatives":
                        return CreateStateWideInitiatives(value.Parameters);
                    case "GetStateWideInitiativeName":
                        return await GetStateWideInitiativeName(value.Parameters);

                    case "DeleteStateWideInitiative":
                        return DeleteStateWideInitiative(value.Parameters);


                    case "InitializeStateWideInitiativesForm":
                        return await InitializeUserForm();

                    case "AllResourcesLinks":
                        return await GetAllResourcesLinks(value.Parameters);
                    case "GetAllResourcesLinksDashBoard":
                        return await GetAllResourcesLinksDashBoard(value.Parameters);
                    case "CreateNewLink":
                        return CreateNewResourceLink(value.Parameters);

                    case "DeleteResourceLink":
                        return DeleteResourceLink(value.Parameters);

                    case "AllGrantYears":
                        return await GetAllGrantYears(value.Parameters);

                    case "CreateNewGrantYear":
                        return CreateNewGrantYear(value.Parameters);
                    case "DeleteGrantYear":
                        return DeleteGrantYear(value.Parameters);

                    case "AllProviders":
                        return await GetAllProviders(value.Parameters);
                    case "SearchProviders":
                        return await SearchProviders(value.Parameters);
                    case "CreateNewProvider":
                        return CreateNewProvider(value.Parameters);
                    case "DeleteProvider":
                        return DeleteProvider(value.Parameters);


                    case "AllLocationsBeds":
                        return await GetAllLocationsBeds(value.Parameters);

                    case "GetAllLocations":
                        return await GetAllLocations(value.Parameters);

                    case "SearchLocationsBeds":
                        return await SearchLocationsBeds(value.Parameters);

                    //case "GetAllRegionCode":
                    //    return await GetAllRegionCode(value.Parameters);

                    case "CreateNewBedCount":
                        return CreateBedCount(value.Parameters);
                    case "DeleteBedCount":
                        return DeleteBedCount(value.Parameters);

                    case "AllServiceLocation":
                        return await GetAllServiceLocation(value.Parameters);
                    case "SearchServiceLocation":
                        return await SearchServiceLocation(value.Parameters);
                    case "CreateNewServiceLocation":
                        return CreateNewServiceLocation(value.Parameters);
                    case "DeleteServiceLocation":
                        return DeleteServiceLocation(value.Parameters);
                    case "IsExistRegionCodeSiteCode":
                        return IsExistRegionCodeSiteCode(value.Parameters);


                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //WebLogger.Error("Exception In StatewideInitiativesController : " + ex.Message); 
                //return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }




        async private Task<HttpResponseMessage> InitializeUserForm()
        {

            var statewide = new statewideInitiative
            {

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
        }




        async private Task<HttpResponseMessage> ValidateStateWideInitiatives(string[] p)
        {
            try
            {
                var id = p[0];
                var Name = p[1];
                statewideInitiative statewide = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var res = await context.statewideInitiatives.FirstOrDefaultAsync(x => x.initiativeName == Name);

                    if (res != null)
                    {
                        statewide = new statewideInitiative
                        {
                            id = res.id,
                            initiativeName = res.initiativeName
                        };
                    }


                }
                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetAllStateWideInitiatives(string[] p)
        {
            try
            {
                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }

                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var statewides = new List<statewideInitiative>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.statewideInitiatives.OrderBy(x => x.initiativeName).CountAsync();
                    var dbUsers = await context.statewideInitiatives.OrderBy(x => x.initiativeName).Skip(skip).Take(take).ToListAsync();  //geting data from view
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                    foreach (var statewide in dbUsers)
                    {

                        statewides.Add(
                            new statewideInitiative
                            {
                                id = statewide.id,
                                initiativeName = statewide.initiativeName,
                                IsActive = statewide.IsActive

                            }
                            );
                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<statewideInitiative>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<statewideInitiative>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, statewides);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> SearchStateWideInitiatives(string[] p)
        {
            try
            {
                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }

                string search = p[1];

                var statewides = new List<statewideInitiative>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var dbUsers = await context.statewideInitiatives.Where(x => x.initiativeName.Contains(search)).OrderBy(x => x.initiativeName).ToListAsync();

                    foreach (var statewide in dbUsers)
                    {

                        statewides.Add(
                            new statewideInitiative
                            {
                                id = statewide.id,
                                initiativeName = statewide.initiativeName,
                                IsActive = statewide.IsActive

                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewides);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        private HttpResponseMessage CreateStateWideInitiatives(string[] p)
        {


            try
            {

                Models.stateInitiatives state = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.stateInitiatives>(p[0]);
                statewideInitiative statewide = new statewideInitiative();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {


                    if (state.id != null && state.id == 0)
                    {

                        statewide.id = Convert.ToInt32(state.id);
                        statewide.initiativeName = state.initiativeName;
                        statewide.IsActive = state.IsActive;
                        context.statewideInitiatives.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        DataGadget.Web.DataModel.statewideInitiative U = new DataGadget.Web.DataModel.statewideInitiative();
                        U = context.statewideInitiatives.Where(p1 => p1.id == state.id).SingleOrDefault();

                        U.id = state.id;
                        U.initiativeName = state.initiativeName;
                        U.IsActive = state.IsActive;

                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public HttpResponseMessage DeleteStateWideInitiative(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //i = context.suretool_DeleteStatewideInitiative(id);
                    //context.SaveChanges();

                    DataGadget.Web.DataModel.statewideInitiative U = new DataGadget.Web.DataModel.statewideInitiative();
                    U = context.statewideInitiatives.Where(p1 => p1.id == id).SingleOrDefault();

                    U.IsActive = false;

                    context.SaveChanges();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        async private Task<HttpResponseMessage> GetStateWideInitiativeName(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                statewideInitiative statewide = null;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var res = await context.statewideInitiatives.FirstOrDefaultAsync(x => x.id == id);
                    if (res != null)
                    {
                        statewide = new statewideInitiative { id = res.id, initiativeName = res.initiativeName };
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        async private Task<HttpResponseMessage> GetAllResourcesLinks(string[] p)
        {
            try
            {

                //if (p[0] == "SIT2A" || p[0] == "SIT3U")
                //{
                //    return null;
                //}
                var lstResourceLink = new List<ResourceLink>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbUsers = await context.Users.ToListAsync();  //geting data from view
                    var dbUsers = await context.ResourceLinks.OrderBy(x => x.ResourceName).ToListAsync();  //geting data from view
                    foreach (var rlink in dbUsers)
                    {

                        lstResourceLink.Add(
                            new ResourceLink
                            {
                                ResourceId = rlink.ResourceId,
                                ResourceName = rlink.ResourceName,
                                ResourceLink1 = rlink.ResourceLink1

                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstResourceLink);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> GetAllResourcesLinksDashBoard(string[] p)
        {
            try
            {

                var lstResourceLink = new List<ResourceLink>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbUsers = await context.Users.ToListAsync();  //geting data from view
                    var dbUsers = await context.ResourceLinks.OrderBy(x => x.ResourceName).ToListAsync();  //geting data from view
                    foreach (var rlink in dbUsers)
                    {

                        lstResourceLink.Add(
                            new ResourceLink
                            {
                                ResourceId = rlink.ResourceId,
                                ResourceName = rlink.ResourceName,
                                ResourceLink1 = rlink.ResourceLink1

                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstResourceLink);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage CreateNewResourceLink(string[] p)
        {

            try
            {
                Models.resourcesLinks state = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.resourcesLinks>(p[0]);
                ResourceLink statewide = new ResourceLink();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (state.resourceId == 0)
                    {
                        statewide.ResourceId = Convert.ToInt32(state.resourceId);
                        statewide.ResourceName = state.resourceName;
                        statewide.ResourceLink1 = state.resourceLink1;
                        statewide.CreatedBy = "Admin";
                        statewide.CreatedDate = System.DateTime.Now;
                        context.ResourceLinks.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        DataGadget.Web.DataModel.ResourceLink U = new DataGadget.Web.DataModel.ResourceLink();
                        U = context.ResourceLinks.Where(p1 => p1.ResourceId == state.resourceId).SingleOrDefault();
                        U.ResourceId = state.resourceId;
                        U.ResourceLink1 = state.resourceLink1;
                        U.ResourceName = state.resourceName;
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        async private Task<HttpResponseMessage> GetAllGrantYears(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var lstGrantYear = new List<GrantYear>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dbUsers = await context.Users.ToListAsync();  //geting data from view
                    var dbUsers = await context.GrantYears.OrderBy(x => x.GrantYearId).ToListAsync();  //geting data from view
                    foreach (var rlink in dbUsers)
                    {

                        lstGrantYear.Add(
                            new GrantYear
                            {
                                GrantYearId = rlink.GrantYearId,
                                GrantYear1 = rlink.GrantYear1,
                                StartDate = rlink.StartDate,
                                EndDate = rlink.EndDate
                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstGrantYear);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage CreateNewGrantYear(string[] p)
        {

            try
            {
                Models.grantYear grantyear = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.grantYear>(p[0]);
                GrantYear statewide = new GrantYear();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (grantyear.grantYearId == 0)
                    {
                        statewide.GrantYearId = Convert.ToInt32(grantyear.grantYearId);
                        statewide.GrantYear1 = grantyear.grantYear1;
                        statewide.StartDate = grantyear.startDate;
                        statewide.EndDate = grantyear.endDate;
                        statewide.IsActive = grantyear.isActive;
                        context.GrantYears.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        var yearID = Convert.ToInt32(grantyear.grantYearId);
                        var result = context.GrantYears.Where(x => x.GrantYearId == yearID).FirstOrDefault();
                        if (result != null)
                        {
                            result.GrantYear1 = grantyear.grantYear1;
                            result.StartDate = grantyear.startDate;
                            result.EndDate = grantyear.endDate;
                            result.IsActive = grantyear.isActive;
                            context.SaveChanges();
                        }
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage DeleteGrantYear(string[] p)
        {

            try
            {
                Models.grantYear grantyear = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.grantYear>(p[0]);
                GrantYear deletedGrantYear = new GrantYear();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var yearID = Convert.ToInt32(grantyear.grantYearId);
                    var result = context.GrantYears.Where(x => x.GrantYearId == yearID).FirstOrDefault();
                    if (result != null)
                    {
                        deletedGrantYear.GrantYearId = Convert.ToInt32(grantyear.grantYearId);
                        deletedGrantYear.GrantYear1 = grantyear.grantYear1;
                        deletedGrantYear.StartDate = grantyear.startDate;
                        deletedGrantYear.EndDate = grantyear.endDate;
                        deletedGrantYear.IsActive = grantyear.isActive;
                        context.GrantYears.Remove(result);
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, deletedGrantYear);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public HttpResponseMessage DeleteResourceLink(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var deletelink = context.ResourceLinks.Where(x => x.ResourceId == id).SingleOrDefault();

                    context.ResourceLinks.Remove(deletelink);

                    int num = context.SaveChanges();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #region "Providers"

        public HttpResponseMessage DeleteProvider(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var deletelink = context.tblProviders.Where(x => x.ID == id).SingleOrDefault();
                    deletelink.IsActive = false;
                    context.SaveChanges();

                }
                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        async private Task<HttpResponseMessage> GetAllProviders(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var lstProviders = new List<tblProvider>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.tblProviders.OrderBy(x => x.Name).CountAsync();
                    var dbProviders = await context.tblProviders.Where(x => x.IsActive == true).OrderBy(x => x.Name).Skip(skip).Take(take).ToListAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();
                    foreach (var rlink in dbProviders)
                    {

                        lstProviders.Add(
                            new tblProvider
                            {
                                ID = rlink.ID,
                                Name = rlink.Name,
                                IsActive = rlink.IsActive
                            }
                            );
                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<tblProvider>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<tblProvider>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstProviders);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        async private Task<HttpResponseMessage> SearchProviders(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                string search = p[1];

                var lstProviders = new List<tblProvider>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dbProviders = await context.tblProviders.Where(x => x.IsActive == true && x.Name.Contains(search)).OrderBy(x => x.Name).ToListAsync();

                    foreach (var rlink in dbProviders)
                    {

                        lstProviders.Add(
                            new tblProvider
                            {
                                ID = rlink.ID,
                                Name = rlink.Name,
                                IsActive = rlink.IsActive
                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstProviders);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage CreateNewProvider(string[] p)
        {

            try
            {
                Models.providerLinks state = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.providerLinks>(p[0]);
                tblProvider statewide = new tblProvider();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (state.providerID == 0)
                    {
                        statewide.ID = Convert.ToInt32(state.providerID);
                        statewide.Name = state.providerName;
                        statewide.CreatedOn = System.DateTime.Now;
                        statewide.UpdatedOn = System.DateTime.Now;
                        statewide.IsActive = true;
                        context.tblProviders.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        DataGadget.Web.DataModel.tblProvider U = new DataGadget.Web.DataModel.tblProvider();
                        U = context.tblProviders.Where(p1 => p1.ID == state.providerID).SingleOrDefault();
                        U.ID = state.providerID;
                        U.Name = state.providerName;
                        U.UpdatedOn = System.DateTime.Now;
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region "Bed Count"

        async private Task<HttpResponseMessage> GetAllLocationsBeds(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var lstProviders = new List<tblLocation>();
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.tblLocations.OrderBy(x => x.StateRegionCode).CountAsync();
                    var dbLocations = await context.tblLocations.Where(x => x.IsActive == true).OrderBy(x => x.StateRegionCode).ThenBy(x => x.SiteName).Skip(skip).Take(take).ToListAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var rlink in dbLocations)
                    {

                        lstProviders.Add(
                            new tblLocation
                            {
                                FriendlyName_loc = rlink.FriendlyName_loc,
                                PKey = rlink.PKey,
                                SiteName = rlink.SiteName,
                                StateRegionCode = rlink.StateRegionCode.Substring(rlink.StateRegionCode.Length - 2, 2),
                                AddressLine1 = rlink.AddressLine1,
                                AddressLine2 = rlink.AddressLine2,
                                City = rlink.City,
                                Zip = rlink.Zip,

                            }
                            );
                    }
                }

                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<tblLocation>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<tblLocation>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstProviders);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        async private Task<HttpResponseMessage> GetAllLocations(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var lstProviders = new List<tblLocation>();
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.tblLocations.OrderBy(x => x.StateRegionCode).CountAsync();
                    var dbLocations = await context.tblLocations.Where(x => x.IsActive == true).OrderBy(x => x.StateRegionCode).ThenBy(x => x.SiteName).Skip(skip).Take(take).ToListAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var rlink in dbLocations)
                    {

                        lstProviders.Add(
                            new tblLocation
                            {
                                PKey = rlink.PKey,
                                StateAbbrev = rlink.StateAbbrev,
                                StateRegionCode = rlink.StateRegionCode,
                                SiteCode = rlink.SiteCode,
                                SiteName = rlink.SiteName,
                                FriendlyName_loc = rlink.FriendlyName_loc,
                                AddressLine1 = rlink.AddressLine1,
                                AddressLine2 = rlink.AddressLine2,
                                City = rlink.City,
                                Zip = rlink.Zip,

                                SIG = rlink.SIG,
                                FS_MH = rlink.FS_MH,
                                FedRegionCode = rlink.FedRegionCode,
                                FedRegionName = rlink.FedRegionName,
                                IsActive = rlink.IsActive

                            }
                            );
                    }
                }

                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<tblLocation>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<tblLocation>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstProviders);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }






        async private Task<HttpResponseMessage> SearchLocationsBeds(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var lstProviders = new List<tblLocation>();
                string searchKey = Convert.ToString(p[1]);

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var dbLocations = await context.tblLocations.Where(x => x.IsActive == true && x.SiteName.Contains(searchKey) || x.FriendlyName_loc.Contains(searchKey)).OrderBy(x => x.StateRegionCode).ThenBy(x => x.SiteName).ToListAsync();


                    foreach (var rlink in dbLocations)
                    {

                        lstProviders.Add(
                            new tblLocation
                            {
                                //FriendlyName_loc = rlink.FriendlyName_loc,
                                //PKey = rlink.PKey,
                                //SiteName = rlink.SiteName,
                                //StateRegionCode = rlink.StateRegionCode.Substring(rlink.StateRegionCode.Length - 2, 2),
                                //AddressLine1 = rlink.AddressLine1,
                                //AddressLine2 = rlink.AddressLine2,
                                //City = rlink.City,
                                //Zip = rlink.Zip,


                                PKey = rlink.PKey,
                                StateAbbrev = rlink.StateAbbrev,
                                StateRegionCode = rlink.StateRegionCode,
                                SiteCode = rlink.SiteCode,
                                SiteName = rlink.SiteName,
                                FriendlyName_loc = rlink.FriendlyName_loc,
                                AddressLine1 = rlink.AddressLine1,
                                AddressLine2 = rlink.AddressLine2,
                                City = rlink.City,
                                Zip = rlink.Zip,

                                SIG = rlink.SIG,
                                FS_MH = rlink.FS_MH,
                                FedRegionCode = rlink.FedRegionCode,
                                FedRegionName = rlink.FedRegionName,
                                IsActive = rlink.IsActive


                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstProviders);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //get user services

        private List<LocationIOP> GetLocationServicesList(int id)
        {
            List<LocationIOP> LocationIopList = new List<LocationIOP>();
            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                LocationIopList = context.LocationIOPs.Where(x => x.LocationId == id).ToList();
                //Select(x => new LocationIOP
                //{
                //    LocationId = x.LocationId,
                //    IopId = x.IopId
                //}).ToList();
            }
            return LocationIopList;
        }

        async private Task<HttpResponseMessage> GetSurveySericesListAsync()
        {
            try
            {
                var lstsurveySerives = new List<surveySerivesModel>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.SurveyServices.OrderBy(x => x.Description).ToListAsync();
                    foreach (var item in dblocations)
                    {

                        lstsurveySerives.Add(
                            new surveySerivesModel { RecNo = item.RecNo, Description = item.Description.Trim() }
                            );
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, lstsurveySerives);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> GetSurveySericesListByLocId(string[] p)
        {
            try
            {
                int locid = Convert.ToInt32(p[0]);
                var lstsurveySerives = new List<LocationIOP>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = await context.LocationIOPs.Where(x => x.LocationId == locid).ToListAsync();
                    foreach (var item in dblocations)
                    {

                        lstsurveySerives.Add(
                            new LocationIOP { Id = item.Id, LocationId = item.LocationId, IopId = item.IopId }
                            );
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, lstsurveySerives);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public HttpResponseMessage DeleteBedCount(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    DataGadget.Web.DataModel.tblLocation U = new DataGadget.Web.DataModel.tblLocation();
                    U = context.tblLocations.Where(p1 => p1.PKey == id).SingleOrDefault();
                    //U.BedCount = 0;
                    context.SaveChanges();

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private HttpResponseMessage CreateBedCount(string[] p)
        {

            try
            {
                DataGadget.Web.DataModel.tblLocation state = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.DataModel.tblLocation>(p[0]);

                List<DataGadget.Web.Models.surveySerivesModel> stateSurveyServices = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DataGadget.Web.Models.surveySerivesModel>>(p[1]);
                List<DataGadget.Web.Models.surveySerivesModel> deleteSurveyServices = new List<DataGadget.Web.Models.surveySerivesModel>();

                tblLocation statewide = new tblLocation();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (state.PKey > 0)  //updateing
                    {

                        DataGadget.Web.DataModel.tblLocation U = new DataGadget.Web.DataModel.tblLocation();
                        U = context.tblLocations.Where(p1 => p1.PKey == state.PKey).SingleOrDefault();
                        U.PKey = state.PKey;
                        //U.BedCount = state.BedCount;
                        //U.AdolescentFemale = state.AdolescentFemale;
                        //U.AdolescentMale = state.AdolescentMale;
                        //U.AdultFemale = state.AdultFemale;

                        U.FriendlyName_loc = state.FriendlyName_loc;

                        U.StateAbbrev = state.StateAbbrev;
                        U.StateRegionCode = state.StateRegionCode;
                        U.SiteCode = state.SiteCode;
                        U.SiteName = state.SiteName;
                        U.AddressLine1 = state.AddressLine1;
                        U.AddressLine2 = state.AddressLine2;
                        U.City = state.City;

                        //U.City = state.City;
                        U.Zip = state.Zip;
                        U.SIG = state.SIG;
                        U.FS_MH = state.FS_MH;

                        U.FedRegionCode = state.FedRegionCode;
                        U.FedRegionName = state.FedRegionName;
                        U.IsActive = state.IsActive;

                        context.SaveChanges();

                        //survey services deleteing 

                        List<DataGadget.Web.DataModel.LocationIOP> UsrServiceList = new List<DataGadget.Web.DataModel.LocationIOP>();
                        UsrServiceList = context.LocationIOPs.Where(cat => cat.LocationId == state.PKey).ToList();
                        if (UsrServiceList != null)
                        {
                            context.LocationIOPs.RemoveRange(UsrServiceList);
                            try { context.SaveChanges(); }
                            catch { }
                        }
                    }

                    else  //inserting
                    {
                        //DataGadget.Web.DataModel.tblLocation U = new DataGadget.Web.DataModel.tblLocation();

                        int id = context.tblLocations.Max(x => x.PKey);
                        statewide.PKey = id + 1;


                        statewide.FriendlyName_loc = state.FriendlyName_loc;

                        statewide.StateAbbrev = state.StateAbbrev;
                        statewide.StateRegionCode = state.StateRegionCode;
                        statewide.SiteCode = state.SiteCode;
                        statewide.SiteName = state.SiteName;
                        statewide.AddressLine1 = state.AddressLine1;
                        statewide.AddressLine2 = state.AddressLine2;
                        statewide.City = state.City;


                        statewide.Zip = state.Zip;
                        statewide.SIG = state.SIG;
                        statewide.FS_MH = state.FS_MH;

                        statewide.FedRegionCode = state.FedRegionCode;
                        statewide.FedRegionName = state.FedRegionName;
                        statewide.IsActive = state.IsActive;
                        context.tblLocations.Add(statewide);
                        context.SaveChanges();

                    }

                    //adding servives 

                    List<DataGadget.Web.DataModel.LocationIOP> UserIOPList = new List<DataModel.LocationIOP>();
                    DataGadget.Web.DataModel.LocationIOP userIOP;
                    if (stateSurveyServices.Count > 0)
                    {
                        foreach (var item in stateSurveyServices)
                        {
                            if (item.IsSerivesSelected)
                            {
                                userIOP = new DataModel.LocationIOP();

                                userIOP.LocationId = state.PKey;

                                if (state.PKey == 0)
                                    userIOP.LocationId = statewide.PKey;
                                else
                                    userIOP.LocationId = state.PKey;

                                userIOP.IopId = item.RecNo;

                                UserIOPList.Add(userIOP);
                            }
                            else //if service id exist in userIop table 
                            {

                                //deleteusersfromUserIop(stateSurveyServices, state.PKey);
                                deleteSurveyServices.Add(item);

                            }

                        }
                        InsertLocationIops(UserIOPList);
                        if (state.PKey > 0)  //updateing
                        {
                            deleteusersfromUserIop(deleteSurveyServices, state.PKey);
                        }

                    }



                }

                return this.Request.CreateResponse(HttpStatusCode.OK, 1);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void InsertLocationIops(List<DataGadget.Web.DataModel.LocationIOP> UserIOPList)
        {
            try
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    DataGadget.Web.DataModel.LocationIOP U = new DataGadget.Web.DataModel.LocationIOP();

                    context.LocationIOPs.AddRange(UserIOPList);

                    context.SaveChanges();
                }
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void DeleteUserIop(int ID)
        {
            List<DataGadget.Web.DataModel.UserIOP> UsrServiceList = new List<DataGadget.Web.DataModel.UserIOP>();
            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {
                UsrServiceList = context.UserIOPs.Where(cat => cat.Id == ID).ToList();
                if (UsrServiceList != null)
                {
                    context.UserIOPs.RemoveRange(UsrServiceList);
                    try
                    {
                        context.SaveChanges();
                    }
                    catch { }
                }
            }

        }

        protected void deleteusersfromUserIop(List<DataGadget.Web.Models.surveySerivesModel> ServiceList, int? LocationID)
        {
            try
            {

                foreach (var item in ServiceList)
                {
                    // List<UserIOP> UserIops = new List<UserIOP>();

                    using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                    {
                        IEnumerable<UserIOP> UserI = (IEnumerable<UserIOP>)new List<UserIOP>();
                        // List<UserIOP> UserIops = new List<UserIOP>();
                        List<DataGadget.Web.DataModel.UserIOP> UserIops = new List<DataGadget.Web.DataModel.UserIOP>();
                        UserI = from U in context.TblUsers
                                join UI in context.UserIOPs on U.UserGUID equals UI.UserId
                                where U.LocationID == LocationID && UI.IopId == item.RecNo
                                select UI;
                        //select new UserIOP
                        //{
                        //    Id = UI.Id,
                        //    IopId = UI.IopId,
                        //    UserId = UI.UserId
                        //}).ToList();
                        foreach (DataGadget.Web.DataModel.UserIOP tp in UserI)
                        {
                            //DataGadget.Web.DataModel.UserIOP p = new DataGadget.Web.DataModel.UserIOP();
                            //p.Id = tp.Id;
                            //p.IopId = tp.IopId;
                            //p.UserId = tp.UserId;

                            //UserIops.Add(p);

                            DeleteUserIop(tp.Id);

                        }
                        //if (UserIops.Count() > 0)
                        //{
                        //    context.UserIOPs.RemoveRange(UserIops);
                        //    //context.SaveChanges();
                        //}
                    }



                }


            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region "Service Location"

        async private Task<HttpResponseMessage> GetAllServiceLocation(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;
                var lstProviders = new List<TblServicelocation>();

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.TblServicelocations.OrderBy(x => x.Servicelocation).CountAsync();
                    var dbLocations = await context.TblServicelocations.OrderBy(x => x.Servicelocation).Skip(skip).Take(take).ToListAsync();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();


                    foreach (var rlink in dbLocations)
                    {

                        lstProviders.Add(
                            new TblServicelocation
                            {
                                Servicelocation = rlink.Servicelocation,
                                IsActive = rlink.IsActive,
                                Id = rlink.Id

                            }
                            );
                    }
                }

                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<TblServicelocation>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<TblServicelocation>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, lstProviders);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception)
            {

                throw;
            }
        }
        async private Task<HttpResponseMessage> SearchServiceLocation(string[] p)
        {
            try
            {

                if (p[0] == "SIT2A" || p[0] == "SIT3U")
                {
                    return null;
                }
                string search = p[1];
                var lstProviders = new List<TblServicelocation>();

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    int rowCount = await context.TblServicelocations.OrderBy(x => x.Servicelocation).CountAsync();
                    var dbLocations = await context.TblServicelocations.Where(x => x.Servicelocation.Contains(search)).OrderBy(x => x.Servicelocation).ToListAsync();

                    foreach (var rlink in dbLocations)
                    {

                        lstProviders.Add(
                            new TblServicelocation
                            {
                                Servicelocation = rlink.Servicelocation,
                                IsActive = rlink.IsActive,
                                Id = rlink.Id

                            }
                            );
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, lstProviders);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private HttpResponseMessage CreateNewServiceLocation(string[] p)
        {

            try
            {
                Models.providerLinks state = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.providerLinks>(p[0]);
                TblServicelocation statewide = new TblServicelocation();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (state.providerID == 0)
                    {
                        statewide.Id = Convert.ToInt32(state.providerID);
                        statewide.Servicelocation = state.providerName;
                        statewide.IsActive = state.isActive;

                        context.TblServicelocations.Add(statewide);
                        context.SaveChanges();
                    }
                    else   //updating statewideinitiative
                    {
                        DataGadget.Web.DataModel.TblServicelocation U = new DataGadget.Web.DataModel.TblServicelocation();
                        U = context.TblServicelocations.Where(p1 => p1.Id == state.providerID).SingleOrDefault();
                        U.Id = state.providerID;
                        U.Servicelocation = state.providerName;
                        U.IsActive = state.isActive;
                        context.SaveChanges();
                    }

                }

                return this.Request.CreateResponse(HttpStatusCode.OK, statewide);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public HttpResponseMessage DeleteServiceLocation(string[] p)
        {

            try
            {
                var id1 = p[0];
                int id = Convert.ToInt32(id1);
                int i = 0;

                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    DataGadget.Web.DataModel.TblServicelocation U = new DataGadget.Web.DataModel.TblServicelocation();
                    U = context.TblServicelocations.Where(p1 => p1.Id == id).SingleOrDefault();
                    //context.TblServicelocations.Remove(U);
                    //int num = context.SaveChanges();


                    U.IsActive = false;
                    context.SaveChanges();


                }

                return this.Request.CreateResponse(HttpStatusCode.OK, i);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        #endregion

        private HttpResponseMessage IsExistRegionCodeSiteCode(string[] p)
        {

            try
            {
                DataGadget.Web.DataModel.tblLocation state = Newtonsoft.Json.JsonConvert.DeserializeObject<DataGadget.Web.DataModel.tblLocation>(p[0]);
                tblLocation statewide = new tblLocation();
                int id = 0;
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    if (state.PKey > 0)  //updateing
                    {

                        DataGadget.Web.DataModel.tblLocation U = new DataGadget.Web.DataModel.tblLocation();
                        U = context.tblLocations.Where(p1 => p1.PKey == state.PKey).SingleOrDefault();
                        if (U.StateRegionCode == state.StateRegionCode && U.SiteCode == state.SiteCode)
                        {
                            
                        }
                        else
                        {
                            id = context.tblLocations.Where(x => x.StateRegionCode == state.StateRegionCode && x.SiteCode == state.SiteCode).Count();
                        }
                    }
                    else  //inserting
                    {

                        id = context.tblLocations.Where(x => x.StateRegionCode == state.StateRegionCode && x.SiteCode == state.SiteCode).Count();
                    }
                }

                return this.Request.CreateResponse(HttpStatusCode.OK, id);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
