﻿(function () {
    'use strict';
    var controllerId = 'newOneTimeEvent';
    angular
        .module('app')
        .controller(controllerId, newOneTimeEvent);

    newOneTimeEvent.$inject = ['$scope', 'common', '$http', '$window', '$location', '$modal', '$route'];

    function newOneTimeEvent($scope, common, $http, $window, $location, $modal, $route) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.onetime = {};
        //$scope.onetime.programType_IP = "Individual";

        $scope.isdisplay = false;

        $scope.isdisplayCount = false;


        $scope.oneTimeEventForm3Submit = false;

        var isSelected = false;
        $scope.oneTimeEventForm1Submit = false;

        vm.onetime.oneTimeEventForm5Submit = false;

        vm.directServiceHoursEvent = function () {
            var directHours = parseFloat(vm.onetime.DirectServiceHours);
            if (directHours <= 0) {
                $scope.directServiceMessage = "Direct service hours must be greater than zero";
            }
            else {
                $scope.directServiceMessage = "";
            }
        }


        vm.helpEvent = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/common/newOneTimeDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',

            });
        };



        $scope.title = 'newOneTimeEvent';
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();

            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];



        $scope.programBaseList = [
               { name: 'Individual Based', value: 'Individual' },
               { name: 'Population Based', value: 'Population' },

        ];
        $scope.programBase = $scope.programBaseList[0]; // 0 -> Open 

        $scope.interventionTypeList = [
                       { name: 'Universal', value: 'Universal' },
                       { name: 'Selected', value: 'Selected' },
                       { name: 'Indicated', value: 'Indicated' },

        ];
        $scope.interventionType = $scope.interventionTypeList[0]; // 0 -> Open 





        // var vm = this;
        vm.programNREPP = [];
        vm.programState = [];
        vm.statewide = [];



        vm.programNames = [];

        var programType = '';
        var showProgram = false;
        activate();



        vm.back = function () {
            $window.history.back();
        }

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }

       function activate() {
            //$scope.beginDate = date();
            $scope.ProgramNameSourceList = [
            { name: 'State Approved', value: 'State' }
            ];

            vm.onetime.ProgramType_IP = 'Population';
            //vm.onetime.ProgramType_IP = 'Individual';


            vm.onetime.startDate = '';// formattedDate();//new Date();
            vm.onetime.endDate = '';//formattedDate();//new Date();

            vm.onetime.InterventionType = "Universal";

            vm.onetime.UniversalType_DI = "Indirect";
            vm.onetime.tyoeofprogram_ea = "Approved";
            //vm.onetime.ProgramNameSource = "State";

            vm.onetime.ProgramNameSource = $scope.ProgramNameSourceList[0].value;

            vm.onetime.CoalitionType = "State";
            vm.onetime.CoalitionType = "";


            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetProgramsAndStrategies", parameters: [vm.onetime.ProgramNameSource] }
            })
            .success(function (result) {
                vm.programNREPP = result;
            })
            .error(function (error) {
                console.log(error);
            })



            //$http({
            //    method: 'POST', url: 'api/oneTimeEvent',
            //    data: { method: "GetProgramsAndStrategies", parameters: ['State'] }
            //})
            //.success(function (result) {
            //    vm.programNREPP = result;
            //})
            //.error(function (error) {
            //    console.log(error);
            //})

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetStatewideInitiatives", parameters: '' }
            })
                       .success(function (result) {
                           // vm.statewide = result;
                           vm.onetime.lstStatewideInitiative = result;
                       })
                       .error(function (error) {
                           console.log(error);
                       })


            vm.onetime.serviceLocationsList = [];

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetServiceLocations", parameters: '' }
            })
           .success(function (result) {
               vm.onetime.serviceLocationsList = result;
           })
           .error(function (error) {
               console.log(error);
           })


        }

        vm.gotoStep1 = function () {

            document.querySelector('core-animated-pages').selected = 0;
        }


        vm.gotoStep2 = function (form, currentStep) {

            $scope.ismessage = "";
            $scope.isdisplay = false;
            var sdt = vm.onetime.startDate;
            var edt = vm.onetime.endDate;
            var issubmit = true;
            if (sdt !== undefined && edt !== undefined) {
                if (sdt > edt) {
                    $scope.ismessage = 'Please enter end date should be greater then start date'
                    issubmit = false;
                    $scope.isdisplay = true;
                }
            }


            if (currentStep == 1) {
                $scope.oneTimeEventForm1Submit = true;

                if (form.$valid && issubmit) {

                    document.querySelector('core-animated-pages').selected = 1;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 1;
            }

            //saveStep1();

        }
        vm.gotoStep3 = function (oneTimeEventForm2, step) {
            //

            var i = vm.onetime.totalParticipant;
            //document.querySelector('core-animated-pages').selected = 2;

            ////if (currentStep == 2)
            ////{
            ////    alert(currentst);
            ////    if (form.$valid) {

            ////        document.querySelector('core-animated-pages').selected = 2;

            ////    }

            ////}

            if (i > 0) {
                $scope.isdisplay = false;
                if (oneTimeEventForm2) {
                    oneTimeEventForm2.$submitted = true;
                    if (oneTimeEventForm2.$valid && !oneTimeEventForm2.$invalid) {
                        document.querySelector('core-animated-pages').selected = 2;
                    }
                }
                else {
                    document.querySelector('core-animated-pages').selected = 2;
                }
            }
            else {
                $scope.isdisplay = true;
            }


        }
        vm.gotoStep4 = function () {

            document.querySelector('core-animated-pages').selected = 3;
        }


        //vm.gotoStep5 = function (form, currentStep) {

        //    //if (vm.onetime.DirectServiceHours != null) {             

        //    //    form.$valid = true;
        //    //}
        //    //else {

        //    //    form.$valid = false;

        //    //}

        //    if (currentStep == 5) {
        //        $scope.oneTimeEventForm5Submit = true;

        //        if (form.$valid) {

        //            document.querySelector('core-animated-pages').selected = 4;
        //        }
        //    }
        //    else {
        //        document.querySelector('core-animated-pages').selected = 4;
        //    }

        //}

        vm.gotoStep5 = function (form, currentStep) {

            if (form) {


                form.$valid = vm.onetime.DirectServiceHours != null;

                $scope.oneTimeEventForm5Submit = true;

                if (form.$valid) {

                    document.querySelector('core-animated-pages').selected = 4;
                }
            }

            else {
                document.querySelector('core-animated-pages').selected = 4;
            }
        }

        vm.showProgName = function () {

            $scope.ProgramNameSourceList = [];

            if (vm.onetime.tyoeofprogram_ea == 'Evidence') {
                $scope.ProgramNameSourceList = [
                             { name: 'State Approved', value: 'State' }, { name: 'NREPP', value: 'NREPP' }
                ];
            }
            else {
                $scope.ProgramNameSourceList = [{ name: 'State Approved', value: 'State' }];
            }

            vm.onetime.ProgramNameSource = $scope.ProgramNameSourceList[0].value;
            vm.onetime.programName = '';

            vm.onetime.programNREPP = "";

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetProgramsAndStrategies", parameters: [vm.onetime.ProgramNameSource] }
            })
           .success(function (result) {
               vm.onetime.programNREPP = result;
           })
           .error(function (error) {
               console.log(error);
           })



        }

        vm.stateApproveChange = function () {
            vm.onetime.programNREPP = "";

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: { method: "GetProgramsAndStrategies", parameters: [vm.onetime.ProgramNameSource] }
            })
           .success(function (result) {
               vm.onetime.programNREPP = result;
           })
           .error(function (error) {
               console.log(error);
           })

        }





        vm.save1 = function (form, currentStep) {

            var userId = localStorage.getItem("userId");
            vm.onetime.userGUID = userId;
            if (vm.onetime.Males === undefined)
                vm.onetime.Males = 0;
            if (vm.onetime.Females === undefined)
                vm.onetime.Females = 0;
            vm.onetime.NumberParticipants = vm.onetime.Males + vm.onetime.Females;

            vm.onetime.startDate = GetFormattedDate(new Date(vm.onetime.startDate));
            vm.onetime.endDate = GetFormattedDate(new Date(vm.onetime.endDate));

            $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: {
                    method: "SaveEventDetails",
                    parameters: [JSON.stringify(vm.onetime)]

                }
            })
            .success(function (result) {
                //vm.statewide = result;
                //$location.path('/oneTimeEvents');
                //$route.reload();

                var addUserModal = $modal.open({
                    templateUrl: 'app/prevention/printsummary.html',
                    controller: 'printsummary',
                    backdrop: 'static',
                    resolve: {
                        eventInfo: function () {
                            return result;
                        }
                    }

                });

            })
            .error(function (error) {
                console.log(error);
                $location.path('/oneTimeEvents');
                $route.reload();
            })

            //$location.path('/oneTimeEvents');
            //$route.reload();



        }




        vm.save = function (form, currentStep) {

            // alert('hi');

            if (currentStep == 3) {

                $scope.oneTimeEventForm3Submit = true;

                if (form.$valid) {

                    // document.querySelector('core-animated-pages').selected = 3;



                    var userId = localStorage.getItem("userId");
                    vm.onetime.userGUID = userId;

                    $http({
                        method: 'POST', url: 'api/oneTimeEvent',
                        data: {
                            method: "SaveEventDetails",
                            parameters: [JSON.stringify(vm.onetime)]

                        }
                    })
                    .success(function (result) {
                        vm.statewide = result;
                    })
                    .error(function (error) {
                        console.log(error);
                    })

                    $location.path('/oneTimeEvents');

                }

                else {

                    //  alert('False');
                }



            }


            //var userId = localStorage.getItem("userId");
            //vm.onetime.userGUID = userId;

            //$http({
            //    method: 'POST', url: 'api/oneTimeEvent',
            //    data: {
            //        method: "SaveEventDetails",
            //        parameters: [JSON.stringify(vm.onetime)]

            //    }
            //})
            //.success(function (result) {
            //    vm.statewide = result;
            //})
            //.error(function (error) {
            //    console.log(error);
            //})

            //$location.path('/oneTimeEvents');
        }



        function saveStep1() {
            //
            var userId = localStorage.getItem("userId");
            vm.onetime.userGUID = userId;

            return $http({
                method: 'POST', url: 'api/oneTimeEvent',
                data: {
                    method: "SaveEventDetails",
                    parameters: [JSON.stringify(vm.onetime)]
                }
            });
            //.success(function (result) {
            //    vm.statewide = result;
            //})
            //.error(function (error) {
            //    console.log(error);
            //})

            // $location.path('/oneTimeEvents');
        }



    }
})();
