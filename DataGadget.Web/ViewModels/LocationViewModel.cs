﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.ViewModels
{
    public class LocationViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string RegionCode { get; set; }

        public string StateAbbrev { get; set; }

        public string SiteCode { get; set; }

        public string FriendlyName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Zip { get; set; }

        public string SIG { get; set; }

        public string FS_MH { get; set; }

        public string FedRegionName { get; set; }

        public string FedRegionCode { get; set; }

        public int BedCount { get; set; }
    }

    public class BedsCount
    {
        public int ? TotalBeds { get; set; }
        public int ? AllocatedBeds { get; set; }
        public int? WaitingClients { get; set; }
    }
}