﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGadget.Web.Models
{
    public class SurveyClientResponsesVM
    {
        public int RecNo { get; set; }
        public int SurveyItemID { get; set; }
        public int SurveyResponseItemID { get; set; }
        public string OtherResponse { get; set; }
    }
}
