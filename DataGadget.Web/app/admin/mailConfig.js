﻿

(function () {
    'use strict';

    angular
        .module('app')
        .controller('mailConfig', mailConfig);

    mailConfig.$inject = ['$scope'];

    mailConfig.$inject = ['$scope', '$http', 'common', '$location', '$route', '$window'];

    function mailConfig($scope, $http, common, $location, $route, $window) {
        $scope.title = 'mailConfig';

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn('MonthlyMailSettings');
        var logError = getLogFn('MonthlyMailSettings', 'error');


        var vm = this;
        vm.links = [];
        $scope.message = {};

        activate();

        function activate() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $scope.userId = userInfo.userId;
            $http({ method: 'POST', url: 'api/User', data: { method: "GetMonthlyMailDetails" } })
                .success(function (result) {
                    vm.links = result;
                    if (result != null) {
                        $scope.message=result
                    }
                })
                .error(function (error) {
                    console.log(error);
                })
        }

        $scope.ok = function (userForm) {
            $scope.isSubmit = true;
            $scope.message.updatedBy = $scope.userId;
            if (userForm.$valid) {
                $http({
                    method: 'POST', url: 'api/user',
                    data: {
                        method: "CreateMonthlyMessages",
                        parameters: [JSON.stringify($scope.message)]
                    }
                })
                    .success(function (result) {

                        //$location.path('/Dashboard');
                        //$route.reload();

                        log("Mail settings has been saved succussfully.");

                    })
                    .error(function (error) {
                        console.log(error);
                    })

                //$location.path('/Dashboard');
                //$route.reload();
            }

        };


    }
})();
