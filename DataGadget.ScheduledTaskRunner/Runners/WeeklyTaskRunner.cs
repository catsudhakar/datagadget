﻿
using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using System.Configuration;


namespace DataGadget.ScheduledTaskRunner.Runners
{
    [ScheduledTaskTag(Tag = "weekly")]
    public class WeeklyTaskRunner : ITaskRunner
    {
        public async Task Run(params string[] args)
        {
            Console.WriteLine("Enter Into run method");
            Logger.Info(string.Format("Weekly task started at {0}", DateTime.Now));
            await ComposeMailAsync();

        }

        private async Task ComposeMailAsync()
        {
            bool b = true;
            StringBuilder sb;
            MailPatientContacts model;
            List<MailPatientContacts> lstPatientContacts;

            Console.WriteLine("Enter Into ComposeMailAsync method");

            try
            {
                using (datagadget_testEntities dbContext = new datagadget_testEntities())
                {
                    var grantYear = dbContext.GrantYears.FirstOrDefault();
                    var startDate = grantYear.StartDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var endDate = grantYear.EndDate.ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                    var query = "";

                    var record = dbContext.tblEmailTemplates.SingleOrDefault();

                    var users = new List<DataGadget.Web.Models.NewUser>();
                    var dbUsers = new List<TblUser>();
                    dbUsers = dbContext.TblUsers.ToList();// await dbContext.TblUsers.Where(x => x.UserLevel == "SIT2A").OrderBy(x => x.LocationID).ToListAsync();


                    users = dbUsers.Select(user => new DataGadget.Web.Models.NewUser
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        EmailId = user.Email_addr,
                        UserId = user.UserGUID,
                        UserName = user.LoginID,
                        LocationId = user.LocationID,
                        AgencyName = user.tblLocation.FriendlyName_loc,
                        Password = user.PW,
                        Level = user.UserLevel,
                        Degree = user.Degree,
                        Experience = user.YrsPrevExp,
                        OfficePhoneNum = user.OfficePhone,
                        OfficeFaxNum = user.OfficeFax,


                    }).ToList();

                    foreach (var item in users.Where(x => x.Level == "SIT2A"))  //&& x.LocationId==32
                    {

                        Console.WriteLine("Enter Into for each method");

                        sb = new StringBuilder();
                        lstPatientContacts = new List<MailPatientContacts>();

                        //FC.FollowupDate ,Fc.FollowupComment  left join FollowUpContacts FC on C.PAT_ID=FC.PatientId

                        //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,
                        //                                        SI.AdminDate as SurveyDate,'Discharge' as SurveyName, from  
                        //                                        SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                        inner Join Client C on SC.ClientID=C.RecNo
                        //                                        where SI.LocationID=" + item.LocationId + " and SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "')  order by SI.RecNo desc";

                        //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,
                        //                                    SI.AdminDate as SurveyDate,'Discharge' as SurveyName,FC.FollowupDate ,Fc.FollowupComment from  
                        //                                    SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                    inner Join Client C on SC.ClientID=C.RecNo
                        //                                    left join FollowUpContacts FC on C.PAT_ID=FC.PatientId
                        //                                    where SI.LocationID=31 and SurveyTypeID=2 AND (EntryDate >= '5/30/2014 6:30:00 PM' and EntryDate <='6/30/2015 6:30:00 PM')  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3)   order by SI.RecNo desc";

                        //Comments on Oct1
                        //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate,FC.FollowupComment,FC.FollowupDate,FC.FollowupId  from 
                        //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                      inner Join Client C on SC.ClientID=C.RecNo left join FollowUpContacts FC on c.PAT_ID=FC.PatientId
                        //                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID =" + item.LocationId + "  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.RecNo desc";

                        query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate,FC.FollowupComment,FC.FollowupDate,FC.FollowupId,ss.Description  from 
                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                                      inner Join Client C on SC.ClientID=C.RecNo 
                                      left join FollowUpContacts FC on c.PAT_ID=FC.PatientId
                                      left join SurveyService ss on si.ServiceID=ss.RecNo  
                                      where SurveyTypeID=2 and si.DischargeId=2 and si.DischargeDate is not null  AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID =" + item.LocationId + "  And  C.PAT_ID   not in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) >= 3) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.RecNo desc";

                        //                                query = @"select SI.RecNo as SurveyItemId,C.PAT_ID as PatientId,SI.SurveyAdminID as SurveyAdmin,'Discharge' as SurveyName,SI.EntryDate from 
                        //                                      SurveyItem SI inner join SurveyClient SC on SI.SurveyClientID=SC.RecNo
                        //                                      inner Join Client C on SC.ClientID=C.RecNo
                        //                                      where SurveyTypeID=2 AND (EntryDate >= '" + startDate + "' and EntryDate <='" + endDate + "') and LocationID =" + item.LocationId + "  And C.PAT_ID   in (select (PatientId) from FollowUpContacts group by PatientId having  count(PatientId) < 3 ) and C.PAT_ID not in (select patientid from followupcontacts group by patientid having max(HasSurveyDone) != 0) order by SI.RecNo desc";




                        var dataResult = dbContext.Database.SqlQuery<MailPatientContacts>(query);
                        if (dataResult != null)
                        {
                            var dataModel = dataResult.ToList();
                            foreach (MailPatientContacts r in dataModel)
                            {
                                model = new MailPatientContacts();
                                model.PatientId = r.PatientId;
                                model.SurveyAdmin = r.SurveyAdmin;
                                model.SurveyAdminName = dbContext.TblUsers.Where(x => x.UserGUID == r.SurveyAdmin).SingleOrDefault().FirstName;
                                model.SurveyDate = r.SurveyDate;
                                model.SurveyName = r.SurveyName;
                                model.SurveyItemId = r.SurveyItemId;
                                model.FollowupDate = r.FollowupDate;
                                model.FollowupComment = r.FollowupComment;
                                model.FollowupId = r.FollowupId;
                                model.Description = r.Description;
                                lstPatientContacts.Add(model);
                            }
                        }


                        if (lstPatientContacts.Count > 0)
                        {
                            sb.Append("<table style='width:650px; border:1px solid #111111;font-family:calibri;font-size:14px; margin:0 auto;margin-left:15px'>");
                            sb.Append("<tr style='height:30px;'><th  width='100px' style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>PatientId</th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Survey Type </th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Followup </th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Followup Date</th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Followup Comment</th>");
                            sb.Append("<th style='background-color:#d9534f;font-family:calibri;font-size:14px; font-weight:bold;'>Survey Admin Name </th></tr>");
                            foreach (MailPatientContacts m in lstPatientContacts)
                            {

                                sb.Append("<tr style='height:30px'><td align='center' width='100px' style='font-family:calibri;font-size:14px;'>" + m.PatientId + "</td>");
                                sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.Description + "</td>");
                                if (m.FollowupId.HasValue)
                                {
                                    if(m.FollowupId==1)
                                    {
                                        sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>30 Days</td>");
                                    }else if(m.FollowupId==2)
                                    {
                                        sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>60 Days</td>");
                                    }
                                    
                                }
                                else
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>Not Started</td>");
                                }

                                if (m.FollowupDate.HasValue)
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + String.Format("{0:MM/dd/yyyy}", m.FollowupDate) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>N/A</td>");
                                }


                                if (m.FollowupComment != null )
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.FollowupComment  + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>N/A</td>");
                                }

                                if (m.SurveyAdminName != null )
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>" + m.SurveyAdminName  + "</td></tr>");
                                }
                                else
                                {
                                    sb.Append("<td align='center' style='font-family:calibri;font-size:14px; '>N/A</td>");
                                }

                            }

                            sb.Append("</table>");


                            string subject = "Weekly Email";
                            string mailBody = record.Template + sb.ToString();
                           // item.EmailId = "sudhakar@cattechnologies.com";
                            SendMail(item.EmailId, subject, mailBody);
                        }
                    }


                }
            }

            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }


        }
        private void SendMail(string toAddress, string subject, string body)
        {

            Console.WriteLine("Enter Into sendmail  method");
            //MailMessage mail = new MailMessage();
            ////mail.From = new MailAddress("rammohan.test1@gmail.com");

            ////string addressFrom = System.Configuration.ConfigurationSettings.AppSettings["generalEmailFromAddress"];
            ////string password = System.Configuration.ConfigurationSettings.AppSettings["emailPassword"];

            //// mail.To.Add(new MailAddress(toAddress));
            //mail.To.Add(new MailAddress("sudhakar@cattechnologies.com"));

            //SmtpClient client = new SmtpClient("smtp.gmail.com");
            //client.Port = 587;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.UseDefaultCredentials = false;
            //client.EnableSsl = true;
            //client.Credentials = new NetworkCredential("rammohan.test1@gmail.com", "cattesting");
            ////client.Credentials = new NetworkCredential(addressFrom, password);
            //mail.Subject = subject;
            //mail.Body = body;
            //mail.IsBodyHtml = true;
            //client.Send(mail);

           


            try
            {



                using (SmtpClient client = new SmtpClient())
                using (MailMessage message = new MailMessage())
                {
                   // toAddress = "sudhakar@cattechnologies.com";
                    message.To.Add(toAddress); // BREAKPOINT will be here
                    message.IsBodyHtml = true;
                    message.Subject = subject;
                    message.Body = body;

                    try
                    {
                        // send the email
                        client.Send(message);
                        Logger.Info("Mail sent to " + toAddress);

                        Console.WriteLine("Mail sent to " + toAddress);
                    }
                    catch (SmtpException ex)
                    {
                        Logger.Error(ex);
                        Console.WriteLine("Error" + toAddress);

                    }
                }


            }
            catch (Exception ex)
            {

            }
        }
    }
}
