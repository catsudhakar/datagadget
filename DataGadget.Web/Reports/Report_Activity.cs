﻿using System;
using System.Collections.Generic;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_Activity : Report_Base
    {
        
        public Report_Activity()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;

        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }



        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;	
            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Activity Report"));

            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);



            //this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataTable dtData = new DataTable();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_Activity ");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");
           

            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dtData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dtData);
                    }
                }
            }


            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(1);
            PdfPCell cell;

            int cCnt = 0;

            //---------------------------------------------------------------------------------------------------

            foreach (DataRow drData in dtData.Rows)
            {
                //cell = new PdfPCell(CreateEntryTable(drData));
                //table.AddCell(cell);

                Paragraph pdfParagraph = NewParagraph("");
                pdfParagraph.Add(CreateEntryTable(drData));
                this._pdfDoc.Add(pdfParagraph);

            }


            //---------------------------------------------------------------------------------------------------

            return table;
        }

        private PdfPTable CreateEntryTable(DataRow drData)
        {
            PdfPTable table = new PdfPTable(6);
            PdfPCell cell;


            table.KeepTogether = true;
            table.SpacingBefore = 10.0F;

            cell = new PdfPCell(NewPhrase(drData["OrgName"].ToString()));
            cell.Colspan = 6;
            table.AddCell(cell);
            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(drData["name"].ToString()));
            cell.Colspan = 6;
            table.AddCell(cell);
            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Begin Date", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDateTime(drData["StartDate"]).ToShortDateString()));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("End Date", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDateTime(drData["EndDate"]).ToShortDateString()));
            table.AddCell(cell);


            cell = new PdfPCell(NewPhrase(" ", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(" ", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Group Name", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["GroupName"].ToString()));
            cell.Colspan = 5;
            table.AddCell(cell);

            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Program Type", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["ProgramType_IP"].ToString()));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Intervention Type", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["InterventionType"].ToString()));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Universal Type", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["UniversalType_DI"].ToString()));
            table.AddCell(cell);

            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Participants Served", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["numberParticipants"].ToString()));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("CSAP Strategies", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            StringBuilder sbStrats = new StringBuilder();

            if (Convert.ToBoolean(drData["Strat_ID_01"]))
                sbStrats.Append("ID01,");

            if (Convert.ToBoolean(drData["Strat_ID_02"]))
                sbStrats.Append("ID02,");

            if (Convert.ToBoolean(drData["Strat_ID_03"]))
                sbStrats.Append("ID03,");

            if (Convert.ToBoolean(drData["Strat_ID_04"]))
                sbStrats.Append("ID04,");

            if (Convert.ToBoolean(drData["Strat_ID_05"]))
                sbStrats.Append("ID05,");

            if (Convert.ToBoolean(drData["Strat_ID_06"]))
                sbStrats.Append("ID06,");

            if (Convert.ToBoolean(drData["Strat_ID_07"]))
                sbStrats.Append("ID07,");

            if (Convert.ToBoolean(drData["Strat_ID_08"]))
                sbStrats.Append("ID08,");

            if (Convert.ToBoolean(drData["Strat_ID_09"]))
                sbStrats.Append("ID09,");

            if (Convert.ToBoolean(drData["Strat_Ed_11"]))
                sbStrats.Append("ED11,");

            if (Convert.ToBoolean(drData["Strat_Ed_12"]))
                sbStrats.Append("ED12,");

            if (Convert.ToBoolean(drData["Strat_Ed_13"]))
                sbStrats.Append("ED13,");

            if (Convert.ToBoolean(drData["Strat_Ed_14"]))
                sbStrats.Append("ED14,");

            if (Convert.ToBoolean(drData["Strat_Ed_15"]))
                sbStrats.Append("ED15,");

            if (Convert.ToBoolean(drData["Strat_Ed_16"]))
                sbStrats.Append("ED16,");

            if (Convert.ToBoolean(drData["Strat_Ed_17"]))
                sbStrats.Append("ED17,");

            if (Convert.ToBoolean(drData["Strat_Alt_21"]))
                sbStrats.Append("ALT21,");

            if (Convert.ToBoolean(drData["Strat_Alt_22"]))
                sbStrats.Append("ALT22,");

            if (Convert.ToBoolean(drData["Strat_Alt_23"]))
                sbStrats.Append("ALT23,");

            if (Convert.ToBoolean(drData["Strat_Alt_24"]))
                sbStrats.Append("ALT24,");

            if (Convert.ToBoolean(drData["Strat_Alt_25"]))
                sbStrats.Append("ALT25,");

            if (Convert.ToBoolean(drData["Strat_Alt_26"]))
                sbStrats.Append("ALT26,");

            if (Convert.ToBoolean(drData["Strat_Alt_27"]))
                sbStrats.Append("ALT27,");

            if (Convert.ToBoolean(drData["Strat_IR_31"]))
                sbStrats.Append("IR31,");

            if (Convert.ToBoolean(drData["Strat_IR_32"]))
                sbStrats.Append("IR32,");

            if (Convert.ToBoolean(drData["Strat_IR_34"]))
                sbStrats.Append("IR34,");

            if (Convert.ToBoolean(drData["Strat_CBP_41"]))
                sbStrats.Append("CBP41,");

            if (Convert.ToBoolean(drData["Strat_CBP_42"]))
                sbStrats.Append("CBP42,");

            if (Convert.ToBoolean(drData["Strat_CBP_43"]))
                sbStrats.Append("CBP43,");

            if (Convert.ToBoolean(drData["Strat_CBP_44"]))
                sbStrats.Append("CBP44,");

            if (Convert.ToBoolean(drData["Strat_CBP_45"]))
                sbStrats.Append("CBP45,");

            if (Convert.ToBoolean(drData["Strat_CBP_46"]))
                sbStrats.Append("CBP46,");

            if (Convert.ToBoolean(drData["Strat_Env_51"]))
                sbStrats.Append("ENV51,");

            if (Convert.ToBoolean(drData["Strat_Env_52"]))
                sbStrats.Append("ENV52,");

            if (Convert.ToBoolean(drData["Strat_Env_53"]))
                sbStrats.Append("ENV53,");

            if (Convert.ToBoolean(drData["Strat_Env_54"]))
                sbStrats.Append("ENV54,");

            if (Convert.ToBoolean(drData["Strat_Env_55"]))
                sbStrats.Append("ENV55,");

            if (Convert.ToBoolean(drData["Strat_Env_56_MW"]))
                sbStrats.Append("ENV56,");

            if (Convert.ToBoolean(drData["Strat_Env_57"]))
                sbStrats.Append("ENV57,");

            try
            {
                sbStrats.Remove(sbStrats.Length - 1, 1);
            }
            catch { }

            cell = new PdfPCell(NewPhrase(sbStrats.ToString()));
            cell.Colspan = 4;
            table.AddCell(cell);


            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Travel Hours", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["TravelHours"].ToString()));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Prep Hours", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["PrepHours"].ToString()));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Direct Service Hours", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(drData["DirectServiceHours"].ToString()));
            table.AddCell(cell);

            //------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("State-wide Initiative", Font.HELVETICA, 7, Font.BOLD));
            table.AddCell(cell);

            if (drData["TargetedStatewideInitiative"].ToString() == "")
                cell = new PdfPCell(NewPhrase("n/a"));
            else
                cell = new PdfPCell(NewPhrase(drData["TargetedStatewideInitiative"].ToString()));

            cell.Colspan = 5;
            table.AddCell(cell);



            //------------------------------------------------------------------------------


            return table;

        }


    }
}
