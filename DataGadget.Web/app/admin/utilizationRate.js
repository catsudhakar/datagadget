﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('utilizationRate', utilizationRate);

    utilizationRate.$inject = ['$scope', '$http', '$location', '$window'];

    function utilizationRate($scope, $http, $location, $window) {
        $scope.title = 'utilizationRate';

        var vm = this;
        vm.onetime = {};


        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }

        $scope.ismessage = "";
        activate();

        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }


            vm.onetime.locationList = "";
            //vm.onetime.beginDate = formattedDate();//new Date();
            //vm.onetime.endDate = formattedDate();//new Date();


            var userInfo = JSON.parse(localStorage.getItem('userInfo'));

            $http({ method: 'POST', url: 'api/ExportData', data: { method: "InitializeUserForm", parameters: ["UtilizationRate", userInfo.level, userInfo.locationId, userInfo.stateabbrev] } })
             .success(function (result) {
                 vm.onetime.locationList = result.locationList;
                 vm.onetime.organization = 'All';


             });
        }

        vm.exportData = function () {
            var res = vm.onetime.organization
            vm.onetime.organizationName = vm.onetime.organization;

            var sdt = vm.onetime.beginDate;
            var edt = vm.onetime.endDate;
            $scope.ismessage = "";

            if (sdt === undefined && edt === undefined) {
                $scope.ismessage = 'Please enter start date and end date';
                return false;
            }
            if (sdt !== undefined && edt !== undefined) {
                var sday = sdt.getDate();
                var smonth = sdt.getMonth();
                var syear = sdt.getFullYear();

                var eday = edt.getDate();
                var emonth = edt.getMonth();
                var eyear = edt.getFullYear();

                var sdate = new Date();
                sdate.setFullYear(syear, smonth, sday);
                var edate = new Date();
                edate.setFullYear(eyear, emonth, eday);
                if (edate < sdate) {
                    $scope.ismessage = 'End date cannot be greater than begin date';
                    return false;
                }
            }
            $http({ method: 'POST', url: 'api/ExportData', data: { method: "GetutilizationRateData", parameters: [JSON.stringify(vm.onetime)] } })
              .success(function (result) {

                  alasql('SELECT * INTO XLSX("utilizationRate.xlsx",{headers:true}) FROM ?', [result]);

              })
               .error(function (error) {
                   console.log(error);
               });
        }
    }
})();
