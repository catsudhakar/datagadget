﻿(function () {
    'use strict';
    var controllerId = 'siteUsage';
    angular
        .module('app')
        .controller(controllerId, siteUsage);

    siteUsage.$inject = ['$scope', '$http', '$modal', '$location', 'common', '$window'];

    function siteUsage($scope, $http, $modal, $location, common, $window) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error')

        $scope.title = 'Site Usage';
        var vm = this;
        $scope.vm.siteUsage = [];
        $scope.vm.siteUsage1 = [];
        $scope.vm.siteUsage2 = [];
        activate();

        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }

            $http({ method: 'POST', url: 'api/siteUsage', data: { method: "AllSiteUsage", parameters: [userInfo.level] } })
            .success(function (result) {

                $scope.vm.siteUsage = result;
            })
            .error(function (error) {
                errorLog(error);
            })
            $http({ method: 'POST', url: 'api/siteUsage', data: { method: "AllSiteUsage1", parameters: '' } })
           .success(function (result) {

               $scope.vm.siteUsage1 = result;
           })
           .error(function (error) {
               $scope.vm.siteUsage1 = null;
           })
            $http({ method: 'POST', url: 'api/siteUsage', data: { method: "AllSiteUsage2", parameters: '' } })
           .success(function (result) {

               $scope.vm.siteUsage2 = result;
           })
           .error(function (error) {
               $scope.vm.siteUsage2 = null;
           })
        }

        $scope.vm.deleteuser = function (user) {
            ;

            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',

            });


            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "DeleteStateWideInitiative", parameters: [user.id] } })

            .success(function (result) {

                var i = vm.statewides.indexOf(user);
                if (i != -1) {
                    vm.statewides.splice(i, 1);
                }
                log("StateWideInitative deleted successfully");

            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {


            });


        };

    }


})();
