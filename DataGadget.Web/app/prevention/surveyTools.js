﻿(function () {
    'use strict';
    var controllerId = 'surveyTools';   //ongoingEvents ng cotroller in ongoingevents.html
    angular
        .module('app')
        .controller(controllerId, surveyTools);
    surveyTools.$inject = ['$scope', '$http', '$location', 'common', '$modal', '$routeParams', '$route', '$window', 'sessionService'];
    function surveyTools($scope, $http, $location, common, $modal, $routeParams, $route, $window, sessionService) {
        var userid = localStorage.getItem('userId');
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'survey tools';
        $scope.survey = [];
        var vm = this;
        $scope.surveyData = [];
        $scope.vm.participant;
        $scope.surveyType = [];
        $scope.programNames = [];
        $scope.groupNames = [];

        $scope.ischecked1 = false;

        $scope.dataType = [];
        //GetSurveyData();
        $scope.vm.programID = "";//"NREPP";
        $scope.SessionID = "";
        $scope.ClientSessionStart = "";


        // vm.onGoingEvent = [];
        $scope.prName = '';
        $scope.grName = '';
        $scope.progId = 0;

        $scope.cnt = 0;

        //if (localStorage.getItem("SurveyCount")) {
        //    $scope.cnt = localStorage.getItem("SurveyCount");
        //}

        activate();

        function activate() {



            $scope.onGoingEvent = {};
            vm.survey = '2';
            //  if ($routeParams.goingEventSurey) {

            //Remove vm
            // $scope.onGoingEvent = JSON.parse($routeParams.goingEventSurey);
            if (!_.isEmpty(sessionService.getData())) {
                $scope.onGoingEvent = sessionService.getData();
                //sessionService.setData({});


                $scope.vm.eventId = $scope.onGoingEvent.id;
                if ($scope.onGoingEvent.programId) {
                    $scope.vm.eventProgramID = $scope.onGoingEvent.programId;
                }
                else {
                    if (localStorage.getItem("prgId")) {
                        $scope.vm.eventProgramID = parseInt(localStorage.getItem("prgId"));
                    }
                    else {
                        $scope.vm.eventProgramID = 0;
                    }
                }

                $scope.vm.surveyID = $scope.onGoingEvent.age0;  //Surevytype pretest or post test

                //vm.onetime.startDate = vm.onGoingEvent.beginDate;
                //vm.onetime.endDate = vm.onGoingEvent.endDate;
                //vm.eventInterventionType = vm.onGoingEvent.interventionType.trim();

                //vm.eventUniversalType_DI = vm.onGoingEvent.universalType_DI.trim();
                //vm.eventtyoeofprogram_ea = vm.onGoingEvent.tyoeOfProgram_EA.trim();

                $scope.vm.programID = $scope.onGoingEvent.programNameSource.trim();

                //$scope.cnt = $scope.onGoingEvent.surveyCount;
                //var ii = parseInt($scope.cnt) + 1;
                //$scope.cnt = ii;
                GetSurveyData();

            }
            else {
                $location.path('/ongoingEvents');

            }

        }

        function GetSurveyData() {
            var userInfo = localStorage.getItem('userInfo');

            //var onGoingEvent = localStorage.getItem('OnGoingEventData');

            //if (onGoingEvent != null)
            //{
            //    $scope.programNameData = onGoingEvent.programName;
            //    $scope.vm.programID = onGoingEvent.programType;
            //    $scope.groupNameData = onGoingEvent.groupName;
            //}

            //if (localStorage.getItem("prgId")) {
            //    $scope.progId = localStorage.getItem("prgId");
            //}

            //if (localStorage.getItem("prgType")) {
            //    $scope.vm.programID = localStorage.getItem("prgType");
            //}
            //else {
            //    $scope.vm.programID = "NREPP"
            //}


            $http({ method: 'POST', url: 'api/PreventionSurvey', data: { method: "GetSurveyCountByEventID", parameters: [$scope.vm.eventId] } })
              .success(function (result) {

                  if (result == null) {
                      $scope.cnt = 1;
                  }
                  else {
                      $scope.cnt = parseInt(result) + 1;
                  }

              })
              .error(function (error) {
                  console.log(error);
              });



            $http({ method: 'POST', url: 'api/PreventionSurvey', data: { method: "GetSurveyData", parameters: [userInfo] } })
               .success(function (result) {
                   $scope.surveyData = result;
               })
               .error(function (error) {
                   console.log(error);
               });

            $http({ method: 'POST', url: 'api/PreventionSurvey', data: { method: "GetParticipantID", parameters: [userInfo] } })
                   .success(function (result) {
                       $scope.vm.participant = result;
                   })
                   .error(function (error) {
                       console.log(error);
                   });

            $http({ method: 'POST', url: 'api/PreventionSurvey', data: { method: "GetSurveyType", parameters: [userInfo] } })
                           .success(function (result) {
                               $scope.surveyType = result;
                           })
                           .error(function (error) {
                               console.log(error);
                           });

            $http({
                method: 'POST', url: 'api/PreventionSurvey',
                //data: { method: "GetProgramName", parameters: ['NREPP',userInfo] }
                data: { method: "GetProgramName", parameters: [$scope.vm.programID, userInfo] }
            })
            .success(function (result) {

                $scope.programNames = result;
                //$scope.progId;
                //if (localStorage.getItem("prgId")) {
                //    vm.programNameData = parseInt(localStorage.getItem("prgId"));
                //    GetGroupData(parseInt(localStorage.getItem("prgId")));

                //}
                //else
                //    GetGroupData(result[0].pid);
                GetGroupData($scope.vm.eventProgramID, 0);

                vm.programNameData = $scope.vm.eventProgramID;




            })
            .error(function (error) {
                console.log(error);
            });

            //get questions

            //$http({ method: 'POST', url: 'api/PreventionSurvey', data: { method: "BindDynamicData" } })
            //          .success(function (result) {
            //              var dataresult = JSON.parse("[" + result + "]");
            //              document.getElementById("dynamichtml").innerHTML = dataresult;
            //              populatedropdown('daydropdown', 'monthdropdown', 'yeardropdown');
            //          })
            //          .error(function (error) {
            //              logError(error);
            //          });

        }

        function GetGroupData(pName, id) {

            $http({
                method: 'POST', url: 'api/PreventionSurvey',
                data: { method: "GetGroupName", parameters: [pName] }
            })
           .success(function (result) {
               $scope.groupNames = result;
               // vm.groupNameData = localStorage.getItem("grName");
               //vm.groupNameData = $scope.vm.groupNameData
               if ($scope.onGoingEvent.groupName && id == 0) {
                   $scope.vm.groupNameData = $scope.onGoingEvent.groupName.trim();
               }
               else {
                   $scope.vm.groupNameData = ''
               }


           })
           .error(function (error) {
               console.log(error);
           });
        }

        $scope.getProgramTypeData = function (score) {

            //var i = $scope.vm.programId;
            //alert(score);
            var userInfo = localStorage.getItem('userInfo');

            $http({
                method: 'POST', url: 'api/PreventionSurvey',
                //data: { method: "GetProgramName", parameters: [score] }
                data: { method: "GetProgramName", parameters: [score, userInfo] }
            })
            .success(function (result) {
                $scope.programNames = result;
                $scope.vm.eventProgramID = result[0].pid;
                vm.programNameData = result[0].pid;//$scope.vm.eventProgramID;



                GetGroupData(result[0].pid, 1);

            })
            .error(function (error) {
                console.log(error);
            })
        };
        $scope.getProgramNameTypeData = function (score) {

            GetGroupData(score, 1);

        };


        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };
        $scope.openBeginDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.openBeginDateopened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];

        $scope.Finish = function (surveyForm) {
            //vm.DynamicDataCtrl = document.getElementById('hdf').value;


            localStorage.removeItem("prgId");
            localStorage.removeItem("prgType");
            localStorage.removeItem("grName");
            localStorage.removeItem("prgName");

            localStorage.removeItem("SurveyCount");

            vm.UserID = userid;
            vm.SessionID = document.getElementById("hdfsd").value;
            vm.ClientSessionStart = document.getElementById("hdfcst").value;



            $scope.isSubmit = true;

            $scope.ischkSubit = false;

            var ii = document.getElementsByName("ch_questionresonse_28");
            var i = 0;
            //alert(ii.length);
            for (var a = 0; a < ii.length; a++) {
                if (ii[a].checked == true) {
                    i++;
                }
            }
            if (i > 0) {
                $scope.isSubmit = true;
                $scope.ischkSubit = false;
            }
            else {

                $scope.isSubmit = false;
                $scope.ischkSubit = true;
            }

            if ($scope.surveyForm.$valid && $scope.isSubmit == true) {

                if (!_.isEmpty(sessionService.getData())) {
                    sessionService.setData({});
                }

                $http({
                    method: 'POST', url: 'api/PreventionSurvey',
                    data: { method: "CreateSurveryTools", parameters: [JSON.stringify($scope.vm)] }
                })
                .success(function (result) {
                    log("Survey submitted succesfully.");
                    $location.path('/ongoingEvents');

                })
                .error(function (error) {
                    console.log(error);
                })
            }
        }



        $scope.newClient = function (surveyForm) {
            //vm.DynamicDataCtrl = document.getElementById('hdf').value;
            vm.UserID = userid;
            vm.SessionID = document.getElementById("hdfsd").value;
            vm.ClientSessionStart = document.getElementById("hdfcst").value;
            $scope.isSubmit = true;
            $scope.ischkSubit = false;

            var ii = document.getElementsByName("ch_questionresonse_28");
            var i = 0;
            //alert(ii.length);
            for (var a = 0; a < ii.length; a++) {
                if (ii[a].checked == true) {
                    i++;
                }
            }
            if (i > 0) {
                $scope.isSubmit = true;
                $scope.ischkSubit = false;
            }
            else {

                $scope.isSubmit = false;
                $scope.ischkSubit = true;
            }



            if ($scope.surveyForm.$valid && $scope.isSubmit == true) {

                //if (localStorage.getItem("SurveyCount")) {
                //    $scope.cnt = localStorage.getItem("SurveyCount");
                //}
                //var ii = parseInt($scope.cnt) + 1;
                //$scope.cnt = ii;

                //localStorage.setItem("SurveyCount", ii);

                $http({
                    method: 'POST', url: 'api/PreventionSurvey',
                    data: { method: "CreateSurveryTools", parameters: [JSON.stringify($scope.vm)] }
                })
                .success(function (result) {
                    log("Survey submitted succesfully.");
                    //$location.path('/dashboard');
                    $route.reload();
                })
                .error(function (error) {
                    console.log(error);
                })
            }
        }

        $scope.textdisable = function (txtid,chkid) {

            //alert(txtid);
         
            var i = document.getElementById(txtid);
            var c = document.getElementById(chkid);
            if (i != null)
            {
                if (c.checked) {
                   // alert("t");
                    i.removeAttribute('disabled');
                }
                else {
                    //alert("f");
                    i.setAttribute('disabled', 'disabled');
                    i.value = "";
                }
            }
            //alert(i);
        }

        $scope.setdisable = function () {
            var i = document.getElementById("chkNoresponse");


            var x = document.getElementsByName("ch_questionresonse_28");
            var a;
            for (a = 0; a < x.length; a++) {
                if (x[a].type == "checkbox") {
                    if (x[a].id != "chkNoresponse") {
                        x[a].checked = false;
                       
                    }
                }
            }

          

            var y = document.getElementsByName("txtOtherResponse_3");
            for (a = 0; a < y.length; a++) {
                if (y[a].type == "text") {
                    
                    y[a].setAttribute('disabled', 'disabled');
                    y[a].value = "";
                    
                }
            }

            $scope.isdis = false;

            if (i.checked == true) {

                $scope.isdis = true;

            }
            else {
                $scope.isdis = false;

            }

        }
    };

}
)();










