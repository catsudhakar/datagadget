﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('resourceLinksList', resourceLinksList);

    resourceLinksList.$inject = ['$scope', '$http', 'common', '$modal', 'mySharedLinks', '$location', '$window'];

    function resourceLinksList($scope, $http, common, $modal, SharedLinks, $location, $window) {
        $scope.title = 'resourceLinksList';
        $scope.isshow = false;
        var vm = this;
        vm.links = [];

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(resourceLinksList);

        activate();

        function activate() {
            
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A" || userInfo.level == "STA1A") {
                // $location.path('/dashboard');
                $scope.isshow = false;
            }
            else
            {
                $scope.isshow = true;
            }

            
           
           // if (userInfo.level == 'Admin') {
                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "AllResourcesLinks", parameters:[ userInfo.level] } })
                .success(function (result) {
                    vm.links = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            //else {
            //    vm.links = [];
            //}
        //}


        vm.addResource = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/addResourceLink.html',
                controller: 'addResourceLink',
                resolve: {
                    resourcesInfo: function () {
                        return undefined;
                    }
                }

            });
            addUserModal.result.then(function (link) {
                vm.links.push(link);

            }, function () {
            });
        }

        $scope.vm.editResource = function (rLinks) {

            var addUserModal = $modal.open({
                templateUrl: 'app/admin/addResourceLink.html',
                controller: 'addResourceLink',
                resolve: {
                    resourcesInfo: function () {
                        return rLinks;
                    }
                }
            });

            addUserModal.result.then(function () {

            }, function () {
            });
        }



        vm.deleteResource = function (rLinks) {
            
            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {
                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "DeleteResourceLink", parameters: [rLinks.resourceId] } })
            .success(function (result) {
               
                log("Deleted successfully");
                activate();
            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {
            });
        };

    }
})();
