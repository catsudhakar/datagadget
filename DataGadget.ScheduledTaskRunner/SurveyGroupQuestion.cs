//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    using System.Collections.Generic;
    
    public partial class SurveyGroupQuestion
    {
        public string Objective { get; set; }
        public string SurveyQTN { get; set; }
        public Nullable<int> GroupSeqNum { get; set; }
        public int QTNSeqNum { get; set; }
        public int RecNo { get; set; }
        public string ASPControlType { get; set; }
        public int SurveyID { get; set; }
        public bool HasOtherResponse { get; set; }
        public int SurveyQTNRecNo { get; set; }
    }
}
