//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    
    public partial class spLU_CourseSeriesNumbers2_Result
    {
        public int ID { get; set; }
        public string Note { get; set; }
        public string Staff { get; set; }
        public Nullable<System.DateTime> myDateStamp { get; set; }
        public string StateRegionCode { get; set; }
        public string SiteCode { get; set; }
    }
}
