﻿(function () {
    'use strict';
    var controllerId = 'ExportUserList';
    angular.module('app').controller(controllerId, ExportUserList);

    //admin.$inject = ['$scope', '$http', '$location', 'common', '$modal','$sce'];

    function ExportUserList($scope, $http, $location, common, $modal, $sce, $window) {


        var vm = this;
        vm.onetime = {};
        vm.title = 'ExportUserList';
        $scope.source = '';
        activate();
        function activate() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }

            vm.onetime.locationList = "";
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            //vm.onetime.organization = userInfo.regionCode + "_" + userInfo.siteCode;
            vm.onetime.organization = '0_0';
            $http({ method: 'POST', url: 'api/ExportUserList', data: { method: "InitializeUserForm", parameters: [userInfo.level] } })
           .success(function (result) {
               vm.onetime.locationList = result.locationList;
           });
        }

        $scope.ok = function () {

            var res = vm.onetime.organization.split("_");
            vm.onetime.organizationName = vm.onetime.organization;

            if (res.length == 2) {
                vm.onetime.regionCode = res[0];
                vm.onetime.siteCode = res[1];

                $http({ method: 'POST', url: 'api/ExportUserList', data: { method: "GetData", parameters: [vm.onetime.regionCode, vm.onetime.siteCode] } })
               .success(function (result) {

                   alasql('SELECT * INTO XLSX("ExportUserList.xlsx",{headers:true}) FROM ?', [result])

               })
                .error(function (error) {
                    console.log(error);
                });
            }//JSON.stringify(vm.onetime)]
        }
    }
})();





