﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_EvidenceSummary : Report_Base
    {

        public Report_EvidenceSummary()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;


        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport("EvidenceSummary");
        }

        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1, string ReportType, string ReportId,string Name)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;
            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;

            CreateReport(ReportType, ReportId,Name);


        }

        private void CreateReport(string ReportType, string ReportId, string Name)
        {

            this._pdfDoc.Open();

            BuildReportList(ReportType, ReportId,Name);

            this._pdfDoc.Close();

        }

        private void BuildReportList(string ReportType, string ReportId, string Name)
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());


            this._pdfDoc.Add(NewParagraph(Name + "Bed Utilization Report"));





            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralUtilizationTableIndigent(ReportType, ReportId);
            this._pdfDoc.Add(table);






        }

        private PdfPTable GeneralUtilizationTableIndigent(string ReportType, string ReportId)
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataSet dsData = new DataSet();
            string query = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (ReportId != "All")
            {
                int Rid = Convert.ToInt32(ReportId);
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem inner join TreatmentSurveyIntakeValues  TSI on SurveyItem.RecNo=TSI.SurveyItemId  where TSI.IntakeValueId=" + Rid + " and LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  inner join TreatmentSurveyIntakeValues  TSI on SurveyItem.RecNo=TSI.SurveyItemId  where TSI.IntakeValueId=" + Rid + " and LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  inner join TreatmentSurveyIntakeValues  TSI on SurveyItem.RecNo=TSI.SurveyItemId  where TSI.IntakeValueId=" + Rid + " and LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  inner join TreatmentSurveyIntakeValues  TSI on SurveyItem.RecNo=TSI.SurveyItemId  where TSI.IntakeValueId=" + Rid + " and LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            query = sb.ToString();

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(query, sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dsData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////
            PdfPTable table = generateReport(ReportType, dsData);
            return table;


        }



        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1, string ReportType)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;
            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;

            CreateReport(ReportType);


        }




        private void CreateReport(string ReportType)
        {

            this._pdfDoc.Open();

            BuildReportList(ReportType);

            this._pdfDoc.Close();

        }

        private void BuildReportList(string ReportType)
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            if (ReportType == "EvidenceSummary")
                this._pdfDoc.Add(NewParagraph("Evidence-Based Summary Report"));
            if (ReportType == "Faith-Based")
                this._pdfDoc.Add(NewParagraph("Faith-Based Summary Report"));
            else if (ReportType == "Utilization")
                this._pdfDoc.Add(NewParagraph("Utilization Report"));
            else if (ReportType == "IndigentReport" || ReportType == "PREGNANT" || ReportType == "PANDP" || ReportType == "IVDRUG" || ReportType == "CO" ||
                ReportType == "WITHDRAWAL" || ReportType == "ALCOHOL" || ReportType == "OPIOID" || ReportType == "METHAMPHETAMINE" || ReportType == "BENZODIAZAPINES" ||
                ReportType == "OTHER" || ReportType == "SELFPAY" || ReportType == "PRIVATEINSURANCE" || ReportType == "MEDICAID")
            {
                if (ReportType == "IndigentReport") this._pdfDoc.Add(NewParagraph("Indigent Bed Utilization Report"));
                else if (ReportType == "PANDP") this._pdfDoc.Add(NewParagraph(" PANDP Bed Utilization Report"));
                else if (ReportType == "CO") this._pdfDoc.Add(NewParagraph(" CO Bed Utilization Report"));
                else
                    this._pdfDoc.Add(NewParagraph(ReportType + " Bed Utilization Report"));
            }




            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            // PdfPTable table = GeneralTable(); // new PdfPTable(1);

            if (ReportType == "EvidenceSummary" || ReportType == "Faith-Based")
            {
               // PdfPTable table = GeneralTable();
                PdfPTable table = GeneralTable(ReportType);
                this._pdfDoc.Add(table);
            }
            else if (ReportType == "Utilization")
            {
                PdfPTable table = GeneralUtilizationTable();
                this._pdfDoc.Add(table);
            }
            else if (ReportType == "IndigentReport" || ReportType == "PREGNANT" || ReportType == "PANDP" || ReportType == "IVDRUG" || ReportType == "CO" ||
                ReportType == "WITHDRAWAL" || ReportType == "ALCOHOL" || ReportType == "OPIOID" || ReportType == "METHAMPHETAMINE" || ReportType == "BENZODIAZAPINES" ||
                ReportType == "OTHER" || ReportType == "SELFPAY" || ReportType == "PRIVATEINSURANCE" || ReportType == "MEDICAID")
            {
                PdfPTable table = GeneralUtilizationTableIndigent(ReportType);
                this._pdfDoc.Add(table);
            }





        }

        private PdfPTable GeneralTable(string ReportType)
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataSet dsData = new DataSet();

            StringBuilder sbSQL = new StringBuilder();
            if (ReportType == "EvidenceSummary")
            {
                sbSQL.Append("suretool_Report_EvidenceSummary ");
            }
            else
            {
                sbSQL.Append("suretool_Report_FaithBased ");
            }
           
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");


            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dsData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dsData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;

            float[] headerwidths = { 75, 25 }; // percentage
            table.SetWidths(headerwidths);

            cell = new PdfPCell(NewPhrase(" ", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.Colspan = 2;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);


            table.HeaderRows = 1;

            int cCnt = 0;

            if (ReportType == "EvidenceSummary")
            {
                cell = new PdfPCell(NewPhrase("Number of Times Evidence-Based Programs Utilized"));
            }
            else
            {
                cell = new PdfPCell(NewPhrase("Number of Times Faith-Based Programs Utilized"));
            }

           // cell = new PdfPCell(NewPhrase("Number of Times Evidence-Based Programs Utilized"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["NumberofPrograms"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(" "));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            cell = new PdfPCell(NewPhrase(" "));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Number of Direct Service Hours"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            cell = new PdfPCell(NewPhrase(dsData.Tables[3].Rows[0]["DirectServiceHours"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Number of Travel Hours"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            cell = new PdfPCell(NewPhrase(dsData.Tables[3].Rows[0]["TravelHours"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Number of Prep Hours"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            cell = new PdfPCell(NewPhrase(dsData.Tables[3].Rows[0]["PrepHours"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Total Hours"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            cell = new PdfPCell(NewPhrase(dsData.Tables[3].Rows[0]["TotalHours"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(" "));
            cell.Colspan = 2;
            table.AddCell(cell);

            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(ParticipantsTable(dsData));
            cell.Colspan = 2;
            table.AddCell(cell);

            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(" "));
            cell.Colspan = 2;
            table.AddCell(cell);

            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(AdaptationsTable(dsData));
            cell.Colspan = 2;
            table.AddCell(cell);

            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(" "));
            cell.Colspan = 2;
            table.AddCell(cell);

            cCnt++;

            //---------------------------------------------------------------------------------------------------
            if (ReportType == "EvidenceSummary")
            {
                cell = new PdfPCell(ProgramsListTable(dsData));
                cell.Colspan = 2;
                table.AddCell(cell);

                cCnt++;
            }
            //---------------------------------------------------------------------------------------------------






            return table;
        }


        private PdfPTable ParticipantsTable(DataSet dsData)
        {
            PdfPTable table = new PdfPTable(3);
            PdfPCell cell;


            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Participant Count", Font.HELVETICA, 6, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["ParticipantTotal"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Gender", Font.HELVETICA, 6, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Male"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Males"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Female"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Females"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Age", Font.HELVETICA, 6, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("0-4"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age0"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("5-11"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age5"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("12-14"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age12"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("15-17"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age15"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("18-20"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age18"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("21-24"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age21"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("25-44"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age25"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("45-64"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age45"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("65 and over"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Age65"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Race", Font.HELVETICA, 6, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("White"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceWhite"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Black"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceBlack"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Asian"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceAsian"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("American Indian/Alaskan Native"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceAmIndian"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Native Hawaiian/Other Pacific Islander"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceHawaii"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Unknown or Other"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceUnknownOther"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("More than one"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["RaceMoreThanOne"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Ethnicity", Font.HELVETICA, 6, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Non-Hispanic"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["NotHispanic"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Hispanic"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["Hispanic"].ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------


            return table;
        }


        private PdfPTable AdaptationsTable(DataSet dsData)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;


            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Number of Times Adaptation Was Used"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dsData.Tables[1].Rows[0]["AdaptationsCount"].ToString()));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------


            return table;
        }


        private PdfPTable ProgramsListTable(DataSet dsData)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;


            int cCnt = 0;


            AddGeneralTableRow(table, "Evidence-Based Programs", " ", cCnt++);

            foreach (DataRow dataRow in dsData.Tables[2].Rows)
            {
                AddGeneralTableRow(table, " ", dataRow["ProgramName"].ToString(), cCnt++);
            }


            cCnt++;
            //---------------------------------------------------------------------------------------------------


            return table;
        }



        private void AddGeneralTableRow(PdfPTable table, string c1, string c2, int cCnt)
        {
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase(c1, Font.HELVETICA, 10, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(c2, Font.HELVETICA, 10, Font.NORMAL));
            //cell.HorizontalAlignment = Cell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);

        }


        private PdfPTable GeneralUtilizationTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataSet dsData = new DataSet();



            string query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
	                                on si.LocationID=L.PKey 
	                                where (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SI.SurveyID=1 and  l.StateRegionCode='" + RegionCode + "' and  l.SiteCode='" + SiteCode + "' group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

            StringBuilder sb = new StringBuilder();
            //sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

            //sb.Append("isnull(bedcount,0) as AdultMale, ");
            //sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
            //sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
            //sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

            //sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SI.SurveyID=1 and   l.StateRegionCode='" + RegionCode + "' and  l.SiteCode='" + SiteCode + "' ");
            //sb.Append("group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");



            sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
            sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   (ClientGenderType='Adult Male' or ClientGenderType is null) and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdultMaleAssignedBeds,");
            sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 ) as TotalAdultFemaleAssignedBeds,");
            sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male'  and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1  ) as TotalAdolescentMaleAssignedBeds,");
            sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 ) as TotalAdolescentFemaleAssignedBeds,");
            sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
            sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
            sb.Append("where tp.IsActive = 1 and tl.IsActive=1");
            sb.Append("order by  FriendlyName_loc");


            query = sb.ToString();


            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dsData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(query, sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dsData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;

            float[] headerwidths = { 75, 25 }; // percentage
            table.SetWidths(headerwidths);

            cell = new PdfPCell(NewPhrase(" ", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.Colspan = 2;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);


            table.HeaderRows = 1;

            int cCnt = 0;

            //cell = new PdfPCell(NewPhrase("Location Name"));
            //table.AddCell(cell);

            ////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            //if (dsData.Tables[0].Rows.Count > 0)
            //    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["FriendlyName_loc"].ToString()));
            //else
            //    cell = new PdfPCell(NewPhrase(""));

            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);


            //cCnt++;
            ////---------------------------------------------------------------------------------------------------

            //cell = new PdfPCell(NewPhrase(" "));
            //table.AddCell(cell);

            ////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            //cell = new PdfPCell(NewPhrase(" "));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);


            //cCnt++;
            //---------------------------------------------------------------------------------------------------

            //cell = new PdfPCell(NewPhrase("Total Male Assigned Beds"));
            cell = new PdfPCell(NewPhrase("Total Male Assigned Beds"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["TotalAdultMaleAssignedBeds"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Total Female Assigned Beds"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["TotalAdultFemaleAssignedBeds"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;

            cell = new PdfPCell(NewPhrase("Total Adolescent Male Assigned Beds"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["TotalAdolescentMaleAssignedBeds"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;


            cell = new PdfPCell(NewPhrase("Total Adolescent Female AssignedBeds"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["TotalAdolescentFemaleAssignedBeds"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;


            //Total Beds

            cell = new PdfPCell(NewPhrase("Total Beds for Male"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultMale"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;

            cell = new PdfPCell(NewPhrase("Total Beds for Female"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultFemale"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;

            cell = new PdfPCell(NewPhrase("Total Beds for Adolescen tMale"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdolescentMale"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;

            cell = new PdfPCell(NewPhrase("Total Beds for Adolescent Female"));
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            if (dsData.Tables[0].Rows.Count > 0)
                cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdolescentFemale"].ToString()));
            else
                cell = new PdfPCell(NewPhrase(""));


            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);


            cCnt++;






            return table;
        }


        private PdfPTable GeneralUtilizationTableIndigent(string ReportType)
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataSet dsData = new DataSet();



            string query = @"SELECT LocationId,L.FriendlyName_loc, count(*) as TotalAssignedBeds,isnull(bedcount,0) as BedCount FROM SurveyItem SI  inner join tblLocations L
	                                on si.LocationID=L.PKey 
	                                where (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SI.SurveyID=1 and  l.StateRegionCode='" + RegionCode + "' and  l.SiteCode='" + SiteCode + "' group by LocationId,L.FriendlyName_loc,bedcount order by LocationId ";

            StringBuilder sb = new StringBuilder();
            //sb.Append("SELECT SI.LocationId,L.FriendlyName_loc,");

            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   (ClientGenderType='Adult Male' or ClientGenderType is null) and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdultMaleAssignedBeds,");
            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdultFemaleAssignedBeds,");
            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdolescentMaleAssignedBeds,");
            //sb.Append("(select count(*)  from SurveyItem  where LocationID=SI.LocationID and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 ) as TotalAdolescentFemaleAssignedBeds,");

            //sb.Append("isnull(bedcount,0) as AdultMale, ");
            //sb.Append("isnull(AdultFemale,0) as AdultFemale, ");
            //sb.Append("isnull(AdolescentMale,0) as AdolescentMale, ");
            //sb.Append("isnull(AdolescentFemale,0) as AdolescentFemale ");

            //sb.Append("FROM SurveyItem SI  inner join tblLocations L on si.LocationID=L.PKey where (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SI.SurveyID=1 and   l.StateRegionCode='" + RegionCode + "' and  l.SiteCode='" + SiteCode + "' ");
            //sb.Append("group by SI.LocationId,L.FriendlyName_loc,bedcount,AdultFemale,AdolescentMale,AdolescentFemale order by LocationId ");


            if (ReportType == "IndigentReport")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsIndigent = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsIndigent = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }
            else if (ReportType == "PREGNANT")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPregnant = 1  ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPregnant = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }

            else if (ReportType == "PANDP")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPregnant = 1  ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPregnant = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "IVDRUG")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsIVDrugUser = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsIVDrugUser = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsIVDrugUser = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsIVDrugUser = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "CO")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsCooccurring = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsCooccurring = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsCooccurring = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsCooccurring = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "WITHDRAWAL")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsWithdrawal = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsWithdrawal = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsWithdrawal = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsWithdrawal = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }




            else if (ReportType == "ALCOHOL")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsAlcohol = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsAlcohol = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsAlcohol = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsAlcohol = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "OPIOID")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOpioid = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOpioid = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOpioid = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOpioid = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "METHAMPHETAMINE")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMethamphetamine = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMethamphetamine = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMethamphetamine = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMethamphetamine = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "BENZODIAZAPINES")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsBenzodiazapines = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsBenzodiazapines = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsBenzodiazapines = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsBenzodiazapines = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "OTHER")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOtherSubstances = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOtherSubstances = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOtherSubstances = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsOtherSubstances = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "SELFPAY")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsSelfPay = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsSelfPay = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsSelfPay = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsSelfPay = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "PRIVATEINSURANCE")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPrivateInsurance = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPrivateInsurance = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPrivateInsurance = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsPrivateInsurance = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }


            else if (ReportType == "MEDICAID")
            {
                sb.Append("SELECT distinct tl.PKey as PKey, tp.RecNo,right(stateregioncode,2) + '-' + tp.Programname as  FriendlyName_loc,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMedicaid = 1  ) as TotalAdultMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adult Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMedicaid = 1 ) as TotalAdultFemaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Male' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMedicaid = 1  ) as TotalAdolescentMaleAssignedBeds,");
                sb.Append("(select count(*)  from SurveyItem  where LocationID=tl.PKey and ProgramID=tp.RecNo and tp.IsActive=1 and tl.IsActive=1 and   ClientGenderType='Adolescent Female' and (EntryDate >= '" + BeginDate + "' and EntryDate <='" + EndDate + "') and  SurveyID=1 and (DischargeDate is null or DischargeDate > GETDATE()) and ServiceID=1 and IsMedicaid = 1 ) as TotalAdolescentFemaleAssignedBeds,");
                sb.Append("isnull(tp.MaleBeds,0) as AdultMale, isnull(tp.FeMaleBeds,0) as AdultFemale, isnull(tp.AdolescentMaleBeds,0) as AdolescentMale, ");
                sb.Append("isnull(tp.AdolescentFeMaleBeds,0) as AdolescentFemale  From TreatmentProgram tp full outer join tblLocations Tl on tp.LocationID=Tl.PKey  ");
                sb.Append("where tp.IsActive = 1 and tl.IsActive=1 and tl.FriendlyName_loc = '" + OrgName + "' order by  FriendlyName_loc");
            }






            query = sb.ToString();


            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dsData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(query, sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dsData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////
            PdfPTable table = generateReport(ReportType, dsData);
            return table;

            //PdfPTable table = new PdfPTable(2);
            //PdfPCell cell;

            //float[] headerwidths = { 75, 25 }; // percentage
            //table.SetWidths(headerwidths);

            //cell = new PdfPCell(NewPhrase(" ", Font.HELVETICA, 8, Font.BOLD));
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            //cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            //cell.Colspan = 2;
            //cell.BackgroundColor = cellHeaderColor;
            //table.AddCell(cell);






            //table.HeaderRows = 1;

            //int cCnt = 0;

            ////cell = new PdfPCell(NewPhrase("Location Name"));
            ////table.AddCell(cell);

            //////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            ////if (dsData.Tables[0].Rows.Count > 0)
            ////    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["FriendlyName_loc"].ToString()));
            ////else
            ////    cell = new PdfPCell(NewPhrase(""));

            ////cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ////table.AddCell(cell);


            ////cCnt++;
            //////---------------------------------------------------------------------------------------------------

            ////cell = new PdfPCell(NewPhrase(" "));
            ////table.AddCell(cell);

            //////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            ////cell = new PdfPCell(NewPhrase(" "));
            ////cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ////table.AddCell(cell);


            ////cCnt++;
            ////---------------------------------------------------------------------------------------------------

            ////cell = new PdfPCell(NewPhrase("Total Male Assigned Beds"));
            //cell = new PdfPCell(NewPhrase("Total Indigent Male Assigned Beds"));
            //table.AddCell(cell);

            ////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            //int TotalAdultMaleAssignedBeds = 0;

            //if (dsData.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow r in dsData.Tables[0].Rows)
            //    {
            //        if (!DBNull.Value.Equals(r["TotalAdultMaleAssignedBeds"]))
            //        {
            //            TotalAdultMaleAssignedBeds = TotalAdultMaleAssignedBeds + Convert.ToInt32(r["TotalAdultMaleAssignedBeds"]);

            //        }
            //    }
            //    //cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["TotalAdultMaleAssignedBeds"].ToString()));
            //    cell = new PdfPCell(NewPhrase(TotalAdultMaleAssignedBeds.ToString()));
            //}
            //else
            //    cell = new PdfPCell(NewPhrase(""));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);


            //cCnt++;
            ////---------------------------------------------------------------------------------------------------

            //cell = new PdfPCell(NewPhrase("Total Indigent Female Assigned Beds"));
            //table.AddCell(cell);

            ////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            //int TotalAdultFemaleAssignedBeds = 0;
            //if (dsData.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow r in dsData.Tables[0].Rows)
            //    {
            //        if (!DBNull.Value.Equals(r["TotalAdultFemaleAssignedBeds"]))
            //        {
            //            TotalAdultFemaleAssignedBeds = TotalAdultFemaleAssignedBeds + Convert.ToInt32(r["TotalAdultFemaleAssignedBeds"]);

            //        }
            //    }
            //    // cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["TotalAdultFemaleAssignedBeds"].ToString()));
            //    cell = new PdfPCell(NewPhrase(TotalAdultFemaleAssignedBeds.ToString()));
            //}
            //else
            //{
            //    cell = new PdfPCell(NewPhrase(""));
            //}


            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);


            //cCnt++;

            ////Total Beds

            //cell = new PdfPCell(NewPhrase("Total Beds for Male"));
            //table.AddCell(cell);

            ////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            //if (dsData.Tables[0].Rows.Count > 0)
            //    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultMale"].ToString()));
            //else
            //    cell = new PdfPCell(NewPhrase(""));


            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);


            //cCnt++;

            //cell = new PdfPCell(NewPhrase("Total Beds for Female"));
            //table.AddCell(cell);

            ////cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

            //if (dsData.Tables[0].Rows.Count > 0)
            //    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultFemale"].ToString()));
            //else
            //    cell = new PdfPCell(NewPhrase(""));


            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell);


            //cCnt++;

            //return table;
        }

        private PdfPTable generateReport(string ReportType, DataSet dsData)
        {

            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;

            float[] headerwidths = { 75, 25 }; // percentage
            table.SetWidths(headerwidths);

            cell = new PdfPCell(NewPhrase(" ", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.Colspan = 2;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            #region "IndigentReport"


            if (ReportType == "IndigentReport")
            {
                table.HeaderRows = 1;

                int cCnt = 0;

                cell = new PdfPCell(NewPhrase("Total Indigent Male Assigned Beds"));
                table.AddCell(cell);

                int TotalAdultMaleAssignedBeds = 0;

                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdultMaleAssignedBeds"]))
                        {
                            TotalAdultMaleAssignedBeds = TotalAdultMaleAssignedBeds + Convert.ToInt32(r["TotalAdultMaleAssignedBeds"]);

                        }
                    }

                    cell = new PdfPCell(NewPhrase(TotalAdultMaleAssignedBeds.ToString()));
                }
                else
                    cell = new PdfPCell(NewPhrase(""));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;
                //---------------------------------------------------------------------------------------------------

                cell = new PdfPCell(NewPhrase("Total Indigent Female Assigned Beds"));
                table.AddCell(cell);

                int TotalAdultFemaleAssignedBeds = 0;
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdultFemaleAssignedBeds"]))
                        {
                            TotalAdultFemaleAssignedBeds = TotalAdultFemaleAssignedBeds + Convert.ToInt32(r["TotalAdultFemaleAssignedBeds"]);

                        }
                    }
                    cell = new PdfPCell(NewPhrase(TotalAdultFemaleAssignedBeds.ToString()));
                }
                else
                {
                    cell = new PdfPCell(NewPhrase(""));
                }


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                //Total Beds

                cell = new PdfPCell(NewPhrase("Total Beds for Male"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultMale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                cell = new PdfPCell(NewPhrase("Total Beds for Female"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultFemale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;
            }
            #endregion

            #region "PREGNANT"

            else if (ReportType == "PREGNANT" || ReportType == "PANDP")
            {
                table.HeaderRows = 1;

                int cCnt = 0;
                if (ReportType == "PREGNANT")
                    cell = new PdfPCell(NewPhrase("Total Pregnant Female Assigned Beds"));
                else
                    cell = new PdfPCell(NewPhrase("Total Pregnant and Parenting Female Assigned Beds"));
                table.AddCell(cell);

                int TotalAdultFemaleAssignedBeds = 0;

                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdultFemaleAssignedBeds"]))
                        {
                            TotalAdultFemaleAssignedBeds = TotalAdultFemaleAssignedBeds + Convert.ToInt32(r["TotalAdultFemaleAssignedBeds"]);

                        }
                    }

                    cell = new PdfPCell(NewPhrase(TotalAdultFemaleAssignedBeds.ToString()));
                }
                else
                    cell = new PdfPCell(NewPhrase(""));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;
                //---------------------------------------------------------------------------------------------------


                if (ReportType == "PREGNANT")
                    cell = new PdfPCell(NewPhrase("Total Pregnant Adolescent Female Assigned Beds"));
                else
                    cell = new PdfPCell(NewPhrase("Total Pregnant and Parenting Adolescent Female Assigned Beds"));
                table.AddCell(cell);

                int TotalAdolescentFemaleAssignedBeds = 0;
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdolescentFemaleAssignedBeds"]))
                        {
                            TotalAdolescentFemaleAssignedBeds = TotalAdolescentFemaleAssignedBeds + Convert.ToInt32(r["TotalAdolescentFemaleAssignedBeds"]);

                        }
                    }
                    cell = new PdfPCell(NewPhrase(TotalAdolescentFemaleAssignedBeds.ToString()));
                }
                else
                {
                    cell = new PdfPCell(NewPhrase(""));
                }


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                //Total Beds

                cell = new PdfPCell(NewPhrase("Total Beds for Male"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultMale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                cell = new PdfPCell(NewPhrase("Total Beds for Female"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultFemale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;
            }
            #endregion

            #region "IVDRUG"

            else if (ReportType == "IVDRUG" || ReportType == "CO" ||
                ReportType == "WITHDRAWAL" || ReportType == "ALCOHOL" || ReportType == "OPIOID" ||
                ReportType == "METHAMPHETAMINE" || ReportType == "BENZODIAZAPINES" ||
                ReportType == "OTHER" || ReportType == "SELFPAY" || ReportType == "PRIVATEINSURANCE" ||
                ReportType == "MEDICAID" || ReportType == "BedUtilizationReport")
            {
                table.HeaderRows = 1;

                int cCnt = 0;
                if (ReportType == "IVDRUG")
                    cell = new PdfPCell(NewPhrase("Total IVDRUG Adult Male Assigned Beds"));
                else if (ReportType == "CO")
                    cell = new PdfPCell(NewPhrase("Total CO Adult Male Assigned Beds"));
                else if (ReportType == "WITHDRAWAL")
                    cell = new PdfPCell(NewPhrase("Total WITHDRAWAL Adult Male Assigned Beds"));
                else if (ReportType == "ALCOHOL")
                    cell = new PdfPCell(NewPhrase("Total ALCOHOL Adult Male Assigned Beds"));
                else if (ReportType == "OPIOID")
                    cell = new PdfPCell(NewPhrase("Total OPIOID Adult Male Assigned Beds"));
                else if (ReportType == "METHAMPHETAMINE")
                    cell = new PdfPCell(NewPhrase("Total METHAMPHETAMINE Adult Male Assigned Beds"));
                else if (ReportType == "BENZODIAZAPINES")
                    cell = new PdfPCell(NewPhrase("Total BENZODIAZAPINES Adult Male Assigned Beds"));
                else if (ReportType == "MEDICAID")
                    cell = new PdfPCell(NewPhrase("Total MEDICAID Adult Male Assigned Beds"));
                else if (ReportType == "PRIVATEINSURANCE")
                    cell = new PdfPCell(NewPhrase("Total PRIVATEINSURANCE Adult Male Assigned Beds"));
                else if (ReportType == "SELFPAY")
                    cell = new PdfPCell(NewPhrase("Total SELFPAY Adult Male Assigned Beds"));
                else if (ReportType == "OTHER")
                    cell = new PdfPCell(NewPhrase("Total OTHER Adult Male Assigned Beds"));

                else if (ReportType == "BedUtilizationReport")
                    cell = new PdfPCell(NewPhrase("Total Adult Male Assigned Beds"));

                table.AddCell(cell);

                int TotalAdultMaleAssignedBeds = 0;

                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdultMaleAssignedBeds"]))
                        {
                            TotalAdultMaleAssignedBeds = TotalAdultMaleAssignedBeds + Convert.ToInt32(r["TotalAdultMaleAssignedBeds"]);

                        }
                    }

                    cell = new PdfPCell(NewPhrase(TotalAdultMaleAssignedBeds.ToString()));
                }
                else
                    cell = new PdfPCell(NewPhrase(""));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;
                //---------------------------------------------------------------------------------------------------


                if (ReportType == "IVDRUG")
                    cell = new PdfPCell(NewPhrase("Total IVDRUG Adult Female Assigned Beds"));
                else if (ReportType == "CO")
                    cell = new PdfPCell(NewPhrase("Total CO Adult Female Assigned Beds"));
                else if (ReportType == "WITHDRAWAL")
                    cell = new PdfPCell(NewPhrase("Total WITHDRAWAL Adult Female Assigned Beds"));
                else if (ReportType == "ALCOHOL")
                    cell = new PdfPCell(NewPhrase("Total ALCOHOL Adult Female Assigned Beds"));
                else if (ReportType == "OPIOID")
                    cell = new PdfPCell(NewPhrase("Total OPIOID Adult Female Assigned Beds"));
                else if (ReportType == "METHAMPHETAMINE")
                    cell = new PdfPCell(NewPhrase("Total METHAMPHETAMINE Adult Female Assigned Beds"));
                else if (ReportType == "BENZODIAZAPINES")
                    cell = new PdfPCell(NewPhrase("Total BENZODIAZAPINES Adult Female Assigned Beds"));
                else if (ReportType == "MEDICAID")
                    cell = new PdfPCell(NewPhrase("Total MEDICAID Adult Female Assigned Beds"));
                else if (ReportType == "PRIVATEINSURANCE")
                    cell = new PdfPCell(NewPhrase("Total PRIVATEINSURANCE Adult Female Assigned Beds"));
                else if (ReportType == "SELFPAY")
                    cell = new PdfPCell(NewPhrase("Total SELFPAY Adult Female Assigned Beds"));
                else if (ReportType == "OTHER")
                    cell = new PdfPCell(NewPhrase("Total OTHER Adult Female Assigned Beds"));
                else if (ReportType == "BedUtilizationReport")
                    cell = new PdfPCell(NewPhrase("Total Adult FeMale Assigned Beds"));
                table.AddCell(cell);

                int TotalAdultFemaleAssignedBeds = 0;
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdultFemaleAssignedBeds"]))
                        {
                            TotalAdultFemaleAssignedBeds = TotalAdultFemaleAssignedBeds + Convert.ToInt32(r["TotalAdultFemaleAssignedBeds"]);

                        }
                    }
                    cell = new PdfPCell(NewPhrase(TotalAdultFemaleAssignedBeds.ToString()));
                }
                else
                {
                    cell = new PdfPCell(NewPhrase(""));
                }


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                //TotalAdolescentMaleAssignedBeds
                //    TotalAdolescentFemaleAssignedBeds

                if (ReportType == "IVDRUG")
                    cell = new PdfPCell(NewPhrase("Total IVDRUG Adolescent Male Assigned Beds"));
                else if (ReportType == "CO")
                    cell = new PdfPCell(NewPhrase("Total CO Adolescent Male Assigned Beds"));
                else if (ReportType == "WITHDRAWAL")
                    cell = new PdfPCell(NewPhrase("Total WITHDRAWAL Adolescent Male Assigned Beds"));
                else if (ReportType == "ALCOHOL")
                    cell = new PdfPCell(NewPhrase("Total ALCOHOL Adolescent Male Assigned Beds"));
                else if (ReportType == "OPIOID")
                    cell = new PdfPCell(NewPhrase("Total OPIOID Adolescent Male Assigned Beds"));
                else if (ReportType == "METHAMPHETAMINE")
                    cell = new PdfPCell(NewPhrase("Total METHAMPHETAMINE Adolescent Male Assigned Beds"));
                else if (ReportType == "BENZODIAZAPINES")
                    cell = new PdfPCell(NewPhrase("Total BENZODIAZAPINES Adolescent Male Assigned Beds"));
                else if (ReportType == "MEDICAID")
                    cell = new PdfPCell(NewPhrase("Total MEDICAID Adolescent Male Assigned Beds"));
                else if (ReportType == "PRIVATEINSURANCE")
                    cell = new PdfPCell(NewPhrase("Total PRIVATEINSURANCE Adolescent Male Assigned Beds"));
                else if (ReportType == "SELFPAY")
                    cell = new PdfPCell(NewPhrase("Total SELFPAY Adolescent Male Assigned Beds"));
                else if (ReportType == "OTHER")
                    cell = new PdfPCell(NewPhrase("Total OTHER Adolescent Male Assigned Beds"));
                else if (ReportType == "BedUtilizationReport")
                    cell = new PdfPCell(NewPhrase("Total Adult Adolescent Male Assigned Beds"));
                table.AddCell(cell);

                int TotalAdolescentMaleAssignedBeds = 0;
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdolescentMaleAssignedBeds"]))
                        {
                            TotalAdolescentMaleAssignedBeds = TotalAdolescentMaleAssignedBeds + Convert.ToInt32(r["TotalAdolescentMaleAssignedBeds"]);

                        }
                    }
                    cell = new PdfPCell(NewPhrase(TotalAdolescentMaleAssignedBeds.ToString()));
                }
                else
                {
                    cell = new PdfPCell(NewPhrase(""));
                }


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;



                //    TotalAdolescentFemaleAssignedBeds

                if (ReportType == "IVDRUG")
                    cell = new PdfPCell(NewPhrase("Total IVDRUG Adolescent Female Assigned Beds"));
                else if (ReportType == "CO")
                    cell = new PdfPCell(NewPhrase("Total CO Adolescent Female Assigned Beds"));
                else if (ReportType == "WITHDRAWAL")
                    cell = new PdfPCell(NewPhrase("Total WITHDRAWAL Adolescent Female Assigned Beds"));
                else if (ReportType == "ALCOHOL")
                    cell = new PdfPCell(NewPhrase("Total ALCOHOL Adolescent Female Assigned Beds"));
                else if (ReportType == "OPIOID")
                    cell = new PdfPCell(NewPhrase("Total OPIOID Adolescent Female Assigned Beds"));
                else if (ReportType == "METHAMPHETAMINE")
                    cell = new PdfPCell(NewPhrase("Total METHAMPHETAMINE Adolescent Female Assigned Beds"));
                else if (ReportType == "BENZODIAZAPINES")
                    cell = new PdfPCell(NewPhrase("Total BENZODIAZAPINES Adolescent Female Assigned Beds"));
                else if (ReportType == "MEDICAID")
                    cell = new PdfPCell(NewPhrase("Total MEDICAID Adolescent Female Assigned Beds"));
                else if (ReportType == "PRIVATEINSURANCE")
                    cell = new PdfPCell(NewPhrase("Total PRIVATEINSURANCE Adolescent Female Assigned Beds"));
                else if (ReportType == "SELFPAY")
                    cell = new PdfPCell(NewPhrase("Total SELFPAY Adolescent Female Assigned Beds"));
                else if (ReportType == "OTHER")
                    cell = new PdfPCell(NewPhrase("Total OTHER Adolescent Female Assigned Beds"));
                else if (ReportType == "BedUtilizationReport")
                    cell = new PdfPCell(NewPhrase("Total Adult Adolescent FeMale Assigned Beds"));
                table.AddCell(cell);

                int TotalAdolescentFemaleAssignedBeds = 0;
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in dsData.Tables[0].Rows)
                    {
                        if (!DBNull.Value.Equals(r["TotalAdolescentFemaleAssignedBeds"]))
                        {
                            TotalAdolescentFemaleAssignedBeds = TotalAdolescentFemaleAssignedBeds + Convert.ToInt32(r["TotalAdolescentFemaleAssignedBeds"]);

                        }
                    }
                    cell = new PdfPCell(NewPhrase(TotalAdolescentFemaleAssignedBeds.ToString()));
                }
                else
                {
                    cell = new PdfPCell(NewPhrase(""));
                }


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                //Total Beds

                cell = new PdfPCell(NewPhrase("Total Beds for Male"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultMale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                cell = new PdfPCell(NewPhrase("Total Beds for Female"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdultFemale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                cell = new PdfPCell(NewPhrase("Total Beds for Adolescent Male"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdolescentMale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;

                cell = new PdfPCell(NewPhrase("Total Beds for Adolescent Female"));
                table.AddCell(cell);

                //cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));

                if (dsData.Tables[0].Rows.Count > 0)
                    cell = new PdfPCell(NewPhrase(dsData.Tables[0].Rows[0]["AdolescentFemale"].ToString()));
                else
                    cell = new PdfPCell(NewPhrase(""));


                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);


                cCnt++;
            }
            #endregion

            return table;

        }




    }
}
