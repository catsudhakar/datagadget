//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserTreatmentProgram
    {
        public int PKey { get; set; }
        public System.Guid UserID { get; set; }
        public int TreatmentProgramID { get; set; }
    
        public virtual TreatmentProgram TreatmentProgram { get; set; }
    }
}
