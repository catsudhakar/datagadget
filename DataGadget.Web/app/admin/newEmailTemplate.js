﻿(function () {
    'use strict';
    var controllerId = 'newEmailTemplate';
    angular
        .module('app')
        .controller(controllerId, newEmailTemplate);
    //newUser.$inject = ['$scope', '$http', 'common', '$routeParams','staffInfo'];
    function newEmailTemplate($scope, $http, $modalInstance, emailInfo, $location, $route, common) {

        var userid = localStorage.getItem('userId');
        if (emailInfo) {
            $scope.title = "Edit Email Template";
        }
        else {

        }
        $scope.vm = {};
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var getLogFn1 = common.logger.getLogFn;
        var log1 = getLogFn1(controllerId, 'warn');

        activate();
        function activate() {
            if (emailInfo) {
                $http({
                    method: 'POST', url: 'api/EmailTemplate',
                    data: {
                        method: "GetTeplate",
                        parameters: [JSON.stringify(emailInfo)]
                    }
                })
                .success(function (result) {

                    $scope.vm.TemplateDesc = result.templateDesc;
                    $scope.vm.EmailTemplateName = emailInfo.emailTemplateName;
                    $scope.vm.ID = emailInfo.id;

                })
            }
            
        }
        $scope.ok = function (emailForm) {
            $http({
                method: 'POST', url: 'api/EmailTemplate',
                data: {
                    method: "CreateEmailTemplate",
                    parameters: [JSON.stringify($scope.vm)]
                }
            })
            .success(function (result) {
                $modalInstance.close(result);
                log("Email teplate updated successfully");
            })
            .error(function (error) {
            })
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    
})();
