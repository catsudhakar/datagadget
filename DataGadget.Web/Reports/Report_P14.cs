﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_P14 : Report_Base
    {
        public Report_P14()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;


        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }

        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;

            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Form P14 - Number of Evidence-Based Programs and Strategies by Type of Intervention"));

            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);



            this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            //////////////////////////////////////////////////////////////////////////////////////
            DataTable dtData = new DataTable();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_P14 ");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");
           

            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dtData);
            //        }
            //    }
            //}

            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dtData);
                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////

            PdfPTable table = new PdfPTable(7);
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase("", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Number of Programs and Strategies by Type of Intervention", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.Colspan = 6;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);


            cell = new PdfPCell(NewPhrase("", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("A.\nUniversal\nDirect", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("A.\nUniversal\nIndirect", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("C.\nUniversal\nTotal", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("D.\nSelective", Font.HELVETICA, 8, Font.BOLD));
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("E.\nIndicated", Font.HELVETICA, 8, Font.BOLD));
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("F.\nTotal", Font.HELVETICA, 8, Font.BOLD));
            cell.HorizontalAlignment = PdfCell.ALIGN_CENTER;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            table.HeaderRows = 2;

            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("1. Number of Evidence-Based\nPrograms and Strategies\nFunded"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct_EvBased"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Indirect_EvBased"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["UnivTotal_EvBased"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Selected_EvBased"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Indicated_EvBased"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Total_EvBased"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);
            
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("2. Total Number of Programs\nand Strategies Funded"));
            table.AddCell(cell);


            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Direct"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Univ_Indirect"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["UnivTotal"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Selected"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Indicated"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(dtData.Rows[0]["Total"].ToString()));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("3. Percent of Evidence-Based\nPrograms and Strategies"));
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDouble(dtData.Rows[0]["Univ_Direct_Perc"]).ToString("n2")));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDouble(dtData.Rows[0]["Univ_Indirect_Perc"]).ToString("n2")));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDouble(dtData.Rows[0]["UnivTotal_Perc"]).ToString("n2")));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDouble(dtData.Rows[0]["Selected_Perc"]).ToString("n2")));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDouble(dtData.Rows[0]["Indicated_Perc"]).ToString("n2")));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(Convert.ToDouble(dtData.Rows[0]["Total_Perc"]).ToString("n2")));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            table.AddCell(cell);

            cCnt++;
            //---------------------------------------------------------------------------------------------------

            return table;
        }


    }
}
