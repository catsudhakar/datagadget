﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace DataGadget.Web.Common
{
    public class DGExceptionLogger : ExceptionLogger
    {
        public override Task LogAsync(ExceptionLoggerContext context, System.Threading.CancellationToken cancellationToken)
        {
            WebLogger.Error(context.Exception.Message, context.Exception);
           // return context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(context.Exception, true));
            return base.LogAsync(context, cancellationToken);
        }
    }
}