﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataGadget.Web.Models;

namespace DataGadget.Web.ViewModels
{
    public class TreatmentProgramViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public int LocationId { get; set; }

        public bool IsActive { get; set; }

        public List<Location> LocationList { get; set; }


        public int ? MaleBeds { get; set; }
        public int  ? FemaleBeds { get; set; }
        public int ? AdolescentMaleBeds { get; set; }
        public int ? AdolescentFemaleBeds { get; set; }

        public bool ? Is60days { get; set; }
        public bool ? Is180days { get; set; }
    }
}