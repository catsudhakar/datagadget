﻿(function () {
    'use strict';
    var controllerId = 'ongoingEvents';
    angular
        .module('app')
        .controller(controllerId, ongoingEvents);

    ongoingEvents.$inject = ['$scope', '$http', '$location', 'common', '$modal', '$route', '$window', 'sessionService'];

    function ongoingEvents($scope, $http, $location, common, $modal, $route, $window, sessionService) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        $scope.title = 'ongoingEvents';
        var userid = localStorage.getItem('userId');
        var eventType = 1;
        var vm = this;
        vm.events = [];
        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;

        $scope.isNrep = false;

        $scope.CurrentDate = new Date();



        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
            $http({ method: 'POST', url: 'api/ongoingEvents', data: { method: "AllEvents", parameters: [localStorage.getItem('userInfo'), eventType, pageId] } })
         .success(function (result) {
             $scope.ispaging = false;
             vm.pagerData = result.item1[0];
             if (parseInt(vm.pagerData.numberOfPages) > 1) {
                 $scope.ispaging = true;
             }
             vm.pageList = [];
             if (pageId > 3) {
                 vm.pageList.push(pageId - 2);
                 vm.pageList.push(pageId - 1);
                 vm.pageList.push(pageId);
                 if (pageId < vm.pagerData.numberOfPages) {
                     vm.pageList.push(pageId + 1);
                     if (pageId < (vm.pagerData.numberOfPages - 1)) {
                         vm.pageList.push(pageId + 2);
                     }
                 }
             }
             else {
                 if (pageId == 1) {
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                     if (pageId + 4 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 4);
                     }

                 }
                 if (pageId == 2) {
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);

                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                 }
                 if (pageId == 3) {
                     vm.pageList.push(pageId - 2);
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                 }

             }

             var userData = result.item2;
             vm.events = userData;

             for (var i = 0; i < vm.events.length; i++) {
                 vm.events[i].compareDate = new Date(vm.events[i].endDate);
             }

         })
         .error(function (error) {
             console.log(error);
         })
        }
        function activate() {
            $scope.isuserLevel = true;
            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            //if (userInfo.level == "SIT3U") {

            //    $scope.isuserLevel = false;

            //}

            paging(1);
        }
        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                $http({ method: 'POST', url: 'api/ongoingEvents', data: { method: "Search", parameters: [localStorage.getItem('userInfo'), eventType, search] } })
                .success(function (result) {
                    vm.events = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    $scope.ispaging = false;
                    paging(1);
                }
            }
        }
        vm.newEvent = function () {
            if (!_.isEmpty(sessionService.getData())) {
                sessionService.setData({});
            }
            $location.path('/newOngoingEvent');
        }


        vm.addSession = function (event) {

            $http({
                method: 'POST', url: 'api/OngoingEvents',
                data: { method: "GetLatestEvent", parameters: [event.id] }
            })
               .success(function (result) {
                   if (result) {
                       sessionService.setData(result);
                   }
                   else {
                       sessionService.setData(event);
                   }
                   $location.path("/newOngoingEvent");

               })
               .error(function (error) {
                   logError(error.message);
               })

        
            //sessionService.setData(event);
            //$location.path("/newOngoingEvent");


        }

        vm.addpreSurvey = function (event) {
            event.age0 = 1;
       
            sessionService.setData(event);
            $location.path('/surveyTools');


        }

        vm.addPostSurvey = function (event) {
            event.age0 = 2;
            //var p = '/surveyTools/' + JSON.stringify(event);
            //$location.path(p);
            sessionService.setData(event);
            $location.path('/surveyTools');


        }

       

        vm.deleteEvent = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',

            });


            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/oneTimeEvent', data: { method: "DeleteEvent", parameters: [event.id] } })


               .success(function (result) {
                   var i = vm.events.indexOf(event)
                   if (i != -1) {
                       vm.events.splice(i, 1);
                   }
                   log("Event deleted successfully");
                   $route.reload();


               })
               .error(function (error) {
                   console.log(error);
               })

                $route.reload();
            }, function () {

            });
        };


        vm.viewReport = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/prevention/reportViewerModal.html',
                controller: 'reportViewerModal',
                backdrop: 'static',
                resolve: {
                    eventInfo: function () {
                        return event;
                    }
                }
            });
        };

        vm.Delegate = function (event) {


            var modalInstance = $modal.open({
                templateUrl: '/app/prevention/userListModal.html',
                controller: 'userListModal',
                backdrop: 'static',
                resolve: {
                    eventInfo: function () {
                        return event;
                    }
                }
            });
        };

        vm.loadSessions = function (e, event) {
            //$(e.target).parent().parent().next().toggle();
            $(e.target).parent().parent().next().toggleClass("expand")
            $(e.target).toggleClass('nestedBtn nestedBtnExpand');
            getSessions(event);
        }

        function getSessions(event) {
            $http({ method: 'POST', url: 'api/ongoingEvents', data: { method: "GetSessions", parameters: [event.id] } })
              .success(function (result) {
                  event.sessionList = result;
              })
            .error(function (error) {
                logError(error);
            })
        }

        vm.editDataListOpen = function (data) {

            data.from = 'onGoing';
            sessionService.setData(data);
            $location.path('/editEventData');

        }

        vm.editData = function (eventId) {

            //data.from = 'onGoing';

            $http({
                method: 'POST', url: 'api/OngoingEvents',
                data: { method: "GetEventByEventId", parameters: [eventId] }
            })
               .success(function (result) {
                   if (result) {
                       if (result.id > 0) {
                           result.from = 'session';
                           sessionService.setData(result);
                           $location.path('/editEventData');
                       }
                   }
               })
               .error(function (error) {
                   logError(error.message);
               })
          

        }
        

    }

})();












//(function () {
//    'use strict';

//    angular
//        .module('app')
//        .controller('ongoingEvents', ['$scope', ongoingEvents]);



//    function ongoingEvents($scope) {
//        $scope.title = 'ongoingEvents';
//        var vm = this;
//        activate();

//        function activate() {
//            console.log("this is ongoing events controller");
//        }
//    }
//})();
