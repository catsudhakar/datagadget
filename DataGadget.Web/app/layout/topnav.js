﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('topnav', topnav);

    topnav.$inject = ['$scope', '$location', '$window'];

    function topnav($scope, $location, $window) {
        $scope.title = 'topnav';
        var vm = this;
        activate();

        function activate() {
            $scope.isPrevention = false;
            $scope.isTreatment = false;
            $scope.isUserTab = false;
            $scope.isAdminTab = false;
            $scope.isGrantTab = false;
            $scope.isCommentTab = false;
            $scope.isUserCommentsTab = true;

            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            try {

                if ($window.localStorage["userInfo"]) {
                    userInfo = JSON.parse($window.localStorage["userInfo"]);
                }
            } catch (e) {
                console.log(e.message)
            }//rafeeq
            if (userInfo) {
                $scope.username = userInfo.firstName + ' ' + userInfo.lastName;



                if (userInfo.level == 'STA1A') {
                    //state level
                    $scope.isUserTab = true;
                    $scope.isAdminTab = true;
                } else if (userInfo.level == 'SIT2A') {
                    //admin level
                    $scope.isUserTab = true;
                    $scope.isAdminTab = false;
                } else if (userInfo.level == 'SIT3U') {
                    //user level
                    $scope.isUserTab = false;
                    $scope.isAdminTab = false;
                } else if (userInfo.level == 'Admin') {
                    //administrator level
                    $scope.isUserTab = true;
                    $scope.isAdminTab = true;
                    $scope.isCommentTab = true;
                    $scope.isUserCommentsTab = false;
                    $scope.isGrantTab = true;
                }
                else {
                    $scope.isUserTab = false;
                    $scope.isAdminTab = false;
                }

                //for (var i = 0; i < userInfo.locationCategoryList.length; i++) {
                //    var recNo = userInfo.locationCategoryList[i].categoryID;
                //    for (var j = 0; j < userInfo.selectedLocationCategoryList.length; j++) {
                //        var recNo1 = userInfo.selectedLocationCategoryList[j].categoryID;

                //        if (recNo == recNo1) {
                //            if (userInfo.locationCategoryList[i].surveyCategory == 'Prevention') {
                //                $scope.isPrevention = true;
                //            }
                //            else if (userInfo.locationCategoryList[i].surveyCategory == 'Treatment') {
                //                $scope.isTreatment = true;
                //            }
                //            break;
                //        }

                //    }
                //}

                for (var j = 0; j < userInfo.selectedLocationCategoryList.length; j++) {

                    if (userInfo.selectedLocationCategoryList[j].surveyCategory == 'Prevention') {
                        $scope.isPrevention = true;
                    }
                    else if (userInfo.selectedLocationCategoryList[j].surveyCategory == 'Treatment') {
                        $scope.isTreatment = true;
                    }

                }

            }
        }


        $scope.surveyToolsClick = function () {
            localStorage.removeItem("SurveyCount");
            $location.path("/surveyTools");
        }
        //$scope.ongoingEventsClick = function () {
        //    $location.path("/surveyTools");
        //}

        $scope.ongoingEventsClick = function () {
            $location.path("/ongoingEvents");
        }

        $scope.oneTimeEventsClick = function () {
            $location.path("/oneTimeEvents");
        }

        $scope.DischargeClick = function () {
            $location.path("/discharges");
        }

        $scope.reportsClick = function () {
            $location.path("/reports");
        }

        $scope.exportDataClick = function () {
            $location.path("/exportData");
        }

        $scope.treatmentsClick = function () {
            $location.path("/treatments");
        }




        $scope.preventionOutcomesClick = function () {
            $location.path("/preventionOutcomes");
        }
        $scope.treatmentOutcomesClick = function () {
            $location.path("/treatmentOutcomes");
        }

        $scope.utilizationRateClick = function () {
            $location.path("/utilizationRate");
        }

        $scope.StaffDevelopmentReportClick = function () {
            $location.path("/staffDevelopmentReport");
        }

        $scope.DischargesReportClick = function () {
            $location.path("/dischargesReport");
        }

        $scope.followupReportClick = function () {
            $location.path("/followupReport");
        }

        $scope.indigentFundReportClick = function () {
            $location.path("/indigentFundReport");
        }

        $scope.ReportClick = function () {
            $location.path("/reports");
        }

        $scope.bedUilizationReport = function () {
            $location.path("/bedUtilizationReport");
        }
        



        $scope.ExportUserListClick = function () {
            $location.path("/ExportUserList");
        }



        $scope.logOut = function () {
            localStorage.clear();
            sessionStorage.clear();
            try {
                $window.localStorage["userInfo"] = null;
            } catch (e) {
                console.log(e.message)
            }//rafeeq
            $location.path('/');
        }

        $scope.treatmentSurveyToolsClick = function () {


            sessionStorage.removeItem('patientId');
            sessionStorage.removeItem('surveyAdmin');
            sessionStorage.removeItem('gender');
            sessionStorage.removeItem('locationId');
            sessionStorage.removeItem('serviceID');

            sessionStorage.removeItem('dischargeDate');
            sessionStorage.removeItem("programId")
            sessionStorage.removeItem('entryDate');
            sessionStorage.removeItem('from');

            $location.path("/treatmentSurveyTools");
        }

        $scope.waitingdataClick = function () {

            $location.path("/waitingList");
        }

        $scope.StatewideInitiativesClick = function () {
            $location.path("/StatewideInitiativesList");
        }
        $scope.programsStateClick = function () {
            $location.path("/programsandStrategiesStateList");
        }
        $scope.programsNREPPClick = function () {
            $location.path("/programsandStrategiesNREPPList");
        }
        $scope.siteUsageClick = function () {
            $location.path("/siteUsage");
        }
        $scope.editDataClick = function () {
            $location.path("/editData");
        }

        $scope.resourceLinkClick = function () {
            $location.path("/resourceLinksList");
        }

        $scope.grantYearClick = function () {
            $location.path("/setGrantYear");
        }
        $scope.Comments = function () {
            $location.path("/ManageComments");
        }
        $scope.EmailTemplate = function () {
            $location.path("/emailTemplates");
        }


        $scope.ProvidersClick = function () {
            $location.path("/ProviderList");
        }

        $scope.BedCountClick = function () {
            $location.path("/bedCountList");
        }

        $scope.ServiceLocationClick = function () {
            $location.path("/serviceLocations");
        }

        $scope.weeklyMailTemplateClick = function () {
            $location.path('/WeeklyMailTemplate');
        }

        $scope.treatmentPrograms = function () {
            $location.path('/treatmentPrograms');
        }

        $scope.treatmentConfigClick = function () {
            $location.path("/treatmentConfig");
        }

        $scope.mailConfigClick = function () {
            $location.path("/mailConfig");
        }

    }
})();
