﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('setGrantYear', setGrantYear);

    var controllerId = "setGrantYear";
    setGrantYear.$inject = ['$scope', '$http', '$modal', '$window', '$location','common'];

    function setGrantYear($scope, $http, $modal, $window, $location, common) {
        $scope.title = 'setGrantYear';
        
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var vm = this;
        vm.grantYears = [];
        activate();

        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }

            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            if (userInfo.level == 'Admin') {
                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "AllGrantYears", parameters: [userInfo.level] } })
                .success(function (result) {
                    vm.grantYears = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                vm.grantYears = [];
            }
        }

        vm.addyear = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/addGrantYear.html',
                controller: 'addGrantYear',
                resolve: {
                    resourcesInfo: function () {
                        return undefined;
                    }
                }

            });
            addUserModal.result.then(function (link) {
                // vm.links.push(link);
                //activate();
                vm.grantYears.push(link);
            }, function () {
            });
        }

        vm.edityear = function (year) {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/addGrantYear.html',
                controller: 'addGrantYear',
                resolve: {
                    resourcesInfo: function () {
                        return year;
                    }
                }

            });
            addUserModal.result.then(function (link) {
                // vm.links.push(link);
                activate();

            }, function () {
            });
        }

        vm.deleteyear = function (year) {
            
            if (year.isActive==null) {
                year.isActive = false;
            }
            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',

                backdrop: 'static',

            });


            modalInstance.result.then(function () {

                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "DeleteGrantYear", parameters: [JSON.stringify(year)] } })

            .success(function (result) {
                
                var i = vm.grantYears.indexOf(year);
                if (i != -1) {
                    vm.grantYears.splice(i, 1);
                }
                log("Grant year deleted successfully");

            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {});
        };
    }
})();
