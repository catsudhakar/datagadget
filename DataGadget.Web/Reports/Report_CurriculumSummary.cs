﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;

using System.Globalization;

using DataGadget.Web.Controllers;

namespace DataGadget.Web.Reports
{
    public class Report_CurriculumSummary
    {
        protected Color cellHeaderColor = Color.YELLOW;
        protected Color cellItemColor = Color.WHITE;
        protected Color cellAlternatingItemColor = new Color(231, 245, 254);
        protected readonly int stdFontSize = 12;

        protected Document _pdfDoc = new Document();


        public Report_CurriculumSummary()
        {
            _pdfDoc = new Document(PageSize.LETTER, 18, 18, 36, 36);

        }


        public Models.OneTimeEvent ThisCurriculum;


        #region "header/footer"

        protected Paragraph HeaderImageElement()
        {
            Paragraph pdfParagraph = new Paragraph();
            //Chunk chunk;

            //iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(Path.Combine(ImagesPath, "ancomm_logo_green.jpg"));
            //pdfImage.ScalePercent(75);
            //chunk = new Chunk(pdfImage, 0, 0);
            //pdfParagraph.Add(chunk);


            return pdfParagraph;
        }

        protected HeaderFooter GetDocumentHeader(string reportTitle)
        {
            HeaderFooter header = new HeaderFooter(new Phrase(reportTitle), false);
            header.Border = Rectangle.NO_BORDER;
            return header;
        }

        protected HeaderFooter GetDocumentFooter()
        {
            HeaderFooter footer = new HeaderFooter(new Phrase("Page "), true);
            footer.Border = Rectangle.NO_BORDER;
            return footer;
        }

        #endregion

        #region "paragraph"

        protected Paragraph NewParagraph(string text)
        {
            return NewParagraph(text, Font.HELVETICA, stdFontSize);
        }

        protected Paragraph NewParagraph(string text, int fontFamily)
        {
            return NewParagraph(text, fontFamily, stdFontSize);
        }

        protected Paragraph NewParagraph(string text, int fontFamily, int fontSize)
        {
            return NewParagraph(text, fontFamily, fontSize, Font.NORMAL);
        }

        protected Paragraph NewParagraph(string text, int fontFamily, float fontSize, int fontStyle)
        {
            Paragraph pdfParagraph = new Paragraph(text, new Font(fontFamily, fontSize, fontStyle));
            return pdfParagraph;
        }

        #endregion

        #region "phrase"

        protected Phrase NewPhrase(string text)
        {
            return NewPhrase(text, Font.HELVETICA, stdFontSize);
        }

        protected Phrase NewPhrase(string text, int fontFamily)
        {
            return NewPhrase(text, fontFamily, stdFontSize);
        }

        protected Phrase NewPhrase(string text, int fontFamily, float fontSize)
        {
            return NewPhrase(text, fontFamily, fontSize, Font.NORMAL);
        }

        protected Phrase NewPhrase(string text, int fontFamily, float fontSize, int fontStyle)
        {
            Phrase pdfPhrase = new Phrase(text, new Font(fontFamily, fontSize, fontStyle));
            return pdfPhrase;
        }

        #endregion

        #region "cell"

        protected Color CellBackgroundColor(int cCnt)
        {
            if ((cCnt % 2) == 0)
                return cellItemColor;
            else
                return cellAlternatingItemColor;
        }

        #endregion

        protected int GetPerc(int a, int b)
        {
            try
            {
                return (int)(((decimal)a / (decimal)b) * (decimal)100);
            }
            catch
            {
                return 0;
            }
        }

        protected int GetDataRowInt(DataRow dataRow, string fieldName)
        {
            try
            {
                return int.Parse(dataRow[fieldName].ToString());
            }
            catch
            {
                return 0;
            }
        }

        protected float GetDataRowFloat(DataRow dataRow, string fieldName)
        {
            try
            {
                return float.Parse(dataRow[fieldName].ToString());
            }
            catch
            {
                return 0.0F;
            }
        }

        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }

        public void GenerateToMemoryStream(MemoryStream memStream, Models.OneTimeEvent onetime)
        {
            ThisCurriculum = onetime;
            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }



        public void GenerateToMemoryStreamForStaff(MemoryStream memStream, Models.StaffAdmin onetime)
        {

            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;

            CreateReport1(onetime);


        }

        private void CreateReport1(Models.StaffAdmin onetime)
        {

            this._pdfDoc.Open();

            BuildReportListStaff(onetime);

            this._pdfDoc.Close();

        }

        private void BuildReportListStaff(Models.StaffAdmin onetime)
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            //this._pdfDoc.Add(NewParagraph(ThisCurriculum.OrgName, Font.HELVETICA, 12, Font.BOLD));

            this._pdfDoc.Add(NewParagraph("Development Summary"));

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);



            PdfPTable table = GeneralStaffTable(onetime); // new PdfPTable(1);
            PdfPCell cell;

            this._pdfDoc.Add(table);


            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);



        }

        private PdfPTable GeneralStaffTable(Models.StaffAdmin onetime)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;


            table.HeaderRows = 0;

            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Training Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.TrainingName));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Provider Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.ProviderName));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Other Provider Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.OtherProviderName));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------
            cell = new PdfPCell(NewPhrase("Start Date"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase(onetime.StartDate.HasValue ? onetime.StartDate.Value.ToShortDateString() : ""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------
            cell = new PdfPCell(NewPhrase("End Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.EndDate.HasValue ? onetime.EndDate.Value.ToShortDateString() : ""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------
            cell = new PdfPCell(NewPhrase("Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.Hours));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------
            cell = new PdfPCell(NewPhrase("Instructor’s Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.InstructorName));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------Instructor’s Credentials

            cell = new PdfPCell(NewPhrase("Instructor’s Credentials"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.TrainerDeatils));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------

            cell = new PdfPCell(NewPhrase("Learning Objectives"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(onetime.LearningObjectives));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //----------------------------------------------------

            return table;
        }
        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph(ThisCurriculum.OrgName, Font.HELVETICA, 12, Font.BOLD));

            this._pdfDoc.Add(NewParagraph("Event Summary"));

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);

            //pdfParagraph = NewParagraph(" ");
            //this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);
            PdfPCell cell;

            this._pdfDoc.Add(table);


            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            //////////////////////////////////////////////////////////////////////////////////
            table = new PdfPTable(3);

            float[] headerwidths = { 49, 2, 49 }; // percentage
            table.SetWidths(headerwidths);

            cell = new PdfPCell(ParticipantsTable());
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            cell = new PdfPCell(NewParagraph(" "));
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            cell = new PdfPCell(HighRiskTable());
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            this._pdfDoc.Add(table);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            //////////////////////////////////////////////////////////////////////////////////

            table = new PdfPTable(3);
            table.SetWidths(headerwidths);

            cell = new PdfPCell(StrategiesTable());
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            //cell = new PdfPCell(HoursTable());
            //cell.Border = Rectangle.NO_BORDER;
            //table.AddCell(cell);
            cell = new PdfPCell(NewParagraph(" "));
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            cell = new PdfPCell(DocOfActivityTable());
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            this._pdfDoc.Add(table);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            //////////////////////////////////////////////////////////////////////////////////

            int numEmployees = 0;
            double travelHours = 0.0;
            double dsHours = 0.0;
            double prepHours = 0.0;

            table = new PdfPTable(3);
            table.SetWidths(headerwidths);

            cell = new PdfPCell(HoursTable(ThisCurriculum));
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            cell = new PdfPCell(NewParagraph(" "));
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            cell = new PdfPCell(NewParagraph(" "));
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            this._pdfDoc.Add(table);

            numEmployees += ThisCurriculum.NumberOfEmployees;
            travelHours += (double)ThisCurriculum.TravelHours;
            dsHours += (double)ThisCurriculum.DirectServiceHours;
            prepHours += (double)ThisCurriculum.PrepHours;

            // foreach (Curriculum curriculum in ThisCurriculum.ChildEvents)
            if (ThisCurriculum.lstEvents != null)
            {
                foreach (Models.OneTimeEvent curriculum in ThisCurriculum.lstEvents)
                {
                    table = new PdfPTable(3);
                    table.SetWidths(headerwidths);

                    cell = new PdfPCell(HoursTable(curriculum));
                    cell.Border = Rectangle.NO_BORDER;
                    table.AddCell(cell);

                    cell = new PdfPCell(NewParagraph(" "));
                    cell.Border = Rectangle.NO_BORDER;
                    table.AddCell(cell);

                    cell = new PdfPCell(NewParagraph(" "));
                    cell.Border = Rectangle.NO_BORDER;
                    table.AddCell(cell);

                    this._pdfDoc.Add(table);

                    numEmployees += curriculum.NumberOfEmployees;
                    travelHours += (double)curriculum.TravelHours;
                    dsHours += (double)curriculum.DirectServiceHours;
                    prepHours += (double)curriculum.PrepHours;

                }
            }

            //if (ThisCurriculum.IsCurriculum && ThisCurriculum.closed)
            if (ThisCurriculum.closed == 1)
            {

                table = new PdfPTable(3);
                table.SetWidths(headerwidths);

                cell = new PdfPCell(HoursSummaryTable((DateTime)ThisCurriculum.BeginDate, (DateTime)ThisCurriculum.EndDate, numEmployees, travelHours, dsHours, prepHours));
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(NewParagraph(" "));
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                cell = new PdfPCell(NewParagraph(" "));
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);

                this._pdfDoc.Add(table);

            }

            //////////////////////////////////////////////////////////////////////////////////

            if (ThisCurriculum.closed == 1) //if (ThisCurriculum.IsCurriculum && ThisCurriculum.Closed)
            {
                this._pdfDoc.Add(NewParagraph(" "));
                //this._pdfDoc.Add(CloseQuestionsTable());
            }
            //////////////////////////////////////////////////////////////////////////////////

            //this._pdfDoc.Add(ParticipantsTable());

            //this._pdfDoc.Add(HighRiskTable());

        }

        private PdfPTable GeneralTable()
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;
            string Edate = string.Empty;

            if (ThisCurriculum.lstEvents != null)
            {
                if (ThisCurriculum.lstEvents.Count > 0 && ThisCurriculum.Curriculum == 1)  //&& ThisCurriculum.closed == 1
                {
                    List<Models.OneTimeEvent> lstOnetime = new List<Models.OneTimeEvent>();
                    lstOnetime = ThisCurriculum.lstEvents.OrderBy(x => x.ID).ToList();

                    Edate = lstOnetime[lstOnetime.Count - 1].EndDate.HasValue ? lstOnetime[lstOnetime.Count - 1].EndDate.Value.ToShortDateString() : "";

                    //foreach (var item in ThisCurriculum.lstEvents)
                    //{
                    //    if (item.closed == 1)
                    //    {
                    //        Edate = item.EndDate.HasValue ? item.EndDate.Value.ToShortDateString() : "";
                    //        break;
                    //    }
                    //}
                }
                //Edate = ThisCurriculum.lstEvents[ThisCurriculum.lstEvents.Count - 1].EndDate.HasValue ? ThisCurriculum.lstEvents[ThisCurriculum.lstEvents.Count - 1].EndDate.Value.ToShortDateString() : "";
                else
                    Edate = ThisCurriculum.EndDate.HasValue ? ThisCurriculum.EndDate.Value.ToShortDateString() : "";
            }
            else
            {
                Edate = ThisCurriculum.EndDate.HasValue ? ThisCurriculum.EndDate.Value.ToShortDateString() : "";
            }


            table.HeaderRows = 0;

            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Program Base"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(ThisCurriculum.ProgramType_IP));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Begin Date"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(ThisCurriculum.StartDate.HasValue ? ThisCurriculum.StartDate.Value.ToShortDateString() : ""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("End Date"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);

            //cell = new PdfPCell(NewPhrase(ThisCurriculum.EndDate.HasValue ? ThisCurriculum.EndDate.Value.ToShortDateString() : ""));
            cell = new PdfPCell(NewPhrase(Edate));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Intervention Type"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(ThisCurriculum.InterventionType));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Program Type"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            /// cell = new PdfPCell(NewPhrase(ThisCurriculum.TypeOfProgram_EA + ", " + ThisCurriculum.UniversalType_DI));
            /// 

            if (ThisCurriculum.TyoeOfProgram_EA.Trim() == "Evidence")
                cell = new PdfPCell(NewPhrase("Evidence Based" + ", " + ThisCurriculum.UniversalType_DI));
            else
                cell = new PdfPCell(NewPhrase(ThisCurriculum.TyoeOfProgram_EA + ", " + ThisCurriculum.UniversalType_DI));

            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Program Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(ThisCurriculum.ProgramName));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Group Name"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(ThisCurriculum.GroupName));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase("Service Location"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            if (string.IsNullOrEmpty(ThisCurriculum.ServiceSchoolName))
            {
                cell = new PdfPCell(NewPhrase(ThisCurriculum.ServiceLocationName));
            }
            else
            {
                cell = new PdfPCell(NewPhrase(ThisCurriculum.ServiceLocationName + ", " + ThisCurriculum.ServiceSchoolName));
            }

            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //////////////////////////////////////////////////////////////////////////////////////////

            //cell = new PdfPCell(NewPhrase("School Name"));
            //cell.BackgroundColor = CellBackgroundColor(cCnt);
            //table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.ServiceSchoolName));

            //cell.BackgroundColor = CellBackgroundColor(cCnt);
            //table.AddCell(cell);
            //cCnt++;

            return table;
        }

        private PdfPTable ParticipantsTable()
        {
            Models.OneTimeEvent lstSessionData = new Models.OneTimeEvent();

            if (ThisCurriculum.lstEvents != null)
            {
                if (ThisCurriculum.lstEvents.Count > 0 && ThisCurriculum.Curriculum == 1)  //&& ThisCurriculum.closed == 1
                {
                    //List<Models.OneTimeEvent> lstOnetime = new List<Models.OneTimeEvent>();

                    lstSessionData = ThisCurriculum.lstEvents.OrderByDescending(x => x.ID).First();
                }
                else
                {
                    lstSessionData = ThisCurriculum;
                }

            }
            else
            {
                lstSessionData = ThisCurriculum;
            }

            PdfPTable table = new PdfPTable(3);
            PdfPCell cell;


            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Participant Count", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.NumberParticipants.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.NumberParticipants.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Gender", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Male"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.Males.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Males.ToString()));

            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Female"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            // cell = new PdfPCell(NewPhrase(ThisCurriculum.Females.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Females.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Age", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("0-4"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.Age0.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Age0.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("5-11"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            // cell = new PdfPCell(NewPhrase(ThisCurriculum.Age5.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Age5.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("12-14"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.Age12.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Age12.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("15-17"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.Age15.ToString()));

            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("18-20"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            // cell = new PdfPCell(NewPhrase(ThisCurriculum.Age18.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Age18.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("21-24"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.Age21.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Age21.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("25-44"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(ThisCurriculum.Age25.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.Age25.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("45-64"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.Age45.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("65 and over"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.Age65.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Race", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("White"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceWhite.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Black"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceBlack.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Asian"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceAsian.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("American Indian/Alaskan Native"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceAmIndian.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Native Hawaiian/Other Pacific Islander"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceHawaii.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Unknown or Other"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceUnknownOther.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("More than one"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.RaceMoreThanOne.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase("Ethnicity", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Non-Hispanic"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.NotHispanic.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Hispanic"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.Hispanic.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------


            return table;
        }

        private PdfPTable HighRiskTable()
        {


            Models.OneTimeEvent lstSessionData = new Models.OneTimeEvent();
            if (ThisCurriculum.lstEvents != null)
            {
                if (ThisCurriculum.lstEvents.Count > 0 && ThisCurriculum.Curriculum == 1)  //&& ThisCurriculum.closed == 1
                {
                    lstSessionData = ThisCurriculum.lstEvents.OrderByDescending(x => x.ID).First();
                }
                else
                {
                    lstSessionData = ThisCurriculum;
                }

            }
            else
            {
                lstSessionData = ThisCurriculum;
            }


            PdfPTable table = new PdfPTable(3);
            PdfPCell cell;


            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("High Risk", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(""));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Children of substance abusers"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            // cell = new PdfPCell(NewPhrase(ThisCurriculum.D_HiRi_ChildOfSA.ToString()));
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_ChildOfSA.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Violent and delinquent behavior"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_Violent.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Physically Disabled"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_PhysDis.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Homeless and/or runaway youth"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_Homel.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Pregnant women/teens"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_PregUse.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Mental Health Problems"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_MentH.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Abuse Victims"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_AbuVict.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Drop-outs"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_K12DO.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Economically Disadvantaged"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_EconDis.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Already Using Substances"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_Using.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------
            cell = new PdfPCell(NewPhrase(""));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase("Other"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(lstSessionData.D_HiRi_Other.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;
            //---------------------------------------------------------------------------------------------------

            if (CellBackgroundColor(cCnt) == cellItemColor)
            {
                cell = new PdfPCell(NewPhrase(""));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(""));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(""));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);

            }

            return table;
        }

        private PdfPTable StrategiesTable()
        {
            PdfPTable table = new PdfPTable(1);
            PdfPCell cell;

            Models.OneTimeEvent lstSessionData = new Models.OneTimeEvent();

            if (ThisCurriculum.lstEvents != null)
            {
                if (ThisCurriculum.lstEvents.Count > 0 && ThisCurriculum.Curriculum == 1)  //&& ThisCurriculum.closed == 1
                {
                    //List<Models.OneTimeEvent> lstOnetime = new List<Models.OneTimeEvent>();

                    lstSessionData = ThisCurriculum.lstEvents.OrderByDescending(x => x.ID).First();
                }
                else
                {
                    lstSessionData = ThisCurriculum;
                }

            }
            else
            {
                lstSessionData = ThisCurriculum;
            }


            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("CSAP Strategies", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);

            cCnt++;

            //if (ThisCurriculum.Strat_ID_01 == 1)
            if (lstSessionData.Strat_ID_01 == 1)
                AddCSAPRow(table, ref cCnt, "01 - Clearinghouse/Information Resource Center");

            if (lstSessionData.Strat_ID_02 == 1)
                AddCSAPRow(table, ref cCnt, "02 - Resource Directories " + "(# distributed: " + lstSessionData.ID02_DirectoriesDistributed.ToString() + ")");

            if (lstSessionData.Strat_ID_03 == 1)
                AddCSAPRow(table, ref cCnt, "03 - Media Campaign");

            if (lstSessionData.Strat_ID_04 == 1)
                AddCSAPRow(table, ref cCnt, "04 - Brochures " + "(# distributed: " + lstSessionData.ID04_BrochuresDistributed.ToString() + ")");

            if (lstSessionData.Strat_ID_05 == 1)
                AddCSAPRow(table, ref cCnt, "05 - Radio and TV Public Service Announcements");

            if (lstSessionData.Strat_ID_06 == 1)
                AddCSAPRow(table, ref cCnt, "06 - Speaking Engagements");

            if (lstSessionData.Strat_ID_07 == 1)
                AddCSAPRow(table, ref cCnt, "07 - Health Fairs and Other Health Information");

            if (lstSessionData.Strat_ID_08 == 1)
                AddCSAPRow(table, ref cCnt, "08 - Information Lines/Hot Lines");

            if (lstSessionData.Strat_ID_09 == 1)
                AddCSAPRow(table, ref cCnt, "09 - Other (" + lstSessionData.Strat_ID_09Text + ")");


            if (lstSessionData.Strat_Ed_11 == 1)
                AddCSAPRow(table, ref cCnt, "11 - Parenting and Family Management");

            if (lstSessionData.Strat_Ed_12 == 1)
                AddCSAPRow(table, ref cCnt, "12 - Ongoing Classroom and/or Small Group Sessions");

            if (lstSessionData.Strat_Ed_13 == 1)
                AddCSAPRow(table, ref cCnt, "13 - Peer Leader/Helper Group");

            if (lstSessionData.Strat_Ed_14 == 1)
                AddCSAPRow(table, ref cCnt, "14 - Education Programs for Youth Groups");

            if (lstSessionData.Strat_Ed_15 == 1)
                AddCSAPRow(table, ref cCnt, "15 - Mentors");

            if (lstSessionData.Strat_Ed_16 == 1)
                AddCSAPRow(table, ref cCnt, "16 - Preschool ATOD Prevention Programs");

            if (lstSessionData.Strat_Ed_17 == 1)
                AddCSAPRow(table, ref cCnt, "17 - Other (" + lstSessionData.Strat_Ed_17Text + ")");


            if (lstSessionData.Strat_Alt_21 == 1)
                AddCSAPRow(table, ref cCnt, "21 - Drug-free Dances and Parties");

            if (lstSessionData.Strat_Alt_22 == 1)
                AddCSAPRow(table, ref cCnt, "22 - Youth/Adult Leadership Activities");

            if (lstSessionData.Strat_Alt_23 == 1)
                AddCSAPRow(table, ref cCnt, "23 - Community Drop-In Centers");

            if (lstSessionData.Strat_Alt_24 == 1)
                AddCSAPRow(table, ref cCnt, "24 - Community Service Activities");

            if (lstSessionData.Strat_Alt_25 == 1)
                AddCSAPRow(table, ref cCnt, "25 - Outward Bound");

            if (lstSessionData.Strat_Alt_26 == 1)
                AddCSAPRow(table, ref cCnt, "26 - Recreation Activities");

            if (lstSessionData.Strat_Alt_27 == 1)
                AddCSAPRow(table, ref cCnt, "27 - Other (" + lstSessionData.Strat_Alt_27Text + ")");



            if (lstSessionData.Strat_IR_31 == 1)
                AddCSAPRow(table, ref cCnt, "31 - Employee Assistance Program");

            if (lstSessionData.Strat_IR_32 == 1)
                AddCSAPRow(table, ref cCnt, "32 - Student Assistance Program");

            if (lstSessionData.Strat_IR_34 == 1)
                AddCSAPRow(table, ref cCnt, "34 - Other (" + lstSessionData.Strat_IR_34Text + ")");



            if (lstSessionData.Strat_CBP_41 == 1)
                AddCSAPRow(table, ref cCnt, "41 - Community and Volunteer Training");

            if (lstSessionData.Strat_CBP_42 == 1)
                AddCSAPRow(table, ref cCnt, "42 - Systematic Planning");

            if (lstSessionData.Strat_CBP_43 == 1)
                AddCSAPRow(table, ref cCnt, "43 - Multi-Agency Coordination");

            if (lstSessionData.Strat_CBP_44 == 1)
                AddCSAPRow(table, ref cCnt, "44 - Community Team-Building");

            if (lstSessionData.Strat_CBP_45 == 1)
                AddCSAPRow(table, ref cCnt, "45 - Assessing Services and Funding");

            if (lstSessionData.Strat_CBP_Faith == 1)
                AddCSAPRow(table, ref cCnt, "46 - Faith-Based ");

            if (lstSessionData.Strat_CBP_46 == 1)
                AddCSAPRow(table, ref cCnt, "47 - Other (" + lstSessionData.Strat_CBP_46Text + ")");

            




            if (lstSessionData.Strat_Env_51 == 1)
                AddCSAPRow(table, ref cCnt, "51 - Promoting the Establishment of Review of ATOD Use Policies in Schools");

            if (lstSessionData.Strat_Env_52 == 1)
                AddCSAPRow(table, ref cCnt, "52 - Guidance and TA on Monitoring Enforcement Governing Availability and Distribution of ATOD");

            if (lstSessionData.Strat_Env_53 == 1)
                AddCSAPRow(table, ref cCnt, "53 - Modifying Alcohol and Tobacco Advertising Practices");

            if (lstSessionData.Strat_Env_54 == 1)
                AddCSAPRow(table, ref cCnt, "54 - Produce Pricing Strategies");

            if (lstSessionData.Strat_Env_55 == 1)
                AddCSAPRow(table, ref cCnt, "55 - SYNAR");

            if (lstSessionData.Strat_Env_56_MW == 1)
                AddCSAPRow(table, ref cCnt, "56 - Meth Watch");

            if (lstSessionData.Strat_Env_57 == 1)
                AddCSAPRow(table, ref cCnt, "57 - Other (" + lstSessionData.Strat_Env_57Text + ")");


            //---------------------------------------------------------------------------------------------------


            return table;
        }

        protected void AddCSAPRow(PdfPTable table, ref int cCnt, string text)
        {
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase(text));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);


            cCnt++;
        }

        protected void AddDocActivRow(PdfPTable table, ref int cCnt, string text)
        {
            PdfPCell cell;

            cell = new PdfPCell(NewPhrase(text));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);


            cCnt++;
        }

        private PdfPTable DocOfActivityTable()
        {
            PdfPTable table = new PdfPTable(1);
            PdfPCell cell;


            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Documentation of Activities", Font.HELVETICA, 12, Font.BOLD));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            if (ThisCurriculum.DocOfActivities_Agenda == 1)
                AddDocActivRow(table, ref cCnt, "Agenda");

            if (ThisCurriculum.DocOfActivities_EvalForm == 1)
                AddDocActivRow(table, ref cCnt, "Evaluation Form");

            if (ThisCurriculum.DocOfActivities_PrePostTest == 1)
                AddDocActivRow(table, ref cCnt, "Pre/Post Test");

            if (ThisCurriculum.DocOfActivities_ContactForm == 1)
                AddDocActivRow(table, ref cCnt, "Contact Form/Progress Note");

            if (ThisCurriculum.DocOfActivities_SignIn == 1)
                AddDocActivRow(table, ref cCnt, "Sign-in Sheet");

            if (ThisCurriculum.DocOfActivities_Brochure == 1)
                AddDocActivRow(table, ref cCnt, "Brochure");

            if (ThisCurriculum.DocOfActivities_Other == 1)
                AddDocActivRow(table, ref cCnt, "Other");


            //---------------------------------------------------------------------------------------------------


            return table;
        }

        private PdfPTable HoursTable(Models.OneTimeEvent curriculum)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;

            int cCnt = 0;

            cell = new PdfPCell(NewPhrase("Hours for " + curriculum.BeginDate.Value.ToShortDateString(), Font.HELVETICA, 12, Font.BOLD));
            cell.Colspan = 2;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Number of Employees"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(curriculum.NumberOfEmployees.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Travel Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(curriculum.TravelHours.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Direct Service Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(curriculum.DirectServiceHours.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Prep Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(curriculum.PrepHours.ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Total Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase((curriculum.TravelHours + curriculum.DirectServiceHours + curriculum.PrepHours).ToString()));
            //cell = new PdfPCell(NewPhrase((curriculum.TravelHours + curriculum.DirectServiceHours + curriculum.PrepHours + (decimal)curriculum.NumberOfEmployees).ToString()));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            //---------------------------------------------------------------------------------------------------


            return table;
        }

        private PdfPTable HoursSummaryTable(DateTime startDate, DateTime endDate,
                        int numEmployees, double travelHours, double dsHours, double prepHours)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;
            string Edate;

            int cCnt = 0;

            if (ThisCurriculum.lstEvents != null)
            {
                if (ThisCurriculum.lstEvents.Count > 0 && ThisCurriculum.Curriculum == 1)  //&& ThisCurriculum.closed == 1
                {
                    List<Models.OneTimeEvent> lstOnetime = new List<Models.OneTimeEvent>();
                    lstOnetime = ThisCurriculum.lstEvents.OrderBy(x => x.ID).ToList();

                    //Edate = lstOnetime[lstOnetime.Count - 1].EndDate.HasValue ? lstOnetime[lstOnetime.Count - 1].EndDate.Value.ToShortDateString() : "";
                    Edate = lstOnetime[lstOnetime.Count - 1].BeginDate.HasValue ? lstOnetime[lstOnetime.Count - 1].BeginDate.Value.ToShortDateString() : "";


                }

                else
                    Edate = ThisCurriculum.BeginDate.HasValue ? ThisCurriculum.BeginDate.Value.ToShortDateString() : "";
            }
            else
            {
                Edate = ThisCurriculum.BeginDate.HasValue ? ThisCurriculum.BeginDate.Value.ToShortDateString() : "";
            }

            //cell = new PdfPCell(NewPhrase("Sum of Hours for " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString(), Font.HELVETICA, 12, Font.BOLD));
            cell = new PdfPCell(NewPhrase("Sum of Hours for " + startDate.ToShortDateString() + " - " + Edate, Font.HELVETICA, 12, Font.BOLD));
            cell.Colspan = 2;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            //cell = new PdfPCell(NewPhrase("Number of Employees"));
            //cell.BackgroundColor = CellBackgroundColor(cCnt);
            //table.AddCell(cell);
            //cell = new PdfPCell(NewPhrase(numEmployees.ToString()));
            //cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            //cell.BackgroundColor = CellBackgroundColor(cCnt);
            //table.AddCell(cell);
            //cCnt++;

            cell = new PdfPCell(NewPhrase("Travel Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(travelHours.ToString("n2")));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Direct Service Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(dsHours.ToString("n2")));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Prep Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase(prepHours.ToString("n2")));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            cell = new PdfPCell(NewPhrase("Total Hours"));
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cell = new PdfPCell(NewPhrase((travelHours + dsHours + prepHours).ToString("n2")));
            cell.HorizontalAlignment = PdfCell.ALIGN_RIGHT;
            cell.BackgroundColor = CellBackgroundColor(cCnt);
            table.AddCell(cell);
            cCnt++;

            //---------------------------------------------------------------------------------------------------


            return table;
        }

        //private PdfPTable CloseQuestionsTable()
        //{
        //    PdfPTable table = new PdfPTable(1);
        //    PdfPCell cell;
        //    int cCnt = 0;


        //    DataTable dtData = new DataTable();
        //    System.Text.StringBuilder sbSQL = new System.Text.StringBuilder();
        //    sbSQL.Append("select * from suretool_Adaptations Where curriculumID = ").Append(ThisCurriculum.ID.ToString());

        //    using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
        //    {
        //        using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
        //        {
        //            sqlCmd.CommandType = CommandType.Text;

        //            using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
        //            {
        //                sqlDA.Fill(dtData);
        //            }
        //        }
        //    }

        //    foreach (DataRow dataRow in dtData.Rows)
        //    {
        //        cell = new PdfPCell(NewPhrase(dataRow["Question"].ToString()));
        //        //cell.Border = Rectangle.NO_BORDER;
        //        table.AddCell(cell);

        //        cell = new PdfPCell(NewPhrase(dataRow["Answer"].ToString()));
        //        //cell.Border = Rectangle.NO_BORDER;
        //        table.AddCell(cell);

        //        cell = new PdfPCell(NewPhrase(" "));
        //        //cell.Border = Rectangle.NO_BORDER;
        //        table.AddCell(cell);

        //    }

        //    return table;
        //}


        public void GenerateTreatmentMemoryStream(MemoryStream memStream, SerializeJsonData treatmentResult, List<ClientQuestionsResponse> lstCqr)
        {

            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateTreatmentReport(treatmentResult, lstCqr);
        }

        public void CreateTreatmentReport(SerializeJsonData treatmentResult, List<ClientQuestionsResponse> lstCqr)
        {
            this._pdfDoc.Open();

            BuildTreatmentReport(treatmentResult, lstCqr);

            this._pdfDoc.Close();

        }

        private void BuildTreatmentReport(SerializeJsonData treatmentResult, List<ClientQuestionsResponse> lstCqr)
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            //this._pdfDoc.Add(NewParagraph(ThisCurriculum.OrgName, Font.HELVETICA, 12, Font.BOLD));

            this._pdfDoc.Add(NewParagraph("Submitted treatment survey summary"));

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("");
            this._pdfDoc.Add(pdfParagraph);



            PdfPTable table = GeneralTreatmentTable(treatmentResult, lstCqr); // new PdfPTable(1);
            PdfPCell cell;

            this._pdfDoc.Add(table);


            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);



        }

        private PdfPTable GeneralTreatmentTable(SerializeJsonData onetime, List<ClientQuestionsResponse> lstCqr)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cell;


            table.HeaderRows = 0;

            int cCnt = 0;

            if (onetime.SurveyType != "Exit (Post-test)")
            {
                cell = new PdfPCell(NewPhrase("Agency Name"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.Agency));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //---------------------------------------------------------------------------------------------------
                cell = new PdfPCell(NewPhrase("Survey Administered By"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.Administered));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //---------------------------------------------------------------------------------------------------
                cell = new PdfPCell(NewPhrase("Survey Completed"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                // cell = new PdfPCell(NewPhrase(onetime.SurveryCompletedDate));
                if (String.IsNullOrEmpty(onetime.SurveryCompletedDate))
                    cell = new PdfPCell(NewPhrase(""));

                else
                    cell = new PdfPCell(NewPhrase(DateTime.Parse(onetime.SurveryCompletedDate, new CultureInfo("en-US")).ToShortDateString()));


                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
                cell = new PdfPCell(NewPhrase("Survey Type"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);

                cell = new PdfPCell(NewPhrase(onetime.SurveyType));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
                cell = new PdfPCell(NewPhrase("Service Provided"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.ServiceProvide));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
                if (onetime.ProgramName != null && onetime.ProgramName != "")
                {
                    cell = new PdfPCell(NewPhrase("Program Name"));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cell = new PdfPCell(NewPhrase(onetime.ProgramName));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cCnt++;
                }

                //----------------------------------------------------
                cell = new PdfPCell(NewPhrase("Case Number (Client ID)"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.CaseNumber));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------Instructor’s Credentials

                cell = new PdfPCell(NewPhrase("Client Gender"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.ClientGenderType));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------

                cell = new PdfPCell(NewPhrase("Client Entry Date"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                if (String.IsNullOrEmpty(onetime.ClientEntryDate))

                    cell = new PdfPCell(NewPhrase(""));
                else
                    cell = new PdfPCell(NewPhrase(DateTime.Parse(onetime.ClientEntryDate, new CultureInfo("en-US")).ToShortDateString()));

                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                var isIndigent = "No";
                foreach (IntakeTypeVlaues IntakeItem in onetime.lstIntakevalues)
                {
                    isIndigent = "No";
                    if (IntakeItem.IsSelect)
                    {
                        isIndigent = "Yes";
                    }
                    cell = new PdfPCell(NewPhrase(IntakeItem.IntakeTypeName));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cell = new PdfPCell(NewPhrase(isIndigent));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cCnt++;
                }
                //----------------------------------------------------
                //var isIndigent = "No";
                //if (onetime.IsIndigent)
                //{
                //    isIndigent = "Yes";
                //}

                //cell = new PdfPCell(NewPhrase("Indigent"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isIndigent));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------
                //var isWithdrwal = "No";
                //if (onetime.IsWithdrawal)
                //{
                //    isWithdrwal = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Withdrawal Managment"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isWithdrwal));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------
                //var isPregnant = "No";
                //if (onetime.IsPregnant)
                //{
                //    isPregnant = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Pregnant"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isPregnant));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                //var isIVDrugUser = "No";
                //if (onetime.IsIVDrugUser)
                //{
                //    isIVDrugUser = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("IV Drug User"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isIVDrugUser));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                //var isPregnantAndParenting = "No";
                //if (onetime.IsPregnantParenting)
                //{
                //    isPregnantAndParenting = "Yes";
                //}

                //cell = new PdfPCell(NewPhrase("Pregnant and Parenting with Children at the facility"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isPregnantAndParenting));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;

                ////----------------------------------------------------

                //var isOpioid = "No";
                //if (onetime.IsOpioid)
                //{
                //    isOpioid = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Opioid"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isOpioid));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;


                ////----------------------------------------------------

                //var isCooccurring = "No";
                //if (onetime.IsCooccurring)
                //{
                //    isCooccurring = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Co-occurring"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(isCooccurring));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsAlcohol = "No";
                //if (onetime.IsAlcohol)
                //{
                //    IsAlcohol = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Alcohol"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsAlcohol));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsMethamphetamine = "No";
                //if (onetime.IsMethamphetamine)
                //{
                //    IsMethamphetamine = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Methamphetamine"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsMethamphetamine));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsBenzodiazapines = "No";
                //if (onetime.IsBenzodiazapines)
                //{
                //    IsBenzodiazapines = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Benzodiazapines"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsBenzodiazapines));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsMarijuana = "No";
                //if (onetime.IsMarijuana)
                //{
                //    IsMarijuana = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Marijuana"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsMarijuana));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsOtherSubstances = "No";
                //if (onetime.IsOtherSubstances)
                //{
                //    IsOtherSubstances = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Other Substances"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsOtherSubstances));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsSelfPay = "No";
                //if (onetime.IsSelfPay)
                //{
                //    IsSelfPay = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Self Pay"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsSelfPay));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsPrivateInsurance = "No";
                //if (onetime.IsPrivateInsurance)
                //{
                //    IsPrivateInsurance = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Private Insurance"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsPrivateInsurance));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                ////----------------------------------------------------

                ////----------------------------------------------------

                //var IsMedicaid = "No";
                //if (onetime.IsMedicaid)
                //{
                //    IsMedicaid = "Yes";
                //}
                //cell = new PdfPCell(NewPhrase("Medicaid"));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(IsMedicaid));
                //cell.BackgroundColor = CellBackgroundColor(cCnt);
                //table.AddCell(cell);
                //cCnt++;
                //----------------------------------------------------


                //----------------------------------------------------


                cell = new PdfPCell(NewPhrase("Comments"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.Comments));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------



                foreach (var c in onetime.QuestAnsPair)
                {
                    cell = new PdfPCell(NewPhrase(c.Key));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cell = new PdfPCell(NewPhrase(c.Value));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cCnt++;
                }

                //----------------------------------------------------

                cell = new PdfPCell(NewPhrase("Client Bed Status"));
                cell.BackgroundColor = Color.RED;// CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.BedStatus));
                cell.BackgroundColor = Color.RED;//CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
            }
            else
            {
                cell = new PdfPCell(NewPhrase("Agency Name"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.Agency));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //---------------------------------------------------------------------------------------------------
                cell = new PdfPCell(NewPhrase("Survey Administered By"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.Administered));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //---------------------------------------------------------------------------------------------------
                cell = new PdfPCell(NewPhrase("Survey Completed"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                // cell = new PdfPCell(NewPhrase(onetime.SurveryCompletedDate));
                if (String.IsNullOrEmpty(onetime.SurveryCompletedDate))
                    cell = new PdfPCell(NewPhrase(""));

                else
                    cell = new PdfPCell(NewPhrase(DateTime.Parse(onetime.SurveryCompletedDate, new CultureInfo("en-US")).ToShortDateString()));


                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
                cell = new PdfPCell(NewPhrase("Survey Type"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);

                cell = new PdfPCell(NewPhrase(onetime.SurveyType));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
                cell = new PdfPCell(NewPhrase("Service Provided"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.ServiceProvide));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
                if (onetime.ProgramName != null && onetime.ProgramName != "")
                {
                    cell = new PdfPCell(NewPhrase("Program Name"));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cell = new PdfPCell(NewPhrase(onetime.ProgramName));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cCnt++;
                }
                //----------------------------------------------------
                cell = new PdfPCell(NewPhrase("Case Number (Client ID)"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.CaseNumber));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------Instructor’s Credentials

                cell = new PdfPCell(NewPhrase("Client Gender"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.ClientGenderType));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------

                cell = new PdfPCell(NewPhrase("Client Entry Date"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                if (String.IsNullOrEmpty(onetime.ClientEntryDate))

                    cell = new PdfPCell(NewPhrase(""));
                else
                    cell = new PdfPCell(NewPhrase(DateTime.Parse(onetime.ClientEntryDate, new CultureInfo("en-US")).ToShortDateString()));

                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------

                cell = new PdfPCell(NewPhrase("Client Discharge Date"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                //cell = new PdfPCell(NewPhrase(onetime.ClientDischargeDate));

                if (String.IsNullOrEmpty(onetime.ClientDischargeDate))
                    cell = new PdfPCell(NewPhrase(""));
                else
                    cell = new PdfPCell(NewPhrase(DateTime.Parse(onetime.ClientDischargeDate, new CultureInfo("en-US")).ToShortDateString()));

                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------

                cell = new PdfPCell(NewPhrase("Comments"));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.Comments));
                cell.BackgroundColor = CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------

                foreach (var c in onetime.QuestAnsPair)
                {
                    cell = new PdfPCell(NewPhrase(c.Key));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cell = new PdfPCell(NewPhrase(c.Value));
                    cell.BackgroundColor = CellBackgroundColor(cCnt);
                    table.AddCell(cell);
                    cCnt++;
                }

                //----------------------------------------------------

                cell = new PdfPCell(NewPhrase("Client Bed Status"));
                cell.BackgroundColor = Color.RED;// CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cell = new PdfPCell(NewPhrase(onetime.BedStatus));
                cell.BackgroundColor = Color.RED;//CellBackgroundColor(cCnt);
                table.AddCell(cell);
                cCnt++;
                //----------------------------------------------------
            }

            return table;
        }

    }
}