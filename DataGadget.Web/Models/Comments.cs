﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class Comments
    {
        public long ID { get; set; }
        public long? UserID { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ToEmail { get; set; }
        public int? ParentId { get; set; }
        public long? ToUserID { get; set; }
        public string Subject { get; set; }
        public string FullName { get; set; }


    }
}