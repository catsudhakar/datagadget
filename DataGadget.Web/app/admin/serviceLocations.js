﻿





(function () {
    'use strict';

    angular
        .module('app')
        .controller('serviceLocations', serviceLocations);

    serviceLocations.$inject = ['$scope', '$http', 'common', '$modal', '$location'];

    function serviceLocations($scope, $http, common, $modal, $location) {
        $scope.title = 'Serivce Locations';
        $scope.isstatelevel = false;
        var vm = this;
        vm.links = [];

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(serviceLocations);

        activate();

        function activate() {
           
            $scope.isstatelevel = false;
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }

            if (userInfo.level == "STA1A") {
                $scope.isstatelevel = true;
            }


            if (userInfo.level == 'Admin' || userInfo.level == 'STA1A') {
               
                paging(1);
            }
            else {
                vm.links = [];
            }
        }
        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));
            $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "AllServiceLocation", parameters: [userInfo.level,pageId] } })
                .success(function (result) {
                $scope.ispaging = false;
                vm.pagerData = result.item1[0];
                if (parseInt(vm.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                vm.pageList = [];
                if (pageId > 3) {
                    vm.pageList.push(pageId - 2);
                    vm.pageList.push(pageId - 1);
                    vm.pageList.push(pageId);
                    if (pageId < vm.pagerData.numberOfPages) {
                        vm.pageList.push(pageId + 1);
                        if (pageId < (vm.pagerData.numberOfPages - 1)) {
                            vm.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);

                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        vm.pageList.push(pageId - 2);
                        vm.pageList.push(pageId - 1);
                        vm.pageList.push(pageId);
                        if (pageId + 1 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= vm.pagerData.numberOfPages) {
                            vm.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                vm.links = userData;

            })
         .error(function (error) {
             console.log(error);
         })
        }

        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                var userInfo = JSON.parse(localStorage.getItem('userInfo'));
                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "SearchServiceLocation", parameters: [userInfo.level,search] } })
               .success(function (result) {
                   vm.links = result;
               })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    activate();
                }
            }
        }

        vm.addProvider = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/addServiceLocation.html',
                controller: 'addServiceLocation',
                resolve: {
                    resourcesInfo: function () {
                        return undefined;
                    }
                }

            });
       
            addUserModal.result.then(function (link) {
                vm.links.push(link);

            }, function () {
            });
        }

        $scope.vm.editBedCount = function (rLinks) {

            var addUserModal = $modal.open({
                templateUrl: 'app/admin/addServiceLocation.html',
                controller: 'addServiceLocation',
                resolve: {
                    resourcesInfo: function () {
                        return rLinks;
                    }
                }
            });

            addUserModal.result.then(function () {

            }, function () {
            });
        }



        vm.deleteBedCount = function (rLinks) {

            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {
                $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "DeleteServiceLocation", parameters: [rLinks.id] } })
            .success(function (result) {

                log("Deleted successfully");
                activate();
            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {
            });
        };

    }
})();


