﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('userAuthorizeService', userAuthorizeService);

    userAuthorizeService.$inject = ['$http', '$q', '$rootScope', '$location', '$route'];

    function userAuthorizeService($http, $q, $rootScope, $location, $route) {
        var service = {
            getPermission: getData
        };

        return service;

        function getData() {
            var deferred = $q.defer();
            var userInfo = JSON.parse(localStorage.getItem('userInfo'));

            if (userInfo.level == 'SIT3U') {
                $location.path('/dashboard');
                $rootScope.$on('$locationChangeSuccess', function (next, current) {
                    deferred.resolve();
                });

            }
            else {
                deferred.resolve();
            }

            return deferred.promise;
        }
    }
})();