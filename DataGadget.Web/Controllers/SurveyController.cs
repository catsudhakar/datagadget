﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataGadget.Web.DataModel;
using System.Threading.Tasks;
using System.Data.Entity;
using DataGadget.Web.Models;

namespace DataGadget.Web.Controllers
{
    public class SurveyController : Controller
    {
        // GET: Survey
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        async public Task<ActionResult> TreatmentSurvey(string id)
        {
            List<QuestionResponseViewModel> model = new List<QuestionResponseViewModel>();
            try
            {
                using (datagadget_testEntities context = new datagadget_testEntities())
                {

                    var dataResult = await context.SurveyGroupQuestions.Where(x => x.SurveyID == 1).OrderBy(x => x.GroupSeqNum).ToListAsync();
                    foreach (var item in dataResult)
                    {
                        QuestionResponseViewModel dataModel = new QuestionResponseViewModel();
                        List<SurveyRespons> responseList = new List<SurveyRespons>();
                        dataModel.SurveyGroupQuestion = item;
                        var quesionResponse = await context.Database.SqlQuery<SurveyRespons>("select * from  SurveyResponses where SurveyGroupQTNID =" + item.RecNo + "").ToListAsync();
                        foreach (var response in quesionResponse)
                        {

                            responseList.Add(response);
                        }
                        dataModel.UserSessionID = Session.SessionID;
                        dataModel.ClientSessionStart = DateTime.Now;
                        dataModel.SurveyQTNResponseList = responseList;
                        model.Add(dataModel);
                    }

                }
                ViewBag.ClientId = "";
            }
            catch (Exception ex)
            {
                WebLogger.Error("Exception In SurveyController : " + ex.Message); 
                throw ex;
            }
            return View(model);
        }


        [HttpGet]
        async public Task<ActionResult> EditTreatmentSurvey(string id)
        {

            int ItemId = 7252;
            List<QuestionResponseViewModel> model = new List<QuestionResponseViewModel>();
            try
            {
                using (datagadget_testEntities context = new datagadget_testEntities())
                {

                    var dataResult = await context.SurveyGroupQuestions.Where(x => x.SurveyID == 1).OrderBy(x => x.GroupSeqNum).ToListAsync();
                    foreach (var item in dataResult)
                    {
                        QuestionResponseViewModel dataModel = new QuestionResponseViewModel();
                        List<SurveyRespons> responseList = new List<SurveyRespons>();
                        dataModel.SurveyGroupQuestion = item;
                        var quesionResponse = await context.Database.SqlQuery<SurveyRespons>("select * from  SurveyResponses where SurveyGroupQTNID =" + item.RecNo + "").ToListAsync();
                        foreach (var response in quesionResponse)
                        {

                            responseList.Add(response);
                        }
                        dataModel.UserSessionID = Session.SessionID;
                        dataModel.ClientSessionStart = DateTime.Now;
                        dataModel.SurveyQTNResponseList = responseList;
                        model.Add(dataModel);
                    }

                }
                ViewBag.ClientId = "";
            }
            catch (Exception ex)
            {
                WebLogger.Error("Exception In SurveyController : " + ex.Message);
                throw ex;
            }
            return View(model);
        }

        

       
    }

}