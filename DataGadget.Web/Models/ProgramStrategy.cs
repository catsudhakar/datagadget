﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class ProgramStrategy
    {
        public int ID;
        public string Type;
        public string Name;
        public string LongDescription;
        public bool ? IsActive;


    }
}