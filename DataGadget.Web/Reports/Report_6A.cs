﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataGadget.Web.Reports
{
    public class Report_6A : Report_Base
    {


        public Report_6A()
        {
            _pdfDoc = new Document(PageSize.LETTER, 36, 36, 36, 36);
        }

        public string ConnectionString;
        public string RegionCode;
        public string SiteCode;
        public DateTime BeginDate;
        public DateTime EndDate;
        public string OrgName;


        public void GenerateToFile(string filename)
        {
            PdfWriter.GetInstance(this._pdfDoc, new FileStream(filename, FileMode.Create));
            CreateReport();
        }

        public void GenerateToMemoryStream(MemoryStream memStream, DateTime BeginDate1, DateTime EndDate1, string RegionCode1, string SiteCode1, string OrgName1)
        {
            BeginDate = BeginDate1;
            EndDate = EndDate1;
            RegionCode = RegionCode1;
            SiteCode = SiteCode1;
            OrgName = OrgName1;

            PdfWriter pdfWriter = PdfWriter.GetInstance(this._pdfDoc, memStream);
            pdfWriter.ViewerPreferences = PdfWriter.FitWindow;
            CreateReport();
        }


        private void CreateReport()
        {

            this._pdfDoc.Open();

            BuildReportList();

            this._pdfDoc.Close();

        }

        private void BuildReportList()
        {

            Paragraph pdfParagraph = new Paragraph();

            this._pdfDoc.Add(this.HeaderImageElement());

            this._pdfDoc.Add(NewParagraph("Form 6A - Risk - Strategies"));

            if (this.OrgName.Trim().Length > 0)
                this._pdfDoc.Add(NewParagraph(OrgName));

            pdfParagraph = NewParagraph(this.BeginDate.ToString("MMMM d, yyyy") + " - " + this.EndDate.ToString("MMMM d, yyyy"));
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph("State:  Mississippi");
            this._pdfDoc.Add(pdfParagraph);

            pdfParagraph = NewParagraph(" ");
            this._pdfDoc.Add(pdfParagraph);

            PdfPTable table = GeneralTable(); // new PdfPTable(1);



            this._pdfDoc.Add(table);
        }

        private PdfPTable GeneralTable()
        {

            PdfPTable table = new PdfPTable(3);
            PdfPCell cell;


            cell = new PdfPCell(NewPhrase("Column A (Risks)", Font.HELVETICA, 8, Font.BOLD));
            cell.BackgroundColor = cellHeaderColor;
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Column B (Strategies)", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            cell = new PdfPCell(NewPhrase("Column C (Providers)", Font.HELVETICA, 8, Font.BOLD));
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.BackgroundColor = cellHeaderColor;
            table.AddCell(cell);

            table.HeaderRows = 1;

            //---------------------------------------------------------------------------------------------------

            GetTable01(table, "01", "Children of Substance Abusers");
            GetTable01(table, "02", "Pregnant women/teens");
            GetTable01(table, "03", "Drop-outs");
            GetTable01(table, "04", "Violent and Delinquent Behavior");
            GetTable01(table, "05", "Mental Health Problems");
            GetTable01(table, "06", "Economically Disadvantaged");
            GetTable01(table, "07", "Physically Disabled");
            GetTable01(table, "08", "Abuse Victims");
            GetTable01(table, "09", "Already Using Substances");
            GetTable01(table, "10", "Homeless and Runaway Youth");
            GetTable01(table, "11", "Free Lunch");
            GetTable01(table, "12", "Other");

            //---------------------------------------------------------------------------------------------------

            return table;
        }



        private void GetTable01(PdfPTable table, string riskNum, string riskName)
        {
            //////////////////////////////////////////////////////////////////////////////////////
            DataTable dtData = new DataTable();

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("suretool_Report_6a_" + riskNum + " ");
            sbSQL.Append("'").Append(BeginDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(EndDate.ToShortDateString()).Append("',");
            sbSQL.Append("'").Append(RegionCode).Append("',");
            sbSQL.Append("'").Append(SiteCode).Append("'");


            //using (SqlConnection sqlCn = new SqlConnection(DB.CnString()))
            //{
            //    using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
            //    {
            //        sqlCmd.CommandType = CommandType.Text;

            //        using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
            //        {
            //            sqlDA.Fill(dtData);
            //        }
            //    }
            //}
            DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities();

            using (SqlConnection sqlCn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                using (SqlCommand sqlCmd = new SqlCommand(sbSQL.ToString(), sqlCn))
                {
                    sqlCmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd))
                    {
                        sqlDA.Fill(dtData);
                    }
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////////

            int cCnt = 0;
            PdfPCell cell;

            foreach (DataRow drData in dtData.Rows)
            {
                if (Convert.ToInt32(drData["providers"]) != 0)
                {
                    if (cCnt == 0)
                    {
                        cell = new PdfPCell(NewPhrase(riskNum + " " + riskName));
                        cCnt++;
                    }
                    else
                        cell = new PdfPCell(NewPhrase(""));

                    table.AddCell(cell);

                    cell = new PdfPCell(NewPhrase(drData["strategy"].ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(NewPhrase(drData["providers"].ToString()));
                    table.AddCell(cell);
                }
            }


        }



    }
}
