﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web
{
    public class ParamObject
    {
        /// <summary>
        /// The method to call
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// Paramters to be used during the method call
        /// </summary>
        public string[] Parameters { get; set; }

       // public List<object> listobject { get; set; }
    }
}