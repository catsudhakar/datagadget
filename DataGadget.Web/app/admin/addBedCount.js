﻿(function () {
    'use strict';
    var controllerId = 'addBedCount';
    angular
        .module('app')
        .controller(controllerId, addBedCount);

    addBedCount.$inject = ['$scope', '$http', '$modalInstance', '$location', 'resourcesInfo', '$window', '$route', 'common'];

    function addBedCount($scope, $http, $modalInstance, $location, resourcesInfo, $window, $route, common) {


        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        if (resourcesInfo) {
            $scope.title = "Edit Location";
        }
        else {
            $scope.title = "Add Location";
        }
        $scope.statewide = {};
        $scope.isSubmit = false;

        $scope.surveyServices = [];
        $scope.surveyServices1 = [];

        $scope.SelectedsurveyServices = [];
        $scope.isRegionError = false;
        $scope.regionErrorMessage = '';


        activate();

        function activate() {


            $http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "GetSurveySericesListAsync" } })
            .success(function (result) {
                $scope.surveyServices = result;

                if (resourcesInfo) {
                    $http({
                        method: 'POST', url: 'api/StatewideInitiatives',
                        data: { method: "GetSurveySericesListByLocId", parameters: [resourcesInfo.pKey] }
                    })
                    .success(function (selectedresult) {
                        $scope.SelectedsurveyServices = selectedresult;

                        for (var i = 0; i < $scope.surveyServices.length; i++) {
                            var recNo = $scope.surveyServices[i].recNo;
                            for (var j = 0; j < $scope.SelectedsurveyServices.length; j++) {
                                var recNo1 = $scope.SelectedsurveyServices[j].iopId;
                                if (recNo == recNo1) {
                                    $scope.surveyServices[i].isSerivesSelected = true;
                                    break;
                                }

                            }

                        }

                    })
                    .error(function (error) {
                        console.log(error);
                    })
                }

            })
            .error(function (error) {
                console.log(error);
            })





            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            if (userInfo.level != "SIT3U" && userInfo.level != "SIT2A") {
                if (resourcesInfo) {
                    $scope.statewide.pKey = resourcesInfo.pKey;
                    //$scope.statewide.bedCount = resourcesInfo.bedCount;
                    //$scope.statewide.adultFemale = resourcesInfo.adultFemale;
                    //$scope.statewide.adolescentMale = resourcesInfo.adolescentMale;
                    //$scope.statewide.adolescentFemale = resourcesInfo.adolescentFemale;

                    $scope.statewide.friendlyName_loc = resourcesInfo.friendlyName_loc;
                    $scope.statewide.addressLine1 = resourcesInfo.addressLine1;
                    $scope.statewide.city = resourcesInfo.city;
                    $scope.statewide.addressLine2 = resourcesInfo.addressLine2;

                    $scope.statewide.stateAbbrev = resourcesInfo.stateAbbrev;
                    $scope.statewide.stateRegionCode = resourcesInfo.stateRegionCode;

                    $scope.statewide.siteCode = resourcesInfo.siteCode;
                    $scope.statewide.siteName = resourcesInfo.siteName;

                    $scope.statewide.zip = resourcesInfo.zip;
                    $scope.statewide.sig = resourcesInfo.sig;

                    $scope.statewide.fS_MH = resourcesInfo.fS_MH;
                    $scope.statewide.fedRegionCode = resourcesInfo.fedRegionCode;
                    $scope.statewide.fedRegionName = resourcesInfo.fedRegionName;

                    $scope.statewide.isActive = resourcesInfo.isActive;

                    //$http({ method: 'POST', url: 'api/StatewideInitiatives', data: { method: "GetSurveySericesListByLocId", parameters: [$scope.statewide.pKey] } })
                    //.success(function (result) {
                    //    $scope.SelectedsurveyServices = result;
                    //})
                    //.error(function (error) {
                    //    console.log(error);
                    //})






                }
            }
            else {
                $location.path('/dashboard');
            }


        }


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.checksiteregioncode = function (regionId, siteId) {
            // alert(regionId); alert(siteId);
          
            if ($scope.statewide.stateRegionCode !== undefined && $scope.statewide.siteCode !== undefined) {
                $scope.isRegionError = false;
                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "IsExistRegionCodeSiteCode",
                        parameters: [JSON.stringify($scope.statewide), JSON.stringify($scope.surveyServices1)]
                    }
                })
                   .success(function (result) {
                       debugger;
                       if (result > 0) {
                           $scope.isRegionError = true;
                           $scope.regionErrorMessage = 'Combination of stateregioncode and site code is exist';
                       }
                       else {
                           $scope.isRegionError = false;
                           $scope.regionErrorMessage = '';
                       }

                   })
                   .error(function (error) {
                       console.log(error);
                   })

            }


        };

        $scope.ok = function (frmProvider) {

            $scope.isSubmit = true;
            $scope.surveyServices1 = $scope.surveyServices;

            if (frmProvider.$valid && $scope.isRegionError==false) {
                $http({
                    method: 'POST', url: 'api/StatewideInitiatives',
                    data: {
                        method: "CreateNewBedCount",
                        parameters: [JSON.stringify($scope.statewide), JSON.stringify($scope.surveyServices1)]
                    }
                })
                    .success(function (result) {
                        debugger;
                        $modalInstance.close(result);
                        $location.path('/bedCountList');
                        $route.reload();
                        log("Location saved successfully");

                    })
                    .error(function (error) {
                        console.log(error);
                    })

                $location.path('/bedCountList');
                $route.reload();
            }
        };
    }
})();


