﻿using DataGadget.Web.DataModel;
using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace DataGadget.Web.Controllers
{
    public class PreventionSurveyToolController : Controller
    {
        // GET: PreventionSurveyTool
        async public Task<ActionResult> Index()
        {
            List<QuestionResponseViewModel> model = new List<QuestionResponseViewModel>();
            try
            {
                using (datagadget_testEntities context = new datagadget_testEntities())
                {

                    var dataResult = await context.SurveyGroupQuestions.Where(x => x.SurveyID == 2).OrderBy(x => x.GroupSeqNum).ToListAsync();
                    foreach (var item in dataResult)
                    {

                       
                        var subcount = context.SurveySubQTNs.Count(a => a.SurveySubQTNId == item.SurveyQTNRecNo);
                        if (subcount == 0)
                        {
                            QuestionResponseViewModel dataModel = new QuestionResponseViewModel();
                            List<SurveyRespons> responseList = new List<SurveyRespons>();
                            List<SurveyRespons> subresponseList = new List<SurveyRespons>();
                            dataModel.SurveyGroupQuestion = item;
                            //var quesionResponse = await context.Database.SqlQuery<SurveyQTNResponse>("select * from  SurveyQTNResponse where RecNo in (select SurveyQTNResponseID from SurveyResponseItems where SurveyGroupQTNID=" + item.RecNo + ");").ToListAsync();
                            if(item.RecNo==28)
                            {

                            }
                           // var quesionResponse = await context.Database.SqlQuery<SurveyRespons>("select * from  SurveyResponses where SurveyGroupQTNID =" + item.RecNo + "").ToListAsync();
                            var quesionResponse = await context.Database.SqlQuery<SurveyRespons>("select * from  SurveyResponses where SurveyGroupQTNID =" + item.RecNo + "  order by ResponseSeqNum").ToListAsync();

                            foreach (var response in quesionResponse)
                            {
                                //SurveyQTNResponse responseModel = new SurveyQTNResponse();
                                //responseModel = response;
                                responseList.Add(response);
                            }
                            // TODO:get surveysub qtn based on SurveyQTN id

                            var SurveySubQuestion = context.SurveySubQTNs.Where(x => x.SurveyRootQTNId == item.SurveyQTNRecNo).ToList();
                            foreach (var subitem in SurveySubQuestion)
                            {
                                //dataModel.SubQuestion = new SubQuestionResponseViewModel();
                                var SUbGroupId = context.SurveyGroupQTNs.Where(x => x.SurveyQTNID == subitem.SurveySubQTNId).Select(x => x.RecNo).SingleOrDefault();
                                var dataSubResult = await context.SurveyGroupQuestions.Where(x => x.SurveyQTNRecNo == subitem.SurveySubQTNId).OrderBy(x => x.GroupSeqNum).SingleOrDefaultAsync();
                                var SubquesionResponse = await context.Database.SqlQuery<SurveyRespons>("select * from  SurveyResponses where SurveyGroupQTNID =" + SUbGroupId + "").ToListAsync();

                                dataModel.SubQuestion.QuetionText = dataSubResult.SurveyQTN;// subitem.SurveyQTN1.SurveyQTN1;
                                dataModel.SubQuestion.SurveyGroupQTNRecNo = dataSubResult.RecNo;//SUbGroupId;// subitem.SurveyQTN1.RecNo;
                                dataModel.SubQuestion.ControlType = dataSubResult.ASPControlType;

                             

                                foreach (var subresponse in SubquesionResponse)
                                {
                                    
                                    subresponseList.Add(subresponse);
                                    
                                }
                                dataModel.SubQuestion.SurveyQTNResponseList = subresponseList;

                            }



                            //var subQTN = dataModel..where(x=>x.SurveyQTNID == dataModel.surve)
                            //dataModel.SubQuestionList = new QuestionResponseViewModel();
                            //dataModel.SubQuestionList.ControlType = 

                           


                            dataModel.SurveyQTNResponseList = responseList;
                            dataModel.UserSessionID = Session.SessionID;
                            dataModel.ClientSessionStart = DateTime.Now;
                            model.Add(dataModel);
                        }
                        
                        
                    }

                }

            }
            catch (Exception ex)
            {
                WebLogger.Error("Exception In PreventionSurveyToolController : " + ex.Message); 
                throw (ex);
            }
            return View(model);
        }
    }
}