﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class TreatmentOutcomes
    {
        public string Agency_Name { get; set; }
        public string Survey_Administered_By { get; set; }
        public string Survey_Completed { get; set; }
        public string Survey { get; set; }
        public int Survey_Type { get; set; }
        public int Service_Provided { get; set; }
        public string Program_Name { get; set; }
        public string Case_Number { get; set; }
        //public DateTime ? Client_Entry_Date { get; set; }
        public string Client_Entry_Date { get; set; }
        //public string Comments { get; set; }
        public int C1_Employment_Status { get; set; }
        public string C1_1_Employment_Status { get; set; }
        public int C2_Living_Situation { get; set; }
        public string C2_2_Living_Situation { get; set; }
        public int C3_Arrested_Past_Year { get; set; }
        public int C4_Arrested_Past_30_Days { get; set; }
        public int C5_Alcohol_Use_Past_30_Days { get; set; }
        public int C6_Illegal_Drug_Use_Past_30_Days { get; set; }
        public int C7_Self_Help_Group { get; set; }
    }
}