﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{


    public partial class UserCategoryModel
    {
        public int RecNo { get; set; }
        public System.Guid UserID { get; set; }
        public int CategoryID { get; set; }
       
    }

    public class LocCategory
    {
        public int RecNo { get; set; }
        public int LocationID { get; set; }
        public int CategoryID { get; set; }
        public string SurveyCategory { get; set; }

        public bool IsSelected { get; set; }
    }

    public class ProgramNREP
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string LongDescription { get; set; }


    }

    public class ServiceLocationsModel
    {
        public int ID { get; set; }
        public string ServiceLocation { get; set; }

        public bool IsActive { get; set; }

        public bool IsSelected { get; set; }


    }

    public class stateInitiatives
    {
        public int id { get; set; }
        public string initiativeName { get; set; }

        public bool IsActive { get; set; }

        public bool IsSelected { get; set; }

        public bool IsDisabled { get; set; }


       
    }

    public class resourcesLinks
    {
        public int resourceId { get; set; }
        public string resourceName { get; set; }
        public string resourceLink1 { get; set; }

    }

    public  class grantYear
    {
        public int grantYearId { get; set; }
        public string grantYear1 { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public bool isActive { get; set; }
    }

    public class providerLinks
    {
        public int providerID { get; set; }
        public string providerName { get; set; }
        public bool  isActive { get; set; }

    }

    public class surveySerivesModel
    {
        public int RecNo { get; set; }
        public string Description { get; set; }
        public bool IsSerivesSelected { get; set; }
    }

    public class followUpDays
    {

        public bool isSelected { get; set; }

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int FollowUpDays { get; set; }
    }
}