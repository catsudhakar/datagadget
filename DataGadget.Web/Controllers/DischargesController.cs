﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataGadget.Web.Models;
using System.Globalization;
using System.Threading.Tasks;



namespace DataGadget.Web.Controllers
{
    public class DischargesController : ApiController
    {
        public DischargesController()
        {


        }


        async public Task<HttpResponseMessage> Post(ParamObject param)
        {
            try
            {
                switch (param.Method)
                {
                    case "AllDischarges":
                        return GetAllEvents(param.Parameters);
                    case "GetWaitingData":
                        return GetWaitingData(param.Parameters);
                    case "DischargesReport":
                        return getDischargeReport(param.Parameters);
                    case "DateEvents":
                        return DateEvents(param.Parameters);
                    case "InitializeStaffForm":
                        return await InitializeStaffForm();
                    case "AllTreatments":
                        return GetAllTreatments(param.Parameters);
                    case "updateWaitingData":
                        return UpdateWaitingData(param.Parameters);
                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                // WebLogger.Error("Exception In DischargesController : " + ex.Message); 

                // return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex, true));
                throw ex;

            }
        }


        private HttpResponseMessage DateEvents(string[] p)
        {
            int id = Convert.ToInt32(p[0]);

            string seldate = p[1];
            int disCodeId = 0;

            bool b = int.TryParse(p[2], out disCodeId);



            string strDate = seldate;
            string[] dateString = strDate.Split('-');
            DateTime enter_date = Convert.ToDateTime(dateString[1] + "-" + dateString[0] + "-" + dateString[2]);




            // DateTime dt = DateTime.ParseExact(id, "yyyy/MM/DD", CultureInfo.InvariantCulture);

            // var date = DateTime.ParseExact(id, "dd/MM/yyyy", null).ToString("MM/dd/yyyy");

            //string sub = id.Substring(0, id.Length - 14);

            //  string s_today = today.ToString("MM/dd/yyyy"); 

            //DateTime date = DateTime.ParseExact(id, "dd/MM/YYYY", null);
            // DateTime dt = DateTime.ParseExact(id, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            var events = new List<Discharge>();


            Discharge dischargemodel = new Discharge();

            using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
            {

                var result = context.SurveyItems.Where(x => x.RecNo == id).SingleOrDefault();

                if (result != null)
                {

                    result.DischargeDate = enter_date;
                    result.DischargeId = disCodeId;
                    //var i = result.SurveyClient.Client.PAT_ID;


                    //var followup = context.FollowUpContacts.FirstOrDefault(x => x.PatientId == i);
                    //if (followup != null)
                    //{
                    //    followup.HasSurveyDone = 1;

                    //}


                    context.SaveChanges();

                }



            }

            return this.Request.CreateResponse(HttpStatusCode.OK, events);

        }

        private HttpResponseMessage GetAllTreatments(string[] p)
        {
            try
            {
                User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }



                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var sortOrder = p[2];


                //var events = new List<Discharge>();
                var events = new List<SurveyItemVM>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var result = (from TU in context.TblUsers
                                  join SI in context.SurveyItems on TU.UserGUID equals SI.SurveyAdminID
                                  join SC in context.SurveyClients on SI.SurveyClientID equals SC.RecNo
                                  join C in context.Clients on SC.ClientID equals C.RecNo
                                  join ss in context.SurveyServices on SI.ServiceID equals ss.RecNo
                                  join l in context.tblLocations on TU.LocationID equals l.PKey
                                  where SI.DischargeDate == null && SI.SurveyID == 1
                                  select new
                                  {
                                      C.PAT_ID,
                                      SI.GroupName,
                                      SI.EntryDate,
                                      SI.DischargeDate,
                                      TU.FirstName,
                                      TU.LastName,
                                      SI.RecNo,
                                      TU.LocationID,
                                      SI.DischargeId,
                                      ss.Description,
                                      l.FriendlyName_loc,
                                      SI.AdminDate,
                                      SI.ClientGenderType,
                                      SI.Notes,
                                      SI.ProgramID,
                                  }).ToList();
                    //admin level  or user level
                    if (newUserId.Level == "SIT2A" || newUserId.Level == "SIT3U")
                        result = result.Where(x => x.LocationID == newUserId.LocationId).ToList();

                    switch (sortOrder)
                    {
                        case "Asc":
                            result = result.OrderBy(s => s.PAT_ID).ToList();
                            break;
                        case "Desc":
                            result = result.OrderByDescending(s => s.PAT_ID).ToList();
                            break;
                        default:
                            result = result.OrderByDescending(x => x.RecNo).ToList();
                            break;

                    }

                    result = result.Where(s => s.PAT_ID != "").ToList();

                    int rowCount = result.Count();

                    //var Disch = result.OrderByDescending(x => x.RecNo).Skip(skip).Take(take).ToList();
                    var Disch = result.Skip(skip).Take(take).ToList();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var DL in Disch)
                    {

                        events.Add(
                             new SurveyItemVM
                             {
                                 LocationID = DL.LocationID,
                                 RecNo = DL.RecNo,
                                 DischargeDate = DL.DischargeDate,
                                 EntryDate = DL.EntryDate,
                                 LocationName = DL.FriendlyName_loc,
                                 AdminDate = DL.AdminDate,
                                 SurveyAdminName = DL.FirstName + " " + DL.LastName,
                                 ClientName = DL.PAT_ID,
                                 ClientGenderType = DL.ClientGenderType,
                                 GroupName = DL.GroupName,
                                 ProgramName = context.Programs.Where(x => x.ID == DL.ProgramID).Select(x => x.Name).SingleOrDefault(),
                                 Notes = DL.Notes
                             });

                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<SurveyItemVM>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<SurveyItemVM>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, events);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetAllEvents(string[] p)
        {
            try
            {
                User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }



                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var sortOrder = p[2];


                var events = new List<Discharge>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var result = (from TU in context.TblUsers
                                  join SI in context.SurveyItems on TU.UserGUID equals SI.SurveyAdminID
                                  join SC in context.SurveyClients on SI.SurveyClientID equals SC.RecNo
                                  join C in context.Clients on SC.ClientID equals C.RecNo
                                  join ss in context.SurveyServices on SI.ServiceID equals ss.RecNo
                                  join l in context.tblLocations on TU.LocationID equals l.PKey
                                  where SI.DischargeDate == null && SI.SurveyID == 1 && (C.ClientStatusId == 3 || C.ClientStatusId == null)
                                  select new
                                  {
                                      C.PAT_ID,
                                      SI.GroupName,
                                      SI.EntryDate,
                                      SI.DischargeDate,
                                      TU.FirstName,
                                      TU.LastName,
                                      SI.RecNo,
                                      TU.LocationID,
                                      SI.DischargeId,
                                      ss.Description,
                                      l.FriendlyName_loc
                                  }).ToList();
                    //admin level  or user level
                    if (newUserId.Level == "SIT2A" || newUserId.Level == "SIT3U")
                        result = result.Where(x => x.LocationID == newUserId.LocationId).ToList();

                    switch (sortOrder)
                    {
                        case "Asc":
                            result = result.OrderBy(s => s.PAT_ID).ToList();
                            break;
                        case "Desc":
                            result = result.OrderByDescending(s => s.PAT_ID).ToList();
                            break;
                        default:
                            result = result.OrderByDescending(x => x.RecNo).ToList();
                            break;

                    }

                    result = result.Where(s => s.PAT_ID != "").ToList();

                    int rowCount = result.Count();

                    //var Disch = result.OrderByDescending(x => x.RecNo).Skip(skip).Take(take).ToList();
                    var Disch = result.Skip(skip).Take(take).ToList();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var DL in Disch)
                    {

                        events.Add(
                             new Discharge { PAT_ID = DL.PAT_ID, DischargeDate = DL.DischargeDate, EntryDate = DL.EntryDate, LoginID = DL.FirstName + " " + DL.LastName, RecNo = DL.RecNo, DischargeId = DL.DischargeId, Description = DL.Description, LocationName = DL.FriendlyName_loc });
                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Discharge>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Discharge>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, events);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage GetWaitingData(string[] p)
        {
            try
            {
                User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }



                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var sortOrder = p[2];


                var events = new List<Discharge>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var result = (from TU in context.TblUsers
                                  join SI in context.SurveyItems on TU.UserGUID equals SI.SurveyAdminID
                                  join SC in context.SurveyClients on SI.SurveyClientID equals SC.RecNo
                                  join C in context.Clients on SC.ClientID equals C.RecNo
                                  join ss in context.SurveyServices on SI.ServiceID equals ss.RecNo
                                  join l in context.tblLocations on TU.LocationID equals l.PKey
                                  join TP in context.TreatmentPrograms on SI.ProgramID equals TP.RecNo
                                  // where SI.DischargeDate == null && SI.SurveyID == 1
                                  where C.ClientStatusId == 1 && SI.SurveyID == 1
                                  select new
                                  {
                                      C.PAT_ID,
                                      SI.GroupName,
                                      SI.EntryDate,
                                      SI.DischargeDate,
                                      TU.FirstName,
                                      TU.LastName,
                                      SurveyItemId = SI.RecNo,
                                      TU.LocationID,
                                      SI.DischargeId,
                                      ss.Description,
                                      l.PKey,
                                      l.FriendlyName_loc,
                                      SI.ProgramID,
                                      TP.ProgramName,
                                      C.RecNo,
                                      SI.ClientGenderType

                                  }).ToList();
                    //admin level  or user level
                    if (newUserId.Level == "SIT2A" || newUserId.Level == "SIT3U")
                        result = result.Where(x => x.LocationID == newUserId.LocationId).ToList();

                    //switch (sortOrder)
                    //{
                    //    case "Asc":
                    //        result = result.OrderBy(s => s.PAT_ID).ToList();
                    //        break;
                    //    case "Desc":
                    //        result = result.OrderByDescending(s => s.PAT_ID).ToList();
                    //        break;
                    //    default:
                    //        result = result.OrderByDescending(x => x.RecNo).ToList();
                    //        break;

                    //}

                    result = result.OrderBy(s => s.EntryDate).ToList();

                    result = result.Where(s => s.PAT_ID != "").ToList();

                    int rowCount = result.Count();

                    //var Disch = result.OrderByDescending(x => x.RecNo).Skip(skip).Take(take).ToList();
                    var Disch = result.Skip(skip).Take(take).ToList();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var DL in Disch)
                    {

                        events.Add(
                             new Discharge
                             {
                                 PAT_ID = DL.PAT_ID,
                                 DischargeDate = DL.DischargeDate,
                                 EntryDate = DL.EntryDate,
                                 LoginID = DL.FirstName + " " + DL.LastName,
                                 RecNo = DL.RecNo,
                                 DischargeId = DL.ProgramID,
                                 Description = DL.ProgramName,
                                 LocationName = DL.FriendlyName_loc,
                                 SurveyItemId = DL.SurveyItemId,
                                 LocationId = DL.PKey,
                                 GenderType = DL.ClientGenderType
                             });
                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Discharge>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Discharge>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, events);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        async private Task<HttpResponseMessage> InitializeStaffForm()
        {


            var locations = new List<DischargeCode>();


            locations = getDischargeCodes();

            return this.Request.CreateResponse(HttpStatusCode.OK, locations);
        }




        private List<DischargeCode> getDischargeCodes()
        {
            try
            {
                var locations = new List<DischargeCode>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var dblocations = context.DischargeCodes.Where(x => x.IsActive == true).OrderBy(x => x.DischargeId);
                    foreach (var item in dblocations)
                    {

                        locations.Add(
                            new DischargeCode { DischargeId = item.DischargeId, DischargeCode1 = item.DischargeName }
                            );
                    }
                }

                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage getDischargeReport(string[] p)
        {
            try
            {
                User newUserId = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(p[0].ToString());
                if (!newUserId.SelectedLocationCategoryList.Any(x => x.CategoryID == 2))
                {
                    return null;
                }



                var pagerResult = new DataGadget.Web.Pager.Pager();
                int take = 10;
                int segment = 3;
                int? pageId = Convert.ToInt32(p[1]);
                int skip = (Convert.ToInt32(pageId) - 1) * take;

                var sortOrder = p[2];

                int locationId = Convert.ToInt32(p[3]);

                DateTime startDate = Convert.ToDateTime(p[4]);
                DateTime endDate = Convert.ToDateTime(p[5]);


                var events = new List<Discharge>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var result = (from TU in context.TblUsers
                                  join SI in context.SurveyItems on TU.UserGUID equals SI.SurveyAdminID
                                  join SC in context.SurveyClients on SI.SurveyClientID equals SC.RecNo
                                  join C in context.Clients on SC.ClientID equals C.RecNo
                                  join ss in context.SurveyServices on SI.ServiceID equals ss.RecNo
                                  join l in context.tblLocations on TU.LocationID equals l.PKey
                                  where SI.DischargeDate != null && SI.SurveyID == 1
                                  select new
                                  {
                                      C.PAT_ID,
                                      SI.GroupName,
                                      SI.EntryDate,
                                      SI.DischargeDate,
                                      TU.FirstName,
                                      TU.LastName,
                                      SI.RecNo,
                                      TU.LocationID,
                                      SI.DischargeId,
                                      ss.Description,
                                      l.FriendlyName_loc
                                  }).ToList();
                    //admin level  or user level
                    //if (newUserId.Level == "SIT2A" || newUserId.Level == "SIT3U")
                    //    result = result.Where(x => x.LocationID == newUserId.LocationId).ToList();

                    if (locationId > 0)
                    {

                        result = result.Where(x => x.LocationID == locationId).ToList();
                    }
                    else
                    {
                        if (newUserId.Level == "SIT2A" || newUserId.Level == "SIT3U")
                        {
                            result = result.Where(x => x.LocationID == newUserId.LocationId).ToList();
                        }
                    }

                    result = result.Where(x => x.DischargeDate >= startDate && x.DischargeDate <= endDate).ToList();

                    switch (sortOrder)
                    {
                        case "Asc":
                            result = result.OrderBy(s => s.PAT_ID).ToList();
                            break;
                        case "Desc":
                            result = result.OrderByDescending(s => s.PAT_ID).ToList();
                            break;
                        default:
                            result = result.OrderByDescending(x => x.RecNo).ToList();
                            break;

                    }

                    result = result.Where(s => s.PAT_ID != "").ToList();

                    int rowCount = result.Count();

                    //var Disch = result.OrderByDescending(x => x.RecNo).Skip(skip).Take(take).ToList();
                    var Disch = result.Skip(skip).Take(take).ToList();
                    pagerResult = DataGadget.Web.Pager.Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    foreach (var DL in Disch)
                    {

                        events.Add(
                             new Discharge { PAT_ID = DL.PAT_ID, DischargeDate = DL.DischargeDate, EntryDate = DL.EntryDate, LoginID = DL.FirstName + " " + DL.LastName, RecNo = DL.RecNo, DischargeId = DL.DischargeId, Description = DL.Description, LocationName = DL.FriendlyName_loc });
                    }
                }
                DataGadget.Web.Pager.PagerValue pagerValue = new Pager.PagerValue();
                pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                pagerValue.HasNextPage = pagerResult.HasNextPage;
                pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Discharge>> data = new Tuple<List<DataGadget.Web.Pager.PagerValue>, List<Discharge>>(new List<DataGadget.Web.Pager.PagerValue> { pagerValue }, events);
                return this.Request.CreateResponse(HttpStatusCode.OK, data);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private HttpResponseMessage UpdateWaitingData(string[] p)
        {


            Discharge Discharge = Newtonsoft.Json.JsonConvert.DeserializeObject<Discharge>(p[0]);
                      

            try
            {
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    var data = context.SurveyItems.Join(context.SurveyClients, si => si.SurveyClientID, sc => sc.RecNo, (si, sc) => new { si, sc })
            .Join(context.Clients, sic => sic.sc.ClientID, c => c.RecNo, (sci, c) => new { sci, c })
            .Where(m => m.sci.si.EntryDate <= Discharge.EntryDate && m.c.ClientStatusId == 1 && m.sci.si.RecNo != Discharge.SurveyItemId).Count();
                    if (data == 0)
                    {
                        //Update 
                        DataGadget.Web.ViewModels.BedsCount bedCout = new DataGadget.Web.ViewModels.BedsCount();
                        bedCout = context.GetAvailabilityCount(Discharge.DischargeId, Discharge.GenderType).Select(x => new DataGadget.Web.ViewModels.BedsCount
                        {
                            AllocatedBeds = x.Allocated_Beds,
                            TotalBeds = x.TotalBeds,
                            WaitingClients = x.WaitingClients

                        }).SingleOrDefault();

                        if (bedCout.AllocatedBeds < bedCout.TotalBeds)
                        {

                            var surveyItem = context.SurveyItems.Where(x => x.RecNo == Discharge.SurveyItemId).SingleOrDefault();
                            var client = context.Clients.Where(x => x.RecNo == Discharge.RecNo).SingleOrDefault();
                            if (surveyItem != null && client != null)
                            {
                                client.ClientStatusId = 3;
                                surveyItem.EntryDate = DateTime.Now;
                                context.Entry(surveyItem).State = System.Data.Entity.EntityState.Modified;  
                                context.Entry(client).State = System.Data.Entity.EntityState.Modified;     
                                context.SaveChanges();
                            }
                           

                            Discharge.returntype = "Yes";
                        }
                    }
                    else
                    {
                        Discharge.returntype = "No";
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



            return this.Request.CreateResponse(HttpStatusCode.OK, Discharge);

        }

      
    }
}
