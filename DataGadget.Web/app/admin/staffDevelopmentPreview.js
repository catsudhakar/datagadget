﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('staffDevelopmentPreview', staffDevelopmentPreview);

    staffDevelopmentPreview.$inject = ['$scope', '$http', 'staffInfo', '$modalInstance', '$sce', '$location'];

    function staffDevelopmentPreview($scope, $http, staffInfo, $modalInstance, $sce, $location) {

        $scope.title = 'staffDevelopmentPreview';

        var vm = this;

        activate();

        function activate() {
            if (staffInfo) {
                $http({
                    method: 'POST', url: 'api/Staff',
                    data: { method: "GetBaseStringDataForReport", parameters: [JSON.stringify(staffInfo)] }
                })
               .success(function (result) {
                   // $scope.source = result;
                   var reportData = "data:application/pdf;base64," + result;
                   $scope.source = $sce.trustAsResourceUrl(reportData);
               })
               .error(function (error) {
                   console.log(error);
               })
            }
        }
        $scope.cancel = function () {

            $modalInstance.close('close');

           // $location.path('/staffDevelopment');
           
        }
    }
})();
