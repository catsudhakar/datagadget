﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class reportModel
    {

        public List<Location> LocationList { get; set; }
       
        public string Organization { get; set; }
        public string OrganizationName { get; set; }
        public string RegionCode { get; set; }
        public string SiteCode { get; set; }
        public string ReportType { get; set; }
        public string ReportData { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate2 { get; set; }
        public string BedUtilizationReportId { get; set; }

    }
}