//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.ScheduledTaskRunner
{
    using System;
    
    public partial class suretool_Report_P13_Result
    {
        public Nullable<int> Individual_Univ_Direct { get; set; }
        public Nullable<int> Individual_Selected { get; set; }
        public Nullable<int> Individual_Indicated { get; set; }
        public Nullable<int> Population_Univ_Indirect { get; set; }
        public Nullable<int> Individual_Total { get; set; }
        public Nullable<int> Population_Total { get; set; }
    }
}
