﻿using DataGadget.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DataGadget.Web.Reports;
using System.Text;

namespace DataGadget.Web.Controllers
{
    public class ExportUserListController : ApiController
    {

        async public Task<HttpResponseMessage> Post(ParamObject value)
        {
            try
            {
                switch (value.Method)
                {
                   

                    case "InitializeUserForm":
                        return await InitializeUserForm(value.Parameters);

                    case "GetData":
                        return await GetData(value.Parameters);
                   

                    default:
                        break;
                }
                return this.Request.CreateResponse();
            }
            catch (Exception ex)
            {

                //WebLogger.Error("Exception In ExportUserListController : " + ex.Message);  
                //return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, new HttpError(ex, true));
                throw ex;

            }
        }


        async private Task<HttpResponseMessage> GetData(string[] p)
        {
           

             //string[] regionsite=
            string p1 = p[0];
            string p2 = p[1];
            if (p1 == "0") { p1 = ""; }
            if (p2 == "0") { p2 = ""; }


            try
            {
                var users = new List<ExportUserList>();
                //ExportUserList users = null;
                //var users = new List<User>();
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {

                    var res = context.getUserlistForreport(p1,p2);

                    if (res != null)
                    {

                      foreach(var user in res)
                      {

                          

                         // users = new ExportUserList{ FirstName = user.FirstName, LastName = user.LastName};
                          users.Add(
                            new ExportUserList
                            {
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                UserName=user.UserName,
                                Email=user.Email,
                                OfficeFax=user.OfficeFax,
                                OfficePhone = user.OfficePhone,
                                Organization=user.Organization

                               // EmailId = user.Email
                                //UserId = user.UserGUID,
                                //UserName = user.LoginID,
                                //AgencyName = user.LocationID.ToString(),
                                //Password = user.PW,
                                //Level = user.UserLevel,
                                //Degree = user.Degree,
                                //Experience = user.YrsPrevExp,
                                //OfficePhoneNum = user.OfficePhone,
                                //OfficeFaxNum = user.OfficeFax,
                                //SelectedTreamProgramList = UserTreatmentProgramList,
                                //SelectedLocationCategoryList = UserCategoryList

                            });
                      }

                    }

                   
                   
                }
                return this.Request.CreateResponse(HttpStatusCode.OK, users);
               
            }
            catch (Exception ex)
            {

                throw ex;
            }
          


        }



        async private Task<HttpResponseMessage> InitializeUserForm(string[] p)
        {

            if (p[0] == "SIT2A" || p[0] == "SIT3U")
            {
                return null;
            }
            var user = new OneTimeEvent
            {

                LocationList = await GetLocationListAsync()

            };

            return this.Request.CreateResponse(HttpStatusCode.OK, user);
        }



        async private Task<List<Location>> GetLocationListAsync()
        {
            //            select stateRegionCode, siteCode, (stateRegionCode + '_' + siteCode) as regionsitecodes, 
            //friendlyName_loc from tblLocations where stateAbbrev = 'MS' order by friendlyName_loc 
            try
            {
                var locations = new List<Location>();
                locations.Add(new Location { PKey = 0, StateRegionCode = "0_0", FriendlyName_loc = "All" });
                using (DataGadget.Web.DataModel.datagadget_testEntities context = new DataModel.datagadget_testEntities())
                {
                    //var dblocations = await context.tblLocations.ToListAsync();
                    var dblocations = await context.tblLocations.OrderBy(x => x.FriendlyName_loc).ToListAsync();
                    foreach (var item in dblocations)
                    {
                        locations.Add(
                            new Location
                            {
                                PKey = item.PKey,
                                StateRegionCode = item.StateRegionCode + '_' + item.SiteCode,
                                FriendlyName_loc = item.FriendlyName_loc
                            }
                            );
                    }
                }

                return locations;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
