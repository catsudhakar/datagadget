//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataGadget.Web.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientStatu
    {
        public ClientStatu()
        {
            this.Clients = new HashSet<Client>();
        }
    
        public int ClientStatusId { get; set; }
        public string Status { get; set; }
    
        public virtual ICollection<Client> Clients { get; set; }
    }
}
