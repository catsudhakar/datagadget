﻿(function () {
    'use strict';
    var controllerId = 'programsandStrategiesNREPPList';
    angular
        .module('app')
        .controller(controllerId, programsandStrategiesNREPPList);
    programsandStrategiesNREPPList.$inject = ['$scope', '$http', '$modal', '$location', 'common', 'psService', '$window'];
    function programsandStrategiesNREPPList($scope, $http, $modal, $location, common, pssharedService, $window) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var errorLog = getLogFn(controllerId, 'error');
        $scope.title = 'Programs and Strategies Maintenance - NREPP';
        var vm = this;
        $scope.vm.programs = pssharedService.programs;
        $scope.programData = {};
        activate();
        vm.pagerData;
        vm.pageList = [];
        $scope.ispaging = true;
        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $http({ method: 'POST', url: 'api/ProgramStategies', data: { method: "GetAllProgramStategies", parameters: ['NREPP', userInfo.level, pageId] } })
         .success(function (result) {
             $scope.ispaging = false;
             vm.pagerData = result.item1[0];
             if (parseInt(vm.pagerData.numberOfPages) > 1) {
                 $scope.ispaging = true;
             }
             vm.pageList = [];
             if (pageId > 3) {
                 vm.pageList.push(pageId - 2);
                 vm.pageList.push(pageId - 1);
                 vm.pageList.push(pageId);
                 if (pageId < vm.pagerData.numberOfPages) {
                     vm.pageList.push(pageId + 1);
                     if (pageId < (vm.pagerData.numberOfPages - 1)) {
                         vm.pageList.push(pageId + 2);
                     }
                 }
             }
             else {
                 if (pageId == 1) {
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                     if (pageId + 4 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 4);
                     }

                 }
                 if (pageId == 2) {
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);

                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                     if (pageId + 3 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 3);
                     }
                 }
                 if (pageId == 3) {
                     vm.pageList.push(pageId - 2);
                     vm.pageList.push(pageId - 1);
                     vm.pageList.push(pageId);
                     if (pageId + 1 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 1);
                     }
                     if (pageId + 2 <= vm.pagerData.numberOfPages) {
                         vm.pageList.push(pageId + 2);
                     }
                 }

             }

             var userData = result.item2;
             $scope.vm.programs = userData;
             pssharedService.programs = userData;
         })
         .error(function (error) {
             console.log(error);
         })
        }
        vm.userSearch = function (search) {
            if (search.length >= 3) {
                $scope.ispaging = false;
                var userInfo = JSON.parse($window.localStorage["userInfo"]);
                $http({ method: 'POST', url: 'api/ProgramStategies', data: { method: "Search", parameters: ['NREPP', userInfo.level, search] } })
                .success(function (result) {
                    $scope.vm.programs = result;
                    pssharedService.programs = result;
                })
                .error(function (error) {
                    console.log(error);
                })
            }
            else {
                if (search.length == 0) {
                    $scope.ispaging = false;
                    paging(1);
                }
            }
        }
        function activate() {

            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }
            else {
                paging(1);
            }
        }
        vm.addprogram = function () {
            var addUserModal = $modal.open({
                templateUrl: 'app/admin/newprogramsandStrategiesForNREPP.html',
                controller: 'newprogramsandStrategiesForNREPP',
                resolve: {
                    programInfo: function () {
                        return undefined;
                    }
                }

            });
            addUserModal.result.then(function (program) {
                vm.programs.push(program);
            }, function () {
            });
        }
        $scope.vm.editprogram = function (program) {

            var addUserModal = $modal.open({
                templateUrl: 'app/admin/newprogramsandStrategiesForNREPP.html',
                controller: 'newprogramsandStrategiesForNREPP',
                resolve: {
                    programInfo: function () {
                        return program;
                    }
                }
            });
            addUserModal.result.then(function () {
            }, function () {
            });
        }
        $scope.vm.deleteprogram = function (program) {

            var modalInstance = $modal.open({
                templateUrl: '/app/common/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',
            });
            modalInstance.result.then(function () {
                $http({ method: 'POST', url: 'api/ProgramStategies', data: { method: "DeleteProgramStategies", parameters: [program.id] } })
            .success(function (result) {
                //var i = vm.programs.indexOf(program);
                //if (i != -1) {
                //    vm.programs.splice(i, 1);
                //}
                log("Program Stategies deleted successfully");
            })
            .error(function (error) {
                console.log(error);
            })
            }, function () {
            });
        };
    }
})();
