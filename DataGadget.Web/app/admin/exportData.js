﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('exportData', exportData);

    exportData.$inject = ['$scope', '$http', '$window','$location'];

    function exportData($scope, $http, $window, $location) {

        $scope.title = 'exportData';

        $scope.isdisplay = false;
        $scope.ismessage = '';

        var vm = this;
        vm.onetime = {};
    

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $scope.openBeginDateopened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.openBeginDate = function ($event) {
            $scope.opened = false;
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openBeginDateopened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
        $scope.format = $scope.formats[4];


        function formattedDate() {
            var d = new Date(Date.now()),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }

        activate();

        function activate() {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            if (userInfo.level == "SIT3U" || userInfo.level == "SIT2A") {
                $location.path('/dashboard');
            }

            vm.onetime.locationList = "";
            vm.onetime.organization = '0_0';


            $http({ method: 'POST', url: 'api/ExportData', data: { method: "InitializeUserForm", parameters: ["ExportData",userInfo.level] } })
          .success(function (result) {
              vm.onetime.locationList = result.locationList;
             
          });
        }

        $scope.exportData = function (frmexportData) {
            vm.onetimeResult = {};
            var issubmit = true;
            var sdt = vm.onetime.beginDate;
            var edt = vm.onetime.endDate;
            if (sdt !== undefined && edt !== undefined) {
                if (sdt > edt) {
                    // $scope.ismessage = 'Please enter end date should be greater then start date'
                    issubmit = false;
                }
            }
            if (frmexportData.$valid && issubmit) {

                $scope.isdisplay = false;
                $scope.ismessage = '';


                var res = vm.onetime.organization.split("_");

                if (res.length == 2) {
                    vm.onetime.regionCode = res[0];
                    vm.onetime.siteCode = res[1];
                }

                vm.onetimeResult.beginDate = GetFormattedDate(vm.onetime.beginDate);
                vm.onetimeResult.endDate = GetFormattedDate(vm.onetime.endDate);
                vm.onetimeResult.siteCode = vm.onetime.siteCode;
                vm.onetimeResult.regionCode = vm.onetime.regionCode;

                $http({ method: 'POST', url: 'api/ExportData', data: { method: "GetData", parameters: [JSON.stringify(vm.onetimeResult)] } })
                  .success(function (result) {

                      alasql('SELECT * INTO XLSX("exportData.xlsx",{headers:true}) FROM ?', [result]);

                  })
                   .error(function (error) {
                       console.log(error);
                   });
            }
            else
            {
               
                if (sdt === undefined && edt === undefined) {
                    $scope.ismessage = 'Please enter start date and end date'
                }
                else if (sdt === undefined) {
                    $scope.ismessage = 'Please enter start date '
                }
                else if (edt === undefined) {
                    $scope.ismessage = 'Please enter end date'
                }
                else if (sdt > edt) {
                    $scope.ismessage = 'Please enter end date should be greater then start date'
                }
                else {
                    $scope.ismessage = 'Please enter start date and end date'
                }
                $scope.isdisplay = true;
            }
        }
    }
})();
