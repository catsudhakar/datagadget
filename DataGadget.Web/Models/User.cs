﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class User
    {
        public Guid UserId { get; set; }

        public int Pkey { get; set; }

        public string Password { get; set; }

        public string ConfirmedPassword { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailId { get; set; }

        public string OfficePhoneNum { get; set; }

        public string OfficeFaxNum { get; set; }

        public int Experience { get; set; }

        public string AgencyName { get; set; }

        public string Degree { get; set; }

        public string Level { get; set; }

        public int? LocationId { get; set; }

        public int TreatmentId { get; set; }

        public List<TreatmentProg> TreamProgramList { get; set; }

        public List<TreatmentProg> SelectedTreamProgramList { get; set; }

        public List<Location> LocationList { get; set; }

        public List<Location> SelectedLocationList { get; set; }

        public List<LocCategory> LocationCategoryList { get; set; }

        public List<LocCategory> SelectedLocationCategoryList { get; set; }

        public string RegionCode { get; set; }

        public string SiteCode { get; set; }

        public string Stateabbrev { get; set; }

        public Nullable<int> Hours { get; set; }

        public bool IsActive { get; set; }

        public List<surveySerivesModel> surveySerivesList { get; set; }

        public List<UserIopModel> SelectedSurveySerivesList { get; set; }

        public string LoginMessage { get; set; }



        public List<DataGadget.Web.DataModel.UserCategory> SelectedUserCategoryModel { get; set; }
    }

    public class LoginMessageModel
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public bool IsActive { get; set; }
    }


    public class MonthlyMessageModel
    {
        public int ID { get; set; }
        public string TreatmentMessage { get; set; }
        public string PreventionMessage { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsTreatment { get; set; }
        public bool IsPrevention { get; set; }
        public bool IsActive { get; set; }
    }



}