﻿(function () {
    'use strict';

    var controllerId = 'shell';
    angular.module('app').controller(controllerId,
        ['$rootScope', 'common', 'config', '$scope', '$location', shell]);

    function shell($rootScope, common, config, $scope, $location) {
        var vm = this;
        vm.isLoginPage = false;
        var logSuccess = common.logger.getLogFn(controllerId, 'success');
        var events = config.events;
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.topMenu = "/app/users/login.html";
        vm.spinnerOptions = {
            radius: 40,
            lines: 7,
            length: 0,
            width: 30,
            speed: 1.7,
            corners: 1.0,
            trail: 100,
            color: '#F58A00'
        };

        activate();

        function activate() {
            //logSuccess('Hot Towel Angular loaded!', null, true);
            common.activateController([], controllerId);
        }

        function toggleSpinner(on) { vm.isBusy = on; }

        $rootScope.$on('$routeChangeStart',
            function (event, next, current) {
                toggleSpinner(false);
            }
        );

        $rootScope.$on(events.controllerActivateSuccess,
            function (data) { toggleSpinner(false); }
        );

        $rootScope.$on(events.spinnerToggle,
            function (data) { toggleSpinner(data.show); }
        );

        var IDLE_TIMEOUT = 600000; //seconds
        var _idleSecondsCounter = 0;
        document.onclick = function () {
            _idleSecondsCounter = 0;
        };
        document.onmousemove = function () {
            _idleSecondsCounter = 0;
        };
        document.onkeypress = function () {
            _idleSecondsCounter = 0;
        };
        window.setInterval(CheckIdleTime, 60);

        function CheckIdleTime() {
            _idleSecondsCounter++;
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                _idleSecondsCounter = 0;
                localStorage.clear();
                document.location('/');
            }
        }

        $scope.$on('$routeChangeSuccess',
           function (event, next, current) {
               var currentPath = $location.path();
               if (localStorage.getItem("isUserAuthentic") == "true") {
                   var path = $location.path().split('/');
                   //$rootScope.pageTitle = $route.current.title;

                   if (path[1]) {
                       vm.topMenu = "/app/topmenu/loginMenu.html";
                       vm.isLoginPage = true;
                   }
                   else {

                       vm.topMenu = "/app/users/login.html";
                       vm.isLoginPage = false;
                   }
               }
               else {
                   var path = $location.path().split('/');
                   if (path[1] == "forgotpassword") {

                       localStorage.setItem("currentPath", currentPath);
                       toggleSpinner(false);
                       vm.topMenu = "/app/users/forgotpassword.html";
                       vm.isLoginPage = false;
                   }
                   else if (path[1] == "resetpassword") {

                       localStorage.setItem("currentPath", currentPath);
                       toggleSpinner(false);
                       vm.topMenu = "/app/users/resetpassword.html";
                       vm.isLoginPage = false;
                   }
                   else {
                       localStorage.setItem("currentPath", currentPath);
                       toggleSpinner(false);
                       vm.topMenu = "/app/users/login.html";
                       vm.isLoginPage = false;
                       $location.path('/');
                   }

               }
           }
       );
    };
})();