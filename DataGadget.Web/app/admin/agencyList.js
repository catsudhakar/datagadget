﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('agencyList', agencyList);

    agencyList.$inject = ['$scope']; 

    function agencyList($scope) {
        $scope.title = 'agencyList';

        activate();

        function activate() { }
    }
})();
