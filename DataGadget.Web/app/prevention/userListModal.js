﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('userListModal', userListModal);

    //userListModal.$inject = ['$scope', '$modalInstance', '$http', '$location', 'common', '$route', eventInfo];

    function userListModal($scope, $modalInstance, $http, $location, common, $route, eventInfo) {
        $scope.title = 'userListModal';

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(userListModal);

        $scope.users = [];
        $scope.pagerData;
        $scope.pageList = [];
        $scope.ispaging = true;
        $scope.onetime = {};
        $scope.color = {
            name: ''
        };
        activate();


        function activate() {
            paging(1);
        }

        $scope.pagination = function (pageId) {
            paging(pageId);
        }

        function paging(pageId) {

            var loginUserInfo = JSON.parse(localStorage.getItem('userInfo'));
            loginUserInfo.pageId = pageId;
            $http({ method: 'POST', url: 'api/User', data: { method: "GetUsersByPage", parameters: [loginUserInfo.level, loginUserInfo.locationId, loginUserInfo.stateabbrev, loginUserInfo.pageId] } })
            .success(function (result) {
                $scope.ispaging = false;
                $scope.pagerData = result.item1[0];
                if (parseInt($scope.pagerData.numberOfPages) > 1) {
                    $scope.ispaging = true;
                }
                $scope.pageList = [];
                if (pageId > 3) {
                    $scope.pageList.push(pageId - 2);
                    $scope.pageList.push(pageId - 1);
                    $scope.pageList.push(pageId);
                    if (pageId < $scope.pagerData.numberOfPages) {
                        $scope.pageList.push(pageId + 1);
                        if (pageId < ($scope.pagerData.numberOfPages - 1)) {
                            $scope.pageList.push(pageId + 2);
                        }
                    }
                }
                else {
                    if (pageId == 1) {
                        $scope.pageList.push(pageId);
                        if (pageId + 1 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 3);
                        }
                        if (pageId + 4 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 4);
                        }

                    }
                    if (pageId == 2) {
                        $scope.pageList.push(pageId - 1);
                        $scope.pageList.push(pageId);

                        if (pageId + 1 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 2);
                        }
                        if (pageId + 3 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 3);
                        }
                    }
                    if (pageId == 3) {
                        $scope.pageList.push(pageId - 2);
                        $scope.pageList.push(pageId - 1);
                        $scope.pageList.push(pageId);
                        if (pageId + 1 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 1);
                        }
                        if (pageId + 2 <= $scope.pagerData.numberOfPages) {
                            $scope.pageList.push(pageId + 2);
                        }
                    }

                }

                var userData = result.item2;
                $scope.users = userData;
            })
            .error(function (error) {
                console.log(error);
            })
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');

        }

        $scope.ok1 = function () {
            debugger;
            //alert($scope.color.name);

            if (eventInfo) {
                $scope.onetime = eventInfo;
                //var element = document.getElementById("radDelegate");
                //if (element.checked) {
                //    var userId = element.value;
                //}
                var userId = $scope.color.name
                eventInfo.userGUID = userId;
                $http({
                    method: 'POST', url: 'api/oneTimeEvent',
                    data: { method: "UpdateUserDetails", parameters: [JSON.stringify(eventInfo)] }
                })

               .success(function (result) {
                   //$modalInstance.close(result);
                   //log("Updated successfully");
                   //$location.path('/dashboard');
                   //$route.reload();


               })
               .error(function (error) {
                   console.log(error);
               })

            }

        }

        $scope.ok = function () {

            debugger;
           // alert("ok");

            if (eventInfo) {
                $scope.onetime = eventInfo;
                var userId = $scope.color.name
                eventInfo.userGUID = userId;
                $http({ method: 'POST', url: 'api/oneTimeEvent', data: { method: "UpdateUserDetails", parameters: [JSON.stringify(eventInfo)] } })
              .success(function (result) {
                  debugger;
                  $modalInstance.close();
                  log("Event Updated successfully");
                  if (eventInfo.curriculum == 1)
                      $location.path('/ongoingEvents');
                  else
                      $location.path('/oneTimeEvents');

                  $route.reload();
              })
              .error(function (error) {
                  console.log(error);
              })


            }



        };
    }
})();
