﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataGadget.Web.Models
{
    public class Provider
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}