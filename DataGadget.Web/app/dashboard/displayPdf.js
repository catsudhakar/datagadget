﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('displayPdf', displayPdf);

    displayPdf.$inject = ['$scope','$modalInstance']; 

    function displayPdf($scope, $modalInstance) {
        $scope.title = 'displayPdf';

        activate();

        function activate() { }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');

        }
    }
})();
